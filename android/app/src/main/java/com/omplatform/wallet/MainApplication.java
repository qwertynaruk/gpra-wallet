package com.omplatform.wallet;

import android.app.Application;
import com.skyward.NotificationManager.NotificationManager;
import com.facebook.react.ReactApplication;
import com.reactnativecommunity.netinfo.NetInfoPackage;
import com.cmcewen.blurview.BlurViewPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import fr.bamlab.rnimageresizer.ImageResizerPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import io.invertase.firebase.fabric.crashlytics.RNFirebaseCrashlyticsPackage;
import io.invertase.firebase.config.RNFirebaseRemoteConfigPackage;
import com.horcrux.svg.SvgPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import fr.greweb.reactnativeviewshot.RNViewShotPackage;
import cl.json.RNSharePackage;
import cl.json.ShareApplication;

import com.rnfingerprint.FingerprintAuthPackage;

import org.devio.rn.splashscreen.SplashScreenReactPackage;

import com.bitgo.randombytes.RandomBytesPackage;
import com.oblador.keychain.KeychainPackage;
import com.imagepicker.ImagePickerPackage;
import com.mkuczera.RNReactNativeHapticFeedbackPackage;
import com.rnfs.RNFSPackage;
import com.rngoldenkeystore.RNGoldenKeystorePackage;
import com.smixx.fabric.FabricPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;

import org.reactnative.camera.RNCameraPackage;

import com.remobile.qrcodeLocalImage.RCTQRCodeLocalImagePackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication, ShareApplication {

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                new MainReactPackage(),
                new NetInfoPackage(),
                new BlurViewPackage(),
                new RNCWebViewPackage(),
                new ImageResizerPackage(),
                new RNFirebasePackage(),
                new RNFirebaseMessagingPackage(),
                new RNFirebaseNotificationsPackage(),
                new RNFirebaseCrashlyticsPackage(),
                new RNFirebaseRemoteConfigPackage(),
                new SvgPackage(),
                new LinearGradientPackage(),
                new RNViewShotPackage(),
                new RNSharePackage(),
                new FingerprintAuthPackage(),
                new SplashScreenReactPackage(),
                new RandomBytesPackage(),
                new KeychainPackage(),
                new ImagePickerPackage(),
                new RNReactNativeHapticFeedbackPackage(),
                new RNFSPackage(),
                new FabricPackage(),
                new RNDeviceInfo(),
                new RNCameraPackage(),
                new RCTQRCodeLocalImagePackage(),
                new RNGoldenKeystorePackage(),
                new NotificationManager()
            );
        }

        @Override
        protected String getJSMainModuleName() {
            return "index";
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }

    @Override
    public String getFileProviderAuthority() {
        return "com.omplatform.wallet.provider";
    }
}
