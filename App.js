/**
 * OM Wallet
 * @flow
 */

import React, { Component } from 'react'
import {
  AppState,
  Keyboard,
  StatusBar,
  View
} from 'react-native'

// Third-party
import NetInfo from '@react-native-community/netinfo'
import firebase from 'react-native-firebase'
import Router from './app/Router'

// Components & Modules
import BlindScreen from './app/components/screens/BlindScreen'
import PushNotificationHelper from './app/commons/PushNotificationHelper'
import Spinner from './app/components/elements/Spinner'
import MainStore from './app/AppStores/MainStore'
import NavStore from './app/AppStores/NavStore'
import NotificationStore from './app/AppStores/stores/Notification'
import NoInternetScreen from './app/components/screens/NoInternetScreen'

console.disableYellowBox = true
console.ignoredYellowBox = [
  'Warning: isMounted',
  'Module RCTImageLoader'
]
export default class App extends Component {
  constructor(props) {
    super(props)

    // Set state
    this.state = {
      loaded: false
    }

    this.appState = 'active'

    // Set status bar
    StatusBar.setTranslucent(true)

    NetInfo.addEventListener((state) => {
      if (state.isConnected) {
        MainStore.appState.setInternetConnection('online')
      } else {
        MainStore.appState.setInternetConnection('offline')
      }
    })
  }

  async componentWillMount() {
    await MainStore.startApp()

    // Set state
    this.setState({
      loaded: true
    })
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange)
    firebase.crashlytics().enableCrashlyticsCollection()
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange)
    Keyboard.dismiss()
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.appState === 'active' && nextAppState === 'inactive') {
      if (NavStore.currentRouteName !== 'UnlockScreen') {
        this.blind.showBlind()
      }
    }

    if (nextAppState === 'inactive' || nextAppState === 'background') {
      Keyboard.dismiss()
    }

    if (nextAppState === 'background') {
      NotificationStore.appState = nextAppState
      this._lock_screen_start_time = Date.now()
    }

    if (nextAppState === 'active' && this.state.loaded) {
      setTimeout(() => {
        NotificationStore.appState = nextAppState
      }, 500)
      MainStore.appState.BgJobs.Aom.init()
      MainStore.appState.BgJobs.CheckBalance.doOnce(false, false)
      MainStore.appState.BgJobs.CheckBalance.start()
      this.blind.hideBlind()
    }

    if (this.appState === 'background' && nextAppState === 'active') {
      const RESTORE_LOCK_SCREEN_INTERVAL = 10000 // 10 sec
      if (this._lock_screen_start_time && Date.now() - this._lock_screen_start_time < RESTORE_LOCK_SCREEN_INTERVAL) {
        return
      }
      if (!MainStore.appState.hasPassword) {
        return
      }

      PushNotificationHelper.resetBadgeNumber()
      NavStore.lockScreen({
        onUnlock: () => {
          if (NotificationStore.isOpenFromTray) {
            NotificationStore.isOpenFromTray = false
            NotificationStore.receivedNotification()
            // NotificationStore.gotoTransaction()
          }
        },

        isLaunchApp: true
      })
    }

    this.appState = nextAppState
  }

  render() {
    if (this.state.loaded) {
      return (
        <View style={{ flex: 1 }}>
          <StatusBar
            backgroundColor="transparent"
            barStyle="dark-content"
            translucent
          />
          <Router />
          <BlindScreen ref={(ref) => { this.blind = ref }} />
          <Spinner
            visible={false}
            ref={(ref) => { NavStore.loading = ref }}
          />
          <NoInternetScreen />
        </View>
      )
    }

    return (
      <View />
    )
  }
}
