# OM Wallet

Hello

## Installation
Install `rn-nodeify` to be able to use Node.js libs.

> `npm i rn-nodeify -g`

Install the dependencies in the local node_modules folder.

> `npm install`


#### IOS (CocoaPods) `CocoaPods 1.3+ is required`
#### Installation CocoaPods
	
> `cd ios`  
> `pod install`  
> `cd .. && react-native run-ios`  

#### Android
  
> `react-native run-android`
  