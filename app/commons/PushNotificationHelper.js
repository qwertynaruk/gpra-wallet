import firebase from 'react-native-firebase'
import { Platform } from 'react-native'
import Permissions from 'react-native-permissions'
import NotificationStore from '../AppStores/stores/Notification'
import MainStore from '../AppStores/MainStore'

class PushNotificationHelper {
  async init() {
    const token = await firebase.messaging().getToken()
    NotificationStore.setDeviceToken(token)
    console.log(`token ${token}`)

    // Subscribe omwallet topic
    firebase.messaging().subscribeToTopic('omwallet')
    firebase.notifications().getInitialNotification().then((notif) => {
      const { nofication } = notif
      const data = nofication._data
      if (nofication && data.address) {
        NotificationStore.isInitFromNotification = true
        NotificationStore.setCurrentNotif(nofication)
        NotificationStore.executeNotification()
      }
    })

    // On notification for Android
    firebase.notifications().onNotification((notif) => {
      console.log('onNotification')
      console.log(notif)
      this.receiveNotification(notif)
    })

    firebase.messaging().onTokenRefresh((refreshToken) => {
      if (!MainStore.appState.enableNotification) {
        return
      }
      if (NotificationStore.deviceToken === refreshToken) {
        return
      }
      NotificationStore.setDeviceToken(refreshToken)
    })

    this.resetBadgeNumber()
    firebase.notifications().removeAllDeliveredNotifications()

    if (Platform.OS === 'ios') {
      Permissions.check('notification').then((response) => {
        if (response === 'undetermined') {
          this.requestPermission()
        }
      }).catch((e) => {
        console.log(e)
      })
    }
  }

  receiveNotification(notif) {
    const data = notif._data
    if (!MainStore.appState.enableNotification) {
      return
    }
    if (!data.address) {
      return
    }
    NotificationStore.setCurrentNotif(notif)
    NotificationStore.executeNotification()
    if (notif && notif.opened_from_tray) {
      this.resetBadgeNumber()
      NotificationStore.isOpenFromTray = true
    }
    MainStore.appState.BgJobs.CheckBalance.doOnce(false, false)
    MainStore.appState.BgJobs.CheckBalance.start()
  }

  getToken() {
    return firebase.messaging().getToken()
  }

  requestPermission() {
    return firebase.messaging().requestPermission()
  }

  removeAllDeliveredNotifications = () => {
    firebase.notifications().removeAllDeliveredNotifications()
  }

  resetBadgeNumber() {
    firebase.notifications().setBadge(0)
  }

  checkPermission() {
    return Permissions.check('notification')
  }
}

export default new PushNotificationHelper()
