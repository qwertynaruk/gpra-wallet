import { observable, action, computed } from 'mobx'
import { BigNumber } from 'bignumber.js'
import MainStore from '../../../AppStores/MainStore'
import Helper from '../../../commons/Helper'

export default class ConfirmStore {
  @observable value = new BigNumber('0')
  @observable gasLimit = new BigNumber('0')
  @observable gasPrice = new BigNumber('0')
  @observable adjust = 'Standard'
  @observable.ref inputValue = null
  @observable _title = 'OM'
  @observable fee = 0
  @observable memo = ''
  enableMemo = true

  @action setAdjust(value) {
    this.adjust = value
  }

  @action setValue(value) {
    this.inputValue = value
    this.value = value
  }

  @action updateValue(value) {
    this.value = value
  }

  @action setGasLimit(gasLimit) {
    this.gasLimit = new BigNumber(`${gasLimit}`)
  }

  @action setGasPrice(gasPrice) {
    const gasP = new BigNumber(gasPrice).times(new BigNumber(1e+9))
    this.gasPrice = gasP
  }

  @action setFee(fee) {
    this.fee = fee
  }

  @action setTitle(title) {
    this._title = title
  }

  @action setMemo(memo) {
    this.memo = memo
  }

  @action estimateGas() {
  }

  @computed get rate() {
    return MainStore.appState.rateAOM
  }

  @computed get rateToken() {
    return MainStore.appState.selectedToken.rate
  }

  @computed get title() {
    return this._title
  }

  @computed get formatedFee() {
    const fee = new BigNumber(this.fee).dividedBy(1e+7).toNumber()
    return `${fee} OM FEE`
  }

  @computed get formatedAmount() {
    return `${Helper.formatETH(this.value, true, 2)} ${this.title}`
  }

  @computed get formatedDolar() {
    // TODO getRate
    const rate = MainStore.sendTransaction.isToken ? this.rateToken : this.rate
    const amountDolar = this.value.times(rate)
    return `$${Helper.formatUSD(amountDolar, false, 1000000, 2)}`
  }

  _onShowAdvance() {
    const formatedGasPrice = this.gasPrice.div(1e+9).toFixed(0)

    MainStore.sendTransaction.advanceStore.setGasLimit(this.gasLimit.toString(10))
    MainStore.sendTransaction.advanceStore.setGasPrice(formatedGasPrice)
  }

  @action validateAmount() {
  }
}
