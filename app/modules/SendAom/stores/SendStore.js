import { Keyboard } from 'react-native'
import { observable, action, computed } from 'mobx'
import BigNumber from 'bignumber.js'
import bitcoin from 'react-native-bitcoinjs-lib'
import bigi from 'bigi'
import AmountStore from './AmountStore'
import AddressInputStore from './AddressInputStore'
import ConfirmStore from './ConfirmStore'
import ConfirmStoreBTC from './ConfirmStore.btc'
import ConfirmStoreLTC from './ConfirmStore.ltc'
import ConfirmStoreDOGE from './ConfirmStore.doge'
import ConfirmStoreAOM from './ConfirmStore.aom'
import ConfirmStoreXLM from './ConfirmStore.xlm'
import ConfirmStoreXRP from './ConfirmStore.xrp'
import AdvanceStore from './AdvanceStore'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import SecureDS from '../../../AppStores/DataSource/SecureDS'
import { sendTransaction } from '../../../api/ether-json-rpc'
import Interface from '../../../Utils/Ethererum/Contract/interface'
import api from '../../../api'
import AppStyle from '../../../commons/AppStyle'
import Stellar from '../../../Utils/Stellar'
import Ripple from '../../../Utils/Ripple'
import URL from '../../../api/url'

const BN = require('bn.js')

class CustomWarning {
  constructor(obj) {
    this.status = obj.status
    this.title = obj.title
    this.message = obj.message
    this.name = 'CustomWarning'
  }
}

class SendStore {
  @observable isToken = false
  @observable isTransfering = false
  @observable isError = false
  @observable error = null
  @observable isWarning = false
  @observable warning = null
  amountStore = null
  addressInputStore = null
  confirmStore = null
  txIDData = []
  completeStep = 0
  txIDLTCData = []
  txIDDOGEData = []
  txData = null
  btcFeePerByte = 0
  ethGasPrice = 0

  @observable transaction = {
    gasLimit: new BN('21000'),
    gasPrice: new BN('1000000000')
  }

  constructor() {
    const { type } = MainStore.appState.selectedWallet
    this.amountStore = new AmountStore()
    this.addressInputStore = new AddressInputStore()
    if (type === 'ethereum') this.confirmStore = new ConfirmStore()
    if (type === 'bitcoin') this.confirmStore = new ConfirmStoreBTC()
    if (type === 'litecoin') this.confirmStore = new ConfirmStoreLTC()
    if (type === 'dogecoin') this.confirmStore = new ConfirmStoreDOGE()
    if (type === 'aomtoken') this.confirmStore = new ConfirmStoreAOM()
    if (type === 'stellar') this.confirmStore = new ConfirmStoreXLM()
    if (type === 'ripple') this.confirmStore = new ConfirmStoreXRP()
    this.advanceStore = new AdvanceStore()
    this.answers = MainStore.appState.fabric.answers
  }

  @action setCompleteStep = (cs) => { this.completeStep = cs }

  @computed get address() {
    return this.addressInputStore.address
  }

  getPrivateKey(ds) {
    MainStore.appState.selectedWallet.setSecureDS(ds)
    return MainStore.appState.selectedWallet.derivePrivateKey()
  }

  @computed get rpcURL() {
    return MainStore.appState.config.getRPCURL()
  }

  @computed get chainId() {
    return MainStore.appState.config.chainID
  }

  @computed get fromAddress() {
    return MainStore.appState.selectedWallet.address
  }

  @action changeIsToken(bool) {
    this.isToken = bool
  }

  @action changeIsTransfering(bool) {
    this.isTransfering = bool
  }

  @action setTxIDData(data) {
    this.txIDData = data
  }

  @action setTxIDLTCData(data) {
    this.txIDLTCData = data
  }

  @action setTxIDDOGEData(data) {
    this.txIDDOGEData = data
  }

  @action setError(error) {
    this.error = error
    this.isError = true
  }

  @action setWarning(warning) {
    this.warning = warning
    this.isWarning = true
  }

  @action clearError() {
    this.error = null
    this.isError = false
  }

  @action clearWarning() {
    this.warning = null
    this.isWarning = false
  }

  @action async goToConfirm() {
    const { selectedWallet } = MainStore.appState
    Keyboard.dismiss()
    if (selectedWallet.type === 'ethereum') return this.estimateGasPriceETH()
    if (selectedWallet.type === 'bitcoin') return this.getTxIDBTC()
    if (selectedWallet.type === 'litecoin') return this.getTxIDLTC()
    if (selectedWallet.type === 'dogecoin') return this.getTxIDDOGE()
    if (
      selectedWallet.type === 'aomtoken' ||
      selectedWallet.type === 'stellar'
    ) return NavStore.pushToScreen('ConfirmScreen')
    if (selectedWallet.type === 'ripple') return NavStore.pushToScreen('ConfirmScreen')
    return this.getTxIDBTC()
  }

  getTxIDLTC() {
    if (this.txIDLTCData.length > 0) {
      NavStore.pushToScreen('ConfirmScreen')
      return
    }
    NavStore.showLoading()
    api.getTxIDLTC(MainStore.appState.selectedWallet.address).then((res) => {
      if (res.data && res.data.data && res.data.data.txs.length > 0) {
        MainStore.sendTransaction.setTxIDLTCData(res.data.data.txs)
        MainStore.sendTransaction.confirmStore.setFee(500000)
        NavStore.hideLoading()
        NavStore.pushToScreen('ConfirmScreen')
      } else {
        NavStore.hideLoading()
      }
    })
  }

  getTxIDDOGE() {
    if (this.txIDDOGEData.length > 0) {
      NavStore.pushToScreen('ConfirmScreen')
      return
    }
    NavStore.showLoading()
    api.getTxIDDOGE(MainStore.appState.selectedWallet.address).then((res) => {
      if (res.data && res.data.data && res.data.data.txs.length > 0) {
        MainStore.sendTransaction.setTxIDDOGEData(res.data.data.txs)
        MainStore.sendTransaction.confirmStore.setFee(100000000)
        NavStore.hideLoading()
        NavStore.pushToScreen('ConfirmScreen')
      } else {
        NavStore.hideLoading()
      }
    })
  }

  getTxIDBTC() {
    NavStore.showLoading()
    api.getTxID(MainStore.appState.selectedWallet.address).then(async (res) => {
      if (res && res.data && res.data.unspent_outputs && res.data.unspent_outputs.length > 0) {
        MainStore.sendTransaction.setTxIDData(res.data.unspent_outputs)
        await this.fetchEstimateFee()
        MainStore.sendTransaction.confirmStore.setFee(this.estimateFeeBTC(res.data.unspent_outputs.length, 2))
        NavStore.hideLoading()
        NavStore.pushToScreen('ConfirmScreen')
      } else {
        NavStore.hideLoading()
      }
    })
  }

  sendTx() {
    if (MainStore.appState.internetConnection === 'offline') {
      NavStore.popupCustom.show('No internet connection')
      return
    }
    NavStore.lockScreen({
      onUnlock: (pincode) => {
        // NavStore.showLoading()
        this.isTransfering = true
        const ds = new SecureDS(pincode)
        if (MainStore.appState.selectedWallet.type === 'litecoin') {
          return this.sendLTC(ds)
            .then(res => this._onSendSuccess(res))
            .catch(err => this._onSendFail(err))
        }
        if (MainStore.appState.selectedWallet.type === 'dogecoin') {
          return this.sendDOGE(ds)
            .then(res => this._onSendSuccess(res))
            .catch(err => this._onSendFail(err))
        }
        if (MainStore.appState.selectedWallet.type === 'bitcoin') {
          return this.sendBTC(ds)
            .then(res => this._onSendSuccess(res))
            .catch(err => this._onSendFail(err))
        }
        if (MainStore.appState.selectedWallet.type === 'aomtoken') {
          return this.sendAOM(ds)
            .then(res => this._onSendSuccess(res))
            .catch(err => this._onSendFail(err))
        }
        if (MainStore.appState.selectedWallet.type === 'stellar') {
          return this.sendXLM(ds)
            .then(res => this._onSendSuccess(res))
            .catch(err => this._onSendFail(err))
        }
        if (MainStore.appState.selectedWallet.type === 'ripple') {
          return this.sendXRP(ds)
            .then(res => this._onSendSuccess(res))
            .catch(err => this._onSendFail(err))
        }
        const transaction = {
          value: this.confirmStore.value,
          to: this.address,
          gasLimit: `0x${this.confirmStore.gasLimit.toString(16)}`,
          gasPrice: `0x${this.confirmStore.gasPrice.toString(16)}`
        }
        if (!this.isToken) {
          return this.sendETH(transaction, ds)
            .then(res => this._onSendSuccess(res))
            .catch(err => this._onSendFail(err))
        }
        return this.sendToken(transaction, ds)
          .then(res => this._onSendSuccess(res))
          .catch(err => this._onSendFail(err))
      }
    }, true)
  }

  _onSendSuccess = (res) => {
    const { value, memo } = this.confirmStore
    const amount = parseFloat(value, 10)
    const toAddress = this.addressInputStore.address

    if (MainStore.appState.selectedWallet.type === 'aomtoken') {
      const { address, BASE_FEE_DP } = MainStore.appState.selectedWallet
      const { data } = res.data
      const { first_name, last_name } = MainStore.appState.userInfo
      const { wallet, userInfomation } = this.addressInputStore
      const { name, avatar } = userInfomation
      const params = {
        amount,
        hash: data.hash,
        to: toAddress,
        name: first_name ? `${first_name} ${last_name}` : false,
        from: address,
        receiver: name,
        avatar,
        asset: wallet.code,
        fee: BASE_FEE_DP,
        memo
      }
      NavStore.pushToScreen('TransactionPassScreen', params)
      this.answers.logCustom('Send OM Success')
      this.isTransfering = false
    }
    if (MainStore.appState.selectedWallet.type === 'stellar') {
      NavStore.pushToScreen('DashboardScreen')
      NavStore.showToastTop(
        `ส่ง ${amount} XLM สำเร็จ`,
        {},
        { color: AppStyle.mainColor, fontFamily: AppStyle.mainFontBoldTH }
      )
      this.answers.logCustom('Send XLM Success')
      this.isTransfering = false
    }
    if (MainStore.appState.selectedWallet.type === 'ripple') {
      NavStore.pushToScreen('DashboardScreen')
      NavStore.showToastTop(
        `ส่ง ${amount} XRP สำเร็จ`,
        {},
        { color: AppStyle.mainColor, fontFamily: AppStyle.mainFontBoldTH }
      )
      this.answers.logCustom('Send XRP Success')
      this.isTransfering = false
    }
    if (MainStore.appState.selectedWallet.type === 'ethereum') {
      NavStore.pushToScreen('DashboardScreen')
      NavStore.showToastTop(
        `ส่ง ${amount} ETH สำเร็จ`,
        {},
        { color: AppStyle.mainColor, fontFamily: AppStyle.mainFontBoldTH }
      )
      this.answers.logCustom('Send ETH Success')
      this.isTransfering = false
    }
    if (MainStore.appState.selectedWallet.type === 'bitcoin') {
      NavStore.pushToScreen('DashboardScreen')
      NavStore.showToastTop(
        `ส่ง ${amount} BTC สำเร็จ`,
        {},
        { color: AppStyle.mainColor, fontFamily: AppStyle.mainFontBoldTH }
      )
      this.answers.logCustom('Send BTC Success')
      this.isTransfering = false
    }
  }

  _onSendFail = (err) => {
    const { address } = MainStore.appState.selectedWallet
    if (MainStore.appState.selectedWallet.type === 'aomtoken') {
      if (err.status && err.status === 404) {
        this.answers.logCustom('Send OM Error', {
          address,
          error: err.title
        })
        this.setWarning('OM')
      } else {
        this.setError(err.message)
      }

      api.Aom.reportBug({
        address: MainStore.appState.selectedWallet.address,
        error_component: 'SendStore',
        error_function: 'SendAOM',
        error_detail: JSON.stringify(this.txData),
        error_message: err.message
      })
    }
    if (MainStore.appState.selectedWallet.type === 'stellar') {
      if (err.status && err.status === 404) {
        this.answers.logCustom('Send XLM Error', {
          address,
          error: err.title
        })
        this.setWarning('OM')
      } else {
        this.setError(err.message)
      }

      api.Aom.reportBug({
        address: MainStore.appState.selectedWallet.address,
        error_component: 'SendStore',
        error_function: 'SendXLM',
        error_detail: err.message
      })
    }
    if (MainStore.appState.selectedWallet.type === 'ripple') {
      if (err.status && err.status === 404) {
        this.answers.logCustom('Send XRP Error', {
          address,
          error: err.message
        })
        this.setWarning('XRP')
      } else {
        this.setError(err.message)
      }

      api.Aom.reportBug({
        address: MainStore.appState.selectedWallet.address,
        error_component: 'SendStore',
        error_function: 'SendXRP',
        error_detail: err.message
      })
    }
    if (MainStore.appState.selectedWallet.type === 'ripple') {
      if (err.status && err.status === 404) {
        this.answers.logCustom('Send XRP Error', {
          address,
          error: err.message
        })
        this.setWarning('XRP')
      } else {
        this.setError(err.message)
      }

      api.Aom.reportBug({
        address: MainStore.appState.selectedWallet.address,
        error_component: 'SendStore',
        error_function: 'SendXRP',
        error_detail: err.message
      })
    }
    if (MainStore.appState.selectedWallet.type === 'ethereum') {
      if (err.status && err.status === 404) {
        this.answers.logCustom('Send ETH Error', {
          address,
          error: err.message
        })
        this.setWarning('ETH')
      } else {
        this.setError(err.message)
      }

      api.Aom.reportBug({
        address: MainStore.appState.selectedWallet.address,
        error_component: 'SendStore',
        error_function: 'SendETH',
        error_detail: err.message
      })
    }
    if (MainStore.appState.selectedWallet.type === 'bitcoin') {
      if (err.status && err.status === 404) {
        this.answers.logCustom('Send BTC Error', {
          address,
          error: err.message
        })
        this.setWarning('BTC')
      } else {
        this.setError(err.message)
      }

      api.Aom.reportBug({
        address: MainStore.appState.selectedWallet.address,
        error_component: 'SendStore',
        error_function: 'SendBTC',
        error_detail: err.message
      })
    }
    this.isTransfering = false
  }

  async fetchEstimateFee() {
    if (this.btcFeePerByte === 0) {
      await api.fetchEstimateFeeBTC().then((res) => {
        this.btcFeePerByte = res.data.halfHourFee
      })
    }
  }

  estimateFeeBTC(m, n) {
    return (93 * m + 102 * n + 200) * this.btcFeePerByte
    // return 93 * m + 102 * n + 200
  }

  async estimateGasPriceETH() {
    NavStore.showLoading()
    if (this.ethGasPrice === 0) {
      await api.fetchGasPrice().then((res) => {
        this.ethGasPrice = res.data.average
      })
    }
    MainStore.sendTransaction.confirmStore.setGasPrice(this.ethGasPrice)
    NavStore.hideLoading()
    NavStore.pushToScreen('ConfirmScreen')
  }

  sendLTC(ds) {
    let amount = parseInt(MainStore.sendTransaction.confirmStore.value.times(new BigNumber(1e+8)).toFixed(0), 10)
    const toAddress = MainStore.sendTransaction.addressInputStore.address
    let balance = 0
    for (let s = 0; s < this.txIDLTCData.length; s++) {
      balance += this.txIDLTCData[s].value * 100000000
    }
    const fee = 500000
    return new Promise((resolve, reject) => {
      this.getPrivateKey(ds)
        .then((privateKey) => {
          const { address: myAddress } = MainStore.appState.selectedWallet

          const mainnet = bitcoin.networks.litecoin
          const keyPair = new bitcoin.ECPair(bigi.fromHex(privateKey), undefined, { network: mainnet })
          const txb = new bitcoin.TransactionBuilder(mainnet)

          for (let ip = 0; ip < this.txIDLTCData.length; ip++) {
            txb.addInput(this.txIDLTCData[ip].txid, this.txIDLTCData[ip].output_no)
          }

          const noNeedBack = amount > balance - fee
          if (noNeedBack) {
            amount = balance - fee
          }

          txb.addOutput(toAddress, amount)
          !noNeedBack && txb.addOutput(myAddress, balance - amount - fee)

          for (let ip = 0; ip < this.txIDLTCData.length; ip++) {
            txb.sign(ip, keyPair, null, null, this.txIDLTCData[ip].value * 100000000)
          }

          const tx = txb.build()

          return api.pushTxLTC(tx.toHex()).then((res) => {
            if (res.status === 200) {
              resolve(res.data.data.txid)
            } else {
              reject(res.data)
            }
          })
        }).catch((err) => {
          reject(err)
        })
    })
  }

  sendDOGE(ds) {
    let amount = parseInt(MainStore.sendTransaction.confirmStore.value.times(new BigNumber(1e+8)).toFixed(0), 10)
    const toAddress = MainStore.sendTransaction.addressInputStore.address
    let balance = 0
    for (let s = 0; s < this.txIDDOGEData.length; s++) {
      balance += this.txIDDOGEData[s].value * 100000000
    }
    const fee = 100000000
    return new Promise((resolve, reject) => {
      this.getPrivateKey(ds)
        .then((privateKey) => {
          const { address: myAddress } = MainStore.appState.selectedWallet

          const mainnet = bitcoin.networks.dogecoin
          const keyPair = new bitcoin.ECPair(bigi.fromHex(privateKey), undefined, { network: mainnet })
          const txb = new bitcoin.TransactionBuilder(mainnet)

          for (let ip = 0; ip < this.txIDDOGEData.length; ip++) {
            txb.addInput(this.txIDDOGEData[ip].txid, this.txIDDOGEData[ip].output_no)
          }

          const noNeedBack = amount > balance - fee
          if (noNeedBack) {
            amount = balance - fee
          }

          txb.addOutput(toAddress, amount)
          !noNeedBack && txb.addOutput(myAddress, balance - amount - fee)

          for (let ip = 0; ip < this.txIDDOGEData.length; ip++) {
            txb.sign(ip, keyPair, null, null, this.txIDDOGEData[ip].value * 100000000)
          }

          const tx = txb.build()

          return api.pushTxDOGE(tx.toHex()).then((res) => {
            if (res.status === 200) {
              resolve(res.data.data.txid)
            } else {
              reject(res.data)
            }
          })
        }).catch((err) => {
          reject(err)
        })
    })
  }

  sendBTC(ds) {
    const wallet = MainStore.appState.selectedWallet

    let amount = parseInt(new BigNumber(MainStore.sendTransaction.confirmStore.value).times(new BigNumber(1e+8)).toFixed(0), 10)
    const toAddress = MainStore.sendTransaction.addressInputStore.address
    let balance = 0
    for (let s = 0; s < this.txIDData.length; s++) {
      balance += this.txIDData[s].value
    }

    const fee = this.estimateFeeBTC(this.txIDData.length, 2)

    return new Promise((resolve, reject) => {
      this.getPrivateKey(ds)
        .then((privateKey) => {
          const mainnet = bitcoin.networks.bitcoin
          const keyPair = new bitcoin.ECPair(bigi.fromHex(privateKey), undefined, { network: mainnet })

          const p2wpkh = bitcoin.payments.p2wpkh({ pubkey: keyPair.publicKey, network: mainnet })
          const p2sh = bitcoin.payments.p2sh({ redeem: p2wpkh, network: mainnet })
          const txb = new bitcoin.TransactionBuilder(mainnet)

          for (let ip = 0; ip < this.txIDData.length; ip++) {
            txb.addInput(this.txIDData[ip].tx_hash_big_endian, this.txIDData[ip].tx_output_n)
          }

          const noNeedBack = amount > balance - fee
          if (noNeedBack) {
            amount = balance - fee
          }

          txb.addOutput(toAddress, amount)
          !noNeedBack && txb.addOutput(wallet.address, balance - amount - fee)

          for (let ip = 0; ip < this.txIDData.length; ip++) {
            txb.sign(ip, keyPair, p2sh.redeem.output, null, this.txIDData[ip].value)
          }

          const tx = txb.build()
          return api.pushTxBTC(tx.toHex()).then((res) => {
            if (res.status === 200) {
              console.log('pass')
              console.log(res)
              wallet.fetchingBalance()
              wallet.fetchingTransaction()

              resolve(tx.getId())
            } else {
              console.log('fail')
              console.log(res)
              reject(res.data)
            }
          })
        }).catch((err) => {
          reject(err)
        })
    })
  }

  sendETH(transaction, ds) {
    const wallet = MainStore.appState.selectedWallet

    if (!this.confirmStore.validateAmount()) {
      const err = { message: 'Not enough gas to send this transaction' }
      return Promise.reject(err)
    }

    // Rewrite transaction value
    Object.assign(transaction, {
      value: new BigNumber(transaction.value)
    })

    const valueFormat = transaction.value.times(new BigNumber(1e+18)).toString(16)
    const transactionSend = { ...transaction, value: `0x${valueFormat}` }
    return new Promise((resolve, reject) => {
      this.getPrivateKey(ds).then((privateKey) => {
        console.log('transactionSend')
        console.log(transactionSend)
        sendTransaction(this.rpcURL, transactionSend, this.fromAddress, this.chainId, privateKey)
          .then((tx) => {
            console.log('tx')
            console.log(tx)

            wallet.fetchingBalance()
            wallet.fetchingTransaction()

            this.addAndUpdateGlobalUnpendTransactionInApp(tx, transaction, this.isToken)
            resolve(tx)
          })
          .catch(err => reject(err))
      }).catch(err => reject(err))
    })
  }

  sendAOM(ds) {
    const { value, fee } = this.confirmStore
    const amount = parseFloat(value, 10)
    const wallet = MainStore.appState.selectedWallet
    const toAddress = this.addressInputStore.address

    return new Promise((resolve, reject) => {
      this.getPrivateKey(ds)
        .then((privateKey) => {
          const selected = this.addressInputStore.wallet
          const { code, issuer } = selected
          const server = Stellar.Server()
          const passphrase = URL.OmChain.getPassphrase()
          return Stellar.Payment(server, privateKey, toAddress, amount, fee, code, issuer, false, passphrase)
        })
        .then((pdata) => {
          this.txData = pdata
          return api.Aom.confirmTransaction(pdata)
        })
        .then((result) => {
          console.log(`sendAom result`)
          console.log(result.data.data)
          if (!result || !result.status) {
            throw new Error(result.data.message)
          }
          if (result.status !== 200 || !(result.data.data && result.data.data.href)) {
            throw new Error(`${result.data.message} (${result.data.data.result.extras.result_codes.transaction || 'unknow'})`)
          }

          // Send notification
          api.paymentNotification(wallet.address, toAddress, amount, result.data.data.href)

          // wallet.transactions = []
          wallet.fetchingBalance()
          wallet.fetchingTransaction()

          resolve(result)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }

  sendXLM(ds) {
    const { value, fee } = this.confirmStore
    const amount = parseFloat(value, 10)
    const wallet = MainStore.appState.selectedWallet
    const toAddress = this.addressInputStore.address

    return new Promise((resolve, reject) => {
      this.getPrivateKey(ds)
        .then((privateKey) => {
          const selected = this.addressInputStore.wallet
          const { code, issuer } = selected
          const server = URL.Stellar.apiURL()
          const passphrase = Stellar.Networks().PUBLIC
          return Stellar.Payment(server, privateKey, toAddress, amount, fee, code, issuer, true, passphrase)
        })
        .then((result) => {
          wallet.fetchingBalance()
          wallet.fetchingTransaction()
          resolve(result)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }

  sendXRP(ds) {
    const { value } = this.confirmStore
    const amount = parseFloat(value, 10)
    const wallet = MainStore.appState.selectedWallet
    const toAddress = this.addressInputStore.address

    // Ripple
    const rippleAPI = URL.Ripple.rippleAPI()

    return new Promise((resolve, reject) => {
      rippleAPI.connect()
        .then(() => rippleAPI.prepareTransaction({
          TransactionType: 'Payment',
          Account: wallet.address,
          Amount: rippleAPI.xrpToDrops(amount),
          Destination: toAddress
        }, { maxLedgerVersionOffset: 75 }))
        .then(async (preparedTx) => {
          const { txJSON } = preparedTx
          const mnemonic = await this.getPrivateKey(ds)
          const txSign = await Ripple.Sign(txJSON, mnemonic)
          return rippleAPI.submit(txSign.signedTransaction)
        })
        .then((result) => {
          const { resultCode } = result
          if (resultCode === 'tesSUCCESS') {
            rippleAPI.disconnect()
            wallet.fetchingBalance()
            wallet.fetchingTransaction()
            resolve(result)
          } else if (resultCode === 'tecNO_DST_INSUF_XRP') {
            throw new CustomWarning({
              status: 404,
              message: 'Account not found'
            })
          }
        })
        .catch((err) => {
          reject(err)
        })
    })
  }

  sendToken(transaction, ds) {
    if (!this.confirmStore.validateAmount()) {
      const err = { message: 'Not enough gas to send this transaction' }
      return Promise.reject(err)
    }
    const token = MainStore.appState.selectedToken
    const {
      to,
      value
    } = transaction
    return new Promise((resolve, reject) => {
      this.getPrivateKey(ds)
        .then((privateKey) => {
          const numberOfDecimals = token.decimals
          const numberOfTokens = `0x${value.times(new BigNumber(`1e+${numberOfDecimals}`)).toString(16)}`
          const inf = new Interface(abi)
          const transfer = inf.functions.transfer(to, numberOfTokens)
          const unspentTransaction = {
            data: transfer.data,
            to: token.address,
            gasLimit: transaction.gasLimit,
            gasPrice: transaction.gasPrice
          }

          return sendTransaction(this.rpcURL, unspentTransaction, this.fromAddress, this.chainId, privateKey)
        })
        .then((tx) => {
          this.addAndUpdateGlobalUnpendTransactionInApp(tx, transaction, this.isToken)
          resolve(tx)
        }).catch((e) => {
          reject(e)
        })
    })
  }

  async addAndUpdateGlobalUnpendTransactionInApp(txHash, transaction, isToken) {
    const { selectedToken, selectedWallet } = MainStore.appState
    const {
      to,
      value,
      gasLimit,
      gasPrice
    } = transaction
    const pendingTransaction = {
      timeStamp: Math.floor(Date.now() / 1000),
      hash: txHash,
      from: selectedWallet.address,
      contractAddress: isToken ? selectedToken.address : '',
      to,
      value: isToken
        ? new BigNumber(value).times(`1.0e+${selectedToken.decimals}`).toString(10)
        : new BigNumber(value).times('1.0e+18').toString(10),
      tokenName: isToken ? selectedToken.title : 'Ethereum',
      tokenSymbol: isToken ? selectedToken.symbol : 'ETH',
      tokenDecimal: isToken ? selectedToken.decimals : 18,
      gas: gasLimit,
      gasPrice
    }

    const unpendTxObj = await selectedToken.addUnspendTransaction(pendingTransaction)
    MainStore.appState.setUnpendTransactions([unpendTxObj, ...MainStore.appState.unpendTransactions])
  }
}

const abi = JSON.parse('[{ "constant": true, "inputs": [], "name": "name", "outputs": [{ "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "stop", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "_spender", "type": "address" }, { "name": "_value", "type": "uint256" }], "name": "approve", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "totalSupply", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_from", "type": "address" }, { "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" }], "name": "transferFrom", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "decimals", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_value", "type": "uint256" }], "name": "burn", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "address" }], "name": "balanceOf", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "stopped", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "symbol", "outputs": [{ "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" }], "name": "transfer", "outputs": [{ "name": "success", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "start", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "name": "_name", "type": "string" }], "name": "setName", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "address" }, { "name": "", "type": "address" }], "name": "allowance", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "inputs": [{ "name": "_addressFounder", "type": "address" }], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "_from", "type": "address" }, { "indexed": true, "name": "_to", "type": "address" }, { "indexed": false, "name": "_value", "type": "uint256" }], "name": "Transfer", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "_owner", "type": "address" }, { "indexed": true, "name": "_spender", "type": "address" }, { "indexed": false, "name": "_value", "type": "uint256" }], "name": "Approval", "type": "event" }]')

export default SendStore
