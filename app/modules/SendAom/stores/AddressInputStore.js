import { observable, computed, action } from 'mobx'
import { chainNames } from '../../../Utils/WalletAddresses'
import MainStore from '../../../AppStores/MainStore'
import Checker from '../../../Handler/Checker'
import Helper from '../../../commons/Helper'
import API from '../../../api'

export default class AddressInputStore {
  @observable.ref amount = 0
  @observable.ref amountUSD = 0
  @observable.ref type = 'ETH'
  @observable address = ''
  @observable.ref addressModal = null
  @observable.ref qrCodeModal = null
  @observable.ref confirmModal = null
  @observable countSentTime = 0
  @observable.ref information = {
    name: null,
    avatar: null
  }

  @observable disableSend = true

  @computed get selectedToken() {
    return MainStore.appState.selectedToken
  }

  @computed get wallet() {
    const wallet = MainStore.appState.selectedWallet

    if (wallet.type === 'aomtoken') {
      const { selectedAsset } = wallet
      return {
        code: selectedAsset.asset_code || 'OM',
        issuer: selectedAsset.asset_issuer || null,
        title: selectedAsset.asset_code ? `${selectedAsset.asset_code} Token` : 'OM',
        balance: selectedAsset.balance,
        balanceText: Helper.formatETH(selectedAsset.balance, true, 2),
        type: wallet.type,
        fee: wallet.BASE_FEE
      }
    } else if (wallet.type === 'stellar') {
      return {
        code: wallet.ext,
        title: wallet.ext,
        balance: wallet.balance,
        balanceText: wallet.balanceText,
        type: wallet.type,
        fee: 100
      }
    } else if (wallet.type === 'ripple') {
      return {
        code: wallet.ext,
        title: wallet.ext,
        balance: wallet.balance,
        balanceText: wallet.balanceText,
        type: wallet.type,
        fee: 0
      }
    } else if (wallet.type === 'ethereum') {
      return {
        code: wallet.ext,
        title: wallet.ext,
        balance: wallet.balance,
        balanceText: wallet.balanceText,
        type: wallet.type,
        fee: 0
      }
    } else if (wallet.type === 'bitcoin') {
      return {
        code: wallet.ext,
        title: wallet.ext,
        balance: wallet.balance,
        balanceText: wallet.balanceText,
        type: wallet.type,
        fee: 0
      }
    }

    return null
  }

  @computed get userInfomation() {
    return this.information
  }

  @action setAddress(address) {
    this.address = address
  }

  @action setAddressFromQrCode(result) {

  }

  @action setDisableSend(bool) {
    this.disableSend = bool
  }

  @action validateAddress() {
    const { selectedWallet } = MainStore.appState
    const coin = this.getSymbol(selectedWallet.type)
    this.disableSend = !Checker.checkAddress(this.address, coin)
  }

  @action async syncAddressInfo() {
    const { selectedWallet } = MainStore.appState
    const coin = this.getSymbol(selectedWallet.type)
    if (coin === chainNames.AOM) {
      await API.userInfo(this.address)
        .then((res) => {
          const { code, success, data } = res.data
          if (code === 200 && success && data && data.first_name) {
            this.information = {
              name: `${data.first_name} ${data.last_name}`,
              avatar: data.profile_image
            }
          } else {
            this.information = {
              name: null,
              avatar: null
            }
          }
        }).catch(() => {
          this.information = {
            name: null,
            avatar: null
          }
        })
    }
  }

  getSymbol(type) {
    switch (type) {
      case 'ethereum': return 'Ethereum'
      case 'bitcoin': return 'Bitcoin'
      case 'litecoin': return 'Litecoin'
      case 'dogecoin': return 'Dogecoin'
      case 'Stellar': return 'Stellar'
      case 'ripple': return 'Ripple'
      case 'aomtoken': return 'AOM Token'
      default: return 'BTC'
    }
  }

  checkSentTime() {
    const from = MainStore.appState.selectedWallet.address
    this.getSentTime(from, this.address).then((res) => {
      if (res > 0) {
        this.setCount(res)
      }
    })
  }

  @action setCount(value) {
    this.countSentTime = value
  }

  @action setAmount(amount) {
    this.amount = amount
  }

  getSentTime(from, to) {
    return API.getSentTime(from, to).then((res) => {
      if (res.data) {
        return res.data.data
      }
      return 0
    })
  }

  @computed get amountFormated() {
    if (this.type === 'ETH') {
      return `${this.amount} ETH`
    }
    return `$${this.amountUSD}`
  }
}
