import React, { Component } from 'react'
import {
  Alert,
  FlatList,
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import PropsType from 'prop-types'
import { observer } from 'mobx-react/native'

// Components & Modules
import icons from '../../../commons/icons'
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import AddressBookStore from '../stores/AddressBookStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'
import Avatar from '../elements/Avatar'

const marginTop = LayoutUtils.getExtraTop()
const title = 'รายการที่ชอบ'

@observer
export default class Favorite extends Component {
  static propTypes = {
    navigation: PropsType.object
  }

  static defaultProps = {
    navigation: null
  }

  constructor(props) {
    super(props)

    this.onDelete = this.onDelete.bind(this)

    MainStore.addressBookStore = new AddressBookStore()
    this.addressBookStore = MainStore.addressBookStore
  }

  onBack = () => {
    NavStore.pushToScreen('AddressInputScreen')
  }

  onDelete = (name, address, index) => () => {
    const addr = address.replace(/^([A-Z0-9]{2})(.+)([A-Z0-9]{5})$/, '$1...$3')
    // Works on both iOS and Android
    Alert.alert(
      'ท่านกำลังลบรายการที่ชอบ',
      `${name}\n${addr}\nกำลังจะถูกลบ! ยืนยันหรือไม่`,
      [
        {
          text: 'ยกเลิก',
          style: 'cancel'
        },
        {
          text: 'ยืนยัน',
          onPress: async () => {
            const selectedAB = this.addressBooks[index]
            await selectedAB.delete()
            MainStore.appState.syncAddressBooks()
          }
        }
      ],
      { cancelable: false }
    )
  }

  get addressBooks() {
    return MainStore.appState.addressBooks
  }

  _keyExtractor = item => item.address

  _onPressItem = item => () => {
    this.props.navigation.state.params.returnData(item)
    this.props.navigation.goBack()
  }

  _renderItem = ({ item, index }) => {
    const address = item.address.replace(/^([A-Z0-9]{2})(.+)([A-Z0-9]{5})$/, '$1...$3')
    return (
      <TouchableOpacity onPress={this._onPressItem(item)}>
        <View style={styles.favoriteList}>
          {/* Begin profile image */}
          <View style={{ width: 44, marginRight: 5 }}>
            <Avatar
              style={{
                width: 34,
                height: 34,
                borderRadius: 17
              }}
              address={item.address}
            />
          </View>
          {/* End profile imgage */}

          {/* Begin name */}
          <View style={{ flex: 1, paddingRight: 25 }}>
            <Text style={styles.favoriteName}>
              {item.title}
            </Text>
            <Text style={styles.favoriteAddress}>
              {address}
            </Text>
          </View>
          {/* End name */}

          <TouchableOpacity
            style={styles.shareButton}
            onPress={this.onDelete(item.title, item.address, index)}
          >
            <Image source={icons.iconTrash} resizeMode="contain" />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    )
  }

  renderHeader = () => (
    <NavigationHeader
      style={{
        borderBottomColor: '#E5E5E5',
        borderBottomWidth: 1,
        marginTop: marginTop + 20,
        paddingHorizontal: 20,
        paddingBottom: 15
      }}
      headerItem={{
        title,
        icon: null,
        button: images.iconBackAlt
      }}
      titleStyle={{
        flex: 1,
        marginRight: 20,
        textAlign: 'center',
        fontFamily: AppStyle.mainFontBoldTH,
        color: AppStyle.colorBlack
      }}
      action={this.onBack}
    />
  )

  renderBody = () => (
    <ScrollView
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      style={{
        flex: 1
      }}
    >
      {this.renderFlatItem()}
    </ScrollView>
  )

  renderFlatItem() {
    const { addressBooks } = MainStore.appState
    return (
      <View>
        <FlatList
          data={addressBooks}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
        />
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        {/* Begin header container */}
        {this.renderHeader()}
        {/* End header container */}

        {/* Begin body container */}
        {this.renderBody()}
        {/* End body container */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  favoriteList: {
    flex: 1,
    flexDirection: 'row',
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
    paddingHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 8
  },
  favoriteName: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 16,
    color: '#000'
  }
})
