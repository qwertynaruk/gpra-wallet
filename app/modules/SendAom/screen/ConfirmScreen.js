import React, { Component } from 'react'
import {
  Alert,
  Animated,
  BackHandler,
  Dimensions,
  Image,
  ImageBackground,
  Keyboard,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from 'react-native'
import PropsType from 'prop-types'

// Third-party
import { action } from 'mobx'
import { observer } from 'mobx-react'
import Modal from 'react-native-modalbox'
import LinearGradient from 'react-native-linear-gradient'
import { KeyboardAccessoryNavigation } from 'react-native-keyboard-accessory'

// Components & Modules
import icons from '../../../commons/icons'
import images from '../../../commons/images'
import AddressBookStore from '../stores/AddressBookStore'
import AppStyle from '../../../commons/AppStyle'
import OmWarning from '../elements/OmWarning'
import XrpWarning from '../elements/XrpWarning'
import LayoutUtils from '../../../commons/LayoutUtils'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const marginTop = LayoutUtils.getExtraTop()
const { width } = Dimensions.get('window')

@observer
export default class ConfirmScreen extends Component {
  static propTypes = {
    navigation: PropsType.object
  }

  static defaultProps = {
    navigation: null
  }

  constructor(props) {
    super(props)

    // Set state
    this.state = {
      fetching: false
    }

    MainStore.addressBookStore = new AddressBookStore()
    this.addressBookStore = MainStore.addressBookStore
    this.sendStore = MainStore.sendTransaction
    this.addressInputStore = this.sendStore.addressInputStore
    this.confirmStore = this.sendStore.confirmStore
    this.answers = MainStore.appState.fabric.answers

    this.handleBackPress = this.handleBackPress.bind(this)
    this.goBack = this.goBack.bind(this)
    this.onConfrimPress = this.onConfrimPress.bind(this)
    this.onReportBugDone = this.onReportBugDone.bind(this)
    this.setMemo = this.setMemo.bind(this)
    this._saveToAddressBook = this._saveToAddressBook.bind(this)
    this._hideKeyboard = this._hideKeyboard.bind(this)
    this._handleSubmitTransaction = this._handleSubmitTransaction.bind(this)

    // Animate
    this.animatedValue = new Animated.Value(0)
  }

  componentWillMount() {
    // Back handler
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }

    // Set address
    const { address } = this.addressInputStore
    this.addressBookStore.setAddress(address)
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  onConfrimPress() {
    this.setState({ fetching: true })
    this._handleSubmitTransaction()
  }

  onReportBugDone() {
    Alert.alert(
      'สำเร็จ !',
      'คุณทำการแจ้งปัญหาเสร็จเรียบร้อย', [{
        text: 'ตกลง',
        onPress: () => NavStore.pushToScreen('DashboardScreen')
      }]
    )
  }

  get modalStyle() {
    if (width > 400) {
      return {
        width: 345
      }
    }

    return {
      paddingHorizontal: 20
    }
  }

  setMemo(memo) {
    if (memo.length > 40) {
      return
    }

    this.confirmStore.setMemo(memo)
  }

  @action async _saveToAddressBook() {
    const { userInfomation } = this.addressInputStore
    this.addressBookStore.setTitle(userInfomation.name)
    this.addressBookStore.setFieldFocus('name')
    await this.addressBookStore.saveAddressBook()
  }

  _hideKeyboard() {
    Keyboard.dismiss()
  }

  _handleSubmitTransaction() {
    this.runAnimation()
    MainStore.sendTransaction.sendTx()
  }

  runAnimation() {
    this.animatedValue.setValue(0)
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 800,
      useNativeDriver: true
    }).start(() => {
      this.runAnimation()
    })
  }

  handleBackPress = () => {
    this.goBack()
    return true
  }

  goBack = () => {
    this.props.navigation.goBack()
  }

  _renderReceiver() {
    const { address, userInfomation } = this.addressInputStore
    if (userInfomation.name) {
      return (
        <View>
          <Text
            style={{
              color: 'white',
              fontFamily: AppStyle.mainFontBoldTH,
              fontSize: 24,
              flexWrap: 'wrap'
            }}
          >
            {userInfomation.name}
          </Text>
          <Text
            style={{
              color: 'rgba(255,255,255,0.56)',
              fontFamily: AppStyle.mainFont,
              fontSize: 12
            }}
          >
            {address}
          </Text>
        </View>
      )
    }

    return (
      <View>
        <Text
          style={{
            color: 'white',
            fontFamily: AppStyle.mainFontBoldTH,
            fontSize: 18,
            flexWrap: 'wrap'
          }}
        >
          {address}
        </Text>
      </View>
    )
  }

  _renderAddToAddressBook() {
    const { userInfomation } = this.addressInputStore
    const { addressBookIsExisted } = this.addressBookStore

    if (userInfomation.name && !addressBookIsExisted) {
      return (
        <View
          style={{
            marginHorizontal: 25,
            marginTop: 30
          }}
        >
          <TouchableOpacity
            style={{
              height: 60,
              width: 200,
              borderWidth: 3,
              borderColor: '#fff',
              borderRadius: 8,
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'row'
            }}
            onPress={this._saveToAddressBook}
          >
            <Image
              style={{
                flex: 20,
                resizeMode: 'center'
              }}
              source={icons.iconAddFavorite}
            />

            <Text
              style={{
                flex: 80,
                color: '#FFF',
                fontFamily: AppStyle.mainFontTH,
                fontSize: 18
              }}
            >
              เพิ่มรายการโปรด
            </Text>
          </TouchableOpacity>
        </View>
      )
    }

    return null
  }

  _renderMemoField() {
    const { memo, enableMemo } = this.confirmStore
    if (enableMemo) {
      return (
        <View
          style={{
            marginHorizontal: 25,
            marginTop: 20
          }}
        >
          <Text
            style={{
              color: 'rgba(255,255,255,0.64)',
              fontFamily: AppStyle.mainFontBoldTH,
              fontSize: 14
            }}
          >
            Memo
          </Text>
          <View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'rgba(240,240,240,0.97)',
                borderRadius: 5,
                height: 50,
                paddingHorizontal: 10,
                marginTop: 3
              }}
            >
              <TextInput
                ref={ref => (this.amountInput = ref)}
                underlineColorAndroid="transparent"
                maxLength={40}
                keyboardAppearance="default"
                keyboardType="default"
                selectionColor="#FFF"
                selectTextOnFocus={true}
                value={memo}
                style={[
                  {
                    flex: 1,
                    fontFamily: AppStyle.mainFontTH,
                    fontSize: 15
                  }
                ]}
                returnKeyType="done"
                onChangeText={this.setMemo}
              />
            </View>
            <Text
              style={{
                color: '#FFF',
                fontSize: 12,
                fontFamily: AppStyle.mainFontTH,
                textAlign: 'right',
                paddingTop: 10
              }}
            >
              {`ความยาวไม่เกิน ${memo.length}/40 ตัวอักษร`}
            </Text>
          </View>
        </View>
      )
    }
    return null
  }

  render() {
    const {
      formatedAmount, formatedFee, value
    } = this.confirmStore
    const {
      isTransfering,
      isWarning,
      isError,
      warning
    } = this.sendStore
    const interpolateRotation = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })
    const animatedStyle = {
      transform: [
        { rotate: interpolateRotation }
      ]
    }

    // Get user info
    const { modalStyle } = this

    return (
      <TouchableWithoutFeedback onPress={this._hideKeyboard}>
        <ImageBackground
          source={images.backgroundSend}
          style={styles.container}
        >
          <LinearGradient colors={['rgba(0,0,0,0.5)', 'rgba(0,0,0,0)']} style={styles.linearGradient} />
          <NavigationHeader
            style={{
              marginTop: marginTop + 20,
              paddingHorizontal: 20,
              width
            }}
            headerItem={{
              title: 'ยืนยันการทำรายการ',
              icon: null,
              button: images.iconBackWhite
            }}
            titleStyle={{
              color: '#FFF',
              fontSize: 18,
              fontFamily: AppStyle.mainFontBoldTH,
              textAlign: 'center',
              flex: 1,
              marginRight: 20
            }}
            action={this.goBack}
          />
          <ScrollView
            style={styles.container}
            showsVerticalScrollIndicator={false}
          >
            <View
              style={{
                marginHorizontal: 25,
                marginTop: 20
              }}
            >
              <View style={{ marginBottom: 20 }}>
                <Text
                  style={{
                    color: 'rgba(255,255,255,0.64)',
                    fontFamily: AppStyle.mainFontBold,
                    fontSize: 14
                  }}
                >
                  {`Send`}
                </Text>
                <View>
                  <Text
                    style={{
                      color: 'white',
                      fontFamily: AppStyle.mainFontBoldTH,
                      fontSize: value > 100000 ? 30 : 40
                    }}
                  >
                    {formatedAmount}
                  </Text>
                </View>
                <Text
                  style={{
                    color: 'rgba(255,255,255,0.56)',
                    fontFamily: AppStyle.mainFont,
                    fontSize: 12
                  }}
                >
                  {formatedFee}
                </Text>
              </View>
              <View>
                <Text
                  style={{
                    color: 'rgba(255,255,255,0.64)',
                    fontFamily: AppStyle.mainFontBoldTH,
                    fontSize: 14
                  }}
                >
                  {`To`}
                </Text>
                {this._renderReceiver()}
              </View>
            </View>

            {/* Add Friend Button */}
            {this._renderAddToAddressBook()}
            {/* End Add Friend Button */}

            {/* Start render memo field */}
            {this._renderMemoField()}
            {/* End render memo field */}

            {/* Confirm Button */}
            <View style={{
              marginHorizontal: 25,
              marginVertical: 30
            }}
            >
              <TouchableOpacity
                style={{
                  backgroundColor: '#7781F5',
                  height: 60,
                  borderRadius: 8,
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
                disable={this.state.fetching}
                onPress={this.onConfrimPress}
              >
                <Text
                  style={{
                    color: '#FFF',
                    fontFamily: AppStyle.mainFontBoldTH,
                    fontSize: 18
                  }}
                >
                  {`ยืนยันการทำรายการ`}
                </Text>
              </TouchableOpacity>
            </View>
            {/* End confirm Button */}
          </ScrollView>
          <KeyboardAccessoryNavigation
            androidAdjustResize={true}
            nextHidden={true}
            previousHidden={true}
            avoidKeyboard={true}
          />

          {
            isTransfering && (
              <View style={styles.transfering}>
                <View
                  style={{
                    backgroundColor: 'rgba(0,0,0,0.3)',
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <View style={{
                    backgroundColor: '#FFF',
                    borderRadius: 3,
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginHorizontal: 20,
                    paddingVertical: 30
                  }}
                  >
                    <Text
                      style={{
                        color: '#535786',
                        fontFamily: AppStyle.mainFontBoldTH,
                        fontSize: 20,
                        marginBottom: 20
                      }}
                    >
                      {`กำลังสร้าง Transaction`}
                    </Text>
                    <Animated.Image source={images.iconLoading} style={animatedStyle} />
                  </View>
                </View>
              </View>
            )
          }

          <Modal
            ref={(ref) => { this.confirmCreateModal = ref }}
            isOpen={isWarning}
            position="center"
            swipeToClose={false}
            backButtonClose={true}
            backdropPressToClose={false}
            backdropColor="#000"
            backdropOpacity={0.5}
            style={[modalStyle, {
              backgroundColor: 'transparent',
              justifyContent: 'center'
            }]}
          >
            <View>
              <View>
                {
                  warning === 'OM' && (
                    <OmWarning
                      onCancel={() => {
                        this.confirmCreateModal.close()
                        this.setState({ fetching: false })
                        this.sendStore.clearWarning()
                      }}
                    />
                  )
                }
                {
                  warning === 'XRP' && (
                    <XrpWarning
                      onCancel={() => {
                        this.confirmCreateModal.close()
                        this.setState({ fetching: false })
                        this.sendStore.clearWarning()
                      }}
                    />
                  )
                }
              </View>
            </View>
          </Modal>

          {/* Transaction Fail Dialog */}
          <Modal
            ref={(ref) => { this.tnsFailDialog = ref }}
            isOpen={isError}
            position="center"
            swipeToClose={true}
            backButtonClose={true}
            backdropPressToClose={true}
            style={[modalStyle, {
              backgroundColor: 'transparent',
              justifyContent: 'center'
            }]}
          >
            <View style={notiStyles.container}>
              <View style={notiStyles.dashboardHeader}>
                <View style={notiStyles.notiContainer}>
                  <Text style={notiStyles.title}>
                    {`ไม่สามารถส่งเหรียญได้`}
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center'
                    }}
                  >
                    <Image
                      style={{ padding: 20 }}
                      source={icons.iconRiskDescription}
                    />
                  </View>

                  <Text style={notiStyles.bodyText}>
                    {`เกิดข้อผิดพลาดระหว่างการส่ง กรุณาตรวจสอบข้อมูล และลองใหม่อีกครั้ง`}
                  </Text>

                  <TouchableOpacity
                    style={notiStyles.containerReportBugBtn}
                    onPress={() => {
                      this.sendStore.clearError()
                      this.onReportBugDone()
                    }}
                  >
                    <Text style={[notiStyles.btnText, { color: '#FFFFFF' }]}>
                      {`แจ้งปัญหา`}
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => {
                      this.sendStore.clearError()
                    }}
                  >
                    <Text style={[notiStyles.btnText, { color: '#535786', marginBottom: 20 }]}>
                      {`ลองใหม่อีกครั้ง`}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
          {/* End Transaction Fail Dialog */}

        </ImageBackground>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  linearGradient: {
    height: 100,
    position: 'absolute',
    width: '100%',
    top: 0
  },
  transfering: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

const notiStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  notiContainer: {
    backgroundColor: '#FFF',
    borderRadius: 3
  },
  title: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20,
    color: '#535786',
    textAlign: 'center',
    marginTop: 25,
    marginBottom: 30
  },
  bodyText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    color: '#000',
    textAlign: 'center',
    padding: 10,
    marginTop: 30
  },
  btnText: {
    textAlign: 'center',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20
  },
  containerReportBugBtn: {
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    marginHorizontal: 20,
    borderRadius: 10,
    backgroundColor: '#535786'
  }
})
