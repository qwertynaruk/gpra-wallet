import React, { Component } from 'react'
import {
  Animated,
  BackHandler,
  Image,
  ImageBackground,
  Keyboard,
  Platform,
  StyleSheet,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import BigNumber from 'bignumber.js'
import { observer } from 'mobx-react'
import { getStatusBarHeight } from 'react-native-status-bar-height'
import LinearGradient from 'react-native-linear-gradient'

// Components & Modules
import icons from '../../../commons/icons'
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import Helper from '../../../commons/Helper'
import BottomButton from '../../../components/elements/BottomButton'
import Checker from '../../../Handler/Checker'
import LayoutUtils from '../../../commons/LayoutUtils'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const marginTop = Platform.OS === 'ios' ? getStatusBarHeight() : 20
const isIPX = LayoutUtils.getIsIPX()

@observer
class AdressInputScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object
  }

  static defaultProps = {
    navigation: {}
  }

  constructor(props) {
    super(props)
    this.state = {
      extraHeight: new Animated.Value(0),
      showQRScanner: true,
      showFavorite: true,
      amount: 0,
      address: '',
      inputAmount: '0',
      dirty: false,
      fetching: false
    }

    this.handleConfirm = this.handleConfirm.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
    this.onBack = this.onBack.bind(this)
    this.openAddressModal = this.openAddressModal.bind(this)
    this.openScanQRCode = this.openScanQRCode.bind(this)
    this.onTouchDismissKeyboard = this.onTouchDismissKeyboard.bind(this)
    this.parseAmount = this.parseAmount.bind(this)
    this.setAmount = this.setAmount.bind(this)

    this.answers = MainStore.appState.fabric.answers

    // Stores
    this.addressInputStore = MainStore.sendTransaction.addressInputStore
  }

  componentWillMount() {
    const show = Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow'
    const hide = Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide'
    this.keyboardDidShowListener = Keyboard.addListener(show, e => this._keyboardDidShow(e))
    this.keyboardDidHideListener = Keyboard.addListener(hide, e => this._keyboardDidHide(e))

    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }

    this.keyboardDidShowListener.remove()
    Keyboard.dismiss()
  }

  onTouchDismissKeyboard() {
    Keyboard.dismiss()
  }

  onBack() {
    NavStore.pushToScreen('DashboardScreen')
  }

  getSymbol(type) {
    switch (type) {
      case 'ethereum': return 'Ethereum'
      case 'bitcoin': return 'Bitcoin'
      case 'litecoin': return 'Litecoin'
      case 'dogecoin': return 'Dogecoin'
      case 'stellar': return 'Stellar'
      case 'aomtoken': return 'AOM Token'
      default: return 'BTC'
    }
  }

  setAmount(input) {
    const { type } = MainStore.appState.selectedWallet
    let inputAmount = input.trim()
      // eslint-disable-next-line no-useless-escape
      .replace(/([^\d\.])/g, '')
      .replace(/^0+/, '0')
      .replace(/^0([1-9])+/, '$1')
      .replace(/^\./, '0.')

    if (type === 'aomtoken') {
      inputAmount = inputAmount
        .replace(/^(\.\d\d?).*/, '$1')
        .replace(/(\d{1,9})(\d+)?(\.?)(\d\d?)?.*/, '$1$3$4')
    } else {
      inputAmount = inputAmount
        .replace(/^(\.\d+).*/, '$1')
        .replace(/(\d{1,9})(\d+)?(\.?)(\d+)?.*/, '$1$3$4')
    }

    const amount = this.parseAmount(inputAmount.replace(/\.$/, '.0'))
    this.setState({
      amount,
      inputAmount,
      dirty: true
    })
  }

  get hasFavorite() {
    if (MainStore.appState.addressBooks.length) {
      return true
    }

    return false
  }

  get amountError() {
    const { kycLevel, selectedWallet } = MainStore.appState
    const { balance, type, selectedAsset } = selectedWallet
    const amount = new BigNumber(this.state.amount)
    let error = false

    if (type === 'aomtoken') {
      const assetBalance = selectedAsset.balance
      if (amount.lte(0) || amount.gt(assetBalance) || amount.lt(0.1)) {
        error = true
      }
      if (kycLevel === 1) {
        if (amount.gt(this.maxTransfer)) {
          error = true
        }
      }
    } else if (amount.lte(0) || amount.gt(balance)) {
      error = true
    }

    return error
  }

  get maxTransfer() {
    const { kycLevel, selectedWallet, userInfo } = MainStore.appState
    const { type } = selectedWallet
    if (type === 'aomtoken' && kycLevel === 1) {
      const { transfer_total } = userInfo
      // Get max transfer
      const max = MainStore.appState.remoteConfig.getValue('kyc1max_transfer')
      return new BigNumber(transfer_total || 0).minus(max).abs().toNumber()
    }

    return null
  }

  parseAmount(input) {
    const amount = Number(`0${input}`)
    return isFinite(amount) ? amount : 0
  }

  handleBackPress() {
    this.onBack()
    return true
  }

  _runExtraHeight(toValue) {
    Animated.timing(
      // Animate value over time
      this.state.extraHeight, // The value to drive
      {
        toValue: -toValue, // Animate to final value of 1
        duration: 250,
        useNativeDriver: true
      }
    ).start()
  }

  _keyboardDidShow(e) {
    if (this.amountInput.isFocused() || this.addressInput.isFocused()) {
      if (e.endCoordinates.screenY < 437 + marginTop) {
        this._runExtraHeight(437 + marginTop - e.endCoordinates.screenY)
      }
    }
  }

  _keyboardDidHide(e) {
    this._runExtraHeight(0)
    Keyboard.dismiss()
  }

  handleConfirm() {
    this.setState({
      fetching: true
    })

    // State
    const { address, amount } = this.state
    const { wallet } = this.addressInputStore

    // Set store
    MainStore.sendTransaction.addressInputStore.setAddress(address)
    MainStore.sendTransaction.confirmStore.setValue(amount)
    MainStore.sendTransaction.confirmStore.setTitle(wallet.code)
    MainStore.sendTransaction.confirmStore.setFee(wallet.fee)
    MainStore.sendTransaction.addressInputStore.syncAddressInfo()
      .then(() => {
        // Go to confirm
        MainStore.sendTransaction.goToConfirm()
          .then(() => {
            this.setState({
              fetching: false
            })
            this.answers.logContentView('Confirm Send', 'screen', 'confirm-send-screen')
          })
      })
  }

  gotoScan() {
    const { navigation } = this.props

    setTimeout(() => {
      navigation.navigate('ScanQRCodeScreen', {
        title: 'Scan QR Code',
        marginTop,
        returnData: this.returnData.bind(this)
      })
      this.answers.logContentView('Scan QR', 'screen', 'scan-qr-screen')
    }, 300)
  }

  openScanQRCode() {
    Keyboard.dismiss()
    this.gotoScan()
  }

  openAddressModal() {
    Keyboard.dismiss()
    const { addressModal } = MainStore.sendTransaction.addressInputStore
    addressModal && addressModal.open()
  }

  validateAddress(address) {
    const { selectedWallet } = MainStore.appState
    const coin = this.getSymbol(selectedWallet.type)
    return Checker.checkAddress(address, coin)
  }

  _goToFavorite = () => {
    const { navigation } = this.props
    setTimeout(() => {
      navigation.navigate('Favorite', {
        returnData: this.selectedFavorite.bind(this)
      })
      this.answers.logContentView('Favorite', 'screen', 'favorite-screen')
    }, 300)
  }

  selectedFavorite = (item) => {
    const { address } = item
    this.setState({
      address,
      showQRScanner: false,
      showFavorite: false,
      showResetBtn: false
    })
  }

  returnData = (address) => {
    this.setState({
      address,
      showQRScanner: false,
      showFavorite: false,
      showResetBtn: false
    })
  }

  renderInputAmount() {
    const { wallet } = this.addressInputStore
    const { title } = wallet
    const { inputAmount, dirty } = this.state
    const { amountError } = this
    const showError = dirty && amountError

    return (
      <View style={styles.walletScreen}>
        <View style={styles.walletBalance}>
          <Text style={styles.textBalanceDesc}>{title}</Text>
          <TextInput
            ref={ref => (this.amountInput = ref)}
            underlineColorAndroid="transparent"
            maxLength={10}
            keyboardAppearance="dark"
            keyboardType="numeric"
            selectionColor="#FFF"
            selectTextOnFocus={true}
            value={inputAmount}
            style={[
              styles.textBalance,
              {
                alignSelf: 'stretch',
                fontSize: this.state.amount >= 100000 ? 38 : 48,
                textAlign: 'center'
              },
              showError ? { color: '#C60003' } : {}
            ]}
            returnKeyType="next"
            onChangeText={this.setAmount}
            onSubmitEditing={() => {
              this.addressInput.focus()
            }}
          />
        </View>
      </View>
    )
  }

  renderInputAddress = () => {
    const { wallet } = this.addressInputStore
    const showFavorite = wallet.type === 'aomtoken'
    return (
      <View style={styles.addressInput}>
        <TextInput
          ref={ref => (this.addressInput = ref)}
          underlineColorAndroid="transparent"
          keyboardAppearance="dark"
          selectionColor="#FFF"
          autoCapitalize="characters"
          autoComplete="off"
          autoCorrect={false}
          maxLength={56}
          value={this.state.address}
          style={{
            color: '#000',
            fontFamily: AppStyle.mainFont,
            fontSize: 18,
            flex: 1
          }}
          onChangeText={(text) => {
            if (text.length) {
              this.setState({
                showQRScanner: false,
                showFavorite: false,
                showResetBtn: true
              })
            } else {
              this.setState({
                showQRScanner: true,
                showFavorite: true,
                showResetBtn: false
              })
            }
            const address = text.replace(/[^a-zA-Z0-9]/g, '')
            this.setState({
              address
            })
          }}
        />
        {
          this.state.showQRScanner &&
          <View style={{
            padding: 10,
            paddingRight: 0,
            borderLeftWidth: 1,
            borderColor: AppStyle.grayColor
          }}
          >
            <TouchableOpacity
              onPress={this.openScanQRCode}
            >
              <Image
                source={images.iconScan}
              />
            </TouchableOpacity>
          </View>
        }
        {
          this.state.showFavorite && showFavorite &&
          <TouchableOpacity
            onPress={this._goToFavorite}
          >
            <Image
              style={{
                width: 30,
                height: 30,
                marginLeft: 10,
                resizeMode: 'contain'
              }}
              source={icons.iconFavorite}
            />
          </TouchableOpacity>
        }

        {/* Reset Button */}
        {
          this.state.showResetBtn &&
          <TouchableOpacity
            onPress={() => {
              this.setState({
                address: '',
                showQRScanner: true,
                showFavorite: true,
                showResetBtn: false
              })
            }}
          >
            <Image
              source={icons.iconResetBtn}
            />
          </TouchableOpacity>
        }
        {/* End Reset Button */}

      </View>
    )
  }

  render() {
    const { wallet } = this.addressInputStore
    const { code, balanceText } = wallet
    const address = this.state.address.trim()
    const checkAddress = this.validateAddress(address)
    const { amountError, maxTransfer } = this

    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback
          onPress={this.onTouchDismissKeyboard}
        >
          <ImageBackground
            source={images.backgroundSend}
            style={styles.viewContainer}
          >
            <ScrollView
              style={{ flex: 1 }}
              contentContainerStyle={{ flex: 1 }}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
            >
              <LinearGradient colors={['rgba(0,0,0,0.5)', 'rgba(0,0,0,0)']} style={styles.linearGradient} />
              <NavigationHeader
                style={{
                  marginTop,
                  paddingVertical: 15
                }}
                headerItem={{
                  title: `SEND ${code}`,
                  icon: null,
                  button: images.iconBackWhite
                }}
                titleStyle={{
                  flex: 1,
                  marginRight: 20,
                  textAlign: 'center',
                  fontFamily: AppStyle.mainFontBoldTH,
                  color: '#FFFFFF'
                }}
                action={this.onBack}
              />

              {/* Begin flex two */}
              <View style={{ flex: 1 }}>
                {this.renderInputAmount()}
                <View style={{ alignItems: 'center', marginVertical: 8 }}>
                  <Text style={{
                    color: '#FFFFFF',
                    fontFamily: AppStyle.mainFontBold,
                    fontSize: 14
                  }}
                  >
                    TO
                  </Text>
                </View>
                {this.renderInputAddress()}
                {
                  (address !== '' && !checkAddress) &&
                  <View style={{ alignItems: 'center', marginTop: 8 }}>
                    <Text style={{
                      color: '#FF0000',
                      fontFamily: AppStyle.mainFontTH,
                      fontSize: 16
                    }}
                    >
                      {`ข้อมูลไม่ถูกต้อง`}
                    </Text>
                  </View>
                }
                <View style={{ alignItems: 'center', marginVertical: 10 }}>
                  <Text style={{
                    color: AppStyle.mainColor,
                    fontFamily: AppStyle.mainFontTH,
                    fontSize: 12,
                    textAlign: 'center'
                  }}
                  >
                    {`คุณมี ${balanceText} ${code}`}
                  </Text>
                  {
                    maxTransfer && (
                      <Text style={{
                        color: '#EA4A4A',
                        fontFamily: AppStyle.mainFontBoldTH,
                        fontSize: 12,
                        textAlign: 'center'
                      }}
                      >
                        {maxTransfer ? `บัญชีท่านจำกัดการส่งไม่เกิน ${Helper.formatETH(maxTransfer, false, 2)} ${code}` : ''}
                      </Text>
                    )
                  }
                </View>
                <BottomButton
                  onPress={this.handleConfirm}
                  disable={(this.state.address == '' || amountError || !checkAddress || this.state.fetching)}
                  backgroundColor="#8E9DE9"
                  color="#fffce9"
                  text="ดำเนินการ"
                />
              </View>
            </ScrollView>
          </ImageBackground>
        </TouchableWithoutFeedback>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#131C4F'
  },
  viewContainer: {
    flex: 1,
    marginBottom: isIPX ? 30 : 0
  },
  linearGradient: {
    height: 150,
    position: 'absolute',
    width: '100%',
    top: 0
  },
  walletScreen: {
    backgroundColor: 'rgba(240,240,240,0.97)',
    borderRadius: 10,
    flexDirection: 'column',
    height: 120,
    marginHorizontal: 20,
    paddingVertical: 15,
    alignItems: 'center'
  },
  walletBalance: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: '100%'
  },
  textBalance: {
    color: '#000',
    fontFamily: AppStyle.mainFontBold
  },
  textBalanceDesc: {
    color: '#000',
    fontFamily: AppStyle.mainFontBold,
    fontSize: 14
  },
  addressInput: {
    alignItems: 'center',
    backgroundColor: 'rgba(240,240,240,0.97)',
    borderRadius: 10,
    flexDirection: 'row',
    marginHorizontal: 20,
    paddingVertical: 10,
    paddingHorizontal: 15
  }
})

export default AdressInputScreen
