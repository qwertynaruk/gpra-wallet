import React, { Component } from 'react'
import {
  Alert,
  Text,
  View,
  CameraRoll,
  BackHandler,
  StyleSheet,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  Platform,
  Image,
  StatusBar
} from 'react-native'
import { observer } from 'mobx-react'
import PropTypes from 'prop-types'
import QRCode from 'react-native-qrcode-svg'
import ViewShot from 'react-native-view-shot'
import RNFS from 'react-native-fs'
import Share from 'react-native-share'
import Permissions from 'react-native-permissions'
import moment from 'moment'

import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import AppStyle from '../../../commons/AppStyle'
import Helper from '../../../commons/Helper'
import icons from '../../../commons/icons'
import images from '../../../commons/images'
import url from '../../../api/url'

moment.locale('th')

@observer
export default class TransactionPassScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object
  }

  static defaultProps = {
    navigation: {}
  }

  constructor(props) {
    super(props)

    this.state = {
      openShare: false
    }

    this.onShare = this.onShare.bind(this)
    this.onClose = this.onClose.bind(this)
    this.onAutoSave = this.onAutoSave.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
    this.saveImage = this.saveImage.bind(this)

    this.answers = MainStore.appState.fabric.answers
    this.answers.logContentView('Receipt', 'screen', 'receipt-screen')
  }

  componentWillMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  onClose() {
    NavStore.pushToScreen('DashboardStack')
  }

  onShare() {
    if (!this.state.openShare) {
      this.viewShot.capture().then((uri) => {
        const filePath = Platform.OS === 'ios' ? uri : uri.replace('file://', '')
        this.openShare(filePath)
      })
    }
  }

  onAutoSave() {
    NavStore.preventOpenUnlockScreen = true
    Permissions.check('photo').then((response) => {
      if (response == 'authorized') {
        this.saveImage()
      } else if (response == 'undetermined') {
        this.requestPhotoPermission()
      } else if (response == 'denied') {
        this._alertForPhotosPermissionDenied()
      } else if (response == 'restricted') {
        this._alertForPhotosPermissionDenied()
      }
    })
  }

  saveImage() {
    this.viewShot.capture().then((uri) => {
      const filePath = Platform.OS === 'ios' ? uri : uri.replace('file://', '')
      CameraRoll.saveToCameraRoll(filePath)
        .then(() => {
          NavStore.showToastTop(
            `บันทึกภาพสำเร็จ`,
            { backgroundColor: AppStyle.backgroundToastColor }, { color: AppStyle.mainColor, fontFamily: AppStyle.mainFontBoldTH }
          )
        })
        .catch(err => console.log('err:', err))
    })
  }

  openShare = (filePath) => {
    NavStore.preventOpenUnlockScreen = true
    RNFS.readFile(filePath, 'base64').then((file) => {
      const { params } = this.props.navigation.state
      const { hash } = params
      const explorer = url.Aomtoken.explorer(`tx/${hash}`)
      const shareOptions = {
        title: 'Transaction Hash',
        message: `Transaction Hash: ${explorer}`,
        url: `data:image/png;base64,${file}`
      }
      Share.open(shareOptions).catch(() => { })
      this.setState({
        openShare: false
      })
      this.answers.logShare('Transaction', 'Receipt', 'receipt', 'share-receipt', { hash })
    })
  }

  requestPhotoPermission = () => {
    NavStore.preventOpenUnlockScreen = true
    Permissions.check('photo').then((response) => {
      if (response != 'authorized') {
        Permissions.request('photo').then((res) => {
          if (res == 'authorized') {
            this.saveImage()
          }
        })
      }
    })
  }

  _alertForPhotosPermissionDenied() {
    if (Platform.OS === 'ios') {
      Alert.alert(
        '"AOM" needs permission to access your photos to save photo',
        'Please go to Setting > AOM > Photos > Read and Write',
        [
          {
            text: `Don't allow`,
            style: 'cancel'
          },
          {
            text: 'Open Settings',
            onPress: Permissions.openSettings
          }
        ]
      )
    } else {
      this._alertForPhotosPermission()
    }
  }

  _alertForPhotosPermission() {
    Alert.alert(
      'Can we access your photos?',
      'We need access so you can upload your photo',
      [
        {
          text: `Don't allow`,
          style: 'cancel'
        },
        {
          text: 'Allow',
          onPress: this.requestPhotoPermission
        }
      ]
    )
  }

  handleBackPress = () => {
    NavStore.pushToScreen('DashboardStack')
    return true
  }

  render() {
    const { params } = this.props.navigation.state
    const {
      name, from, to, amount, hash, receiver, avatar, asset, fee, memo
    } = params
    const { userInfo } = MainStore.appState

    const date = moment()
    const day = date.format('DD')
    const month = date.format('MMM')
    const year = date.format('YYYY')
    const time = date.format('HH:mm')
    const BDYear = `${(Number(year) + 543)}`.substr(2)
    const toStr = to.replace(/^([A-Z0-9]{2})(.+)([A-Z0-9]{5})$/, '$1...$3')
    const fromStr = from.replace(/^([A-Z0-9]{2})(.+)([A-Z0-9]{5})$/, '$1...$3')
    const explorer = url.Aomtoken.explorer(`tx/${hash}`)

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="light-content"
          translucent
        />

        <ImageBackground
          source={images.backgroundSend}
          style={styles.container}
        >
          {/* Start Header */}
          <ScrollView
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
          >
            <View style={{ height: 50, flexDirection: 'row', marginTop: 20 }}>

              <View style={{ flex: 15 }} />

              <View style={{ flex: 70 }}>
                <Text style={styles.centerText}>
                  ทำรายการสำเร็จ
                </Text>
              </View>

              <View style={{ flex: 15 }}>
                <TouchableOpacity onPress={this.onClose}>
                  <Text
                    style={[styles.centerText, {
                      fontFamily: AppStyle.mainFontTH
                    }]}
                  >
                    ปิด
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            {/* End Header */}

            {/* Start Body */}
            <ViewShot
              ref={(ref) => { this.viewShot = ref }}
              style={{
                backgroundColor: 'rgba(255,255,255,0.9)',
                borderRadius: 5,
                marginHorizontal: 10,
                padding: 15
              }}
            >

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  borderBottomColor: '#CFD4DF',
                  borderBottomWidth: 2,
                  paddingBottom: 20,
                  marginBottom: 20
                }}
              >
                <View
                  style={{
                    flexDirection: 'column',
                    borderLeftColor: '#1B2646',
                    borderLeftWidth: 5,
                    paddingLeft: 10
                  }}
                >
                  <Text style={styles.headerText}>
                    ส่งออมสำเร็จ
                  </Text>
                  <Text style={styles.subText}>
                    {`${day} ${month} ${BDYear} ${time} น.`}
                  </Text>
                </View>
              </View>

              <View style={{ flexDirection: 'row' }}>
                <View style={{ flexDirection: 'column', marginRight: 15 }}>
                  <Image
                    style={styles.imageProfile}
                    source={userInfo.profile_image ? { uri: userInfo.profile_image } : icons.iconProfileOm}
                    resizeMode="cover"
                  />
                </View>

                <View style={{ flexDirection: 'column' }} >
                  <Text style={styles.firstTransaction}>
                    {name}
                  </Text>
                  <Text style={styles.addressText}>
                    {fromStr}
                  </Text>
                </View>
              </View>

              <View
                style={{
                  marginLeft: 15,
                  marginVertical: 10
                }}
              >
                <Image style={{ resizeMode: 'center' }} source={icons.iconArrow} />
              </View>

              <View style={{ flexDirection: 'row' }}>
                <View style={{ flexDirection: 'column', marginRight: 15 }}>
                  <Image
                    style={styles.imageProfile}
                    source={avatar ? { uri: avatar } : icons.iconProfileOm}
                    resizeMode="cover"
                    onLoadEnd={this.onAutoSave}
                  />
                </View>

                <View style={{ flexDirection: 'column' }} >
                  {
                    receiver && (
                      <Text style={styles.firstTransaction}>{receiver}</Text>
                    )
                  }
                  <Text style={styles.addressText}>
                    {toStr}
                  </Text>

                </View>
              </View>

              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  marginTop: 30
                }}
              >
                <View style={{ flex: 1, paddingRight: 15 }}>
                  <Text style={styles.bottomHeader}>TX Hash:</Text>
                  <Text style={styles.bottomText}>{hash} </Text>

                  <Text style={[styles.bottomHeader, { marginTop: 10 }]}>จำนวน: </Text>
                  <View style={{ flexDirection: 'row', alignItems: 'baseline' }}>
                    <Text style={[styles.bottomText, styles.textAmount]}>{Helper.formatETH(amount, true, 2)}</Text>
                    <Text style={[styles.bottomText, styles.textSubAmount]}>{asset}</Text>
                    <Text style={[styles.bottomText, styles.textSubAmount]}>(Fee {fee} OM)</Text>
                  </View>

                  {
                    memo !== '' && (
                      <View>
                        <Text style={[styles.bottomHeader, { marginTop: 10, fontSize: 12 }]}>บันทึกช่วยจำ: </Text>
                        <Text style={styles.bottomText}>{memo}</Text>
                      </View>
                    )
                  }
                </View>

                <View style={{ width: 130 }}>
                  <View
                    style={{
                      backgroundColor: '#FFF',
                      borderRadius: 5,
                      borderWidth: 1,
                      borderColor: '#DDD',
                      padding: 10
                    }}
                  >
                    <QRCode
                      value={explorer}
                      size={110}
                      bgColor="black"
                      fgColor="white"
                    />
                  </View>
                </View>
              </View>
            </ViewShot>

            <View
              style={{
                flex: 1,
                alignItems: 'center',
                marginBottom: 15
              }}
            >
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.saveQRContainer}
                onPress={this.onShare}
              >
                <View style={styles.saveQRView}>
                  <Text style={styles.saveQRText}>แชร์</Text>
                </View>
              </TouchableOpacity>
            </View>

          </ScrollView>
        </ImageBackground>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  headerText: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18,
    color: '#1B2646'
  },
  subText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    color: '#1B2646'
  },
  firstTransaction: {
    fontSize: 18,
    textAlign: 'left',
    color: '#1B2646',
    fontFamily: AppStyle.mainFontBoldTH
  },
  addressText: {
    fontSize: 18,
    textAlign: 'left',
    marginRight: 25,
    fontFamily: AppStyle.mainFontTH
  },
  centerText: {
    textAlign: 'center',
    fontSize: 18,
    color: '#FFF',
    justifyContent: 'center',
    margin: 10,
    fontFamily: AppStyle.mainFontBoldTH
  },
  bottomHeader: {
    color: '#828EAF',
    fontSize: 14,
    fontFamily: AppStyle.mainFontBoldTH
  },
  bottomText: {
    color: '#1B2646',
    fontSize: 12,
    fontFamily: AppStyle.mainFontTH,
    marginLeft: 4
  },
  saveQRView: {
    backgroundColor: '#657EFF',
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    height: 34,
    width: 144
  },
  saveQRText: {
    color: '#FFF',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 14
  },
  textAmount: {
    fontSize: 16,
    fontFamily: AppStyle.mainFontBoldTH
  },
  textSubAmount: {
    fontSize: 16,
    fontFamily: AppStyle.mainFontTH
  },
  imageProfile: {
    width: 50,
    height: 50,
    borderRadius: 25
  }
})
