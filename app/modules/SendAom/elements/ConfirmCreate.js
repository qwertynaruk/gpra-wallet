import React, { Component } from 'react'
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import MainStore from '../../../AppStores/MainStore'
import icons from '../../../commons/icons'
import AppStyle from '../../../commons/AppStyle'

@observer
export default class ConfirmCreate extends Component {
  static propTypes = {
    onCancel: PropTypes.func
  }

  static defaultProps = {
    onCancel: () => { }
  }

  constructor(props) {
    super(props)

    this.addressStore = MainStore.sendTransaction.addressInputStore
  }

  render() {
    const { wallet } = this.addressStore
    const { code } = wallet
    return (
      <View style={styles.popupContent}>
        <Text style={styles.title}>
          {`กระเป๋าปลายทางยังไม่เปิดใช้งาน`}
        </Text>
        <Image source={icons.iconWarning} style={styles.icon} />
        <Text style={styles.description}>กระเป๋าปลายทางที่ท่านกำลังส่ง {code} ยังไม่มีการเปิดใช้งานในระบบ</Text>
        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              activeOpacity={0.6}
              style={styles.button}
              onPress={this.props.onCancel}
            >
              <Text style={styles.buttonText}>รับทราบ</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  popupContent: {
    backgroundColor: '#FFF',
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 35
  },
  title: {
    color: '#535786',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20,
    marginBottom: 20
  },
  icon: {
    marginTop: 10,
    marginBottom: 20
  },
  description: {
    color: '#121212',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    textAlign: 'center',
    paddingHorizontal: 20,
    marginBottom: 25
  },
  buttonsContainer: {
    flexDirection: 'column',
    width: '100%'
  },
  buttonContainer: {
    flexDirection: 'row'
  },
  button: {
    height: 60,
    marginHorizontal: 20,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#535786',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20
  }
})
