import React, { Component } from 'react'
import { Image } from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import api from '../../../api'
import images from '../../../commons/images'

@observer
class Avatar extends Component {
  static propTypes = {
    style: PropTypes.object,
    address: PropTypes.string
  }

  static defaultProps = {
    style: {},
    address: ''
  }

  constructor(props) {
    super(props)

    this.state = {
      avatar: ''
    }
  }

  componentWillMount() {
    api.Aom.getAvatar(this.props.address).then((res) => {
      if (res.status === 200) {
        const { data } = res.data
        this.setState({
          avatar: `${data.image_url}?t=${new Date().getTime()}`
        })
      }
    }).catch((error) => {
      console.log(`Get user avatar error:`, error)
    })
  }

  render() {
    const { avatar } = this.state
    return (
      <Image
        style={this.props.style}
        source={avatar ? { uri: avatar } : images.iconProfile}
        size="cover"
      />
    )
  }
}

export default Avatar
