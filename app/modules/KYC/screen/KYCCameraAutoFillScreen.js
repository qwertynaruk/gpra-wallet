import React, { Component } from 'react'
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { RNCamera } from 'react-native-camera'
// Components & Modules
import images from '../../../commons/images'

import NavStore from '../../../AppStores/NavStore'

// eslint-disable-next-line import/first
import Image from 'react-native-scalable-image'

export default class KYCCameraAutoFillScreen extends Component {
    static propTypes = {
      navigation: PropTypes.object
    }

    static defaultProps = {
      navigation: {}
    }

    constructor(props) {
      super(props)
      this.goBack = this.goBack.bind(this)
      this.goNext = this.goNext.bind(this)
      this.takePicture = this.takePicture.bind(this)
    }

    goBack() {
      NavStore.goBack()
    }

    goNext() {
      NavStore.goBack()
    }

  takePicture = async() => {
    if (this.camera) {
      const options = { quality: 0.5, base64: true, fixOrientation: true }
      const data = await this.camera.takePictureAsync(options)
      this.props.navigation.state.params.onGoBack(data)
      this.goBack()
    }
  }

  render() {
    const type = this.props.navigation.state.params.onRefreshType()

    return (
      <View style={styles.container}>
        <RNCamera
          ref={(ref) => {
            this.camera = ref
          }}
          style={styles.preview}
          type={type !== 'selfie' ? RNCamera.Constants.Type.back : RNCamera.Constants.Type.front}
          flashMode={RNCamera.Constants.FlashMode.off}
          captureAudio={false}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel'
          }}
          // onGoogleVisionBarcodesDetected={({ barcodes }) => {
          //   console.log(barcodes)
          // }}
        >
          <View style={{
            position: 'absolute', top: deviceHeight / 11, left: deviceWidth / 9, zIndex: 1
          }}
          >
            <TouchableOpacity
              onPress={this.goBack}
            >
              <Image source={images.iconBackWhite} />

            </TouchableOpacity>
          </View>
          { type !== 'selfie' &&
          <View style={styles.rectangle}>

            <View style={styles.rectangleColor} />
            <View style={styles.topLeft} />
            <View style={styles.topRight} />
            <View style={styles.bottomLeft} />
            <View style={styles.bottomRight} />
          </View>
          }
          <View style={{
            position: 'absolute', bottom: 20, flexDirection: 'row', justifyContent: 'center', zIndex: 1
          }}
          >
            <TouchableOpacity onPress={this.takePicture} style={styles.capture}>
              <Text style={{ fontSize: 14 }}> SNAP </Text>
            </TouchableOpacity>
          </View>
        </RNCamera>
      </View >
    )
  }
}

const deviceHeight = Dimensions.get('window').height

const deviceWidth = Dimensions.get('window').width

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black'
  },
  preview: {
    flex: 1,
    // justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20
  },
  rectangle: {
    borderLeftColor: 'rgba(0,0,0,0.6)',
    borderRightColor: 'rgba(0,0,0,0.6)',
    borderTopColor: 'rgba(0,0,0,0.6)',
    borderBottomColor: 'rgba(0,0,0,0.6)',
    borderLeftWidth: deviceWidth / 5,
    borderRightWidth: deviceWidth / 5,
    borderTopWidth: deviceHeight / 4.8,
    borderBottomWidth: deviceHeight / 4.8,
    height: deviceHeight
  },
  rectangleColor: {
    // marginTop: 50,
    height: 480,
    width: 300,
    backgroundColor: 'transparent'
  },
  topLeft: {
    width: 50,
    height: 50,
    borderTopWidth: 2,
    borderLeftWidth: 2,
    position: 'absolute',
    left: -1,
    top: -1,
    borderLeftColor: 'white',
    borderTopColor: 'white'
  },
  topRight: {
    width: 50,
    height: 50,
    borderTopWidth: 2,
    borderRightWidth: 2,
    position: 'absolute',
    right: -1,
    top: -1,
    borderRightColor: 'white',
    borderTopColor: 'white'
  },
  bottomLeft: {
    width: 50,
    height: 50,
    borderBottomWidth: 2,
    borderLeftWidth: 2,
    position: 'absolute',
    left: -1,
    bottom: -1,
    borderLeftColor: 'white',
    borderBottomColor: 'white'
  },
  bottomRight: {
    width: 50,
    height: 50,
    borderBottomWidth: 2,
    borderRightWidth: 2,
    position: 'absolute',
    right: -1,
    bottom: -1,
    borderRightColor: 'white',
    borderBottomColor: 'white'
  }
})
