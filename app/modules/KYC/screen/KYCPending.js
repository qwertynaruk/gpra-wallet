import React, { Component } from 'react'
import {
  ScrollView,
  BackHandler,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

// Components & Modules
import icons from '../../../commons/icons'
import AppStyle from '../../../commons/AppStyle'
import NavStore from '../../../AppStores/NavStore';

export default class KYCPending extends Component {
  constructor(props) {
    super(props)

    this.handleBackPress = this.handleBackPress.bind(this)
    this.goBack = this.goBack.bind(this)
  }

  componentWillMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  goBack() {
    NavStore.pushToScreen('DashboardStack')
  }

  render() {
    return (
      <View style={styles.container}>
        <LinearGradient colors={['#A8BBE1', '#3A3977']} style={styles.linearGradient} />
        <ScrollView
          style={{ flex: 1 }}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        >
          <Image style={styles.icon} source={icons.iconKycPending} />
          <View style={styles.column}>
            <Text style={styles.textTitle}>ระบบกำลังทำการยืนยันข้อมูลของคุณ</Text>
            <Text style={styles.textDesc}>{`คุณสามารถใช้กระเป๋า Om Token\nได้อย่างสมบูรณ์หลังจากได้รับการยืนยัน\nข้อมูลจากระบบ`}</Text>
          </View>
          <View style={styles.row}>
            <TouchableOpacity
              style={styles.button}
              onPress={this.goBack}
            >
              <Text style={styles.textButton}>กลับสู่หน้าหลัก</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  column: {
    flexDirection: 'column'
  },
  row: {
    flexDirection: 'row'
  },
  icon: {
    marginTop: 50
  },
  linearGradient: {
    height: '100%',
    position: 'absolute',
    width: '100%',
    top: 0,
    left: 0
  },
  textTitle: {
    color: '#FFF',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20,
    textAlign: 'center'
  },
  textDesc: {
    color: '#FFF',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    textAlign: 'center'
  },
  button: {
    flex: 1,
    backgroundColor: '#FFF',
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    marginHorizontal: 25,
    marginTop: 25
  },
  textButton: {
    color: '#535786',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20
  }
})
