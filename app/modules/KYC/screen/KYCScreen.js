import React, { Component } from 'react'
import {
  ActionSheetIOS,
  BackHandler,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  Picker,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View, ImageBackground, Image
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { computed } from 'mobx'
import { observer } from 'mobx-react'
import { KeyboardAccessoryNavigation } from 'react-native-keyboard-accessory'
import moment from 'moment'
import 'moment/locale/th'
import CircleCheckBox from 'react-native-circle-checkbox'
import DateTimePicker from 'react-native-modal-datetime-picker'
import RadioForm from 'react-native-simple-radio-button'
import ImageResizer from 'react-native-image-resizer'
import RNFS from 'react-native-fs'

// Components & Modules
import api from '../../../api'
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import Loading from '../element/Loading'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'
import KYCStore from '../KYCStore'

const marginTop = LayoutUtils.getExtraTop()
const title = 'ยืนยันตัวตนเบื้องต้น'

const genderOption = [
  { label: 'ชาย', value: 1 },
  { label: 'หญิง', value: 2 }
]

const maritalStatusOptions = [
  'โสด',
  'สมรส',
  'หย่า',
  'หม้าย'
]

const nationalityOptions = [
  'Thai',
  'Chinese',
  'American'
]

@observer
class KYCScreen extends Component {
    static propTypes = {
      navigation: PropTypes.object
    }

    static defaultProps = {
      navigation: {}
    }

    constructor(props) {
      super(props)
      this.state = {
        dateOfBirth: '',
        complete: false,
        checked: false,
        uploading: false,
        fnameEngFocused: false,
        lnameEngFocused: false,
        fnameFocused: false,
        lnameFocused: false,
        birthDateFocused: false,
        idcardFocused: false,
        telFocused: false,
        emailFocused: false,
        isDateTimePickerVisible: false,
        first_name_eng: '',
        last_name_eng: '',
        first_name: '',
        last_name: '',
        email: '',
        idCard: '',
        tel: '',
        gender: 'Male',
        nationality: 'Thai',
        maritalStatus: 'เลือก...',
        keyboardAvoidingViewKey: 'keyboardAvoidingViewKey',
        invalidFNameEng: false,
        invalidLNameEng: false,
        invalidFName: false,
        invalidLName: false,
        invalidDOB: false,
        invalidIDCard: false,
        invalidTel: false,
        invalidEmail: false,
        invalidSelfie: false,
        invalidNumberIDCardBack: false,
        isEmailExist: false,
        idCardFront: null,
        idCardBack: null,
        selfie: null,
        numberIDCardBack: '',
        numberIDCardBackFocused: '',
        type: ''

      }

      this._handleDatePicked = this._handleDatePicked.bind(this)
      this._handleSubmit = this._handleSubmit.bind(this)
      this._hideDateTimePicker = this._hideDateTimePicker.bind(this)
      this._showDateTimePicker = this._showDateTimePicker.bind(this)
      this.goBack = this.goBack.bind(this)
      this.goToLayerTwo = this.goToLayerTwo.bind(this)
      this.onSelectMaritalStatus = this.onSelectMaritalStatus.bind(this)
      this.onSelectNationality = this.onSelectNationality.bind(this)
      this.handleBackPress = this.handleBackPress.bind(this)
      this.handleOnFocus = this.handleOnFocus.bind(this)
      this.loadProfile = this.loadProfile.bind(this)
      this.handleOnSubmitEditing = this.handleOnSubmitEditing.bind(this)
      this.handleOnBlur = this.handleOnBlur.bind(this)
      this.handleOnFocus = this.handleOnFocus.bind(this)
      this.goToVerifyEmail = this.goToVerifyEmail.bind(this)
      this.openCamera = this.openCamera.bind(this)
      this.keyboardHideListener = this.keyboardHideListener.bind(this)

      MainStore.kycStore = new KYCStore()
      this.kycStore = MainStore.kycStore
    }

    componentWillMount() {
      if (Platform.OS === 'android') {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
      }
    }

    componentDidMount() {
      const { kycLayerOne, isSendVerify } = MainStore.appState

      if (!kycLayerOne && isSendVerify) {
        this.loadProfile()
      }

      if (Platform.OS === 'android') {
        this.keyboardHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardHideListener)
      } else {
        this.keyboardHideListener = Keyboard.addListener('keyboardWillHide', this.keyboardHideListener)
      }
    }

    componentWillUnmount() {
      if (Platform.OS === 'android') {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
      }

      this.keyboardHideListener.remove()
    }

    onSelectMaritalStatus() {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          options: maritalStatusOptions
        },
        buttonIndex => this.setState({ maritalStatus: maritalStatusOptions[buttonIndex] })
      )
    }

    onSelectNationality() {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          options: nationalityOptions
        },
        buttonIndex => this.setState({ nationality: nationalityOptions[buttonIndex] })
      )
    }

    @computed get dateOfBirth() {
      return this.state.dateOfBirth ? moment(this.state.dateOfBirth).format('L') : 'DD/MM/YYYY'
    }

    get disabledSubmit() {
      if (this.state.first_name_eng &&
            this.state.last_name_eng &&
            this.state.first_name &&
            this.state.last_name &&
            this.state.dateOfBirth &&
            this.state.idCard &&
            this.state.tel &&
            this.state.email &&
            this.state.gender &&
            this.state.nationality &&
            this.state.maritalStatus &&
            this.state.checked &&
            !this.state.invalidFNameEng &&
            !this.state.invalidLNameEng &&
            !this.state.invalidFName &&
            !this.state.invalidLName &&
            !this.state.invalidIDCard &&
            !this.state.invalidTel &&
            !this.state.invalidEmail &&
            !this.state.invalidDOB &&
            !this.state.invalidNumberIDCardBack &&
            !this.state.isEmailExist &&
            !this.state.invalidSelfie &&
            this.state.idCardFront &&
            this.state.idCardBack &&
            this.state.numberIDCardBack &&
            this.state.numberIDCardBack.length === 12 &&
            this.state.selfie
      ) {
        return false
      }

      return true
    }

    get editing() {
      const { kycLayerOne, isSendVerify } = MainStore.appState
      return !kycLayerOne && isSendVerify
    }

    setImageFile(src) {
      // const { path, uri } = src
      // const filePath = Platform.OS === 'ios' ? uri : path

      // iOS and Android use uri
      const { uri } = src
      const filePath = uri
      // Resize image
      ImageResizer.createResizedImage(filePath, 900, 900, 'JPEG', 80, 0)
        .then(res => RNFS.readFile(res.uri, 'base64'))
        .then((base64) => {
          if (this.state.type === 'id_card_front') {
            this.setState({
              idCardFront: {
                name: 'id_card_front.jpg',
                path: filePath,
                data: base64
              }
            })
          } else if (this.state.type == 'id_card_back') {
            this.setState({
              idCardBack: {
                name: 'id_card_back.jpg',
                path: filePath,
                data: base64
              }
            })
          } else if (this.state.type == 'selfie') {
            this.setState({
              selfie: {
                name: 'selfie.jpg',
                path: filePath,
                data: base64
              }
            })
          }
        })
        .catch((err) => {
          // Get wallet
          const wallet = MainStore.appState.selectedWallet
          api.Aom.reportBug({
            address: wallet.address,
            error_component: 'KYCScreen',
            error_function: 'setImageFile',
            error_detail: err.message
          })
        })
    }

    async loadProfile() {
      const wallet = MainStore.appState.selectedWallet
      if (!wallet) {
        return
      }

      NavStore.showLoading()
      const profile = await this.kycStore.getKycInfo(wallet.address)

      this._import(profile)
      NavStore.hideLoading()
    }

    _import(data) {
      let status
      const kycData = data.kyc_data

      if (kycData.status === 'Single') {
        status = 'โสด'
      } else if (kycData.status === 'Married') {
        status = 'สมรส'
      } else if (kycData.status === 'Divorce') {
        status = 'หย่า'
      } else {
        status = 'หม้าย'
      }

      const state = {
        dateOfBirth: moment(kycData.birth_date, ['YYYY-MM-DD', 'DD/MM/YYYY', 'DD/MMM/YYYY', 'YYYY-MM-DD HH:mm:ss']).toDate(),
        email: kycData.email,
        idCard: kycData.id_card,
        first_name_eng: String(kycData.name_en).trim(),
        last_name_eng: String(kycData.lname_en).trim(),
        first_name: String(kycData.name_th).trim(),
        last_name: String(kycData.lname_th).trim(),
        gender: kycData.gender,
        maritalStatus: status,
        nationality: kycData.nation,
        tel: kycData.tel,
        checked: true
      }

      this.setState(state)
    }

    handleBackPress() {
      if (!this.state.uploading && !this.state.complete) {
        this.goBack()
      }
      return true
    }

    keyboardHideListener() {
      this.setState({
        keyboardAvoidingViewKey: `keyboardAvoidingViewKey${new Date().getTime()}`
      })
    }

    _showDateTimePicker() {
      this.setState({
        isDateTimePickerVisible: true,
        birthDateFocused: true
      })
    }

    _hideDateTimePicker() {
      this.setState({
        isDateTimePickerVisible: false,
        birthDateFocused: false
      })

      // Android platform
      if (Platform.OS === 'android') {
        this.idcardInput.focus()
      }
    }

    _handleDatePicked(date) {
      const age = moment().diff(date, 'years')

      if (age < 20) {
        this.setState({
          invalidDOB: true,
          dateOfBirth: date
        })
      } else {
        this.setState({
          invalidDOB: false,
          dateOfBirth: date
        })
      }

      this._hideDateTimePicker()
    }

    goToLayerTwo() {
      NavStore.pushToScreen('KYCLayerTwoScreen')
    }

    goToVerifyEmail() {
      NavStore.pushToScreen('CompleteVerifyEmail')
    }

    goBack() {
      NavStore.goBack()
    }

    _handleSubmit() {
      // Show loading
      this.setState({
        complete: false,
        uploading: true
      })

      let status = ''
      let gender = ''
      const wallet = MainStore.appState.selectedWallet

      if (this.state.maritalStatus === 'โสด' || this.state.maritalStatus === 'Single') {
        status = 'Single'
      } else if (this.state.maritalStatus === 'สมรส' || this.state.maritalStatus === 'Married') {
        status = 'Married'
      } else if (this.state.maritalStatus === 'หย่า' || this.state.maritalStatus === 'Divorce') {
        status = 'Divorce'
      } else {
        status = 'Widow'
      }

      if (this.state.gender === 'Male' || this.state.gender === 1) {
        gender = 'Male'
      } else if (this.state.gender === 'Female' || this.state.gender === 2) {
        gender = 'Female'
      }

      api.kycLayerOne(
        this.state.first_name,
        this.state.last_name,
        this.state.first_name_eng,
        this.state.last_name_eng,
        this.state.idCard,
        `${moment(this.state.dateOfBirth).format('YYYY-MM-DD')} 00:00:00`,
        this.state.numberIDCardBack,
        this.state.idCardFront.data,
        this.state.idCardBack.data,
        this.state.selfie.data,
        status,
        gender,
        this.state.tel,
        this.state.nationality,
        this.state.email,
        wallet.address
      ).then((res) => {
        console.log('res')
        console.log(res)
        if (!res.data.success) {
          this.setState({ isEmailExist: true })
        } else {
          MainStore.appState.setIsSendVerify(true)
          NavStore.pushToScreen('VerificationEmailScreen')
        }

        this.setState({
          complete: true,
          uploading: false
        })
      }).catch((error) => {
        api.Aom.reportBug({
          address: wallet.address,
          error_component: 'KYCScreen',
          error_function: '_handleSubmit',
          error_detail: error.message
        })
      })
    }

    _validationName(name, type) {
      // eslint-disable-next-line no-useless-escape
      const regNumber = /[-]{0,1}[\d]*[\.]{0,1}[\d]+/g
      // eslint-disable-next-line no-useless-escape
      const regSpecialChr = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/
      const valid = name ? !(regSpecialChr.test(name.replace(/ /g, '')) || regNumber.test(String(name.replace(/ /g, '')).toLowerCase())) : false

      if (type === 'fname') {
        this.setState({
          first_name: name,
          invalidFName: !valid
        })
      } else if (type === 'lname') {
        this.setState({
          last_name: name,
          invalidLName: !valid
        })
      } else if (type === 'fname_eng') {
        this.setState({
          first_name_eng: name,
          invalidFNameEng: !valid
        })
      } else if (type === 'lname_eng') {
        this.setState({
          last_name_eng: name,
          invalidLNameEng: !valid
        })
      }
    }

    _validateFrontIdCard(idCard) {
      const valid = this._isValidFrontIdCard(idCard)

      this.setState({
        idCard,
        invalidIDCard: !valid
      })
    }

    _validateBackIdCard(numberIDCardBack) {
      const valid = this._isValidBackIdCard(numberIDCardBack)

      this.setState({
        numberIDCardBack,
        invalidNumberIDCardBack: !valid
      })
    }

    _isValidBackIdCard(numberIDCardBack) {
      const reg = /^[A-Z][A-Z][0-9]{10}$/
      const valid = reg.test(numberIDCardBack)

      if (valid && numberIDCardBack.trim().length === 12) {
        return true
      }
      return false
    }

    _isValidFrontIdCard(idCard) {
      if (idCard.match(/[0-9]{13}/)) {
        let sum = 0
        const nums = idCard.split('')
        const checksum = Number(nums.pop())
        nums.forEach((num, index) => {
          sum += Math.abs(Number(num) * (13 - index))
        })
        const mod = 11 - (sum % 11)
        if ((mod > 9 && ((mod - 10) === checksum)) || (mod === checksum)) {
          return true
        }
      }

      return false
    }

    _validationTelephone(tel) {
      const reg = /^0[123456789][0-9]{8}$/
      const valid = reg.test(tel)

      this.setState({
        tel,
        invalidTel: !valid
      })

      return valid
    }

    _validationEmail(email) {
    // eslint-disable-next-line no-useless-escape
      const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      const valid = reg.test(String(email).toLowerCase())

      this.setState({
        email,
        invalidEmail: !valid
      })
    }

    borderColor(focus, error) {
      if (error) {
        return {
          borderColor: '#C60003'
        }
      } else if (focus) {
        return {
          borderColor: '#4B5391'
        }
      }
      return {}
    }

    handleOnSubmitEditing = name => () => {
      if (name === 'fname') {
        this.setState({ fnameFocused: false })
        this.lnameInput.focus()
      } else if (name === 'lname') {
        this.setState({ lnameFocused: false })
        // Android platform
        if (Platform.OS === 'android') {
          this._showDateTimePicker()
        }
      } else if (name === 'fname_eng') {
        this.setState({ fnameEngFocused: false })
        this.lnameInputEng.focus()
      } else if (name === 'lname_eng') {
        this.setState({ lnameEngFocused: false })
        this.fnameInput.focus()
      } else if (name === 'idcard') {
        this.setState({ idcardFocused: false })
        this.telInput.focus()
      } else if (name === 'tel') {
        this.setState({ telFocused: false })
        this.emailInput.focus()
      } else if (name === 'numberIDCardBack') {
        this.idcardInput.focus()
      }
    }

    handleOnBlur = name => () => {
      if (name === 'fname') {
        this.setState({ fnameFocused: false })
      } else if (name === 'lname') {
        this.setState({ lnameFocused: false })
      } else if (name === 'fname_eng') {
        this.setState({ fnameEngFocused: false })
      } else if (name === 'lname_eng') {
        this.setState({ lnameEngFocused: false })
      } else if (name === 'idcard') {
        this.setState({ idcardFocused: false })
      } else if (name === 'tel') {
        this.setState({ telFocused: false })
      } else if (name === 'email') {
        this.setState({ emailFocused: false })
      } else if (name === 'numberIDCardBack') {
        this.setState({ numberIDCardBackFocused: false })
      }
    }

    handleOnFocus = name => () => {
      if (name === 'fname') {
        this.setState({ fnameFocused: true })
      } else if (name === 'lname') {
        this.setState({ lnameFocused: true })
      } else if (name === 'fname_eng') {
        this.setState({ fnameEngFocused: true })
      } else if (name === 'lname_eng') {
        this.setState({ lnameEngFocused: true })
      } else if (name === 'idcard') {
        this.setState({ idcardFocused: true })
      } else if (name === 'tel') {
        this.setState({ telFocused: true })
      } else if (name === 'email') {
        this.setState({
          emailFocused: true,
          isEmailExist: false
        })
      } else if (name === 'numberIDCardBack') {
        this.setState({ numberIDCardBackFocused: true })
      }
    }

    imageProcessingFront = (cameraData) => {
      const data = {
        dateOfBirth: '',
        idCard: '',
        first_name_eng: '',
        last_name_eng: '',
        first_name: '',
        last_name: '',
        gender: 'Male'
      }

      try {
        for (let i = 0; i < cameraData.length; i++) {
          if (cameraData[i].description === 'นาย' || cameraData[i].description === 'นาง' || cameraData[i].description === 'น.ส.') {
            if (cameraData[i].description === 'นาย') {
              data.gender = 'Male'
            } else {
              data.gender = 'Female'
            }

            console.log(cameraData[i + 1].description)
            // First Name TH
            if (isNaN(cameraData[i + 1].description)) {
              data.first_name = cameraData[i + 1].description
            }
            // Last Name TH
            console.log(cameraData[i + 2].description)
            if (isNaN(cameraData[i + 2].description)) {
              data.last_name = cameraData[i + 2].description
            }
          }

          if (cameraData[i].description === 'Mr.' || cameraData[i].description === 'Miss' || cameraData[i].description === 'Mrs.') {
            if (cameraData[i].description === 'Mr.') {
              data.gender = 'Male'
            } else {
              data.gender = 'Female'
            }
            // First Name EN
            console.log(cameraData[i + 1].description)
            if (isNaN(cameraData[i + 1].description)) {
              data.first_name_eng = cameraData[i + 1].description
            }
          }

          if (cameraData[i].description === 'Lastname' || cameraData[i].description === 'name') {
            // Last Name EN
            console.log(cameraData[i + 1].description)
            if (isNaN(cameraData[i + 1].description) && cameraData[i + 1].description !== 'Date') {
              data.last_name_eng = cameraData[i + 1].description
            }
          }
          // ID
          if (cameraData[i].description === 'เลขประจำตัวประชาชน') {
            if (!isNaN(cameraData[i + 1].description)) {
              console.log(cameraData[i + 1].description)

              data.idCard = cameraData[i + 1].description

              console.log(cameraData[i + 2].description)
              if (!isNaN(cameraData[i + 2].description)) {
                data.idCard += cameraData[i + 2].description
              }
              console.log(cameraData[i + 3].description)
              if (!isNaN(cameraData[i + 3].description)) {
                data.idCard += cameraData[i + 3].description
              }

              console.log(cameraData[i + 4].description)
              if (!isNaN(cameraData[i + 4].description)) {
                data.idCard += cameraData[i + 4].description
              }
              console.log(cameraData[i + 5].description)
              if (!isNaN(cameraData[i + 5].description)) {
                data.idCard += cameraData[i + 5].description
              }
            }
          }

          // ID
          if (cameraData[i].description === 'Number') {
            if (!isNaN(cameraData[i + 1].description)) {
              console.log(cameraData[i + 1].description)

              data.idCard = cameraData[i + 1].description

              console.log(cameraData[i + 2].description)
              if (!isNaN(cameraData[i + 2].description)) {
                data.idCard += cameraData[i + 2].description
              }
              console.log(cameraData[i + 3].description)
              if (!isNaN(cameraData[i + 3].description)) {
                data.idCard += cameraData[i + 3].description
              }

              console.log(cameraData[i + 4].description)
              if (!isNaN(cameraData[i + 4].description)) {
                data.idCard += cameraData[i + 4].description
              }
              console.log(cameraData[i + 5].description)
              if (!isNaN(cameraData[i + 5].description)) {
                data.idCard += cameraData[i + 5].description
              }
            }
          }

          // DATE OF BIRTH
          if (cameraData[i].description === 'Birth' || cameraData[i].description === 'DateofBirth' || cameraData[i].description === 'ofBirth') {
            if (!isNaN(cameraData[i + 1].description) && !isNaN(cameraData[i + 3].description)) {
              console.log(cameraData[i + 1].description)
              // console.log(cameraData[i + 2].description)

              data.dateOfBirth = `${cameraData[i + 1].description}/`
              let months = ''
              switch (cameraData[i + 2].description) {
                case 'Jan':
                  months = '01'
                  break
                case 'Feb':
                  months = '02'
                  break
                case 'Mar':
                  months = '03'
                  break
                case 'Apr':
                  months = '04'
                  break
                case 'May':
                  months = '05'
                  break
                case 'Jun':
                  months = '06'
                  break
                case 'Jul':
                  months = '07'
                  break
                case 'Aug':
                  months = '08'
                  break
                case 'Sep':
                  months = '09'
                  break
                case 'Oct':
                  months = '10'
                  break
                case 'Nov':
                  months = '11'
                  break
                case 'Dec':
                  months = '12'
                  break
                case 'Jan.':
                  months = '01'
                  break
                case 'Feb.':
                  months = '02'
                  break
                case 'Mar.':
                  months = '03'
                  break
                case 'Apr.':
                  months = '04'
                  break
                case 'May.':
                  months = '05'
                  break
                case 'Jun.':
                  months = '06'
                  break
                case 'Jul.':
                  months = '07'
                  break
                case 'Aug.':
                  months = '08'
                  break
                case 'Sep.':
                  months = '09'
                  break
                case 'Oct.':
                  months = '10'
                  break
                case 'Nov.':
                  months = '11'
                  break
                case 'Dec.':
                  months = '12'
                  break
                default:
                  months = ''
              }

              console.log(months)
              data.dateOfBirth += `${months}/`
              data.dateOfBirth += `${cameraData[i + 3].description}`
            }
          }
        }
      } catch (e) {
        console.log(e)
      }

      return data
    }

    imageProcessingBack = (cameraData) => {
      const data = {
        numberIDCardBack: ''
      }

      try {
        data.numberIDCardBack = String(cameraData[cameraData.length - 1].description).replace(/-/g, '').trim()

        console.log('imageProcessingBack')
        console.log(data.numberIDCardBack)
      } catch (e) {
        console.log(e)
      }
      return data
    }

    async refresh(data) {
      const { type } = this.state
      NavStore.showLoading()
      const cameraData = await api.kycCameraAutoFill(type, data.base64)

      cameraData.data = JSON.parse(cameraData.data.body)
      console.log('cameraData')
      console.log(cameraData)

      const dataObject = {
        cameraData: data
      }

      if (cameraData.data.responses[0].faceAnnotations && type === 'selfie') {
        console.log('Human Face')
        this.setState({
          invalidSelfie: false
        })
        // dataObject.googleResponse = this.imageProcessing(cameraData.data.responses[0].faceAnnotations)
      } else if (type === 'selfie') {
        console.log('selfie')
        this.setState({
          invalidSelfie: true
        })
      } else if (type === 'id_card_front') {
        console.log(`It's not Human Face id_card_front`)
        dataObject.googleResponse = this.imageProcessingFront(cameraData.data.responses[0].textAnnotations)
      } else if (type === 'id_card_back') {
        console.log(`It's not Human Face id_card_back`)
        dataObject.googleResponse = this.imageProcessingBack(cameraData.data.responses[0].textAnnotations)
      }

      this.fetchData(dataObject)
      NavStore.hideLoading()
    }

    fetchData(data) {
      const {
        type, dateOfBirth
      } = this.state
      if (type === 'id_card_front') {
        const state = {
          dateOfBirth: moment(data.googleResponse.dateOfBirth, ['YYYY-MM-DD', 'DD/MM/YYYY', 'DD/MMM/YYYY', 'YYYY-MM-DD HH:mm:ss']).toDate(),
          idCard: data.googleResponse.idCard.trim(),
          first_name_eng: data.googleResponse.first_name_eng.trim(),
          last_name_eng: data.googleResponse.last_name_eng.trim(),
          first_name: data.googleResponse.first_name.trim(),
          last_name: data.googleResponse.last_name.trim(),
          gender: data.googleResponse.gender.trim(),
          invalidIDCard: !this._isValidFrontIdCard(data.googleResponse.idCard.trim())
        }

        this._validationName(data.googleResponse.first_name_eng.trim(), 'fname_eng')
        this._validationName(data.googleResponse.last_name_eng.trim(), 'lname_eng')
        this._validationName(data.googleResponse.first_name.trim(), 'fname')
        this._validationName(data.googleResponse.last_name.trim(), 'lname')
        this._handleDatePicked(dateOfBirth)

        this.setState(state)
      } else if (type === 'id_card_back') {
        console.log('fetchData')
        console.log(data)

        const state = {
          numberIDCardBack: data.googleResponse.numberIDCardBack.trim(),
          invalidNumberIDCardBack: !this._isValidBackIdCard(data.googleResponse.numberIDCardBack.trim())
        }
        this.setState(state)
      }

      this.setImageFile(data.cameraData)
    }

    openCamera() {
      NavStore.pushToScreen('KYCCameraAutoFillScreen', {
        onGoBack: (cameraData) => {
          this.refresh(cameraData)
        },
        onRefreshType: () => this.refreshType()
      })
    }

    refreshType() {
      const { type } = this.state
      console.log('refreshType')
      console.log(type)
      return type
    }

    renderGenderRadio(radioInitial) {
      const { complete } = this.state
      const { params } = this.props.navigation.state
      if ((!complete && radioInitial) || (params && params.editSeting)) {
        return (
          <RadioForm
            borderWidth={1}
            // buttonInnerColor={'#e74c3c'}
            buttonSize={15}
            buttonOuterSize={25}
            buttonStyle={{}}
            buttonWrapStyle={{ marginLeft: 10, marginRight: 10 }}
            radio_props={genderOption}
            formHorizontal={true}
            labelHorizontal={true}
            buttonColor="#CBCCD9"
            selectedButtonColor="#535786"
            initial={radioInitial - 1}
            labelStyle={{ paddingHorizontal: 10, fontFamily: AppStyle.mainFontTH }}
            onPress={value => this.setState({ gender: value })}
          />
        )
      }

      return null
    }

    render() {
      let btnStyle
      let btnTextStyle
      let radioInitial = 1
      const { dateOfBirth, disabledSubmit, editing } = this
      const {
        maritalStatus,
        nationality,
        gender,
        keyboardAvoidingViewKey
      } = this.state

      if (gender === 'Male' || gender === 1) {
        radioInitial = 1
      } else if (gender === 'Female' || gender === 2) {
        radioInitial = 2
      }

      if (disabledSubmit) {
        btnStyle = styles.disableNextBtn
        btnTextStyle = styles.disableNextText
      } else {
        btnStyle = styles.containerNextBtn
        btnTextStyle = styles.nextText
      }

      return (
        <View style={styles.container}>
          <StatusBar
            backgroundColor="transparent"
            barStyle="dark-content"
            translucent
          />
          <NavigationHeader
            style={{
              marginTop: marginTop + 20,
              marginBottom: 20
            }}
            headerItem={{
              title,
              icon: null,
              button: images.iconBackAlt
            }}
            titleStyle={{
              color: '#000',
              fontFamily: AppStyle.mainFontBoldTH,
              textAlign: 'center',
              flex: 1,
              marginRight: 20
            }}
            action={this.goBack}
          />

          {/* Begin scrollview container */}
          <ScrollView style={{ flex: 1, paddingHorizontal: 20 }}>
            <KeyboardAvoidingView
              behavior={Platform.OS === 'ios' ? 'padding' : null}
              style={{ flex: 1 }}
              enabled
              key={keyboardAvoidingViewKey}
            >
              <View>
                <Text style={styles.title}>ภาพด้านหน้าบัตรประชาชน</Text>
                <View style={{ flexDirection: 'row', marginTop: 6 }}>
                  <View style={styles.leftColumn}>
                    <TouchableOpacity
                      activeOpacity={0.5}
                      style={styles.uploadButton}
                      onPress={() => {
                        this.setState({
                          type: 'id_card_front'
                        })

                        setTimeout(() => {
                          this.openCamera()
                        })
                      }}
                    >
                      <ImageBackground
                        source={images.iconUploadBtn}
                        style={styles.imageUploadBg}
                        resizeMode="stretch"
                      >
                        {
                          this.state.idCardFront &&
                          <Image
                            resizeMode="cover"
                            style={{
                              width: '100%',
                              height: '100%',
                              borderWidth: 1,
                              borderColor: '#DDD'
                            }}
                            source={{ uri: `file://${this.state.idCardFront.path}` }}
                          />
                        }
                      </ImageBackground>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.rightColumn}>
                    <View style={{ flexDirection: 'column' }}>
                      <Text style={[styles.titleExample]}>ตัวอย่าง</Text>
                      <Image style={styles.icon} source={images.img_id_card_front} />
                    </View>
                  </View>
                </View>
              </View>

              <View style={{ marginTop: 30, borderBottomColor: '#F1F1F1', borderBottomWidth: 1 }} />

              <View style={{ marginTop: 26 }}>
                <Text style={styles.title}>ภาพด้านหลังบัตรประชาชน</Text>
                <View style={{ flexDirection: 'row', marginTop: 6 }}>
                  <View style={styles.leftColumn}>
                    <TouchableOpacity
                      activeOpacity={0.5}
                      style={styles.uploadButton}
                      onPress={() => {
                        this.setState({
                          type: 'id_card_back'
                        })
                        setTimeout(() => {
                          this.openCamera()
                        })
                      }}
                    >
                      <ImageBackground
                        source={images.iconUploadBtn}
                        style={styles.imageUploadBg}
                        resizeMode="stretch"
                      >
                        {
                          this.state.idCardBack &&
                          <Image
                            resizeMode="cover"
                            style={{
                              width: '100%',
                              height: '100%',
                              borderWidth: 1,
                              borderColor: '#DDD'
                            }}
                            source={{ uri: `file://${this.state.idCardBack.path}` }}
                          />
                        }
                      </ImageBackground>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.rightColumn}>
                    <View style={{ flexDirection: 'column' }}>
                      <Text style={[styles.titleExample]}>ตัวอย่าง</Text>
                      <Image style={styles.icon} source={images.img_id_card_back} />
                    </View>
                  </View>
                </View>

                <View style={{ marginTop: 16 }}>
                  <Text style={styles.title}>
                                    เลขเลเซอร์หลังบัตรประจำตัวประชาชน
                  </Text>
                  <TextInput
                    maxLength={12}
                    onFocus={this.handleOnFocus('numberIDCardBack')}
                    onBlur={this.handleOnBlur('numberIDCardBack')}
                    onSubmitEditing={this.handleOnSubmitEditing('numberIDCardBack')}
                    onChangeText={(numberIDCardBack) => {
                      this._validateBackIdCard(numberIDCardBack)
                    }}
                    ref={(ref) => {
                      this.idcardInput = ref
                    }}
                    returnKeyType="done"
                    style={[styles.textInput, this.borderColor(this.state.numberIDCardBackFocused, this.state.invalidNumberIDCardBack), {
                      marginTop: 6,
                      paddingHorizontal: 25
                    }]}
                    underlineColorAndroid="transparent"
                    value={this.state.numberIDCardBack}
                  />
                </View>
                {this.state.invalidNumberIDCardBack && (
                  <Text style={styles.errorDescribeText}>
                                    กรุณากรอก เลขเลเซอร์หลังบัตรประจำตัวประชาชน ให้ถูกต้อง
                  </Text>
                )}
              </View>

              <View style={{ marginTop: 30, borderBottomColor: '#F1F1F1', borderBottomWidth: 1 }} />

              <View style={{ marginTop: 26 }}>
                <Text style={styles.title}>เซลฟี่</Text>
                <View style={{ flexDirection: 'row', marginTop: 6 }}>
                  <View style={styles.leftColumn}>
                    <View style={{ flexDirection: 'column', marginTop: 10 }}>
                      <TouchableOpacity
                        activeOpacity={0.5}
                        style={styles.uploadButton}
                        onPress={() => {
                          this.setState({
                            type: 'selfie'
                          })
                          setTimeout(() => {
                            this.openCamera()
                          })
                        }}
                      >
                        <ImageBackground
                          source={images.iconUploadBtn}
                          style={styles.imageUploadBg}
                          resizeMode="stretch"
                        >
                          {
                            this.state.selfie &&
                            <Image
                              resizeMode="cover"
                              style={{
                                width: '100%',
                                height: '100%',
                                borderWidth: 1,
                                borderColor: '#DDD'
                              }}
                              source={{ uri: `file://${this.state.selfie.path}` }}
                            />
                          }
                        </ImageBackground>
                      </TouchableOpacity>
                      {this.state.invalidSelfie && (
                        <Text style={styles.errorDescribeText}>
                          {`เราไม่พบภาพใบหน้าในรูปของคุณ\nกรุณาถ่ายภาพใหม่อีกครั้ง`}
                        </Text>
                      )}
                    </View>
                  </View>
                  <View style={styles.rightColumn}>
                    <View style={{ flexDirection: 'column' }}>
                      <Text style={[styles.titleExample]}>ตัวอย่าง</Text>
                      <Image style={styles.icon} source={images.img_selfie} />
                    </View>
                  </View>
                </View>

                <View style={{ marginTop: 16 }}>
                  <Text style={styles.textDesc}>ถ่ายรูปหน้าของคุณที่ถือบัตรประชาชน</Text>
                  <Text style={styles.textDesc}>(หรือ พาสปอร์ต สำหรับชาวต่างชาติ)</Text>
                  <Text style={styles.textDesc}>พร้อมกระดาษที่เขียน Om Platform</Text>
                  <Text style={styles.textDesc}>และลายเซ็นกับวันที่</Text>
                  <Text style={styles.textDesc}>- ใบหน้าของคุณจะต้องไม่มีอะไรบดบัง</Text>
                  <Text style={styles.textDesc}>- รายละเอียดบัตรประชาชนหรือพาสปอร์ต</Text>
                  <Text style={styles.textDesc}>จะต้องชัดเจน</Text>
                </View>
              </View>

              <View style={{ marginTop: 30, borderBottomColor: '#F1F1F1', borderBottomWidth: 1 }} />

              {/* Begin full-name input */}
              <View style={{ marginTop: 26, flexDirection: 'row' }}>
                <View style={{ flex: 1, marginRight: 7.5 }}>
                  <Text style={styles.inputDescribeText}>
                                    ชื่อ (ภาษาอังกฤษ)
                  </Text>
                  <TextInput
                    ref={(ref) => {
                      this.fnameInputEng = ref
                    }}
                    autoCapitalize="none"
                    onFocus={this.handleOnFocus('fname_eng')}
                    onBlur={this.handleOnBlur('fname_eng')}
                    onSubmitEditing={this.handleOnSubmitEditing('fname_eng')}
                    onChangeText={(name) => {
                      this._validationName(name, 'fname_eng')
                    }}
                    returnKeyType={Platform.OS === 'android' ? 'done' : 'default'}
                    style={[styles.textInput, this.borderColor(this.state.fnameEngFocused, this.state.invalidFNameEng)]}
                    underlineColorAndroid="transparent"
                    value={this.state.first_name_eng}
                  />
                </View>

                <View style={{ flex: 1, marginLeft: 7.5 }}>
                  <Text style={styles.inputDescribeText}>
                                    นามสกุล (ภาษาอังกฤษ)
                  </Text>
                  <TextInput
                    ref={(ref) => {
                      this.lnameInputEng = ref
                    }}
                    autoCapitalize="none"
                    onFocus={this.handleOnFocus('lname_eng')}
                    onBlur={this.handleOnBlur('lname_eng')}
                    onSubmitEditing={this.handleOnSubmitEditing('lname_eng')}
                    onChangeText={(name) => {
                      this._validationName(name, 'lname_eng')
                    }}
                    returnKeyType={Platform.OS === 'android' ? 'done' : 'default'}
                    style={[styles.textInput, this.borderColor(this.state.lnameEngFocused, this.state.invalidLNameEng)]}
                    underlineColorAndroid="transparent"
                    value={this.state.last_name_eng}
                  />
                </View>
              </View>
              {/* End full-name input */}
              {(this.state.invalidFNameEng || this.state.invalidLNameEng) && (
                <Text style={styles.errorDescribeText}>
                                กรุณากรอก ชื่อ - นามสกุล ให้ถูกต้อง
                </Text>
              )}

              {/* Begin full-name input */}
              <View style={{ marginTop: 16, flexDirection: 'row' }}>
                <View style={{ flex: 1, marginRight: 7.5 }}>
                  <Text style={styles.inputDescribeText}>
                                    ชื่อ (ภาษาไทย)
                  </Text>
                  <TextInput
                    ref={(ref) => {
                      this.fnameInput = ref
                    }}
                    autoCapitalize="none"
                    onFocus={this.handleOnFocus('fname')}
                    onBlur={this.handleOnBlur('fname')}
                    onSubmitEditing={this.handleOnSubmitEditing('fname')}
                    onChangeText={(name) => {
                      this._validationName(name, 'fname')
                    }}
                    returnKeyType={Platform.OS === 'android' ? 'done' : 'default'}
                    style={[styles.textInput, this.borderColor(this.state.fnameFocused, this.state.invalidFName)]}
                    underlineColorAndroid="transparent"
                    value={this.state.first_name}
                  />
                </View>

                <View style={{ flex: 1, marginLeft: 7.5 }}>
                  <Text style={styles.inputDescribeText}>
                                    นามสกุล (ภาษาไทย)
                  </Text>
                  <TextInput
                    ref={(ref) => {
                      this.lnameInput = ref
                    }}
                    autoCapitalize="none"
                    onFocus={this.handleOnFocus('lname')}
                    onBlur={this.handleOnBlur('lname')}
                    onSubmitEditing={this.handleOnSubmitEditing('lname')}
                    onChangeText={(name) => {
                      this._validationName(name, 'lname')
                    }}
                    returnKeyType={Platform.OS === 'android' ? 'done' : 'default'}
                    style={[styles.textInput, this.borderColor(this.state.lnameFocused, this.state.invalidLName)]}
                    underlineColorAndroid="transparent"
                    value={this.state.last_name}
                  />
                </View>
              </View>
              {/* End full-name input */}
              {(this.state.invalidFName || this.state.invalidLName) && (
                <Text style={styles.errorDescribeText}>
                                กรุณากรอก ชื่อ - นามสกุล ให้ถูกต้อง
                </Text>
              )}

              {/* Begin gender input */}
              <View style={{ marginTop: 18 }}>
                <Text style={styles.inputDescribeText}>
                                เพศ
                </Text>
                {this.renderGenderRadio(radioInitial)}
              </View>
              {/* End gender input */}

              {/* Begin status input */}
              <View style={{ marginTop: 18 }}>
                <Text style={styles.inputDescribeText}>
                                สถานภาพ
                </Text>
                <View style={{
                  height: 58,
                  borderColor: '#CBCCD9',
                  borderWidth: 1,
                  borderRadius: 13,
                  justifyContent: 'center',
                  paddingHorizontal: 10
                }}
                >
                  {Platform.OS === 'android' &&
                  <Picker
                    selectedValue={this.state.maritalStatus}
                    style={{ flex: 1 }}
                    onValueChange={(itemValue, itemIndex) =>
                      this.setState({ maritalStatus: itemValue })
                    }
                  >
                    {maritalStatusOptions.map((item, index) => (<Picker.Item
                      key={index}
                      value={item}
                      label={item}
                    />))}
                  </Picker>
                  }
                  {
                    Platform.OS === 'ios' &&
                    <TouchableOpacity
                      style={{ flex: 1, display: 'flex', justifyContent: 'center' }}
                      onPress={this.onSelectMaritalStatus}
                    >
                      <Text>
                        {maritalStatus}
                      </Text>
                    </TouchableOpacity>
                  }
                </View>
              </View>
              {/* End status input */}

              {/* Begin birth date input */}
              <View style={{ marginTop: 16 }}>
                <Text style={styles.inputDescribeText}>
                                วัน/เดือน/ปี เกิด (ต้องมีอายุมากกว่า 20 ปี)
                </Text>
                <TouchableOpacity
                  activeOpacity={1}
                  style={(this.state.birthDateFocused)
                    ? {
                      height: 58,
                      borderColor: '#4B5391',
                      borderWidth: 1,
                      borderRadius: 13,
                      justifyContent: 'center',
                      paddingHorizontal: 10
                    }
                    : {
                      height: 58,
                      borderColor: '#CBCCD9',
                      borderWidth: 1,
                      borderRadius: 13,
                      justifyContent: 'center',
                      paddingHorizontal: 10
                    }
                  }
                  onPress={this._showDateTimePicker}
                >
                  <Text>{dateOfBirth}</Text>
                </TouchableOpacity>
                <DateTimePicker
                  mode="date"
                  isVisible={this.state.isDateTimePickerVisible}
                  onConfirm={this._handleDatePicked}
                  onCancel={this._hideDateTimePicker}
                  maximumDate={new Date()}
                />
                {this.state.invalidDOB && (
                  <Text style={styles.errorDescribeText}>
                                    ต้องอายุครบ 20 ปีบริบูรณ์ ขึ้นไป
                  </Text>
                )}
              </View>
              {/* End birth date input */}

              {/* Begin id card number input */}
              <View style={{ marginTop: 16 }}>
                <Text style={styles.inputDescribeText}>
                                เลขประจำตัวประชาชน
                </Text>
                <TextInput
                  keyboardType="numeric"
                  maxLength={13}
                  onFocus={this.handleOnFocus('idcard')}
                  onBlur={this.handleOnBlur('idcard')}
                  onSubmitEditing={this.handleOnSubmitEditing('idcard')}
                  onChangeText={(idCard) => {
                    this._validateFrontIdCard(idCard)
                  }}
                  placeholder="X-XXXX-XXXXX-XX-X"
                  placeholderTextColor="#CBCCD9"
                  ref={(ref) => {
                    this.idcardInput = ref
                  }}
                  returnKeyType={Platform.OS === 'android' ? 'done' : 'default'}
                  style={[styles.textInput, this.borderColor(this.state.idcardFocused, this.state.invalidIDCard)]}
                  underlineColorAndroid="transparent"
                  value={this.state.idCard}
                />
                {this.state.invalidIDCard && (
                  <Text style={styles.errorDescribeText}>
                                    กรุณากรอก เลขประจำตัวประชาชน ให้ถูกต้อง
                  </Text>
                )}
              </View>
              {/* End id card number input */}

              {/* Begin telephone input */}
              <View style={{ marginTop: 16 }}>
                <Text style={styles.inputDescribeText}>
                                เบอร์มือถือ
                </Text>
                <TextInput
                  keyboardType="phone-pad"
                  maxLength={10}
                  onFocus={this.handleOnFocus('tel')}
                  onBlur={this.handleOnBlur('tel')}
                  onSubmitEditing={this.handleOnSubmitEditing('tel')}
                  onChangeText={(tel) => {
                    this._validationTelephone(tel)
                  }}
                  placeholder="0812345678"
                  placeholderTextColor="#CBCCD9"
                  ref={(ref) => {
                    this.telInput = ref
                  }}
                  returnKeyType={Platform.OS === 'android' ? 'done' : 'default'}
                  style={[styles.textInput, this.borderColor(this.state.telFocused, this.state.invalidTel)]}
                  underlineColorAndroid="transparent"
                  value={this.state.tel}
                />
                {this.state.invalidTel && (
                  <Text style={styles.errorDescribeText}>
                                    กรุณากรอก เบอร์มือถือ ให้ถูกต้อง
                  </Text>
                )}
              </View>
              {/* End telephone input */}

              {/* Begin nationality input */}
              <View style={{ marginTop: 16 }}>
                <Text style={styles.inputDescribeText}>
                                สัญชาติ
                </Text>
                <View style={{
                  height: 58,
                  borderColor: '#CBCCD9',
                  borderWidth: 1,
                  borderRadius: 13,
                  justifyContent: 'center',
                  paddingHorizontal: 10
                }}
                >
                  {Platform.OS === 'android' &&
                  <Picker
                    selectedValue={this.state.nationality}
                    style={{ flex: 1 }}
                    onValueChange={(value, index) => this.setState({ nationality: value })}
                  >
                    {nationalityOptions.map((item, index) => (<Picker.Item
                      key={index}
                      value={item}
                      label={item}
                    />))}
                  </Picker>
                  }
                  {
                    Platform.OS === 'ios' &&
                    <TouchableOpacity
                      style={{ flex: 1, display: 'flex', justifyContent: 'center' }}
                      onPress={this.onSelectNationality}
                    >
                      <Text>
                        {nationality}
                      </Text>
                    </TouchableOpacity>
                  }
                </View>
              </View>
              {/* End nationality input */}

              {/* Begin email input */}
              <View style={{ marginTop: 16 }}>
                <Text style={styles.inputDescribeText}>
                                อีเมลล์
                </Text>
                <TextInput
                  autoCapitalize="none"
                  keyboardType="email-address"
                  onFocus={this.handleOnFocus('email')}
                  onBlur={this.handleOnBlur('email')}
                  onChangeText={(email) => {
                    this._validationEmail(email)
                  }}
                  ref={(ref) => {
                    this.emailInput = ref
                  }}
                  returnKeyType={Platform.OS === 'android' ? 'done' : 'default'}
                  style={[styles.textInput, this.borderColor(this.state.emailFocused, this.state.invalidEmail)]}
                  underlineColorAndroid="transparent"
                  value={this.state.email}
                />
                {this.state.invalidEmail && !this.state.isEmailExist && (
                  <Text style={styles.errorDescribeText}>
                                    กรุณากรอก email ให้ถูกต้อง
                  </Text>
                )}
                {this.state.isEmailExist && !this.state.invalidEmail && (
                  <Text style={styles.errorDescribeText}>
                                    กรุณาใช้ email อื่น เนื่องจาก email นี้ถูกใช้งานแล้ว
                  </Text>
                )}
              </View>
              {/* End email input */}

              {/* Begin accept condition */}
              {
                !editing && (
                  <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                    marginVertical: 20
                  }}
                  >
                    {/* Begin checkbox container */}
                    <View style={{
                      flex: 20,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}
                    >
                      <CircleCheckBox
                        checked={this.state.checked}
                        onToggle={(checked) => {
                          this.setState({ checked })
                        }}
                        outerColor="#535786"
                        innerColor="#535786"
                        outerSize={40}
                        filterSize={35}
                      />
                    </View>
                    {/* End checkbox container */}

                    {/* Begin description container */}
                    <View style={{ flex: 80 }}>
                      <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.acceptText}>
                                                ฉันยอมรับ
                          <Text style={[
                            styles.acceptHighlightText,
                            { paddingRight: 5 }]}
                          >
                                                    เงื่อนไขการใช้บริการ
                          </Text>
                        </Text>
                        <Text style={[
                          styles.acceptText,
                          { marginLeft: 5 }]}
                        >
                                                และ
                        </Text>
                      </View>
                      <Text style={[styles.acceptText, styles.acceptHighlightText]}>
                                            นโยบายความเป็นส่วนตัว
                      </Text>
                    </View>
                    {/* End description container */}
                  </View>
                )
              }
              {/* End accept condition */}

              {/* Begin next button container */}
              <TouchableOpacity
                disabled={disabledSubmit}
                style={[btnStyle, { marginTop: 20 }]}
                onPress={this._handleSubmit}
              >
                <Text style={btnTextStyle}>
                                ยืนยันตัวตน
                </Text>
              </TouchableOpacity>
              {/* End next button container */}
            </KeyboardAvoidingView>
          </ScrollView>
          {/* End scrollview container */}
          <KeyboardAccessoryNavigation
            androidAdjustResize={true}
            nextHidden={true}
            previousHidden={true}
            avoidKeyboard={true}
          />
          {/* End flex two */}
          {
            this.state.uploading && !this.state.complete &&
            <Loading title="กำลังบันทึกข้อมูล" />
          }
        </View>
      )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  inputDescribeText: {
    marginBottom: 6,
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14,
    color: '#121212'
  },
  errorDescribeText: {
    marginTop: 3,
    fontFamily: AppStyle.mainFontTH,
    fontSize: 13,
    color: '#DC3545'
  },
  containerNextBtn: {
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    borderRadius: 10,
    backgroundColor: '#535786'
  },
  nextText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 22,
    color: '#FFF'
  },
  disableNextBtn: {
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    borderRadius: 10,
    backgroundColor: '#F8F8F8'
  },
  disableNextText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 22,
    color: '#DFDFDF'
  },
  acceptText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    color: '#909090'
  },
  acceptHighlightText: {
    color: '#535786'
  },
  textInput: {
    height: 58,
    borderColor: '#CBCCD9',
    borderWidth: 1,
    borderRadius: 13,
    paddingHorizontal: 10,
    fontFamily: AppStyle.mainFontTH
  },
  imageUploadBg: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    height: '100%'
  },
  uploadButton: {
    height: 100,
    width: 100
  },
  rightColumn: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  leftColumn: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14,
    color: '#141527'
  },
  titleExample: {
    color: '#666666',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 12,
    textAlign: 'right'
  },
  textDesc: {
    color: '#333',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16
  }
})

export default KYCScreen
