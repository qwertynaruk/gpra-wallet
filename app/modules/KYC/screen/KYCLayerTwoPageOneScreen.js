import React, { Component } from 'react'
import {
  ActionSheetIOS,
  BackHandler,
  Dimensions,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  Picker,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { computed } from 'mobx'
import { KeyboardAccessoryNavigation } from 'react-native-keyboard-accessory'

// Components & Modules
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const { width } = Dimensions.get('window')
const marginTop = LayoutUtils.getExtraTop()
const title = 'ยืนยันตัวตนเต็มรูปแบบ'

const occupationOptions = [
  'พนักงานบริษัท',
  'เจ้าของกิจการ',
  'ข้าราชการ',
  'อาชีพอิสระ/Freelance',
  'นักเรียน/นักศึกษา',
  'พนักงานรัฐวิสาหกิจ',
  'ธุรกิจออนไลน์',
  'นักลงทุนอิสระ',
  'แม่บ้าน/พ่อบ้าน',
  'พนักงานทำความสะอาด',
  'เกษียณ',
  'ว่างงาน',
  'นักการเมือง',
  'ค้าของเก่า หรือขายทอดตลาด',
  'ค้าอัญมณี เพชรพลอย ทองคำ หรือเครื่องประดับที่มีส่วนประกอบดังกล่าว',
  'รับแลกเปลี่ยนเงินตรา',
  'ธุรกิจคาสิโนหรือบ่อนการพนัน',
  'ธุรกิจสถานบริการตามกฎหมายว่าด้วยสถานบริการ',
  'ค้าอาวุธยุทธภัณฑ์',
  'นายหน้าจัดหางานเกี่ยวกับการรับหรือส่งคนทำงานในประเทศหรือนอกประเทศ',
  'ธุรกิจนำเที่ยวหรือบริษัททัวร์',
  'เกษตรกร',
  'ชาวประมง',
  'ให้บริการโอนและรับโอนเงิน ทั้งในและข้ามประเทศ ที่ไม่ใช่สถาบันการเงิน',
  'อื่นๆ'
]

const salaryOptions = [
  '0 - 15,000 THB',
  '15,001 - 30,000 THB',
  '30,001 - 50,000 THB',
  '50,001 - 100,000 THB',
  '100,001 - 200,000 THB',
  '200,001 - 400,000 THB',
  '400,001 - 700,000 THB',
  '700,001 - 1,000,000 THB',
  '1,000,001 - 2,000,000 THB',
  'มากกว่า 2,000,000 THB'
]

export default class KYCLayerTwoPageOneScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object
  }

  static defaultProps = {
    navigation: {}
  }

  constructor(props) {
    super(props)
    this.state = {
      address: null,
      careerAndroid: 'พนักงานบริษัท',
      careeriOS: 'เลือก...',
      company: null,
      contactAddress: null,
      companyAddress: null,
      salaryAndroid: '0-15000 THB',
      salaryiOS: 'เลือก...',
      keyboardAvoidingViewKey: 'keyboardAvoidingViewKey',
      invalidAddress: false,
      invalidContactAddress: false,
      invalidCompany: false,
      invalidCompanyAddress: false
    }

    this.goBack = this.goBack.bind(this)
    this.goNext = this.goNext.bind(this)
    this.onSelectOccupation = this.onSelectOccupation.bind(this)
    this.onSelectSalary = this.onSelectSalary.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
  }

  componentWillMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentDidMount() {
    this.keyboardHideListener = Keyboard.addListener(Platform.OS === 'android' ? 'keyboardDidHide' : 'keyboardWillHide', this.keyboardHideListener.bind(this))
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }

    this.keyboardHideListener.remove()
  }

  onSelectOccupation() {
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options: occupationOptions
      },
      buttonIndex => this.setState({ careeriOS: occupationOptions[buttonIndex] })
    )
  }

  onSelectSalary() {
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options: salaryOptions
      },
      buttonIndex => this.setState({ salaryiOS: salaryOptions[buttonIndex] })
    )
  }

  @computed get disabled() {
    const career = Platform.OS === 'android' ? this.state.careerAndroid : this.state.careeriOS
    const salary = Platform.OS === 'android' ? this.state.salaryAndroid : this.state.salaryiOS
    const {
      address,
      company,
      companyAddress,
      contactAddress,
      invalidAddress,
      invalidContactAddress,
      invalidCompany,
      invalidCompanyAddress
    } = this.state

    if (
      address &&
      career &&
      company &&
      companyAddress &&
      contactAddress &&
      salary &&
      !invalidAddress &&
      !invalidContactAddress &&
      !invalidCompany &&
      !invalidCompanyAddress
    ) {
      return false
    }

    return true
  }

  keyboardHideListener() {
    this.setState({
      keyboardAvoidingViewKey: `keyboardAvoidingViewKey${new Date().getTime()}`
    })
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  borderColor(focus, error) {
    if (error) {
      return {
        borderColor: '#C60003'
      }
    } else if (focus) {
      return {
        borderColor: '#4B5391'
      }
    }

    return {}
  }

  goBack() {
    const { params } = this.props.navigation.state
    if (params && params.sourceScreen === 'DashboardScreen') {
      NavStore.pushToScreen('DashboardScreen')
    } else {
      NavStore.pushToScreen('SettingScreen')
    }
  }

  goNext() {
    const career = Platform.OS === 'android' ? this.state.careerAndroid : this.state.careeriOS
    const salary = Platform.OS === 'android' ? this.state.salaryAndroid : this.state.salaryiOS
    const {
      address,
      contactAddress,
      company,
      companyAddress
    } = this.state

    if (address.trim() !== '' && contactAddress.trim() !== '' && company.trim() !== '' && companyAddress.trim() !== '') {
      MainStore.appState.kycData = {
        address: address.trim(),
        career,
        company: company.trim(),
        companyAddress: companyAddress.trim(),
        contactAddress: contactAddress.trim(),
        salary
      }

      // NavStore.pushToScreen('KYCLayerTwoPageTwoScreen')
      NavStore.pushToScreen('KYCLayerTwoPageThreeFetchaAScreen')
    }

    this.setState({
      invalidAddress: address.trim() === '',
      invalidContactAddress: contactAddress.trim() === '',
      invalidCompany: company.trim() === '',
      invalidCompanyAddress: companyAddress.trim() === ''
    })
  }

  render() {
    const { disabled } = this
    const {
      address,
      company,
      contactAddress,
      companyAddress,
      careeriOS,
      careerAndroid,
      salaryiOS,
      salaryAndroid,
      keyboardAvoidingViewKey,
      invalidAddress,
      invalidContactAddress,
      invalidCompany,
      invalidCompanyAddress
    } = this.state

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: marginTop + 20,
            paddingHorizontal: 20,
            width
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            color: '#000',
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.goBack}
          rightView={{
            rightViewTitle: '1/2',
            styleTitle: { fontFamily: AppStyle.mainFont, fontSize: 16, color: '#CBCCD9' }
          }}
        />
        <ScrollView
          style={styles.container}
          showsHorizontalScrollIndicator={false}
        >
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : null}
            style={{ paddingHorizontal: 25 }}
            enabled
            key={keyboardAvoidingViewKey}
          >
            <Text style={[styles.title, { marginTop: 26 }]}>
              ที่อยู่ตามบัตรประชาชน
            </Text>
            <TouchableWithoutFeedback onPress={() => { this.addressInput.focus() }}>
              <View
                style={[
                  styles.textInputContainer,
                  {
                    height: 100,
                    marginTop: 6,
                    // paddingVertical: Platform.OS === 'ios' ? 10 : 0,
                    borderColor: invalidAddress ? '#C60003' : '#CBCCD9',
                    overflow: 'hidden'
                  }]}
              >
                <TextInput
                  ref={(input) => { this.addressInput = input }}
                  multiline={true}
                  style={{
                    alignItems: 'flex-start',
                    height: 100,
                    textAlignVertical: 'top',
                    fontFamily: AppStyle.mainFontTH,
                    paddingVertical: 10
                  }}
                  onFocus={() => {
                    this.setState({ invalidAddress: false })
                  }}
                  onChangeText={(value) => {
                    this.setState({ address: value })
                  }}
                  value={address}
                />
              </View>
            </TouchableWithoutFeedback>
            {(invalidAddress) && (
              <Text style={styles.errorDescribeText}>
                กรุณากรอก ที่อยู่ตามบัตรประชาชน
              </Text>
            )}

            <View style={styles.grap}>
              <Text style={styles.title}>
                ที่อยู่ที่สะดวกในการติดต่อ
              </Text>
              <TouchableWithoutFeedback onPress={() => { this.contactAddressInput.focus() }}>
                <View style={[
                  styles.textInputContainer,
                  {
                    height: 100,
                    marginTop: 6,
                    // paddingVertical: Platform.OS === 'ios' ? 10 : 0,
                    borderColor: invalidContactAddress ? '#C60003' : '#CBCCD9',
                    overflow: 'hidden'
                  }]}
                >
                  <TextInput
                    ref={(input) => { this.contactAddressInput = input }}
                    multiline={true}
                    style={{
                      alignItems: 'flex-start',
                      height: 100,
                      textAlignVertical: 'top',
                      fontFamily: AppStyle.mainFontTH,
                      paddingVertical: 10
                    }}
                    onChangeText={(value) => {
                      this.setState({ contactAddress: value })
                    }}
                    onFocus={() => {
                      this.setState({ invalidContactAddress: false })
                    }}
                    value={contactAddress}
                  />
                </View>
              </TouchableWithoutFeedback>
            </View>
            {(invalidContactAddress) && (
              <Text style={styles.errorDescribeText}>
                กรุณากรอก ที่อยู่สะดวกในการติดต่อ
              </Text>
            )}

            <View style={[styles.grap, { marginTop: 44 }]}>
              <Text style={styles.title}>
                อาชีพ
              </Text>
              <View style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 6,
                height: 44,
                width: '100%',
                borderColor: '#CBCCD9',
                borderWidth: 1,
                borderRadius: 8,
                paddingHorizontal: 10
              }}
              >
                <View style={{ width: '100%', height: '100%' }}>
                  {Platform.OS === 'android' &&
                    <Picker
                      selectedValue={careerAndroid}
                      style={{ flex: 1 }}
                      onValueChange={(value, index) => this.setState({ careerAndroid: value })}
                    >
                      {occupationOptions.map((item, index) => <Picker.Item key={index} value={item} label={item} />)}
                    </Picker>
                  }
                  {Platform.OS === 'ios' &&
                    <TouchableOpacity
                      style={{ flex: 1, display: 'flex', justifyContent: 'center' }}
                      onPress={this.onSelectOccupation}
                    >
                      <Text>
                        {careeriOS}
                      </Text>
                    </TouchableOpacity>
                  }
                </View>
              </View>
            </View>

            <View style={styles.grap}>
              <Text style={styles.title}>
                ช่วงรายได้
              </Text>
              <View style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 6,
                height: 44,
                width: '100%',
                borderColor: '#CBCCD9',
                borderWidth: 1,
                borderRadius: 8,
                paddingHorizontal: 10
              }}
              >
                <View style={{ width: '100%', height: '100%' }}>
                  {Platform.OS === 'android' &&
                    <Picker
                      selectedValue={salaryAndroid}
                      style={{ flex: 1 }}
                      onValueChange={(value, index) => this.setState({ salaryAndroid: value })}
                    >
                      {salaryOptions.map((item, index) => <Picker.Item key={index} value={item} label={item} />)}
                    </Picker>
                  }
                  {Platform.OS === 'ios' &&
                    <TouchableOpacity
                      style={{ flex: 1, display: 'flex', justifyContent: 'center' }}
                      onPress={this.onSelectSalary}
                    >
                      <Text>
                        {salaryiOS}
                      </Text>
                    </TouchableOpacity>
                  }
                </View>
              </View>
            </View>

            <View style={styles.grap}>
              <Text style={styles.title}>
                ชื่อบริษัทที่ทำงาน
              </Text>
              <View style={[
                styles.textInputContainer,
                {
                  height: 44,
                  marginTop: 6,
                  paddingVertical: Platform.OS === 'ios' ? 10 : 0,
                  borderColor: invalidCompany ? '#C60003' : '#CBCCD9'
                }]}
              >
                <TextInput
                  style={{
                    flex: 1,
                    fontFamily: AppStyle.mainFontTH
                  }}
                  onChangeText={(value) => {
                    this.setState({ company: value })
                  }}
                  onFocus={() => {
                    this.setState({ invalidCompany: false })
                  }}
                  value={company}
                  returnKeyType="done"
                />
              </View>
            </View>
            {(invalidCompany) && (
              <Text style={styles.errorDescribeText}>
                กรุณากรอก ชื่อบริษัทที่ทำงาน
              </Text>
            )}

            <View style={styles.grap}>
              <Text style={styles.title}>
                ที่อยู่ที่ทำงาน
              </Text>
              <TouchableWithoutFeedback onPress={() => { this.companyAddressInput.focus() }}>
                <View style={[
                  styles.textInputContainer,
                  {
                    height: 100,
                    marginTop: 6,
                    // paddingVertical: Platform.OS === 'ios' ? 10 : 0,
                    borderColor: invalidCompanyAddress ? '#C60003' : '#CBCCD9',
                    overflow: 'hidden'
                  }]}
                >
                  <TextInput
                    ref={(input) => { this.companyAddressInput = input }}
                    multiline={true}
                    style={{
                      alignItems: 'flex-start',
                      height: 100,
                      textAlignVertical: 'top',
                      fontFamily: AppStyle.mainFontTH,
                      paddingVertical: 10
                    }}
                    onChangeText={(value) => {
                      this.setState({ companyAddress: value })
                    }}
                    onFocus={() => {
                      this.setState({ invalidCompanyAddress: false })
                    }}
                    value={companyAddress}
                  />
                </View>
              </TouchableWithoutFeedback>
            </View>
            {(invalidCompanyAddress) && (
              <Text style={styles.errorDescribeText}>
                กรุณากรอก ที่อยู่ที่ทำงาน
              </Text>
            )}

            <TouchableOpacity
              style={disabled ? styles.disabledBtn : styles.button}
              onPress={this.goNext}
              disabled={disabled}
            >
              <Text style={styles.buttonText}>ถัดไป</Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
        </ScrollView>

        <KeyboardAccessoryNavigation
          androidAdjustResize={true}
          nextHidden={true}
          previousHidden={true}
          avoidKeyboard={true}
        />
      </View >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14,
    color: '#141527'
  },
  grap: {
    flexDirection: 'column',
    marginTop: 16
  },
  button: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16,
    marginBottom: 30,
    height: 60,
    backgroundColor: '#535786',
    borderRadius: 8
  },
  disabledBtn: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16,
    marginBottom: 30,
    height: 60,
    backgroundColor: '#D3D3D3',
    borderRadius: 8
  },
  buttonText: {
    color: '#FFF',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20
  },
  textInputContainer: {
    paddingHorizontal: 10,
    borderWidth: 1,
    borderRadius: 8
  },
  errorDescribeText: {
    marginTop: 3,
    fontFamily: AppStyle.mainFontTH,
    fontSize: 13,
    color: '#DC3545'
  }
})
