import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  BackHandler,
  StatusBar,
  Image,
  TouchableOpacity,
  Text,
  Platform,
  Dimensions,
  ScrollView
} from 'react-native'

// Third-party
import LinearGradient from 'react-native-linear-gradient'
import PropTypes from 'prop-types'
import ViewShot from 'react-native-view-shot'
import QRCode from 'react-native-qrcode-svg'
import Share from 'react-native-share'
import RNFS from 'react-native-fs'

// Components & Modules
import NavStore from '../../../AppStores/NavStore'
import MainStore from '../../../AppStores/MainStore'
import AppStyle from '../../../commons/AppStyle'
import Helper from '../../../commons/Helper'
import icons from '../../../commons/icons'
import PaymentTypes from '../element/PaymentTypes'

const { height, width } = Dimensions.get('window')
const isSmallScreen = height < 569

const payments = [
  {
    id: 'thb',
    name: 'บาท',
    icon: icons.iconBuyTHB,
    iconAlt: icons.iconAltBuyTHB,
    promtpay: true
  },
  {
    id: 'btc',
    name: 'BTC',
    slug: 'bitcoin',
    icon: icons.iconBuyBTC,
    iconAlt: icons.iconAltBuyBTC
  },
  {
    id: 'eth',
    name: 'ETH',
    slug: 'ethereum',
    icon: icons.iconBuyETH,
    iconAlt: icons.iconAltBuyETH
  },
  {
    id: 'xrp',
    name: 'XRP',
    slug: 'ripple',
    icon: icons.iconBuyXRP,
    iconAlt: icons.iconAltBuyXRP
  },
  {
    id: 'xlm',
    name: 'XLM',
    slug: 'stellar',
    icon: icons.iconBuyXLM,
    iconAlt: icons.iconAltBuyXLM
  }
]

export default class BuyAomSecondScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object
  }

  static defaultProps = {
    navigation: {}
  }

  constructor(props) {
    super(props)
    this.state = {
      selected: 'thb'
    }

    this.goBack = this.goBack.bind(this)
    this.onPressType = this.onPressType.bind(this)
    this.onShare = this.onShare.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
    this.closePromptPay = this.closePromptPay.bind(this)
    this.alreadyTransfer = this.alreadyTransfer.bind(this)

    this.answers = MainStore.appState.fabric.answers
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  onPressType(id) {
    this.setState({
      selected: id
    })
  }

  onShare() {
    if (!this.state.isSave) {
      this.setState({ isSave: true })
      this.viewShot.capture().then((uri) => {
        const filePath = Platform.OS === 'ios' ? uri : uri.replace('file://', '')
        this.openShare(filePath)
      })
    }
  }

  get gateway() {
    const { params } = this.props.navigation.state
    const { gateway } = params
    return gateway
  }

  get selectedPayment() {
    return payments.find(obj => obj.id === this.state.selected)
  }

  get payment() {
    if (this.gateway) {
      const key = this.state.selected.toUpperCase()
      const keys = Object.keys(this.gateway)
      if (keys.includes(key)) {
        const gateway = this.gateway[key]
        if (gateway) {
          return gateway
        }
      }
    }

    return 0
  }

  get detail() {
    const { text } = this.selectedPayment
    return text
  }

  get qrWidth() {
    if (width > 400) {
      return {
        width: 300
      }
    }

    return {}
  }

  closePromptPay() {
    NavStore.pushToScreen('DashboardScreen')
  }

  alreadyTransfer() {
    NavStore.pushToScreen('DashboardScreen')
    NavStore.showToastTop(`อยู่ระหว่างดำเนินการ อาจใช้เวลา 5 ถึง 10 นาที`, {}, {})
  }

  openShare(filePath) {
    NavStore.preventOpenUnlockScreen = true
    RNFS.readFile(filePath, 'base64').then((file) => {
      this.answers.logShare('QRCode', 'Buy OM', 'buyom', 'share-buyom')
      const shareOptions = {
        title: 'Buy OM',
        message: `QR Payment`,
        url: `data:image/png;base64,${file}`
      }
      Share.open(shareOptions).catch(() => { })
      this.setState({ isSave: false })
    })
  }

  QRCodeData() {
    if (this.gateway) {
      const key = this.state.selected.toUpperCase()
      const keys = Object.keys(this.gateway)
      if (keys.includes(key)) {
        const gateway = this.gateway[key]
        if (gateway) {
          return gateway.account
        }
      }
    }

    return null
  }

  goBack() {
    NavStore.pushToScreen('BuyAomScreen')
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  _renderDescriptionText(text) {
    const desc = text.map((data, i) => (
      <Text key={`text-${i}`} style={styles.textDescription}>{data}</Text>
    ))
    return (
      <View style={{ alignItems: 'center' }}>
        {desc}
      </View>
    )
  }

  _renderQRCode(qrData, promtpay, icon) {
    const { payment, qrWidth } = this
    const { cost, account } = payment

    if (promtpay) {
      return (
        <ViewShot
          ref={(ref) => { this.viewShot = ref }}
          style={[
            qrWidth,
            {
              backgroundColor: '#FFF',
              alignItems: 'center'
            }
          ]}
        >
          <View
            style={[styles.headerThaiQR, { width: '100%' }]}
          >
            <Image
              style={styles.headerThaiQRIcon}
              source={icons.iconThaiQR}
              resizeMode="contain"
            />
          </View>
          <View style={styles.bodyThaiQR}>
            <View style={styles.bodyThaiQRContainer}>
              <View>
                <Image
                  style={styles.iconPromptpayL}
                  source={icons.iconPromptpayL}
                  resizeMode="contain"
                />
              </View>
              <View style={styles.bodyThaiQR}>
                <View style={styles.bodyThaiQRContainer}>
                  <QRCode
                    value={qrData}
                    size={isSmallScreen ? 210 : 230}
                  />
                  <View style={[styles.assetThaiQRContainer]}>
                    <Image
                      style={styles.iconThaiQRS}
                      source={icons.iconThaiQRS}
                      resizeMode="contain"
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.footerThaiQR}>
            <View style={styles.footerThaiQRContainer}>
              <View>
                <Text style={styles.footerThaiQRCurrency}>{Helper.formatCUR(cost, false, 0)} บาท</Text>
                <Text style={styles.footerThaiQRText}>{account.name}</Text>
                <Text style={styles.footerThaiQRTextSub}>{account.number}</Text>
              </View>
            </View>
          </View>
        </ViewShot>
      )
    }

    return (
      <View
        style={{
          backgroundColor: '#FFF',
          alignItems: 'center',
          borderRadius: 10,
          borderColor: '#C5C5C5',
          borderWidth: 1,
          padding: 5
        }}
      >
        <ViewShot
          ref={(ref) => { this.viewShot = ref }}
          style={{
            backgroundColor: '#FFF',
            padding: 10
          }}
        >
          <QRCode
            value={qrData}
            size={isSmallScreen ? 220 : 240}
          />
          <View style={styles.assetIconContainer}>
            <Image style={styles.assetIcon} source={icon} />
          </View>
        </ViewShot>
      </View>
    )
  }

  render() {
    const { selectedPayment, payment } = this
    const {
      promtpay, slug, name, icon
    } = selectedPayment
    const { cost, account, qrcode } = payment
    const qrData = promtpay
      ? qrcode
      : `${slug}:${account}?amount=${cost}`

    return (
      <View style={styles.container}>
        <StatusBar
          hidden
        />
        <LinearGradient colors={['#4B5391', '#2C6E9E']} style={styles.linearGradient} />

        <ScrollView
          style={{ flex: 1 }}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
        >
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
              paddingTop: 22,
              paddingRight: 22
            }}
          >
            <TouchableOpacity onPress={this.closePromptPay}>
              <Image source={icons.iconClose} />
            </TouchableOpacity>
          </View>

          <View>
            <Text style={{
              textAlign: 'center',
              fontFamily: AppStyle.mainFontBoldTH,
              fontSize: 18,
              color: '#FFF',
              marginBottom: 10,
              marginTop: 20
            }}
            >
              กรุณาโอนเงินตามรายละเอียดด้านล่าง
            </Text>
          </View>

          {/* <View style={{ marginVertical: 3 }}>
            <Text style={{
              textAlign: 'center',
              fontFamily: AppStyle.mainFontBoldTH,
              fontSize: cost.length > 10 ? 24 : 28,
              color: '#FFF',
              paddingHorizontal: 20
            }}
            >
              {cost}
            </Text>
          </View> */}

          {/* <View>
            <Text style={{
              textAlign: 'center',
              fontFamily: AppStyle.mainFontBoldTH,
              fontSize: 18,
              color: '#FFF'
            }}
            >
              {name}
            </Text>
          </View> */}

          {/* <View style={styles.paymentTypesContainer}>
            <PaymentTypes
              data={payments}
              selected={this.state.selected}
              onPressItem={this.onPressType}
            />
          </View> */}

          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            {this._renderQRCode(qrData, promtpay, icon)}
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <TouchableOpacity
              style={{
                height: 35,
                width: 144,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#657EFF',
                borderBottomRightRadius: 5,
                borderBottomLeftRadius: 5
              }}
              onPress={this.onShare}
            >
              <Image source={icons.iconSave} />
              <Text style={{
                color: AppStyle.mainColor,
                fontSize: 16,
                fontFamily: AppStyle.mainFontTH,
                marginLeft: 5
              }}
              >
                แชร์ QR
              </Text>
            </TouchableOpacity>
          </View>
          <View>
            {
              !promtpay && (
                <View>
                  <Text style={{
                    textAlign: 'center',
                    fontFamily: AppStyle.mainFontBoldTH,
                    fontSize: 18,
                    color: '#FFF',
                    marginTop: 20,
                    paddingHorizontal: 20
                  }}
                  >
                    Address
                  </Text>
                  <Text style={{
                    textAlign: 'center',
                    fontFamily: AppStyle.mainFontTH,
                    fontSize: 14,
                    color: '#FFF',
                    paddingHorizontal: 20
                  }}
                  >
                    {account}
                  </Text>
                </View>
              )
            }
            {/* on press show popup pending */}

            <TouchableOpacity onPress={this.alreadyTransfer}>
              <View style={styles.confirmBtnContainer} >
                <Text style={{
                  padding: 10,
                  textAlign: 'center',
                  fontFamily: AppStyle.mainFontTH,
                  fontSize: 16,
                  color: '#FFF'
                }}
                >
                  โอนเรียบร้อยแล้ว
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
        {/* End flex two */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  confirmBtnContainer: {
    borderWidth: 3,
    borderRadius: 10,
    borderColor: '#fff',
    height: 50,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
    marginBottom: 30
  },
  paymentTypesContainer: {
    marginHorizontal: 20,
    marginVertical: 15,
    height: 80,
    alignItems: 'center',
    justifyContent: 'center'
  },
  linearGradient: {
    height: '100%',
    position: 'absolute',
    width: '100%',
    top: 0
  },
  assetIconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    position: 'absolute',
    width: '100%',
    top: 10,
    left: 10
  },
  assetIcon: {
    borderColor: '#FFF',
    borderWidth: 3,
    borderRadius: 15,
    height: 30,
    width: 30
  },
  assetThaiQRContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    position: 'absolute',
    width: '100%',
    left: 0,
    top: 0
  },
  iconThaiQRS: {
    height: '10%'
  },
  iconPromptpayL: {
    height: 25,
    marginVertical: 10
  },
  headerThaiQR: {
    backgroundColor: '#0E3468',
    paddingVertical: 15
  },
  headerThaiQRIcon: {
    height: 35
  },
  bodyThaiQR: {
    flexDirection: 'row',
    backgroundColor: '#FFF'
  },
  bodyThaiQRContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  footerThaiQR: {
    flexDirection: 'row'
  },
  footerThaiQRContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FFF',
    justifyContent: 'center',
    paddingBottom: 10,
    paddingTop: 5
  },
  footerThaiQRCurrency: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 30,
    textAlign: 'center'
  },
  footerThaiQRText: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 15,
    textAlign: 'center'
  },
  footerThaiQRTextSub: {
    color: '#000',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 12,
    textAlign: 'center'
  }
})
