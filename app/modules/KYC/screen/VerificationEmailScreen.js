import React, { Component } from 'react'
import {
  Alert,
  BackHandler,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import { action } from 'mobx'

// Components & Modules
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import icons from '../../../commons/icons'
import AppStyle from '../../../commons/AppStyle'
import api from '../../../api'

export default class VerificationEmailScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      kyc: null
    }

    this.onPressResend = this.onPressResend.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
    this.gotoDashboard = this.gotoDashboard.bind(this)
    this.gotoKYC = this.gotoKYC.bind(this)

    this.kycStore = MainStore.kycStore
  }

  async componentWillMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }

    const wallet = MainStore.appState.selectedWallet
    const kyc = await this.kycStore.getKycInfo(wallet.address)
    this.setKyc(kyc)
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  onPressResend() {
    const { kyc } = this
    const email = kyc.email || kyc.pending_email

    NavStore.showLoading()
    const wallet = MainStore.appState.selectedWallet
    api.resendVerification(wallet.address).then((res) => {
      Alert.alert(
        'ส่งอีเมล์ยืนยันเรียบร้อย',
        `ระบบได้ส่งการยืนยันตัวตนไปยังอีเมลล์\n${email} แล้ว\nโปรดตรวจสอบลิงค์ในเมลล์เปิดใช้งานกระเป๋า`,
        [
          {
            text: 'โอเค',
            onPress: () => {
              NavStore.hideLoading()
              NavStore.pushToScreen('DashboardStack')
            }
          }
        ],
        {
          cancelable: false
        }
      )
    }).catch(err => console.log(`onPressResend ${err.message}`))
  }

  @action setKyc(kyc) {
    this.setState({
      kyc
    })
  }

  get kyc() {
    return this.state.kyc
  }

  handleBackPress() {
    return true
  }

  gotoDashboard() {
    NavStore.pushToScreen('DashboardStack')
  }

  gotoKYC() {
    NavStore.pushToScreen('KYCScreen', { editSeting: true })
  }

  render() {
    const { kyc } = this
    if (!kyc) {
      return (
        <View style={styles.container}>
          <Text style={styles.loading}>กำลังโหลดข้อมูล</Text>
        </View>
      )
    }
    const email = kyc.email || kyc.pending_email
    return (
      <View style={styles.container}>
        <Image style={styles.image} source={icons.iconEmail} />
        <Text style={styles.title}>กรุณาตรวจสอบอีเมลล์ของคุณ</Text>
        <Text style={styles.desc}>{`ระบบได้ส่งการยืนยันตัวตนไปยังอีเมลล์\n${email} แล้ว\nโปรดตรวจสอบลิงค์ในเมลล์เปิดใช้งานกระเป๋า`}</Text>
        <View style={styles.row}>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={this.onPressResend}
          >
            <Text style={styles.resendText}>คลิกที่นี่เพื่อส่งเมลล์อีกครั้ง</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.backBtn}
            onPress={this.gotoDashboard}
          >
            <Text style={styles.backText}>กลับสู่หน้าหลัก</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.editBtn}
          onPress={this.gotoKYC}
        >
          <Text style={styles.editText}>แก้ไขข้อมูล</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    marginBottom: 20
  },
  title: {
    color: '#141527',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20,
    marginBottom: 10
  },
  desc: {
    color: '#121212',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    marginBottom: 10,
    textAlign: 'center'
  },
  loading: {
    color: '#BBB',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16
  },
  row: {
    flexDirection: 'row'
  },
  resendText: {
    color: '#535786',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 10
  },
  backBtn: {
    flex: 1,
    backgroundColor: '#535786',
    borderRadius: 8,
    marginHorizontal: 25,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center'
  },
  backText: {
    color: '#FFF',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20
  },
  editBtn: {
    paddingVertical: 20
  },
  editText: {
    color: '#535786',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18
  }
})
