import React, { Component } from 'react'
import {
  Alert,
  BackHandler,
  Dimensions,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  Linking,
  TouchableOpacity,
  View
} from 'react-native'

// Third-party
import { computed } from 'mobx'
import CircleCheckBox from 'react-native-circle-checkbox'

// Components & Modules
import api from '../../../api'
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import Loading from '../element/Loading'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const { width } = Dimensions.get('window')
const marginTop = LayoutUtils.getExtraTop()
const title = 'ยืนยันตัวตนเต็มรูปแบบ'

export default class KYCLayerTwoPageThreeFetchaAScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      checked: false,
      complete: false,
      uploading: false
    }

    this.goBack = this.goBack.bind(this)
    this.goNext = this.goNext.bind(this)
    this.onVerification = this.onVerification.bind(this)
    this.contactSupport = this.contactSupport.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
    this.answers = MainStore.appState.fabric.answers
  }

  componentWillMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  onVerification() {
    const { address } = MainStore.appState.selectedWallet
    const { kycData } = MainStore.appState
    const data = {
      public_key: address,
      profile: {
        addressFromIDCard: kycData.address,
        addressFromContact: kycData.contactAddress,
        job: kycData.career,
        income: kycData.salary,
        workplaceName: kycData.company,
        workplaceAddress: kycData.companyAddress
      }
    }

    this.setState({ uploading: true })
    api.Aom.kycVerication(data).then((res) => {
      if (res.status === 200) {
        MainStore.appState.setKycLayerTwo(true)
        this.setState({ uploading: false, complete: true })
        this.goNext()
      }
    }).catch((err) => {
      Alert.alert(
        'แจ้งเตือน',
        'การส่งข้อมูลล้มเหลว กรุณาลองใหม่อีกครั้ง', [{
        }, {
          text: 'ตกลง',
          onPress: () => { this.setState({ uploading: false, complete: true }) }
        }]
      )

      api.Aom.reportBug({
        address: kycData.address,
        error_component: 'KYCLayerTwoPageThreeFetchaAScreen',
        error_function: 'onVerification',
        error_detail: err.message
      })
    })
  }

  @computed get disabled() {
    if (
      this.state.checked
    ) {
      return false
    }

    return true
  }

  handleBackPress() {
    if (!this.state.uploading && !this.state.complete) {
      this.goBack()
    }

    return true
  }

  goBack() {
    NavStore.goBack()
  }

  goNext() {
    NavStore.pushToScreen('KYCPending')
  }

  contactSupport() {
    Linking.openURL(`mailto:support@omplatform.com`)
  }

  render() {
    const { disabled } = this

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: marginTop + 20,
            paddingHorizontal: 20,
            width
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}

          titleStyle={{
            color: '#000',
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.goBack}
          rightView={{
            rightViewTitle: '2/2',
            styleTitle: { fontFamily: AppStyle.mainFont, fontSize: 16, color: '#CBCCD9' }
          }}
        />

        <View style={styles.scroll}>
          <ScrollView style={{ flex: 1 }}>
            <View style={styles.fetchaContainer}>
              <Text style={styles.textTitle}>FATCHA</Text>
              <Text style={[styles.textMain, { marginVertical: 10 }]}>คุณยืนยันว่าข้อมูลเหล่านี้เป็นความจริง
                และสอดคล้องกับสถานะของท่าน
              </Text>
              <Text style={[styles.textTitle, { marginTop: 10 }]}>ส่วนที่ 1: สถานะของลูกค้า</Text>
              <Text style={[styles.textMain, { marginBottom: 10 }]}>{`หากข้อมูลส่วนใดส่วนหนึ่งหรือทั้งหมดไม่ถูกต้อง กรุณาติดต่อ `}
                <Text style={[styles.underline, { marginLeft: 5 }]} onPress={this.contactSupport}>support@omplatform.com</Text>
              </Text>
              <Text style={styles.textSpecial}>1. ท่านไม่ใช่พลเมืองอเมริกัน</Text>
              <Text style={styles.textDesc}>2. ท่านไม่ใช่ผู้ถือบัตรประจำตัวผู้มีถิ่นที่อยู่อย่างถูกต้อง ตามกฏหมายในสหรัฐอเมริกา (เช่น กรีนการ์ด)</Text>
              <Text style={styles.textDesc}>3. ท่านไม่มีสถานะเป็นผู้มีถิ่นที่อยู่ในสหรัฐอเมริกาเพื่อวัตถุประสงค์ในการเก็บภาษีอากรของสหรัฐอเมริกา</Text>
              <Text style={styles.textDesc}>4. ท่านไม่ได้เกิดในสหรัฐอเมริกา (หรือดินแดนที่เป็นของสหรัฐอเมริกา)</Text>
              <Text style={styles.textDesc}>5. ท่านไม่มีคำสั่งทำรายการโอนเงินเป็นประจำโดยอัตโนมัติ จากบัญชีที่เปิดไว้หรือมีอยู่กับ บริษัท ออมแพลทฟอร์ม จำกัด ไปยังบัญชีในสหรัฐอเมริกา</Text>
              <Text style={styles.textDesc}>6. ท่านไม่มีการมอบอำนาจหรือให้อำนาจในการลงลายมือชื่อ แก่บุคคลที่มีที่อยู่ในสหรัฐอเมริกา เพื่อการใดๆ ที่เกี่ยวข้อง กับบัญชีที่เปิดไว้หรือมีอยู่กับ บริษัท ออมแพลทฟอร์ม จำกัด</Text>
              <Text style={styles.textDesc}>7. ท่านไม่มีที่อยุ่เพื่อการติดต่อหรือดำเนินการเกี่ยวกับบัญชี ที่เปิดไว้หรือมีอยู่กับ บริษัท ออมแพลทฟอร์ม จำกัด แต่เพียงที่อยู่เดียวซึ่งเป็นที่อยู่ในสหรัฐอเมริกา สำหรับ รับไปรษณีย์แทนหรือสำหรับการส่งต่อ</Text>
              <Text style={styles.textDesc}>8. ท่านไม่มีที่อยู่อาศัยในปัจจุบัน หรือที่อยู่อาศัย เพื่อการติดต่อในสหรัฐอเมริกา สำหรับบัญชีที่เปิดไว้ หรือมีอยู่กับ บริษัท ออมแพลทฟอร์ม จำกัด</Text>
              <Text style={styles.textDesc}>9. ท่านไม่มีหมายเลขโทรศัพท์ในสหรัฐอเมริกา เพื่อการติดต่อท่านหรือบุคคลอื่นที่เกี่ยวข้องกับบัญชีที่เปิด ไว้หรือมีอยู่กับ บริษัท ออมแพลทฟอร์ม จำกัด</Text>

              <Text style={[styles.textMain, { marginBottom: 10 }]}>{`หากข้อมูลส่วนใดส่วนหนึ่งหรือทั้งหมดไม่ถูกต้อง กรุณาติดต่อ `}
                <Text style={[styles.underline, { marginLeft: 5 }]} onPress={this.contactSupport}>support@omplatform.com</Text>
              </Text>
              <Text style={[styles.textTitle, { marginTop: 10 }]}>ส่วนที่ 2: การยืนยันและการเปลี่ยนแปลงสถานะ</Text>
              <Text style={styles.textDesc}>1. ท่านยืนยันว่า ข้อความข้างต้นเป็นความจริง ถูกต้อง และสมบูรณ์ครบถ้วน</Text>
              <Text style={styles.textDesc}>2. ท่านรับทราบและตกลงว่า หากท่านมีสถานะเป็นบุคคลอเมริกัน แต่ข้อมูลที่ให้ตามแบบฟอร์มนี้ หรือตามแบบฟอร์ม W-9 เป็นข้อมูลอันเท็จ ไม่ถูกต้อง หรือไม่ครบถ้วนสมบูรณ์ บริษัทมีสิทธิในการใช้ดุลยพินิจอย่างเด็ดขาดแต่เพียงฝ่ายเดียว โดยไม่จำต้องบอกกล่าวล่วงหน้า ที่จะยุติการให้ดำเนินธุรกรรม การให้บริการ หรือ ความสัมพันธ์ทางธุรกิจกับท่าน ไม่ว่า ทั้งหมดหรือบางส่วน ตามที่บริษัทเห็นสมควร</Text>
              <Text style={styles.textDesc}>3. ท่านตกลงจะแจ้งให้บริษัท ทราบและนำส่งเอกสารประกอบให้แก่ บริษัท ภายใน 30 วัน หลังจากมีเหตุการณ์เปลี่ยนแปลงอันทำให้ข้อมูลของท่านที่ระบุในแบบฟอร์มนี้ไม่ถูกต้อง</Text>
              <Text style={styles.textDesc}>4. ท่านรับทราบและตกลงว่า ในกรณีที่ท่านไม่ได้ดำเนินการตามข้อ 3 ข้างต้น หรือมีการนำส่งข้อมูลอันเป็นเท็จ ไม่ถูกต้อง หรือไม่ครบถ้วนสมบูรณ์เกี่ยวกับสถานะของท่าน บริษัทมีสิทธิใช้ดุลยพินิจอย่างเด็ดขาดแต่เพียงฝ่ายเดียวโดยไม่จำต้องบอกกล่าวล่วงหน้า ที่จะยุติการให้บริการกระเป๋าเงิน (wallet)/ความสัมพันธ์ทางธุรกิจกับท่าน ไม่ว่าทั้งหมดหรือบางส่วน ตามที่บริษัทเห็นสมควร</Text>

              <Text style={[styles.textTitle, { marginTop: 10 }]}>ส่วนที่ 3: การยินยอมให้เปิดเผยข้อมูลและการหักบัญชี</Text>
              <Text style={[styles.textDesc, { marginBottom: 10 }]}>ท่านตกลงให้ความยินยอมที่ไม่อาจยกเลิกเพิกถอนแก่ บริษัท ในการดำเนินการดังต่อไปนี้</Text>
              <Text style={styles.textDesc}>1. เปิดเผยข้อมูลต่างๆ ของท่านให้แก่บริษัท บริษัทในเครือของบริษัท และ/หรือ บริษัทตัวแทน และ/หรือ สถาบันการเงินอื่น เพื่อประโยชน์ในการทำธุรกรรมทางการเงินของท่าน เพื่อประโยชน์ในการปฏิบัติตาม FATCA หน่วยงานจัดเก็บภาษีอากรในประเทศ และ/หรือต่างประเทศ ซึ่งรวมถึงหน่วยงานจัดเก็บภาษีอากรของสหรัฐอเมริกา (Internal Revenue Service: IRS) ข้อมูลดังกล่าวรวมถึง ชื่อลูกค้า ที่อยู่ เลขประจำตัวผู้เสียภาษี หมายเลขบัญชี สถานะตามหลักเกณฑ์เรื่อง FATCA (คือ เป็นผู้ปฏิบัติตาม หรือผู้ให้ความร่วมมือ) จำนวนเงินหรือมูลค่าคงเหลือในบัญชี การจ่ายเงินเข้า-ออกจากบัญชี รายการเคลื่อนไหวทางบัญชี จำนวนเงิน ประเภทและมูลค่าของสินทรัพย์ดิจิทัล และ/หรือ ทรัพย์สินอื่นๆ ที่มีอยู่กับบริษัท ตลอดจนจำนวนรายได้ และข้อมูลอื่นๆ ที่เกี่ยวกับความสัมพันธ์ทางการเงิน/ทางธุรกิจ ที่บริษัท บริษัทในเครือของบริษัท และ/หรือ ตัวแทน และ/หรือ หน่วยงานทางภาษีอากรในประเทศ และ/หรือ ต่างประเทศ ซึ่งรวมถึง IRS อาจต้องดำเนินการเปิดเผย ตามกฎหมายที่เกี่ยวข้อง</Text>
              <Text style={styles.textDesc}>2. หักเงินจากบัญชีของท่าน และ/หรือ เงินที่ท่านได้รับจากหรือผ่านบริษัท ในจำนวนที่กำหนดโดยหน่วยงานจัดเก็บภาษีอากรในประเทศ และ/หรือ ต่างประเทศ ซึ่งรวมถึง IRS ภายใต้บทบัญญัติของกฎหมาย และ/หรือ กฎเกณฑ์ต่างๆ รวมถึงข้อตกลงใดๆ ระหว่าง บริษัท กับหน่วยงานเก็บภาษีอากรดังกล่าว</Text>
              <Text style={styles.textDesc}>3. ให้บุคคล หรือ หน่วยงานอื่น ใช้เอกสารข้อมูล คำยืนยัน และคำยินยอมใดๆ เกี่ยวกับการแสดงตนและการเปิดเผยข้อมูล/การหักภาษี ณ ที่จ่าย (รวมถึงเอกสารฉบับนี้และเอกสารที่อ้างถึง) ได้ ตามกฎหมายที่เกี่ยวข้องทั้งในและต่างประเทศ (รวมถึงกฎหมาย FATCA และกฎหมายการฟอกเงิน) เสมือนหนึ่งว่า ท่านได้มอบเอกสารและข้อมูลนั้น กับบุคคลดังกล่าวด้วยตนเอง และ บุคคลผู้ได้รับข้อมูลดังกล่าว สามารถใช้ และเผยแพร่ข้อมูลดังกล่าวได้ต่อเนื่อง ภายใต้ กรอบกฎหมายที่เกี่ยวข้อง</Text>
            </View>
          </ScrollView>
        </View>

        <View style={{ flex: 30 }}>
          <View style={{
            display: 'flex',
            flexDirection: 'row',
            marginRight: 20,
            marginBottom: 20,
            marginLeft: 20,
            marginTop: 5
          }}
          >
            <View style={{ flex: 15, justifyContent: 'center', alignItems: 'center' }}>
              <CircleCheckBox
                checked={this.state.checked}
                onToggle={(checked) => { this.setState({ checked }) }}
                outerColor="#535786"
                innerColor="#535786"
                outerSize={40}
                filterSize={35}
              />
            </View>
            <View style={{ flex: 85 }}>
              <Text style={[styles.acceptText, { marginLeft: 12 }]}>
                ข้าพเจ้าขอรับรองว่าข้อมูลและเอกสารที่ได้ส่งให้แก่ทาง Om Platform ถูกต้อง เป็นความจริงและข้าพเจ้าตกลง ให้ความ ยินยอม รวมถึงให้การอนุญาตตามเงื่อนไขที่ กำหนดทั้งหมด
              </Text>
            </View>
          </View>
        </View>

        <TouchableOpacity
          style={disabled ? styles.disabledBtn : styles.button}
          onPress={this.onVerification}
          disabled={disabled}
        >
          <Text style={styles.buttonText}>ยืนยันตัวตน</Text>
        </TouchableOpacity>

        {
          this.state.uploading && !this.state.complete &&
          <Loading title="กำลังส่งข้อมูล" />
        }
      </View >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  scroll: {
    flex: 70,
    marginHorizontal: 20,
    marginBottom: 20,
    marginTop: 22,
    borderColor: '#DDDDDD',
    borderWidth: 1
  },
  underline: {
    textDecorationLine: 'underline'
  },
  textDesc: {
    color: '#000000',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 13
  },
  textTitle: {
    color: '#000000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 13
  },
  textSpecial: {
    color: AppStyle.colorRed,
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 13
  },
  textMain: {
    color: '#535786',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 13
  },
  fetchaContainer: {
    marginRight: 20,
    marginBottom: 20,
    marginLeft: 20,
    marginTop: 20
  },
  button: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16,
    marginBottom: 30,
    marginHorizontal: 25,
    height: 60,
    backgroundColor: '#535786',
    borderRadius: 8
  },
  buttonText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20,
    color: '#FFF'
  },
  disabledBtn: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16,
    marginBottom: 30,
    marginHorizontal: 25,
    height: 60,
    backgroundColor: '#D3D3D3',
    borderRadius: 8
  },
  acceptText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 13,
    color: '#909090'
  }
})
