import React, { Component } from 'react'
import {
  Alert,
  BackHandler,
  Image,
  Keyboard,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from 'react-native'

// Third party
import BigNumber from 'bignumber.js'

// Components & Modules
import api from '../../../api'
import icons from '../../../commons/icons'
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import Constants from '../../../commons/constant'
import LayoutUtils from '../../../commons/LayoutUtils'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'
import Spinner from '../../../components/elements/Spinner'

const marginTop = LayoutUtils.getExtraTop()
const title = 'ซื้อ Om'

export default class BuyAomScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      minAmount: 50,
      dirty: false,
      amount: '',
      loading: false
    }

    this.addAmount = this.addAmount.bind(this)
    this.goBack = this.goBack.bind(this)
    this.goToStepTwo = this.goToStepTwo.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
    this.onInputAmount = this.onInputAmount.bind(this)
    this.onTouchDismissKeyboard = this.onTouchDismissKeyboard.bind(this)
    this.validateLimit = this.validateLimit.bind(this)

    // Set fabric answer
    this.answers = MainStore.appState.fabric.answers
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  onInputAmount(amount) {
    const parseAmount = amount.replace(/[^0-9]+/g, '')

    if (parseAmount) {
      this.setState({
        amount: parseInt(parseAmount, 10),
        dirty: true
      })
    } else {
      this.setState({
        amount: '',
        dirty: true
      })
    }
  }

  onTouchDismissKeyboard() {
    Keyboard.dismiss()
  }

  get buyLimit() {
    const { kycLayerTwo, kycApproved, userInfo } = MainStore.appState

    if (kycLayerTwo && kycApproved) {
      return new BigNumber(1e5).toNumber()
    }

    // Get topup total
    const { topup_total } = userInfo

    // Get max buy
    const max = MainStore.appState.remoteConfig.getValue('kyc1max_buy')
    return new BigNumber(topup_total || 0).minus(max).abs().toNumber()
  }

  get invalid() {
    const amount = new BigNumber(this.state.amount)
    if (!this.state.dirty) {
      return false
    } else if (amount.isNaN()) {
      return 'กรุณากรอกตัวเลข'
    } else if (amount.lt(this.state.minAmount)) {
      return 'เริ่มต้นขั้นต่ำที่ 50 บาท'
    } else if (amount.gt(this.buyLimit)) {
      return `บัญชีท่านจำกัดการซื้อไม่เกิน ${this.buyLimit} บาท`
    }

    return false
  }

  addAmount(value) {
    let amount = Number(this.state.amount)
    if (!isFinite(amount)) {
      amount = value
    } else {
      amount += value
    }

    // Log Answers
    this.answers.logAddToCart(amount, 'THB', `Buy +${amount} OM`, `OM`)

    if (amount < this.state.minAmount) {
      amount = this.state.minAmount
    }

    this.setState({
      amount,
      dirty: true
    })
  }

  goBack() {
    const { kycLayerOne } = MainStore.appState

    if (kycLayerOne) {
      NavStore.pushToScreen('DashboardScreen')
    } else {
      NavStore.pushToScreen('KYCScreen')
    }
  }

  goToStepTwo() {
    const { address } = MainStore.appState.selectedWallet

    // Set state
    this.setState({
      loading: true
    })

    // Set API
    api.topup(address, this.state.amount).then((res) => {
      this.setState({
        loading: false
      })

      const { status, data } = res
      const promptpay = data.THB.account

      if (!data) {
        Alert.alert(
          'แจ้งเตือน',
          Constants.CONNECTION_PROBLEM,
          [
            {
              text: 'ปิด'
            }
          ]
        )
        return false
      }

      // limit exceed
      if (status == 400) {
        Alert.alert(
          'แจ้งเตือน',
          data.message,
          [
            {
              text: 'ปิด'
            }
          ]
        )
      } else if (status == 405) {
        Alert.alert(
          'แจ้งเตือน',
          Constants.PAYMENT_LIMIT_EXCEED,
          [
            {
              text: 'ปิด'
            }
          ]
        )
      } else if (status == 408) {
        Alert.alert(
          'แจ้งเตือน',
          Constants.PAYMENT_RECEIPT_FAIL,
          [
            {
              text: 'ปิด'
            }
          ]
        )
      }

      NavStore.pushToScreen('BuyAomSecondScreen', {
        gateway: data,
        promptpay
      })
      this.answers.logContentView('Buy Om', 'screen', 'buyom-screen')

      return true
    }).catch((res) => {
      this.setState({
        loading: false
      })
      console.log(res)
      Alert.alert(
        'แจ้งเตือน',
        res.message,
        [
          {
            text: 'ปิด'
          }
        ]
      )
    })
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  validateLimit() {
    const { amount } = this.state

    if (amount[0] !== '0' && amount > this.buyLimit) {
      this.setState({ amount: this.buyLimit })
    }

    if (amount[0] === '0') {
      this.setState({ amount: parseInt(amount, 10) })
    }
  }

  render() {
    const invalid = Boolean(this.invalid)
    const { loading } = this.state

    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback
          onPress={this.onTouchDismissKeyboard}
        >
          <View style={styles.container}>
            <StatusBar
              backgroundColor="transparent"
              barStyle="dark-content"
              translucent
            />
            <NavigationHeader
              style={{
                marginTop: marginTop + 20,
                marginBottom: 20
              }}
              headerItem={{
                title,
                icon: null,
                button: images.iconBackAlt
              }}
              titleStyle={{
                color: '#000',
                fontFamily: AppStyle.mainFontBoldTH,
                textAlign: 'center',
                flex: 1,
                marginRight: 20
              }}
              action={this.goBack}
            />

            {/* Begin Buy OM */}
            <View style={{ flex: 1 }}>

              <View style={{
                height: 94,
                margin: 20
              }}
              >
                <Text style={styles.inputDescribeText}>
                  จำนวน (บาท)
                </Text>

                <View style={{
                  flex: 1,
                  flexDirection: 'row',
                  height: 58,
                  borderColor: '#CBCCD9',
                  borderWidth: 1,
                  borderRadius: 13,
                  justifyContent: 'center',
                  marginTop: 5
                }}
                >
                  <View style={{
                    flex: 90,
                    height: 58,
                    justifyContent: 'center',
                    marginTop: 5
                  }}
                  >
                    <TextInput
                      autoCapitalize="none"
                      keyboardType="numeric"
                      maxLength={8}
                      onChangeText={(value) => { this.onInputAmount(value) }}
                      onBlur={this.validateLimit}
                      returnKeyType="done"
                      selectTextOnFocus={true}
                      style={styles.inputAmount}
                      underlineColorAndroid="transparent"
                      value={this.state.amount.toString()}
                      placeholder="0"
                    />
                  </View>

                  <View style={{
                    flex: 10,
                    height: 58,
                    justifyContent: 'center',
                    marginTop: 3,
                    marginRight: 5
                  }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ amount: 0 })
                      }}
                    >
                      <Image
                        style={{
                          width: 25,
                          height: 25
                        }}
                        source={icons.iconResetBtn}
                      />
                    </TouchableOpacity>
                  </View>

                </View>
              </View>

              {
                this.invalid !== false && (
                  <View >
                    <Text style={{
                      color: '#C30000',
                      marginBottom: 10,
                      fontFamily: AppStyle.mainFontTH,
                      fontSize: 14,
                      textAlign: 'center'
                    }}
                    >
                      {this.invalid}
                    </Text>
                  </View>
                )
              }

              <View style={{ flex: 1 }}>
                <View style={{ flexDirection: 'row', marginBottom: 8 }}>
                  <View style={{
                    flex: 1,
                    justifyContent: 'flex-end',
                    alignItems: 'flex-end',
                    marginRight: 4
                  }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.addAmount(50)
                      }}
                    >
                      <Image
                        style={{
                          width: 155,
                          height: 44
                        }}
                        source={icons.iconBuy50Aom}
                        resizeMode="center"
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={{
                    flex: 1,
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    marginLeft: 4
                  }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.addAmount(100)
                      }}
                    >
                      <Image
                        style={{
                          width: 155,
                          height: 44
                        }}
                        source={icons.iconBuy100Aom}
                        resizeMode="center"
                      />
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={{ flexDirection: 'row', marginBottom: 8 }}>
                  <View style={{
                    flex: 1,
                    justifyContent: 'flex-end',
                    alignItems: 'flex-end',
                    marginRight: 4
                  }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.addAmount(500)
                      }}
                    >
                      <Image
                        style={{
                          width: 155,
                          height: 44
                        }}
                        source={icons.iconBuy500Aom}
                        resizeMode="center"
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={{
                    flex: 1,
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    marginLeft: 4
                  }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.addAmount(1000)
                      }}
                    >
                      <Image
                        style={{
                          width: 155,
                          height: 44
                        }}
                        source={icons.iconBuy1000Aom}
                        resizeMode="center"
                      />
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={{ flexDirection: 'row', marginBottom: 8 }}>
                  <View style={{
                    flex: 1,
                    justifyContent: 'flex-end',
                    alignItems: 'flex-end',
                    marginRight: 4
                  }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.addAmount(5000)
                      }}
                    >
                      <Image
                        style={{
                          width: 155,
                          height: 44
                        }}
                        source={icons.iconBuy5000Aom}
                        resizeMode="center"
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={{
                    flex: 1,
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    marginLeft: 4
                  }}
                  >
                    <TouchableOpacity
                      onPress={() => {
                        this.addAmount(10000)
                      }}
                    >
                      <Image
                        style={{
                          width: 155,
                          height: 44
                        }}
                        source={icons.iconBuy10000Aom}
                        resizeMode="center"
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{ flex: 40 }} />
              </View>

              <View style={{ height: 60 }}>
                <TouchableOpacity
                  disabled={invalid || loading}
                  style={[styles.containerConfirmBtn, invalid ? styles.touchDisabled : '']}
                  onPress={this.goToStepTwo}
                >
                  <Text style={styles.confirmText}>
                    ยืนยันจำนวน
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            {/* End Buy OM */}

            {
              this.state.loading &&
              <Spinner />
            }
          </View>
        </TouchableWithoutFeedback >
      </View >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  inputAmount: {
    marginLeft: 20,
    marginRight: 20,
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18,
    color: '#141527'
  },
  inputDescribeText: {
    marginBottom: 3,
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14,
    color: '#121212'
  },
  containerConfirmBtn: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#535786'
  },
  confirmText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 22,
    color: '#DDD'
  },
  touchDisabled: {
    backgroundColor: '#AAA'
  }
})
