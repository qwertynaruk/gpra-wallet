import React, { Component } from 'react'
import {
  Alert,
  BackHandler,
  Linking,
  Dimensions,
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { computed } from 'mobx'
import { observer } from 'mobx-react/native'
import CircleCheckBox from 'react-native-circle-checkbox'
import ImagePicker from 'react-native-image-picker'
import ImageResizer from 'react-native-image-resizer'
import RNFS from 'react-native-fs'

// Components & Modules
import api from '../../../api'
import images from '../../../commons/images'
import icons from '../../../commons/icons'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import Loading from '../element/Loading'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const { width } = Dimensions.get('window')
const marginTop = LayoutUtils.getExtraTop()
const title = 'ยืนยันตัวตนเต็มรูปแบบ'

@observer
class KYCLayerTwoPageThreeFetchaBScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      complete: false,
      uploading: false,
      checked: false,
      fatcaForm: null
    }

    this.goBack = this.goBack.bind(this)
    this.goNext = this.goNext.bind(this)
    this.onDownload = this.onDownload.bind(this)
    this.onVerication = this.onVerication.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
    this.openImageLibrary = this.openImageLibrary.bind(this)

    this.answers = MainStore.appState.fabric.answers
  }

  componentWillMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  onDownload() {
    api.Aom.getFATCAForm().then((res) => {
      Linking.openURL(res.data.data)
    })
  }

  onVerication() {
    const { address } = MainStore.appState.selectedWallet
    const { kycData } = MainStore.appState
    const data = {
      address,
      img_id_card: {
        name: 'id_card.png',
        data: kycData.idCardFront
      },
      img_id_card_back: {
        name: 'id_card_back.png',
        data: kycData.idCardBack
      },
      img_selfie: {
        name: 'selfie.png',
        data: kycData.selfie
      },
      img_fatca: {
        name: 'fatca.png',
        data: this.state.fatcaForm.data
      },
      id_card_address: kycData.address,
      contact_address: kycData.contactAddress,
      career: kycData.career,
      income_range: kycData.salary,
      company_name: kycData.company,
      company_address: kycData.companyAddress,
      laser_code: kycData.numberIDCardBack
    }

    this.setState({ uploading: true })
    api.Aom.kycVerication(data).then((res) => {
      if (res.status === 200) {
        MainStore.appState.setKycLayerTwo(true)
        this.setState({ uploading: false, complete: true })
        this.goNext()
      }
    })
  }

  setImageFile(src) {
    const {
      path, uri
    } = src
    const filePath = Platform.OS === 'ios' ? uri : path

    ImageResizer.createResizedImage(filePath, 900, 900, 'JPEG', 80, 0)
      .then(res => RNFS.readFile(res.uri, 'base64'))
      .then((base64) => {
        this.setState({
          fatcaForm: {
            name: 'fatca_form.jpg',
            path: filePath,
            data: base64
          }
        })
      })
      .catch((err) => {
        const wallet = MainStore.appState.selectedWallet
        this.answers.logCustom('KYCLayerTwoPageThreeFetchaBScreen setImageFile() ERROR', {
          address: wallet.address,
          error: err.message
        })
      })
  }

  @computed get disabled() {
    if (
      this.state.checked && this.state.fatcaForm
    ) {
      return false
    }

    return true
  }

  handleBackPress() {
    if (!this.state.uploading && !this.state.complete) {
      this.goBack()
    }

    return true
  }

  goBack() {
    NavStore.goBack()
  }

  goNext() {
    NavStore.pushToScreen('KYCPending')
  }

  pickPhotosFromGallery = () => {
    NavStore.preventOpenUnlockScreen = true
    const options = {
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    }

    ImagePicker.launchImageLibrary(options, (res) => {
      if (res.error) {
        const wallet = MainStore.appState.selectedWallet
        this.answers.logCustom('KYCLayerTwoPageThreeFetchaBScreen pickPhotosFromGallery() ERROR', {
          address: wallet.address,
          error: res.error
        })

        Alert.alert(res)
      } else if (!res.didCancel) {
        this.setImageFile(res)
      }
    })
  }

  openImageLibrary() {
    NavStore.preventOpenUnlockScreen = true
    this.pickPhotosFromGallery()
  }

  render() {
    const { disabled } = this

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: marginTop + 20,
            paddingHorizontal: 20,
            width
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}

          titleStyle={{
            color: '#000',
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.goBack}
          rightView={{
            rightViewTitle: '3/3',
            styleTitle: { fontFamily: AppStyle.mainFont, fontSize: 16, color: '#CBCCD9' }
          }}
        />
        <ScrollView
          style={styles.container}
          showsHorizontalScrollIndicator={false}
        >
          <View style={{ marginTop: 46, paddingHorizontal: 25 }}>
            <Text style={styles.title}>ขั้นตอนที่ 1</Text>
            <Text style={[styles.textDesc, { marginTop: 6 }]}>ดาวน์โหลดและกรอกแบบฟอร์ม FATCHA</Text>
            <View>
              <TouchableOpacity
                style={styles.buttonDownload}
                onPress={this.onDownload}
              >
                <View style={{ flexDirection: 'row' }}>
                  <Image
                    style={{
                      marginTop: Platform.OS === 'android' ? 5 : 2.5,
                      marginHorizontal: 5
                    }}
                    source={icons.iconDownload}
                  />
                  <Text style={styles.buttonText}>
                    Download
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{ marginTop: 30, borderBottomColor: '#F1F1F1', borderBottomWidth: 1 }} />

          <View style={{ marginTop: 26, paddingHorizontal: 25 }}>
            <Text style={styles.title}>ขั้นตอนที่ 2</Text>
            <View style={{ flexDirection: 'row', marginTop: 10 }}>
              <View style={styles.leftFlexColumn}>
                <TouchableOpacity
                  activeOpacity={0.5}
                  style={[styles.uploadButton, { marginBottom: 15 }]}
                  onPress={() => {
                    this.openImageLibrary()
                  }}
                >
                  <ImageBackground
                    source={images.iconUploadBtn}
                    style={styles.imageUploadBg}
                    resizeMode="stretch"
                  >
                    {
                      this.state.fatcaForm &&
                      <Image
                        resizeMode="cover"
                        style={{
                          width: '100%', height: '100%', borderWidth: 1, borderColor: '#DDD'
                        }}
                        source={{ uri: `file://${this.state.fatcaForm.path}` }}
                      />
                    }
                  </ImageBackground>
                </TouchableOpacity>
              </View>
              <View style={styles.rightFlexColumn}>
                <Text style={styles.textDesc}>{`*Allowed file types pdf,\njpg, jpeg, bmp, gif, tif,\ntiff, png`}</Text>
              </View>
            </View>
          </View>

          <View style={{ marginTop: 30, borderBottomColor: '#F1F1F1', borderBottomWidth: 1 }} />

          <View style={{ marginTop: 14, paddingHorizontal: 25 }}>
            <Text style={{
              paddingTop: Platform.OS === 'ios' ? 5 : 0,
              fontFamily: AppStyle.mainFontTH,
              fontSize: 16,
              lineHeight: Platform.OS === 'android' ? 36 : 0,
              color: '#535786'
            }}
            >
              กลับไปยังหน้า FATCA ก่อนหน้านี้
            </Text>

            <View style={{ display: 'flex', flexDirection: 'row', marginTop: 16 }}>
              <View style={{ flex: 15, justifyContent: 'center', alignItems: 'center' }}>
                <CircleCheckBox
                  checked={this.state.checked}
                  onToggle={(checked) => { this.setState({ checked }) }}
                  outerColor="#535786"
                  innerColor="#535786"
                  outerSize={40}
                  filterSize={35}
                />
              </View>
              <View style={{ flex: 85 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={[styles.acceptText, { marginLeft: 12 }]}>
                    ข้าพเจ้าขอรับรองว่าข้อมูลและเอกสารที่ได้ส่งให้แก่ทาง Om Platform ถูกต้อง เป็นความจริงและข้าพเจ้าตกลง ให้ความ ยินยอม รวมถึงให้การอนุญาตตามเงื่อนไขที่ กำหนดทั้งหมด
                  </Text>
                </View>
              </View>
            </View>
          </View>

          <TouchableOpacity
            onPress={this.onVerication}
            disabled={disabled}
            style={disabled ? styles.disabledBtn : styles.button}
          >
            <Text style={styles.buttonText}>ยืนยันตัวตน</Text>
          </TouchableOpacity>
        </ScrollView>

        {
          this.state.uploading && !this.state.complete &&
          <Loading title="กำลังบันทึกข้อมูล" />
        }
      </View >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14,
    color: '#141527'
  },
  textDesc: {
    color: '#333',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16
  },
  uploadButton: {
    height: 100,
    width: 100
  },
  button: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 18,
    marginBottom: 30,
    marginHorizontal: 25,
    height: 60,
    backgroundColor: '#535786',
    borderRadius: 8
  },
  buttonText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20,
    color: '#FFF'
  },
  disabledBtn: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16,
    marginBottom: 30,
    marginHorizontal: 25,
    height: 60,
    backgroundColor: '#D3D3D3',
    borderRadius: 8
  },
  buttonDownload: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 8,
    width: 180,
    height: 42,
    backgroundColor: '#535786',
    borderRadius: 8
  },
  imageUploadBg: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    height: '100%'
  },
  leftFlexColumn: {
    flex: 40
  },
  rightFlexColumn: {
    flex: 60
  },
  acceptText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 13,
    color: '#000'
  }
})

export default KYCLayerTwoPageThreeFetchaBScreen
