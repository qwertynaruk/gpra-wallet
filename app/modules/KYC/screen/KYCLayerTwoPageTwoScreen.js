import React, { Component } from 'react'
import {
  Alert,
  BackHandler,
  Dimensions,
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text, TextInput,
  TouchableOpacity,
  View
} from 'react-native'

// Third-party
import { computed } from 'mobx'
import { observer } from 'mobx-react/native'
import { KeyboardAccessoryNavigation } from 'react-native-keyboard-accessory'

import ImagePicker from 'react-native-image-picker'
import ImageResizer from 'react-native-image-resizer'
import Permissions from 'react-native-permissions'
import RNFS from 'react-native-fs'

// Components & Modules
import api from '../../../api'
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const { width } = Dimensions.get('window')
const marginTop = LayoutUtils.getExtraTop()
const title = 'ยืนยันตัวตนเต็มรูปแบบ'

@observer
class KYCLayerTwoPageTwoScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      complete: false,
      uploading: false,
      idCardFront: null,
      idCardBack: null,
      numberIDCardBack: '',
      numberIDCardBackFocused: false,
      invalidNumberIDCardBack: false,
      selfie: null,
      type: ''
    }

    this.goBack = this.goBack.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
    this.openImageLibrary = this.openImageLibrary.bind(this)
    this.goNext = this.goNext.bind(this)
    this.handleOnFocus = this.handleOnFocus.bind(this)
    this.handleOnBlur = this.handleOnBlur.bind(this)
    this.handleOnSubmitEditing = this.handleOnSubmitEditing.bind(this)
    this.answers = MainStore.appState.fabric.answers

    this.kycData = MainStore.appState.kycData
  }

  componentWillMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  setImageFile(src) {
    const {
      path, uri
    } = src
    const filePath = Platform.OS === 'ios' ? uri : path

    // Resize image
    ImageResizer.createResizedImage(filePath, 900, 900, 'JPEG', 80, 0)
      .then(res => RNFS.readFile(res.uri, 'base64'))
      .then((base64) => {
        if (this.state.type === 'id_card_front') {
          this.setState({
            idCardFront: {
              name: 'id_card_front.jpg',
              path: filePath,
              data: base64
            }
          })
        } else if (this.state.type == 'id_card_back') {
          this.setState({
            idCardBack: {
              name: 'id_card_back.jpg',
              path: filePath,
              data: base64
            }
          })
        } else if (this.state.type == 'selfie') {
          this.setState({
            selfie: {
              name: 'selfie.jpg',
              path: filePath,
              data: base64
            }
          })
        }
      })
      .catch((err) => {
        // Get wallet
        const wallet = MainStore.appState.selectedWallet
        api.Aom.reportBug({
          address: wallet.address,
          error_component: 'KYCLayerTwoPageTwoScreen',
          error_function: 'setImageFile',
          error_detail: err.message
        })
      })
  }

  @computed get disabled() {
    if (
      this.state.idCardFront &&
      this.state.idCardBack &&
      this.state.numberIDCardBack &&
      this.state.numberIDCardBack.length === 12 &&
      this.state.selfie
    ) {
      return false
    }

    return true
  }

  _alertForPhotosPermission() {
    Alert.alert(
      'Can we access your photos?',
      'We need access so you can upload your document',
      [
        {
          text: `Don't allow`,
          style: 'cancel'
        },
        { text: 'Allow', onPress: this._requestPermission }
      ]
    )
  }

  _alertForPhotosPermissionDenied() {
    if (Platform.OS === 'ios') {
      Alert.alert(
        '"OM Wallet" needs permission to access your photos to upload your document',
        'Please go to Setting > OM Wallet > Photos > Read and Write',
        [
          {
            text: `Don't allow`,
            style: 'cancel'
          },
          {
            text: 'Open Settings',
            onPress: Permissions.openSettings
          }
        ]
      )
    } else {
      this._alertForPhotosPermission()
    }
  }

  _alertForPhotosPermissionRestricted() {
    if (Platform.OS === 'ios') {
      Permissions.check('photo').then((response) => {
        if (response === 'authorized') {
          this.pickPhotosFromGallery()
        } else {
          Alert.alert(
            '"OM Wallet" needs permission to access your photo library to upload your document.',
            'Please go to Setting > OM Wallet > Photos > Read / Write',
            [
              {
                text: `Don't allow`,
                style: 'cancel'
              },
              {
                text: 'Open Settings',
                onPress: Permissions.openSettings
              }
            ]
          )
        }
      })
    } else {
      Permissions.check('photo').then((response) => {
        if (response === 'authorized') {
          this.pickPhotosFromGallery()
        } else {
          Alert.alert(
            '"OM Wallet" needs permission to access your photo library to upload your document.',
            'Please go to Setting > Apps > OM Wallet > Permission > Photo \n Then close and open "AOM" application',
            [
              {
                text: `Don't allow`,
                style: 'cancel'
              },
              {
                text: 'OK'
              }
            ]
          )
        }
      })
    }
  }

  _requestPermission = () => {
    NavStore.preventOpenUnlockScreen = true
    Permissions.check('photo').then((response) => {
      if (response != 'authorized') {
        Permissions.request('photo').then((res) => {
          if (res == 'authorized') {
            this._requestCameraPermission()
          }
        })
      }
    })
  }

  _requestCameraPermission = () => {
    NavStore.preventOpenUnlockScreen = true
    Permissions.check('camera').then((res) => {
      if (res == 'authorized') {
        this.pickPhotosFromGallery()
      } else {
        Permissions.request('camera').then((resp) => {
          if (resp == 'authorized') {
            this.pickPhotosFromGallery()
          }
        })
      }
    })
  }

  _validationIDCard = () => {
    const reg = /^[A-Z][A-Z][0-9]{10}$/
    const valid = reg.test(this.state.numberIDCardBack)

    if (valid && this.state.numberIDCardBack.trim().length === 12) {
      this.setState({ invalidNumberIDCardBack: false })
    } else {
      this.setState({ invalidNumberIDCardBack: true })
    }
  }

  handleBackPress() {
    if (!this.state.uploading && !this.state.complete) this.goBack()
    return true
  }

  goBack() {
    NavStore.goBack()
  }

  goNext() {
    const {
      idCardFront,
      idCardBack,
      numberIDCardBack,
      selfie
    } = this.state
    const { kycData } = MainStore.appState
    MainStore.appState.kycData = {
      ...kycData,
      idCardFront: idCardFront.data,
      idCardBack: idCardBack.data,
      numberIDCardBack,
      selfie: selfie.data
    }

    NavStore.pushToScreen('KYCLayerTwoPageThreeFetchaAScreen')
  }

  handleOnSubmitEditing = name => () => {
    if (name === 'numberIDCardBack') {
      this.setState({ numberIDCardBackFocused: false })
    }
  }

  handleOnBlur = name => () => {
    if (name === 'numberIDCardBack') {
      this.setState({ numberIDCardBackFocused: false })
      this._validationIDCard()
    }
  }

  handleOnFocus = name => () => {
    if (name === 'numberIDCardBack') {
      this.setState({ numberIDCardBackFocused: true })
    }
  }

  pickPhotosFromGallery = () => {
    NavStore.preventOpenUnlockScreen = true
    const options = {
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    }

    ImagePicker.launchImageLibrary(options, (res) => {
      if (res.error) {
        const wallet = MainStore.appState.selectedWallet
        api.Aom.reportBug({
          address: wallet.address,
          error_component: 'KYCLayerTwoPageTwoScreen',
          error_function: 'pickPhotosFromGallery',
          error_detail: res.error
        })

        Alert.alert(res)
      } else if (!res.didCancel) {
        this.setImageFile(res)
      }
    })
  }

  borderColor(focus, error) {
    if (error) {
      return {
        borderColor: '#C60003'
      }
    } else if (focus) {
      return {
        borderColor: '#4B5391'
      }
    }

    return {}
  }

  openImageLibrary() {
    NavStore.preventOpenUnlockScreen = true
    Permissions.check('photo').then((response) => {
      if (response == 'authorized') {
        this.pickPhotosFromGallery()
      } else if (response == 'undetermined') {
        this._alertForPhotosPermission()
      } else if (response == 'denied') {
        this._alertForPhotosPermissionDenied()
      } else if (response == 'restricted') {
        this._alertForPhotosPermissionRestricted()
      }
    })
  }

  render() {
    const { disabled } = this

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: marginTop + 20,
            paddingHorizontal: 20,
            width
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}

          titleStyle={{
            color: '#000',
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.goBack}
          rightView={{
            rightViewTitle: '2/3',
            styleTitle: { fontFamily: AppStyle.mainFont, fontSize: 16, color: '#CBCCD9' }
          }}
        />
        <ScrollView
          style={styles.container}
          showsHorizontalScrollIndicator={false}
        >
          <View style={{ marginTop: 26, paddingHorizontal: 25 }}>
            <Text style={styles.title}>ภาพด้านหน้าบัตรประชาชน</Text>
            <View style={{ flexDirection: 'row', marginTop: 6 }}>
              <View style={styles.leftColumn}>
                <TouchableOpacity
                  activeOpacity={0.5}
                  style={styles.uploadButton}
                  onPress={() => {
                    this.setState({
                      type: 'id_card_front'
                    })
                    this.openImageLibrary()
                  }}
                >
                  <ImageBackground
                    source={images.iconUploadBtn}
                    style={styles.imageUploadBg}
                    resizeMode="stretch"
                  >
                    {
                      this.state.idCardFront &&
                      <Image
                        resizeMode="cover"
                        style={{
                          width: '100%', height: '100%', borderWidth: 1, borderColor: '#DDD'
                        }}
                        source={{ uri: `file://${this.state.idCardFront.path}` }}
                      />
                    }
                  </ImageBackground>
                </TouchableOpacity>
              </View>
              <View style={styles.rightColumn}>
                <View style={{ flexDirection: 'column' }}>
                  <Text style={[styles.titleExample]}>ตัวอย่าง</Text>
                  <Image style={styles.icon} source={images.img_id_card_front} />
                </View>
              </View>
            </View>
          </View>

          <View style={{ marginTop: 30, borderBottomColor: '#F1F1F1', borderBottomWidth: 1 }} />

          <View style={{ marginTop: 26, paddingHorizontal: 25 }}>
            <Text style={styles.title}>ภาพด้านหลังบัตรประชาชน</Text>
            <View style={{ flexDirection: 'row', marginTop: 6 }}>
              <View style={styles.leftColumn}>
                <TouchableOpacity
                  activeOpacity={0.5}
                  style={styles.uploadButton}
                  onPress={() => {
                    this.setState({
                      type: 'id_card_back'
                    })
                    this.openImageLibrary()
                  }}
                >
                  <ImageBackground
                    source={images.iconUploadBtn}
                    style={styles.imageUploadBg}
                    resizeMode="stretch"
                  >
                    {
                      this.state.idCardBack &&
                      <Image
                        resizeMode="cover"
                        style={{
                          width: '100%', height: '100%', borderWidth: 1, borderColor: '#DDD'
                        }}
                        source={{ uri: `file://${this.state.idCardBack.path}` }}
                      />
                    }
                  </ImageBackground>
                </TouchableOpacity>
              </View>
              <View style={styles.rightColumn}>
                <View style={{ flexDirection: 'column' }}>
                  <Text style={[styles.titleExample]}>ตัวอย่าง</Text>
                  <Image style={styles.icon} source={images.img_id_card_back} />
                </View>
              </View>
            </View>

            <View style={{ marginTop: 16 }}>
              <Text style={styles.title}>
                เลขเลเซอร์หลังบัตรประจำตัวประชาชน
              </Text>
              <TextInput
                maxLength={12}
                onFocus={this.handleOnFocus('numberIDCardBack')}
                onBlur={this.handleOnBlur('numberIDCardBack')}
                onSubmitEditing={this.handleOnSubmitEditing('numberIDCardBack')}
                onChangeText={(numberIDCardBack) => {
                  this.setState({ numberIDCardBack })
                }}
                ref={(ref) => { this.idcardInput = ref }}
                returnKeyType="done"
                style={[styles.textInput, this.borderColor(this.state.numberIDCardBackFocused, this.state.invalidNumberIDCardBack), { marginTop: 6, paddingHorizontal: 25 }]}
                underlineColorAndroid="transparent"
                value={this.state.numberIDCardBack}
              />
            </View>
            {this.state.invalidNumberIDCardBack && (
              <Text style={styles.errorDescribeText}>
                กรุณากรอก เลขเลเซอร์หลังบัตรประจำตัวประชาชน ให้ถูกต้อง
              </Text>
            )}
          </View>

          <View style={{ marginTop: 30, borderBottomColor: '#F1F1F1', borderBottomWidth: 1 }} />

          <View style={{ marginTop: 26, paddingHorizontal: 25 }}>
            <Text style={styles.title}>เซลฟี่</Text>
            <View style={{ flexDirection: 'row', marginTop: 6 }}>
              <View style={styles.leftColumn}>
                <TouchableOpacity
                  activeOpacity={0.5}
                  style={styles.uploadButton}
                  onPress={() => {
                    this.setState({
                      type: 'selfie'
                    })
                    this.openImageLibrary()
                  }}
                >
                  <ImageBackground
                    source={images.iconUploadBtn}
                    style={styles.imageUploadBg}
                    resizeMode="stretch"
                  >
                    {
                      this.state.selfie &&
                      <Image
                        resizeMode="cover"
                        style={{
                          width: '100%', height: '100%', borderWidth: 1, borderColor: '#DDD'
                        }}
                        source={{ uri: `file://${this.state.selfie.path}` }}
                      />
                    }
                  </ImageBackground>
                </TouchableOpacity>
              </View>
              <View style={styles.rightColumn}>
                <View style={{ flexDirection: 'column' }}>
                  <Text style={[styles.titleExample]}>ตัวอย่าง</Text>
                  <Image style={styles.icon} source={images.img_selfie} />
                </View>
              </View>
            </View>

            <View style={{ marginTop: 16 }}>
              <Text style={styles.textDesc}>ถ่ายรูปหน้าของคุณที่ถือบัตรประชาชน</Text>
              <Text style={styles.textDesc}>(หรือ พาสปอร์ต สำหรับชาวต่างชาติ)</Text>
              <Text style={styles.textDesc}>พร้อมกระดาษที่เขียน Om Platform</Text>
              <Text style={styles.textDesc}>และลายเซ็นกับวันที่</Text>
              <Text style={styles.textDesc}>- ใบหน้าของคุณจะต้องไม่มีอะไรบดบัง</Text>
              <Text style={styles.textDesc}>- รายละเอียดบัตรประชาชนหรือพาสปอร์ต</Text>
              <Text style={styles.textDesc}>จะต้องชัดเจน</Text>
            </View>
          </View>

          <TouchableOpacity
            disabled={disabled}
            style={disabled ? styles.disabledBtn : styles.button}
            onPress={this.goNext}
          >
            <Text style={styles.buttonText}>ถัดไป</Text>
          </TouchableOpacity>
        </ScrollView>

        <KeyboardAccessoryNavigation
          androidAdjustResize={true}
          nextHidden={true}
          previousHidden={true}
          avoidKeyboard={true}
        />
      </View >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  title: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14,
    color: '#141527'
  },
  titleExample: {
    color: '#666666',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 12,
    textAlign: 'right'
  },
  textDesc: {
    color: '#333',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16
  },
  uploadButton: {
    height: 100,
    width: 100
  },
  button: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16,
    marginBottom: 30,
    marginHorizontal: 25,
    height: 60,
    backgroundColor: '#535786',
    borderRadius: 8
  },
  buttonText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20,
    color: '#FFF'
  },
  disabledBtn: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16,
    marginBottom: 30,
    marginHorizontal: 25,
    height: 60,
    backgroundColor: '#D3D3D3',
    borderRadius: 8
  },
  imageUploadBg: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    height: '100%'
  },
  leftColumn: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  rightColumn: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  textInput: {
    height: 58,
    borderColor: '#CBCCD9',
    borderWidth: 1,
    borderRadius: 13,
    paddingHorizontal: 10,
    fontFamily: AppStyle.mainFontTH
  },
  errorDescribeText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 13,
    color: '#DC3545'
  }
})

export default KYCLayerTwoPageTwoScreen
