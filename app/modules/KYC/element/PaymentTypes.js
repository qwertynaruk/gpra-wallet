import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  FlatList
} from 'react-native'
import PropTypes from 'prop-types'
import PaymentIcon from './PaymentIcon'

export default class PaymentTypes extends Component {
  static propTypes = {
    data: PropTypes.array,
    selected: PropTypes.string,
    onPressItem: PropTypes.func
  }

  static defaultProps = {
    data: [],
    selected: '',
    onPressItem: () => {}
  }

  constructor(props) {
    super(props)
    this._onPressItem = this._onPressItem.bind(this)
  }

  _keyExtractor = item => item.id

  _onPressItem = (key) => {
    this.props.onPressItem(key)
  }

  _renderItem = ({ item }) => {
    const selected = this.props.selected === item.id
    return (
      <PaymentIcon
        id={item.id}
        icon={selected ? item.icon : item.iconAlt}
        name={item.name}
        selected={selected}
        onPressItem={this._onPressItem}
      />
    )
  }

  render() {
    return (
      <View style={styles.flatListContainer}>
        <FlatList
          contentContainerStyle={styles.flatListStyle}
          data={this.props.data}
          extraData={this.props}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  flatListContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  flatListStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  }
})
