import React, { Component } from 'react'
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'

const { width } = Dimensions.get('window')

@observer
export default class StartAom extends Component {
  static propTypes = {
    onPress: PropTypes.func
  }

  static defaultProps = {
    onPress: () => { }
  }

  render() {
    const popupWidth = width < 400 ? {
      flex: 1,
      marginHorizontal: 20
    } : {
      width: 345
    }

    return (
      <View style={styles.overlay}>
        <View
          style={styles.overlayBackground}
        >
          <View style={[styles.popupContent, popupWidth]}>
            <Text style={styles.title}>
              {`อัพโหลดเอกสารสมบูรณ์`}
            </Text>
            <Image source={images.iconUploadFinish} style={styles.icon} />
            <Text style={styles.description}>{`คุณสามารถใช้กระเป๋า Om Token\nได้อย่างสมบูรณ์หลังจากได้รับการยืนยัน\nข้อมูลจากระบบ`}</Text>
            <View style={styles.buttonContainer}>
              <TouchableOpacity activeOpacity={0.6} style={styles.button} onPress={this.props.onPress}>
                <Text style={styles.buttonText}>กลับสู่หน้าหลัก</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  overlayBackground: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%'
  },
  popupContent: {
    backgroundColor: '#FFF',
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 35
  },
  title: {
    color: '#535786',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20,
    marginBottom: 20
  },
  icon: {
    marginTop: 20,
    marginBottom: 20
  },
  description: {
    color: '#121212',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 25
  },
  buttonContainer: {
    flexDirection: 'row'
  },
  button: {
    height: 70,
    margin: 5,
    flex: 1,
    borderTopWidth: 1,
    borderTopColor: '#E7EAF0',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#535786',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20
  }
})
