import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import AppStyle from '../../../commons/AppStyle'

export default class PaymentIcon extends Component {
  static propTypes = {
    id: PropTypes.string,
    icon: PropTypes.number,
    name: PropTypes.string,
    selected: PropTypes.bool,
    onPressItem: PropTypes.func
  }

  static defaultProps = {
    id: '',
    icon: 0,
    name: '',
    selected: false,
    onPressItem: () => {}
  }

  get id() {
    return this.props.id
  }

  get icon() {
    return this.props.icon
  }

  get name() {
    return this.props.name
  }

  get selected() {
    return this.props.selected
  }

  _onPress = id => () => {
    this.props.onPressItem(id)
  }

  activeStyle(active) {
    return active ? {
      opacity: 1
    } : {
      opacity: 0.2
    }
  }

  render() {
    return (
      <TouchableOpacity
        key={this.id}
        activeOpacity={1}
        onPress={this._onPress(this.id)}
      >
        <View style={[styles.button]}>
          <Image source={this.icon} />
          <Text style={[styles.buttonText, { opacity: this.selected ? 1 : 0.6 }]}>{this.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center'
  },
  buttonText: {
    color: '#DFDFDF',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 16,
    paddingTop: 3
  }
})
