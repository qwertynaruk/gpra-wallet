import React, { Component } from 'react'
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import NavStore from '../../../AppStores/NavStore'

const { width } = Dimensions.get('window')

@observer
export default class StartAom extends Component {
  static propTypes = {
    onLayerTwo: PropTypes.func,
    onBuyAom: PropTypes.func,
    EditMode: PropTypes.bool
  }

  static defaultProps = {
    onLayerTwo: () => { },
    onBuyAom: () => { },
    EditMode: false
  }

  constructor(props) {
    super(props)

    this.goToSetting = this.goToSetting.bind(this)
  }

  onLayerTwo = () => {
    const { onLayerTwo } = this.props
    onLayerTwo()
  }

  onBuyAom = () => {
    const { onBuyAom } = this.props
    onBuyAom()
  }

  goToSetting() {
    NavStore.pushToScreen('SettingScreen', {})
  }

  render() {
    const popupWidth = width < 400 ? {
      flex: 1,
      marginHorizontal: 20
    } : {
      width: 345
    }

    return (
      <View style={styles.overlay}>
        <View
          style={styles.overlayBackground}
        >
          <View style={[styles.popupContent, popupWidth]}>
            <Text style={styles.title}>
              {`เสร็จการยืนยันตัวตนเบื้องต้น`}
            </Text>
            <Image source={images.iconUploadFinish} style={styles.icon} />
            <Text style={styles.description}>{`อัพโหลดเอกสารส่วนบุคคลเพิ่มเติม\nเพื่อการยืนยันอย่างสมบูรณ์`}</Text>
            <View style={styles.buttonsContainer}>
              <View style={styles.buttonContainer}>
                <TouchableOpacity activeOpacity={0.6} style={[styles.button, styles.buttonBackground]} onPress={this.onLayerTwo}>
                  <Text style={styles.buttonTextAlt}>อัพโหลด</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.buttonContainer}>
                <TouchableOpacity activeOpacity={0.6} style={[styles.button, { display: this.props.EditMode ? 'none' : 'flex' }]} onPress={this.onBuyAom}>
                  <Text style={styles.buttonText}>เริ่มออม</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={0.6}
                  style={[styles.button, { display: !this.props.EditMode ? 'none' : 'flex' }]}
                  onPress={this.goToSetting}
                >
                  <Text style={styles.buttonText}>ปิด</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  overlayBackground: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%'
  },
  popupContent: {
    backgroundColor: '#FFF',
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 35
  },
  title: {
    color: '#535786',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20,
    marginBottom: 20
  },
  icon: {
    marginTop: 20,
    marginBottom: 20
  },
  description: {
    color: '#121212',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 25
  },
  buttonsContainer: {
    flexDirection: 'column',
    width: '100%'
  },
  buttonContainer: {
    flexDirection: 'row'
  },
  button: {
    height: 70,
    marginHorizontal: 20,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonBackground: {
    backgroundColor: '#535786',
    borderRadius: 8
  },
  buttonText: {
    color: '#535786',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20
  },
  buttonTextAlt: {
    color: 'white',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20
  }
})
