import { action } from 'mobx'
import api from '../../api'

class KYCStore {
  @action async getKycInfo(address) {
    return await api.getKYCProfile(address).then((res) => {
      if (res.data.data) {
        return res.data.data
      }
      return {}
    })
  }
}

export default KYCStore
