// Components & Modules
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NotificationStore from '../../../AppStores/stores/Notification'

export default class AppSettingStore {
  switchEnableNotification(isEnable) {
    if (MainStore.appState.internetConnection === 'offline') {
      NavStore.popupCustom.show('Network Error.')
      return
    }

    MainStore.appState.setEnableNotification(isEnable)

    if (isEnable) {
      NotificationStore.onNotif().then(res => console.log(res))
    } else {
      NotificationStore.offNotif().then(res => console.log(res))
    }
  }
}
