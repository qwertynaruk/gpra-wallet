// Third-party
import { observable, action } from 'mobx'
import MainStore from '../../../AppStores/MainStore'

export default class DeveloperStore {
  @observable developMenu = [
    {
      id: 'kyc_layer_1',
      name: 'KYC Layer 1',
      value: MainStore.appState.kycLayerOne
    },
    {
      id: 'kyc_layer_2',
      name: 'KYC Layer 2',
      value: MainStore.appState.kycLayerTwo
    }
  ]

  @action toggleKycLayerOne() {
    MainStore.appState.setKycLayerOne(!MainStore.appState.kycLayerOne)
  }

  @action toggleKycLayerTwo() {
    if (MainStore.appState.kycLayerTwo) {
      MainStore.appState.setKycLayerTwo(false)
      MainStore.appState.setKycApproved(false)
    } else {
      MainStore.appState.setKycLayerTwo(true)
      MainStore.appState.setKycApproved(true)
    }
  }
}
