import React, { Component } from 'react'
import {
  Alert,
  BackHandler,
  Image,
  Linking,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'

// Third-party
import DeviceInfo from 'react-native-device-info'
import Permissions from 'react-native-permissions'
import ImagePicker from 'react-native-image-picker'
import ImageResizer from 'react-native-image-resizer'
import RNFS from 'react-native-fs'
import { observer } from 'mobx-react/native'

// Components & Modules
import api from '../../../api'
import icons from '../../../commons/icons'
import images from '../../../commons/images'
import AppSettingStore from '../stores/AppSettingStore'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import MainStore from '../../../AppStores/MainStore'
import ManageWalletStore from '../../Setting/stores/ManageWalletStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'
import SettingStore from '../stores/SettingStore'

const currentVersion = DeviceInfo.getVersion()
const currentBuild = DeviceInfo.getBuildNumber()
const marginTop = LayoutUtils.getExtraTop()
const title = 'เมนู'

@observer
class SettingScreen extends Component {
  constructor(props) {
    super(props)
    this.settingStore = new SettingStore()
    this.appSettingStore = new AppSettingStore()
    this.manageWalletStore = new ManageWalletStore()

    this.goToKYC = this.goToKYC.bind(this)
    this.goBack = this.goBack.bind(this)
    this.onChangeAvatar = this.onChangeAvatar.bind(this)
    this.goToWalletInfoScreen = this.goToWalletInfoScreen.bind(this)
    this.goToSecurityScreen = this.goToSecurityScreen.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
    this.connectAsset = this.connectAsset.bind(this)
  }

  componentWillMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentDidMount() {
    this.manageWalletStore.setSelectedWallet(MainStore.appState.selectedWallet)
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  onChangeAvatar() {
    NavStore.preventOpenUnlockScreen = true
    Permissions.check('photo').then((response) => {
      if (response === 'authorized') {
        this.pickPhotosFromGallery()
      } else if (response === 'undetermined') {
        this._alertForPhotosPermission()
      } else if (response === 'denied') {
        this._alertForPhotosPermissionDenied()
      } else if (response === 'restricted') {
        this._alertForPhotosPermissionRestricted()
      }
    })
  }

  _alertForPhotosPermission() {
    Alert.alert(
      'Can we access your photos?',
      'We need access so you can upload your photo',
      [
        {
          text: `Don't allow`,
          style: 'cancel'
        },
        { text: 'Allow', onPress: this._requestPermission }
      ]
    )
  }

  _alertForPhotosPermissionDenied() {
    if (Platform.OS === 'ios') {
      Alert.alert(
        '"OM Wallet" needs permission to access your photos to upload your photo',
        'Please go to Setting > OM Wallet > Photos > Read and Write',
        [
          {
            text: `Don't allow`,
            style: 'cancel'
          },
          {
            text: 'Open Settings',
            onPress: Permissions.openSettings
          }
        ]
      )
    } else {
      this._alertForPhotosPermission()
    }
  }

  _alertForPhotosPermissionRestricted() {
    if (Platform.OS === 'ios') {
      Permissions.check('photo').then((response) => {
        if (response === 'authorized') {
          this.pickPhotosFromGallery()
        } else {
          Alert.alert(
            '"OM Wallet" needs permission to access your photo library to upload your photo.',
            'Please go to Setting > OM Wallet > Photos > Read / Write',
            [
              {
                text: `Don't allow`,
                style: 'cancel'
              },
              {
                text: 'Open Settings',
                onPress: Permissions.openSettings
              }
            ]
          )
        }
      })
    } else {
      Permissions.check('photo').then((response) => {
        if (response === 'authorized') {
          this.pickPhotosFromGallery()
        } else {
          Alert.alert(
            '"OM Wallet" needs permission to access your photo library to upload your photo.',
            'Please go to Setting > Apps > OM Wallet > Permission > Photo \n Then close and open "OM Wallet" application',
            [
              {
                text: `Don't allow`,
                style: 'cancel'
              },
              {
                text: 'OK'
              }
            ]
          )
        }
      })
    }
  }

  _requestPermission = () => {
    NavStore.preventOpenUnlockScreen = true
    Permissions.check('photo').then((response) => {
      if (response != 'authorized') {
        Permissions.request('photo').then((res) => {
          if (res == 'authorized') {
            this._requestCameraPermission()
          }
        })
      }
    })
  }

  _requestCameraPermission = () => {
    NavStore.preventOpenUnlockScreen = true
    Permissions.check('camera').then((res) => {
      if (res == 'authorized') {
        this.pickPhotosFromGallery()
      } else {
        Permissions.request('camera').then((resp) => {
          if (resp == 'authorized') {
            this.pickPhotosFromGallery()
          }
        })
      }
    })
  }

  goBack() {
    NavStore.goBack()
  }

  goToWalletInfoScreen() {
    NavStore.pushToScreen('WalletInfoScreen')
  }

  goToSecurityScreen() {
    NavStore.pushToScreen('SecurityScreen')
  }

  goToKYC() {
    const {
      kycLayerOne, kycLayerTwo, isSendVerify, kycApproved
    } = MainStore.appState

    if (!kycLayerOne && !isSendVerify) {
      NavStore.pushToScreen('KYCScreen')
    } else if (!kycLayerOne && isSendVerify) {
      NavStore.pushToScreen('VerificationEmailScreen')
    } else if (kycLayerOne && !kycLayerTwo) {
      NavStore.pushToScreen('KYCLayerTwoPageOneScreen')
    } else if (kycLayerOne && kycLayerTwo && !kycApproved) {
      NavStore.pushToScreen('KYCPending')
    } else {
      Alert.alert(
        `KYC Level 3`,
        `เตรียมเปิดให้ยืนยันเร็ว ๆ นี้`, [
          {
            text: 'รับทราบ',
            style: 'cancel'
          }
        ], {
          cancelable: true
        }
      )
    }
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  pickPhotosFromGallery = () => {
    NavStore.preventOpenUnlockScreen = true
    const options = {
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    }

    ImagePicker.launchImageLibrary(options, (res) => {
      if (res.error) {
        Alert.alert(res)
      } else if (!res.didCancel) {
        const { path, uri } = res
        const filePath = Platform.OS === 'ios' ? uri : path
        // Get wallet
        const wallet = MainStore.appState.selectedWallet

        // Resize image
        ImageResizer.createResizedImage(filePath, 100, 100, 'JPEG', 80, 0)
          .then(resizeImage => RNFS.readFile(resizeImage.uri, 'base64'))
          .then(base64 => api.uploadUserProfile(wallet.address, {
            name: 'profile.jpg',
            data: base64
          }))
          .then((avatar) => {
            const { data } = avatar
            if (data.success) {
              return api.getKYCProfile(wallet.address)
            }

            throw new Error('Upload not success')
          })
          .then((profile) => {
            const { data } = profile.data
            MainStore.appState.userInfo = data
          })
          .catch((error) => {
            console.log('Error upload avatar:', error)
          })
      }
    })
  }

  connectAsset() {
    const { kycLayerOne } = MainStore.appState

    if (kycLayerOne) {
      NavStore.pushToScreen('ConnectAssetScreen')
    }
  }

  _renderMenuKYC() {
    const { kycLevel } = MainStore.appState
    if (kycLevel === 2) {
      return null
    }

    return (
      <View>
        <TouchableOpacity
          style={{
            display: 'flex',
            flexDirection: 'row'
          }}
          onPress={this.goToKYC}
        >
          <View style={{ flex: 1 }}>
            <View style={styles.marginText}>
              <Text style={[styles.menuText, styles.blueText]}>
                ยืนยันตัวตน
              </Text>
              <Text style={styles.textSmall}>
                LEVEL {kycLevel}/2
              </Text>
            </View>
          </View>
          <View style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'flex-end'
          }}
          >
            <Image
              style={{
                flex: 1,
                marginRight: 25,
                display: 'flex'
              }}
              source={icons.iconHighPriority}
              resizeMode="center"
            />
          </View>
        </TouchableOpacity>
        <View style={styles.bottomLine} />
      </View>
    )
  }

  _renderMenuSecurity() {
    return (
      <View>
        <TouchableOpacity onPress={this.goToSecurityScreen} >
          <Text style={[styles.menuText, styles.marginText]}>
            ข้อมูลความปลอดภัย
          </Text>
        </TouchableOpacity >
        <View style={styles.bottomLine} />
      </View>
    )
  }

  render() {
    const {
      kycLayerOne, userInfo
    } = MainStore.appState

    return (
      <View style={styles.container} >
        {/* Begin flex-one */}
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: marginTop + 20,
            marginBottom: 10
          }}
          headerItem={{
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            color: '#000',
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.goBack}
        />
        {/* End flex-one */}

        {/* Begin flex-two */}
        <View style={{
          flex: 1,
          flexDirection: 'column'
        }}
        >
          {/* Begin title and avatar container */}
          <View style={{
            height: 50,
            flexDirection: 'row',
            marginBottom: 15,
            paddingHorizontal: 25,
            justifyContent: 'space-between'
          }}
          >
            <View style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
            >
              <Text style={styles.mainText}>
                {title}
              </Text>
            </View>

            <View style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
            >
              <View style={{ paddingRight: 3 }}>
                <Image
                  key={userInfo && userInfo.profile_image ? userInfo.profile_image : 'avatar'}
                  style={{
                    height: 50, width: 50, borderWidth: 3, borderColor: '#535688', borderRadius: 25
                  }}
                  source={userInfo && userInfo.profile_image ? { uri: userInfo.profile_image } : images.iconProfile}
                  resizeMode="cover"
                />
                <View
                  style={{ position: 'absolute', bottom: -5, right: -5 }}
                >
                  {kycLayerOne &&
                    <TouchableOpacity onPress={this.onChangeAvatar}>
                      <Image
                        style={{ height: 26, width: 26 }}
                        source={icons.iconChangeAvatar}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  }
                </View>
              </View>

            </View>
          </View >
          {/* End title and avatar container */}

          <ScrollView style={{ flex: 1 }}>
            {/* Begin menu first part */}
            <View>

              {/* Begin Wallet Info */}
              <TouchableOpacity onPress={this.goToWalletInfoScreen} >
                <Text style={[styles.menuText, styles.marginText]}>
                  ข้อมูลกระเป๋า
                </Text>
              </TouchableOpacity >
              <View style={styles.bottomLine} />
              {/* End Wallet Info */}

              {/* Begin identify */}
              {
                this._renderMenuKYC()
              }
              {/* End identify */}

              {/* Begin security menu */}
              {
                this._renderMenuSecurity()
              }
              {/* End security menu */}

              {/* Begin recovery seed words */}
              {
                <View>
                  <TouchableOpacity
                    disabled={!kycLayerOne}
                    style={{ opacity: kycLayerOne ? 1 : 0.2 }}
                    onPress={this.connectAsset}
                  >
                    <Text style={[styles.menuText, styles.marginText]}>
                      เชื่อมเหรียญอื่น ๆ
                    </Text>
                  </TouchableOpacity>
                  <View style={styles.bottomLine} />
                </View>
              }
              {/* End recovery seed words */}
            </View >
            {/* End menu first part */}

            {/* Begin menu second part */}
            <View style={{ marginTop: 60 }}>

              {/* Begin privacy rule */}
              <TouchableOpacity onPress={() => Linking.openURL('https://www.omplatform.com/mobile-content/terms-and-conditions/')}>
                <Text style={[styles.menuText, styles.marginText]}>
                  ข้อกำหนดและเงื่อนไข
                </Text>
              </TouchableOpacity>
              <View style={styles.bottomLine} />
              {/* End privacy rule */}

              {/* Begin about us */}
              <TouchableOpacity onPress={() => Linking.openURL('https://www.omplatform.com/')}>
                <Text style={[styles.menuText, styles.marginText]}>
                  เกี่ยวกับเรา
                </Text>
              </TouchableOpacity>
              <View style={styles.bottomLine} />
              {/* End about us */}

              {/* Begin contact us */}
              <TouchableOpacity onPress={() => Linking.openURL('https://www.omplatform.com/mobile-content__trashed/contact-us/')}>
                <Text style={[styles.menuText, styles.marginText]}>
                  ติดต่อเรา
                </Text>
              </TouchableOpacity>
              <View style={styles.bottomLine} />
              {/* End contact us */}

              {/* Begin app describe */}
              <View style={{
                marginBottom: 20,
                marginTop: 30,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
              }}
              >
                <Text style={styles.appDescribeText}>
                  © 2019 OM WALLET
                </Text>
                <Text style={styles.appDescribeText}>
                  Version {currentVersion} ({currentBuild})
                </Text>
              </View>
              {/* End app describe */}

            </View>
            {/* End menu second part */}
          </ScrollView >
        </View >
      </View >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  mainText: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 30,
    color: '#000000'
  },
  menuText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    color: '#000000'
  },
  marginText: {
    marginLeft: 25,
    marginVertical: 13
  },
  blueText: {
    color: '#4B5391'
  },
  bottomLine: {
    marginLeft: 20,
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1
  },
  appDescribeText: {
    fontFamily: AppStyle.mainFont,
    fontSize: 12,
    color: '#000000',
    opacity: 0.33
  },
  textSmall: {
    color: '#C6C9DF',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 10,
    lineHeight: 12
  }
})

export default SettingScreen
