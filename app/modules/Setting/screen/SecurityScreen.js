import React, { Component } from 'react'
import {
  Alert,
  BackHandler,
  Platform,
  StatusBar,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'

// Third-party
import TouchID from 'react-native-touch-id'
import Permissions from 'react-native-permissions'
import ImagePicker from 'react-native-image-picker'
import ImageResizer from 'react-native-image-resizer'
import RNFS from 'react-native-fs'
import { observer } from 'mobx-react/native'

// Components & Modules
import api from '../../../api'
import images from '../../../commons/images'
import AppSettingStore from '../stores/AppSettingStore'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import MainStore from '../../../AppStores/MainStore'
import ManageWalletStore from '../stores/ManageWalletStore'
import NavStore from '../../../AppStores/NavStore'
import Router from '../../../AppStores/Router'
import NavigationHeader from '../../../components/elements/NavigationHeader'
import SettingStore from '../stores/SettingStore'

const marginTop = LayoutUtils.getExtraTop()
const title = 'ข้อมูลความปลอดภัย'

@observer
class SecurityScreen extends Component {
  constructor(props) {
    super(props)
    this.settingStore = new SettingStore()
    this.manageWalletStore = new ManageWalletStore()

    this.state = {
      touchSupport: false
    }

    this.goBack = this.goBack.bind(this)
    this.onBackup = this.onBackup.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
  }

  componentWillMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentDidMount() {
    this.manageWalletStore.setSelectedWallet(MainStore.appState.selectedWallet)
    this.checkTouchID()
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  onBackup() {
    NavStore.lockScreen({
      onUnlock: async (pincode) => {
        NavStore.showLoading()
        await Router.Backup.gotoBackup(pincode)
      }
    }, true)
  }

  checkTouchID() {
    TouchID.isSupported({
      unifiedErrors: false,
      passcodeFallback: false
    }).then((biometryType) => {
      if (biometryType === 'FaceID') {
        this.setState({
          touchSupport: 'FaceID'
        })
      } else {
        this.setState({
          touchSupport: 'TouchID'
        })
      }
    })
      .catch(() => { })
  }

  goBack() {
    NavStore.goBack()
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  render() {
    const {
      selectedWallet
    } = MainStore.appState
    const { type } = selectedWallet

    return (
      <View style={styles.container} >
        {/* Begin flex-one */}
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: marginTop + 20,
            marginBottom: 10
          }}
          headerItem={{
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            color: '#000',
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.goBack}
        />
        {/* End flex-one */}

        {/* Begin flex-two */}
        <View style={{
          flex: 1,
          flexDirection: 'column'
        }}
        >
          {/* Begin title and avatar container */}
          <View style={{
            height: 60,
            flexDirection: 'row',
            marginBottom: 15,
            paddingHorizontal: 25,
            justifyContent: 'space-between'
          }}
          >
            <View style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
            >
              <Text style={styles.mainText}>
                {title}
              </Text>
            </View>
          </View >
          {/* End title and avatar container */}

          <ScrollView style={{ flex: 1 }}>
            {/* Begin menu first part */}
            <View>
              {/* Begin Change pin */}
              <TouchableOpacity onPress={this.settingStore.showChangePincode}>
                <Text style={[styles.menuText, styles.marginText]}>
                  เปลี่ยนรหัสผ่าน (PIN)
                </Text>
              </TouchableOpacity>
              <View style={styles.bottomLine} />
              {/* End Change pin */}

              {/* Begin Setup FingerPrint */}
              <TouchableOpacity onPress={() => { this.state.touchSupport && NavStore.pushToScreen('FingerPrintSettingScreen', { biometryType: this.state.touchSupport }) }} >
                <Text style={[styles.menuText, styles.marginText, { opacity: this.state.touchSupport ? 1.0 : 0.2 }]}>
                  ปลดล็อคการใช้งานด้วยอุปกรณ์
                </Text>
              </TouchableOpacity>
              <View style={styles.bottomLine} />
              {/* End Setup FingerPrint */}

              {/* Begin recovery seed words */}
              <TouchableOpacity onPress={this.onBackup}>
                <Text style={[styles.menuText, styles.marginText]}>
                  คำกู้ข้อมูล (Seed Words)
                </Text>
              </TouchableOpacity>
              <View style={styles.bottomLine} />
              {/* End recovery seed words */}

              {/* Begin recovery seed words */}
              {
                (type !== 'ripple') && (
                  <View>
                    <TouchableOpacity onPress={this.manageWalletStore.onExportPrivateKey}>
                      <Text style={[styles.menuText, styles.marginText]}>
                        คีย์ลับ (Private Key)
                      </Text>
                    </TouchableOpacity>
                    <View style={styles.bottomLine} />
                  </View>
                )
              }
              {/* End recovery seed words */}
            </View >
            {/* End menu first part */}
          </ScrollView >
        </View >
      </View >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  mainText: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 26,
    color: '#000000'
  },
  menuText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    color: '#000000'
  },
  marginText: {
    marginLeft: 25,
    marginVertical: 13
  },
  bottomLine: {
    marginLeft: 20,
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1
  }
})

export default SecurityScreen
