import React, { Component } from 'react'
import {
  Alert,
  Image,
  BackHandler,
  StatusBar,
  Platform,
  StyleSheet,
  Keyboard,
  Dimensions,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native'
import Modal from 'react-native-modalbox'
import PropsType from 'prop-types'

import SecureDS from '../../../AppStores/DataSource/SecureDS'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'
import Checker from '../../../Handler/Checker'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import images from '../../../commons/images'
import icons from '../../../commons/icons'
import api from '../../../api'
import Stellar from '../../../Utils/Stellar'

import Loading from '../elements/Loading'
import TrustLine from '../elements/TrustLine'

const { width } = Dimensions.get('window')
const marginTop = LayoutUtils.getExtraTop()
const title = 'เพิ่มเหรียญในระบบ'

export default class CreateTrustLineScreen extends Component {
  static propTypes = {
    navigation: PropsType.object
  }

  static defaultProps = {
    navigation: null
  }

  constructor(props) {
    super(props)

    this.state = {
      assetCode: '',
      assetIssuer: '',
      assetCodeFocused: false,
      invalidAssetCode: false,
      invalidAssetCodeMsg: '',
      assetIssuerFocused: false,
      invalidAssetIssuer: false,
      invalidAssetIssuerMsg: '',
      loading: false,
      submiting: false
    }

    this.onScanAsset = this.onScanAsset.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
    this.handleOnFocus = this.handleOnFocus.bind(this)
    this.handleOnBlur = this.handleOnBlur.bind(this)
    this.handleOnSubmitEditing = this.handleOnSubmitEditing.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.changeTrust = this.changeTrust.bind(this)
    this.handleScan = this.handleScan.bind(this)
  }

  componentWillMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }

    // Set param
    const { params } = this.props.navigation.state
    if (params) {
      this.setState({
        assetCode: params.asset_code,
        assetIssuer: params.asset_issuer
      })
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
    Keyboard.dismiss()
  }

  onScanAsset = (qrcode) => {
    const [chain, issuer, type, code] = qrcode.split(':')
    if (
      (chain !== 'om' && chain !== 'OM') ||
      !Checker.checkAddressAOM(issuer) ||
      type != 3 ||
      !(/^([0-9A-Z]{4,6})$/.test(code))
    ) {
      Alert.alert(
        'ข้อมูลไม่ถูกต้อง',
        'ข้อมูลใน QR Code ไม่ถูกต้องกรุณาตรวจสอบหรือสแกนใหม่',
        [
          {
            text: 'รับทราบ',
            style: 'cancel'
          }
        ]
      )
      return
    }

    // Set trust line
    this.setState({
      assetCode: code,
      assetIssuer: issuer
    })
  }

  get disabledSubmit() {
    if (this.state.assetCode &&
      this.state.assetIssuer &&
      !this.state.invalidAssetCode &&
      !this.state.invalidAssetIssuer) {
      return false
    }

    return true
  }

  get modalStyle() {
    if (width > 400) {
      return {
        width: 345
      }
    }

    return {
      paddingHorizontal: 25
    }
  }

  getPrivateKey(ds) {
    MainStore.appState.selectedWallet.setSecureDS(ds)
    return MainStore.appState.selectedWallet.derivePrivateKey()
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  handleSubmit() {
    const { balance, BASE_FEE_DP } = MainStore.appState.selectedWallet
    const { assetCode } = this.state

    if (balance < BASE_FEE_DP) {
      Alert.alert(
        'ยอด Om ไม่เพียงพอ',
        `จำนวนต้องมี ${BASE_FEE_DP} Om เป็นขั้นต่ำในการเชื่อมเหรียญ ${assetCode}`,
        [
          {
            text: 'รับทราบ',
            onPress: () => {
              this.setState({
                loading: false
              })
            }
          }
        ],
        { cancelable: false }
      )
    } else {
      Keyboard.dismiss()
      setTimeout(() => {
        this.trustModal.open()
      }, 150)
    }
  }

  changeTrust() {
    NavStore.lockScreen({
      onUnlock: async (pincode) => {
        if (this.state.submiting) return

        this.trustModal.close()
        this.setState({
          loading: true,
          submiting: true
        })

        // Call wallet
        const { BASE_FEE } = MainStore.appState.selectedWallet
        const ds = new SecureDS(pincode)

        // Asset data
        const { assetCode, assetIssuer } = this.state

        // Get private key
        setTimeout(() => {
          this.getPrivateKey(ds)
            .then(privateKey => Stellar.ChangeTrust(Stellar.Server(), privateKey, assetCode.toUpperCase(), assetIssuer, BASE_FEE))
            .then((pdata) => {
              console.log(pdata)
              return api.Aom.confirmTransaction(pdata)
            })
            .then((result) => {
              console.log(`trustlist result`)
              console.log(result.data.data)
              if (!result || !result.status) {
                throw new Error(result)
              }
              if (result.status !== 200 || !(result.data.data && result.data.data.href)) {
                throw new Error(result.data.message ? result.data.message : String(result.data))
              }
              NavStore.showToastTop(`ยอมรับเหรียญ ${assetCode} สำเร็จ`, { backgroundColor: AppStyle.backgroundToastColor }, { color: AppStyle.mainColor, fontFamily: AppStyle.mainFontBoldTH })
              NavStore.pushToScreen('DashboardScreen')
              MainStore.appState.OMWallet.fetchingBalance()
              if (MainStore.smartContractStore) {
                MainStore.smartContractStore.getTokeniz()
              }
              this.setState({
                loading: false,
                submiting: false
              })
            })
            .catch((err) => {
              console.log(`trustlist err ${err.message}`)
              console.log(err)
              this.setState({
                loading: false,
                submiting: false
              })
              this.tnsFailDialog.open()
            })
        }, 200)
      }
    }, true)
  }

  handleScan() {
    Keyboard.dismiss()
    this.gotoScan()
  }

  gotoScan() {
    setTimeout(() => {
      NavStore.pushToScreen('ScanQRCodeScreen', {
        title: 'Scan QR Code',
        marginTop,
        returnData: this.onScanAsset
      })
    }, 300)
  }

  goBack() {
    NavStore.goBack()
  }

  _validationCode(code) {
    let error = false
    let msg = ''
    const reg = /^[a-z0-9]+$/i
    if (!reg.test(code)) {
      error = true
      msg = 'กรอกได้เฉพาะ A-Z และ 0-9'
    } else if (code.length < 1 || code.length > 6) {
      error = true
      msg = 'จำนวนตัวอักษรอยู่ระหว่าง 1 ถึง 6 ตัวอักษร'
    } else if (code.toUpperCase() === 'OM') {
      error = true
      msg = 'ระบบไม่อนุญาติให้ใช้ชื่อนี้'
    }

    this.setState({
      assetCode: code,
      invalidAssetCode: error,
      invalidAssetCodeMsg: msg
    })
  }

  _validationIssuer(issuer) {
    let error = false
    let msg = ''

    if (!Checker.checkAddressAOM(issuer)) {
      error = true
      msg = 'รูปแบบไม่ถูกต้อง'
    }

    this.setState({
      assetIssuer: issuer,
      invalidAssetIssuer: error,
      invalidAssetIssuerMsg: msg
    })
  }

  borderColor(focus, error) {
    if (error) {
      return {
        borderColor: '#C60003'
      }
    } else if (focus) {
      return {
        borderColor: '#4B5391'
      }
    }
    return {}
  }

  handleOnFocus = name => () => {
    if (name === 'assetCode') {
      this.setState({ assetCodeFocused: true })
    } else if (name === 'assetIssuer') {
      this.setState({ assetIssuerFocused: true })
    }
  }

  handleOnBlur = name => () => {
    if (name === 'assetCode') {
      this.setState({ assetCodeFocused: false })
    } else if (name === 'assetIssuer') {
      this.setState({ assetIssuerFocused: false })
    }
  }

  handleOnSubmitEditing = name => () => {
    if (name === 'assetCode') {
      this.setState({ assetCodeFocused: false })
      this.assetIssuerInput.focus()
    } else if (name === 'assetIssuer') {
      this.setState({ assetIssuerFocused: false })
    }
  }

  render() {
    const { disabledSubmit, modalStyle } = this
    const { assetCode, assetIssuer } = this.state
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: marginTop + 20,
            paddingHorizontal: 20,
            width
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            color: '#000',
            fontSize: 18,
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.goBack}
        />
        <View
          style={[styles.container, {
            flex: 1, flexDirection: 'column', paddingHorizontal: 20
          }]}
        >
          <View style={{ marginTop: 10 }}>
            <View>
              <Text style={styles.inputDescribeText}>Asset code</Text>
            </View>
            <View style={styles.row}>
              <TextInput
                ref={(ref) => { this.assetCodeInput = ref }}
                autoCapitalize="none"
                onFocus={this.handleOnFocus('assetCode')}
                onBlur={this.handleOnBlur('assetCode')}
                onSubmitEditing={this.handleOnSubmitEditing('assetCode')}
                onChangeText={(code) => {
                  this._validationCode(code)
                }}
                returnKeyType={Platform.OS === 'android' ? 'done' : 'default'}
                style={[styles.textInput, this.borderColor(this.state.assetCodeFocused, this.state.invalidAssetCode)]}
                underlineColorAndroid="transparent"
                value={assetCode}
              />
            </View>
            {this.state.invalidAssetCode && (
              <View>
                <Text style={styles.errorDescribeText}>{this.state.invalidAssetCodeMsg}</Text>
              </View>
            )}
          </View>

          <View style={{ marginTop: 20 }}>
            <View>
              <Text style={styles.inputDescribeText}>Issuer Account ID</Text>
            </View>
            <View style={styles.row}>
              <TextInput
                ref={(ref) => { this.assetIssuerInput = ref }}
                autoCapitalize="none"
                onFocus={this.handleOnFocus('assetIssuer')}
                onBlur={this.handleOnBlur('assetIssuer')}
                onSubmitEditing={this.handleOnSubmitEditing('assetIssuer')}
                onChangeText={(issuer) => {
                  this._validationIssuer(issuer)
                }}
                returnKeyType={Platform.OS === 'android' ? 'done' : 'default'}
                style={[styles.textInput, this.borderColor(this.state.assetIssuerFocused, this.state.invalidAssetIssuer)]}
                underlineColorAndroid="transparent"
                value={assetIssuer}
              />
            </View>
            {this.state.invalidAssetIssuer && (
              <View>
                <Text style={styles.errorDescribeText}>{this.state.invalidAssetIssuerMsg}</Text>
              </View>
            )}
          </View>

          <View style={styles.buttonContainer}>
            <TouchableOpacity
              disabled={disabledSubmit}
              style={disabledSubmit ? styles.disableNextBtn : styles.containerNextBtn}
              onPress={this.handleSubmit}
            >
              <Text style={disabledSubmit ? styles.disableNextText : styles.nextText}>ดำเนินการ</Text>
            </TouchableOpacity>
          </View>

          <View>
            <TouchableOpacity onPress={this.handleScan}>
              <View style={styles.qrScanner}>
                <Image source={images.iconScan} />
                <Text style={styles.qrScannerText}>Scan QR Code</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        <Modal
          ref={(ref) => { this.trustModal = ref }}
          position="center"
          swipeToClose={false}
          backButtonClose={true}
          backdropPressToClose={false}
          backdropColor="#000"
          backdropOpacity={0.5}
          style={[modalStyle, {
            backgroundColor: 'transparent',
            justifyContent: 'center'
          }]}
        >
          <View>
            <View>
              <TrustLine
                token={this.state.assetCode}
                onConfirmPress={this.changeTrust}
                onCancelPress={() => {
                  this.trustModal.close()
                }}
              />
            </View>
          </View>
        </Modal>

        {/* Transaction Fail Dialog */}
        <Modal
          ref={(ref) => { this.tnsFailDialog = ref }}
          position="center"
          swipeToClose={true}
          backButtonClose={true}
          backdropPressToClose={true}
          style={[modalStyle, {
            backgroundColor: 'transparent',
            justifyContent: 'center'
          }]}
        >
          <View style={notiStyles.container}>
            <View style={notiStyles.dashboardHeader}>
              <View style={notiStyles.notiContainer}>
                <Text style={notiStyles.title}>
                  การเชื่อมเหรียญเกิดข้อผิดพลาด
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center'
                  }}
                >
                  <Image
                    style={{ padding: 20 }}
                    source={icons.iconRiskDescription}
                  />
                </View>

                <Text style={notiStyles.bodyText}>
                  เกิดข้อผิดพลาดระหว่างการเชื่อมเหรียญ กรุณาตรวจสอบข้อมูล และลองใหม่อีกครั้ง
                </Text>

                <TouchableOpacity
                  onPress={() => {
                    this.tnsFailDialog.close()
                  }}
                >
                  <Text style={notiStyles.btnText}>
                    ปิดหน้าต่างนี้
                  </Text>
                </TouchableOpacity>

              </View>
            </View>
          </View>
        </Modal>
        {/* End Transaction Fail Dialog */}

        {
          this.state.loading && (
            <Loading title="กำลังประมวลผล" />
          )
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  row: {
    flexDirection: 'row'
  },
  inputDescribeText: {
    marginBottom: 3,
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14,
    color: '#121212'
  },
  textInput: {
    flex: 1,
    height: 58,
    borderColor: '#CBCCD9',
    borderWidth: 1,
    borderRadius: 13,
    paddingHorizontal: 10,
    fontSize: 16,
    fontFamily: AppStyle.mainFontTH
  },
  errorDescribeText: {
    marginTop: 3,
    fontFamily: AppStyle.mainFontTH,
    fontSize: 13,
    color: '#DC3545'
  },
  buttonContainer: {
    marginVertical: 20
  },
  disableNextBtn: {
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: '#F8F8F8'
  },
  disableNextText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 22,
    color: '#DFDFDF'
  },
  containerNextBtn: {
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: '#535786'
  },
  nextText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 22,
    color: '#FFF'
  },
  qrScanner: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  qrScannerText: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 16,
    paddingLeft: 10
  }
})

const notiStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  notiContainer: {
    backgroundColor: '#FFF',
    borderRadius: 3
  },
  title: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20,
    color: '#535786',
    textAlign: 'center',
    marginTop: 25,
    marginBottom: 30
  },
  bodyText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    color: '#000',
    textAlign: 'center',
    padding: 10,
    marginTop: 40
  },
  btnText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20,
    color: '#535786',
    textAlign: 'center',
    marginBottom: 20
  }
})
