import React, { Component } from 'react'
import {
  Alert,
  BackHandler,
  FlatList,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  View
} from 'react-native'
import Share from 'react-native-share'
import { observer } from 'mobx-react/native'

// Components & Modules
import api from '../../../api'
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import AssetStore from '../../../AppStores/stores/AssetStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'
import { chainNames } from '../../../Utils/WalletAddresses'
import CreateAssetStore from '../stores/CreateAssetStore'
import AssetItem from '../elements/AssetItem'
import Loading from '../elements/Loading'

const marginTop = LayoutUtils.getExtraTop()
const title = 'เปิดใช้งานเหรียญอื่นๆ'

@observer
class AnotherAssetScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      selected: []
    }

    this.assetStore = new AssetStore()
    this.createAssetStore = new CreateAssetStore()

    this.handleBackPress = this.handleBackPress.bind(this)
    this.onShare = this.onShare.bind(this)
    this._confirmAsset = this._confirmAsset.bind(this)

    this.fabric = MainStore.appState.fabric

    this.assetStore
      .getAsset()
      .then((selected) => {
        this.setState({ selected })
      })
  }

  componentWillMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  onShare = (code, issuer) => () => {
    Share.open({
      title: `Share asset ${code}`,
      message: `Asset code:\n${code}\nAsset issuer:\n${issuer}`
    }).then(() => {
      this.fabric.answers.logShare('Asset', 'Custom Asset', 'asset', 'share-promptpay')
    }).catch((error) => {
      console.log(`share asset error ${error.message}`)
    })
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  goBack() {
    NavStore.goBack()
  }

  _onPressItem = (symbol, name) => {
    Alert.alert(
      `กำลังเพิ่ม ${name}`,
      `ยืนยันเพิ่ม ${name} เข้าสู่กระเป๋าของท่าน กระบวนการนี้ไม่สามารถย้อนคืน`,
      [
        {
          text: 'ยกเลิก',
          style: 'cancel'
        }, {
          text: 'ยืนยัน',
          onPress: () => this._confirmAsset(symbol)
        }
      ], {
        cancelable: false
      }
    )
  }

  _confirmAsset(type) {
    const chain = chainNames[type]
    const { address } = MainStore.appState.selectedWallet

    // Set state
    this.setState({
      loading: true
    })

    // Create wallet
    this.createAssetStore.setTitle(type)
    this.createAssetStore.handleCreateWallet(chain)
      .then(() => {
        this.assetStore
          .save(type)
          .then((selected) => {
            if (chain) {
              this.setState({
                selected,
                loading: false
              })
            }
          })
      })
      .catch((error) => {
        // Set state
        this.setState({
          loading: false
        })

        // Show alert
        Alert.alert(
          `เกิดข้อผิดพลาด`,
          `ระบบไม่สามารถเปิด ${type} ให้ท่านได้ในขณะนี้`,
          [
            {
              text: 'ปิด',
              style: 'cancel'
            }
          ], {
            cancelable: false
          }
        )

        // Report bug
        api.Aom.reportBug({
          address,
          error_component: 'AnotherAssetScreen',
          error_function: 'confirmAsset',
          error_detail: error.message
        })
      })
  }

  _renderItem = ({ item }) => {
    const { selected } = this.state
    const active = selected.includes(item.symbol)
    return (
      <AssetItem
        name={item.name}
        symbol={item.symbol}
        icons={item.icons}
        active={active}
        onPressItem={this._onPressItem}
      />
    )
  }

  render() {
    const { assetList } = this.assetStore

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            borderBottomColor: '#DDD',
            borderBottomWidth: 1,
            marginTop: marginTop + 20,
            paddingHorizontal: 25,
            paddingBottom: 15
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            color: '#000',
            fontSize: 18,
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.goBack}
        />
        <ScrollView
          style={styles.container}
          showsVerticalScrollIndicator={false}
        >
          <FlatList
            data={assetList}
            extraData={this.state}
            keyExtractor={item => item.key}
            renderItem={this._renderItem}
          />
        </ScrollView>

        {
          this.state.loading && (
            <Loading title="กำลังเปิดใช้งาน" />
          )
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

export default AnotherAssetScreen
