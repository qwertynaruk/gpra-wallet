import React, { Component } from 'react'
import {
  BackHandler,
  Platform,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  View
} from 'react-native'

// Components & Modules
import api from '../../../api'
import images from './../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import icons from '../../../commons/icons'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const marginTop = LayoutUtils.getExtraTop()
const title = 'ข้อมูลกระเป๋า'

export default class Favorite extends Component {
  constructor(props) {
    super(props)

    this.state = {
      wallet_id: '',
      email: '',
      show_wallet_id: false
    }

    this.goBack = this.goBack.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }

    this.fetchData()
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  goBack() {
    NavStore.pushToScreen('SettingScreen')
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  fetchData() {
    const wallet = MainStore.appState.OMWallet
    if (wallet) {
      NavStore.showLoading()

      api.getKYCProfile(wallet.address).then((res) => {
        const { status, data } = res
        if (status === 200) {
          this.setState({
            wallet_id: wallet.address,
            ...data.data
          })
        }

        NavStore.hideLoading()
      })
    }
  }

  visibleAddress() {
    const { wallet_id, show_wallet_id } = this.state

    if (wallet_id) {
      return show_wallet_id ? wallet_id : wallet_id.replace(/^([A-Z0-9]{51})([A-Z0-9]+)$/g, '••••••••••$2')
    }

    return 'N/A'
  }

  renderHeader = () => (
    <NavigationHeader
      style={{
        marginTop: marginTop + 20,
        marginBottom: 20
      }}
      headerItem={{
        title,
        icon: null,
        button: images.iconBackAlt
      }}
      titleStyle={{
        color: '#000',
        fontFamily: AppStyle.mainFontBoldTH,
        textAlign: 'center',
        flex: 1,
        marginRight: 20
      }}
      action={this.goBack}
    />
  )

  renderBody = () => {
    const { show_wallet_id } = this.state
    return (
      <View>
        <View style={styles.itemContainer}>
          <Text style={styles.mainText}>Wallet ID</Text>
          <View style={styles.row}>
            <View style={styles.visibleView}>
              <Text style={styles.subText}>{this.visibleAddress()}</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                this.setState(prevState => ({
                  show_wallet_id: !prevState.show_wallet_id
                }))
              }}
            >
              <Image
                source={show_wallet_id ? icons.iconVisibleHide : icons.iconVisible}
              />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.itemContainer}>
          <Text style={styles.mainText}>อีเมลล์</Text>
          <Text style={styles.subText}>{this.state.email != '' ? this.state.email : 'N/A'}</Text>
        </View>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        {/* Begin header container */}
        {this.renderHeader()}
        {/* End header container */}

        {/* Begin body container */}
        {this.renderBody()}
        {/* End body container */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  mainText: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14,
    color: AppStyle.colorBlack
  },
  subText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    color: '#9696A3'
  },
  itemContainer: {
    marginLeft: 25,
    marginRight: 25,
    marginBottom: 25
  },
  row: {
    flexDirection: 'row'
  },
  visibleView: {
    flex: 1,
    marginRight: 25
  }
})
