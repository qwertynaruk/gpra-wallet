import React, { Component } from 'react'
import {
  Alert,
  BackHandler,
  FlatList,
  Platform,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View
} from 'react-native'
import { observer } from 'mobx-react/native'
import Share from 'react-native-share'

// Components & Modules
import api from '../../../api'
import icons from '../../../commons/icons'
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import AssetStore from '../../../AppStores/stores/AssetStore'
import { chainNames } from '../../../Utils/WalletAddresses'
import NavigationHeader from '../../../components/elements/NavigationHeader'
import CreateAssetStore from '../stores/CreateAssetStore'
import AssetItem from '../elements/AssetItem'
import Loading from '../elements/Loading'

const marginTop = LayoutUtils.getExtraTop()
const title = 'เปิดใช้งานเหรียญอื่น ๆ'

@observer
class ConnectAssetScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      selected: []
    }

    this.handleBackPress = this.handleBackPress.bind(this)
    this.onShare = this.onShare.bind(this)
    this._renderHeader = this._renderHeader.bind(this)
    this._renderAsset = this._renderAsset.bind(this)
    this._renderToken = this._renderToken.bind(this)

    this.assetStore = new AssetStore()
    this.createAssetStore = new CreateAssetStore()

    this.fabric = MainStore.appState.fabric

    this.assetStore
      .getAsset()
      .then((selected) => {
        this.setState({ selected })
      })
  }

  componentWillMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  onShare = (code, issuer) => () => {
    Share.open({
      title: `Share asset ${code}`,
      message: `Asset code:\n${code}\nAsset issuer:\n${issuer}`
    }).then(() => {
      this.fabric.answers.logShare('Asset', 'Custom Asset', 'asset', 'share-promptpay')
    }).catch((error) => {
      console.log(`share asset error ${error.message}`)
    })
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  goBack() {
    NavStore.goBack()
  }

  _onPressItem = (symbol, name) => {
    Alert.alert(
      `กำลังเพิ่ม ${name}`,
      `ยืนยันเพิ่ม ${name} เข้าสู่กระเป๋าของท่าน กระบวนการนี้ไม่สามารถย้อนคืน`,
      [
        {
          text: 'ยกเลิก',
          style: 'cancel'
        }, {
          text: 'ยืนยัน',
          onPress: () => this._confirmAsset(symbol)
        }
      ], {
        cancelable: false
      }
    )
  }

  _confirmAsset(type) {
    const chain = chainNames[type]
    const { address } = MainStore.appState.selectedWallet

    // Set state
    this.setState({
      loading: true
    })

    // Create wallet
    this.createAssetStore.setTitle(type)
    this.createAssetStore.handleCreateWallet(chain)
      .then(() => {
        this.assetStore
          .save(type)
          .then((selected) => {
            if (chain) {
              this.setState({
                selected,
                loading: false
              })
            }
          })
      })
      .catch((error) => {
        // Set state
        this.setState({
          loading: false
        })

        // Show alert
        Alert.alert(
          `เกิดข้อผิดพลาด`,
          `ระบบไม่สามารถเปิด ${type} ให้ท่านได้ในขณะนี้`,
          [
            {
              text: 'ปิด',
              style: 'cancel'
            }
          ], {
            cancelable: false
          }
        )

        // Report bug
        api.Aom.reportBug({
          address,
          error_component: 'AnotherAssetScreen',
          error_function: 'confirmAsset',
          error_detail: error.message
        })
      })
  }

  _renderToken = ({ item }) => {
    if (item.asset_code) {
      const { asset_code, asset_issuer } = item
      const issuer = asset_issuer.replace(/^([A-Z0-9]{2})(.+)([A-Z0-9]{5})$/, '$1...$3')
      return (
        <SafeAreaView style={styles.container}>
          <View style={styles.switchAsset}>
            <Image
              style={styles.tokenIcon}
              source={icons.iconTokenOmDefault}
              resizeMode="contain"
            />
            <Text style={styles.tokenListText}>{asset_code}</Text>
            <Text style={[styles.tokenListDesc, { flex: 1 }]}>({issuer})</Text>
            <TouchableOpacity
              onPress={this.onShare(asset_code, asset_issuer)}
            >
              <Image source={icons.iconShareAsset} resizeMode="contain" />
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      )
    }

    return null
  }

  _renderHeader() {
    const { OMWallet } = MainStore.appState

    if (!OMWallet.funded) {
      return null
    }

    return (
      <SafeAreaView style={styles.switchAsset}>
        <TouchableOpacity
          style={styles.createAsset}
          onPress={() => {
            NavStore.pushToScreen('CreateTrustLineScreen')
          }}
        >
          <Image source={icons.iconCreateAsset} resizeMode="contain" />
          <Text style={styles.createAssetText}>เพิ่มเหรียญในระบบ</Text>
        </TouchableOpacity>
      </SafeAreaView>
    )
  }

  _renderAsset = ({ item }) => {
    const { selected } = this.state
    const active = selected.includes(item.symbol)
    return (
      <AssetItem
        name={item.name}
        symbol={item.symbol}
        icons={item.icons}
        active={active}
        onPressItem={this._onPressItem}
      />
    )
  }

  render() {
    const { balances } = MainStore.appState.OMWallet
    const { assetList } = this.assetStore

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            borderBottomColor: '#F4F4F7',
            borderBottomWidth: 1,
            marginTop: marginTop + 20,
            paddingHorizontal: 20,
            paddingBottom: 15
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            color: '#000',
            fontSize: 16,
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.goBack}
        />
        <ScrollView
          style={styles.container}
          showsVerticalScrollIndicator={false}
        >
          <FlatList
            data={assetList}
            extraData={this.state}
            keyExtractor={item => item.key}
            renderItem={this._renderAsset}
            ListHeaderComponent={this._renderHeader}
          />
          <FlatList
            data={balances}
            keyExtractor={item => `${item.asset_code}-${item.asset_issuer}`}
            renderItem={this._renderToken}
          />
        </ScrollView>

        {
          this.state.loading && (
            <Loading title="กำลังเปิดใช้งาน" />
          )
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  switchAsset: {
    borderBottomWidth: 1,
    borderBottomColor: '#F4F4F7',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 13,
    paddingHorizontal: 20,
    justifyContent: 'space-between'
  },
  createAsset: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  createAssetText: {
    color: '#D5D6DF',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    marginLeft: 14
  },
  tokenIcon: {
    height: 40,
    width: 40
  },
  tokenListText: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18,
    marginLeft: 14
  },
  tokenListDesc: {
    color: '#AFB0BD',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    marginLeft: 14
  }
})

export default ConnectAssetScreen
