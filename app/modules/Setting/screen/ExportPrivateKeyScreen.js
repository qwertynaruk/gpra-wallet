import React, { Component } from 'react'
import {
  BackHandler,
  Clipboard,
  Dimensions,
  Platform,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import PropsType from 'prop-types'

// Third-party
import QRCode from 'react-native-qrcode-svg'
import ViewShot from 'react-native-view-shot'

// Components & Modules
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const { width } = Dimensions.get('window')
const marginTop = LayoutUtils.getExtraTop()
const title = 'คีย์ลับ (Private Key)'

export default class ExportPrivateKeyScreen extends Component {
  static propTypes = {
    navigation: PropsType.object
  }

  static defaultProps = {
    navigation: null
  }

  constructor(props) {
    super(props)

    this.goBack = this.goBack.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  goBack() {
    NavStore.goBack()
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  render() {
    const { pk, ext } = this.props.navigation ? this.props.navigation.state.params : {}

    return (
      <SafeAreaView style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: Platform.OS === 'ios' ? marginTop + 10 : marginTop + 20,
            marginBottom: 20
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            color: '#000',
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.goBack}
        />

        {/* Begin QRCode */}
        <View style={styles.qrCodeContainer}>
          <ViewShot style={styles.borderQRCode}>
            <QRCode
              value={pk}
              size={width - 150}
            />
          </ViewShot>
          <View style={{ marginVertical: 20 }}>
            <Text style={styles.privateExt}>{ext}</Text>
            <Text style={styles.privateKey}>{pk}</Text>
          </View>
          <TouchableOpacity
            style={styles.copyBtn}
            onPress={() => {
              Clipboard.setString(pk)
              NavStore.showToastTop('คัดลอกสำเร็จ', { backgroundColor: AppStyle.backgroundToastColor }, { color: AppStyle.mainColor, fontFamily: AppStyle.mainFontBoldTH })
            }}
          >
            <Text style={styles.copy}>คัดลอก</Text>
          </TouchableOpacity>
        </View>
        {/* End QRCode */}
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  qrCodeContainer: {
    flex: 1,
    alignItems: 'center',
    marginHorizontal: 25
  },
  privateExt: {
    marginHorizontal: 15,
    textAlign: 'center',
    fontFamily: AppStyle.mainFontBold,
    fontSize: 20,
    color: AppStyle.textColorBlack
  },
  privateKey: {
    marginHorizontal: 15,
    textAlign: 'center',
    fontFamily: AppStyle.mainFont,
    fontSize: 16,
    color: AppStyle.textColorBlack
  },
  copyBtn: {
    width: 190,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderRadius: 8,
    borderColor: '#181818'
  },
  copy: {
    fontSize: 16,
    fontFamily: AppStyle.mainFontBoldTH,
    color: AppStyle.textColorBlack
  },
  borderQRCode: {
    alignItems: 'center',
    backgroundColor: 'white',
    width: '100%'
  }
})
