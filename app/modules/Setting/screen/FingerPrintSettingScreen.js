import React, { Component } from 'react'
import {
  Alert,
  BackHandler,
  Dimensions,
  Platform,
  SafeAreaView,
  StyleSheet,
  View
} from 'react-native'
import PropsType from 'prop-types'

// Third-party
import images from '../../../commons/images'
import AomViewButton from '../../../components/elements/AomViewButton'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const { width } = Dimensions.get('window')
const marginTop = LayoutUtils.getExtraTop()

export default class FingerPrintSettingScreen extends Component {
  static propTypes = {
    navigation: PropsType.object
  }

  static defaultProps = {
    navigation: null
  }

  constructor(props) {
    super(props)

    this.goBack = this.goBack.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
    this.disableTouchFaceID = this.disableTouchFaceID.bind(this)
    this.setupTouchFaceID = this.setupTouchFaceID.bind(this)
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  setupTouchFaceID() {
    const { biometryType } = this.props.navigation ? this.props.navigation.state.params : {}
    Alert.alert(
      'ปลดล็อคการใช้งานด้วยอุปกรณ์',
      'คุณต้องการ "ปลดล็อคการใช้งานด้วยอุปกรณ์" ใช่หรือไม่?',
      [
        {
          text: 'ปิด',
          style: 'cancel'
        },
        {
          text: 'ยืนยัน',
          onPress: () => {
            MainStore.appState.setBiometryType(biometryType)
            MainStore.appState.setEnableTouchFaceID(true)
            NavStore.showToastTop('ปลดล็อคการใช้งานด้วยอุปกรณ์สำเร็จ', { backgroundColor: AppStyle.backgroundToastColor }, { color: AppStyle.mainColor })
            this.goBack()
          }
        }
      ],
      { cancelable: false }
    )
  }

  disableTouchFaceID() {
    Alert.alert(
      'ยกเลิกปลดล็อคการใช้งานด้วยอุปกรณ์',
      'คุณต้องการ "ยกเลิกปลดล็อคการใช้งานด้วยอุปกรณ์" ใช่หรือไม่?',
      [
        {
          text: 'ปิด',
          style: 'cancel'
        },
        {
          text: 'ยืนยัน',
          onPress: () => {
            MainStore.appState.setBiometryType(null)
            MainStore.appState.setEnableTouchFaceID(false)
            NavStore.showToastTop('ยกเลิกปลดล็อคการใช้งานด้วยอุปกรณ์สำเร็จ', { backgroundColor: AppStyle.backgroundToastColor }, { color: AppStyle.mainColor })
            this.goBack()
          }
        }
      ],
      { cancelable: false }
    )
  }

  goBack() {
    NavStore.goBack()
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  render() {
    const { enableTouchFaceID } = MainStore.appState
    const { biometryType } = this.props.navigation ? this.props.navigation.state.params : false

    return (
      <SafeAreaView style={styles.container}>
        <NavigationHeader
          style={{
            marginTop: marginTop + 20,
            marginBottom: 20
          }}
          headerItem={{
            title: 'ตั้งค่าการปลดล็อคการใช้งานด้วยอุปกรณ์',
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            flex: 1,
            marginRight: 20,
            textAlign: 'center',
            fontFamily: AppStyle.mainFontBoldTH,
            fontSize: 16,
            color: '#000000'
          }}
          action={this.goBack}
        />
        <View style={[styles.container, { alignItems: 'center' }]}>
          <AomViewButton enable={!enableTouchFaceID && biometryType == 'FaceID'} onPress={this.setupTouchFaceID} title="ปลดล็อคด้วยใบหน้า" />
          <AomViewButton enable={!enableTouchFaceID && biometryType == 'TouchID'} onPress={this.setupTouchFaceID} title="ปลดล็อคด้วยลายนิ้วมือ" />
          <AomViewButton enable={enableTouchFaceID} onPress={this.disableTouchFaceID} title="ยกเลิกการปลดล๊อคด้วยอุปกรณ์" />
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
