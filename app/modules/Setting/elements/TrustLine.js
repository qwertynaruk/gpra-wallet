import React, { Component } from 'react'
import {
  View,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text
} from 'react-native'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react/native'

import MainStore from '../../../AppStores/MainStore'
import AppStyle from '../../../commons/AppStyle'
import images from '../../../commons/images'

@observer
class Trustline extends Component {
  static propTypes = {
    token: PropTypes.string,
    onConfirmPress: PropTypes.func,
    onCancelPress: PropTypes.func
  }

  static defaultProps = {
    token: '',
    onConfirmPress: () => { },
    onCancelPress: () => { }
  }

  render() {
    const { BASE_FEE_DP } = MainStore.appState.selectedWallet
    return (
      <View style={styles.popupContent}>
        <ScrollView>
          <View
            style={styles.mainContent}
          >
            <Text style={styles.title}>ยอมรับการเชื่อมเหรียญในเครือข่าย</Text>
            <Image
              source={images.iconTrustLine}
              style={styles.icon}
            />
            <Text style={styles.paragraph}>
              {`${this.props.token} เป็นโทเคนที่อยู่บนเครือข่าย Om ดังนั้นคุณจะต้องทำการยอมรับการเชื่อมต่อนี้บนเครือข่ายก่อน จึงจะสามารถใช้งาน ${this.props.token} บนกระเป๋าของคุณได้\n(ค่าธรรมเนียมการเชื่อมเหรียญ ${BASE_FEE_DP} Om)`}
            </Text>
          </View>
          <View style={styles.buttonsContainer}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                activeOpacity={0.6}
                style={[styles.button, styles.buttonBackground]}
                onPress={this.props.onConfirmPress}
              >
                <Text style={styles.buttonTextAlt}>ยืนยัน</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                activeOpacity={0.6}
                style={styles.button}
                onPress={this.props.onCancelPress}
              >
                <Text style={styles.buttonText}>ยกเลิก</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  popupContent: {
    backgroundColor: '#FFF',
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10
  },
  mainContent: {
    paddingHorizontal: 20,
    alignItems: 'center',
    alignContent: 'center'
  },
  icon: {
    marginVertical: 10
  },
  title: {
    color: '#121212',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18,
    textAlign: 'center',
    marginTop: 10
  },
  paragraph: {
    color: '#121212',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    textAlign: 'center'
  },
  buttonsContainer: {
    flexDirection: 'column',
    marginTop: 20
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  buttonBackground: {
    backgroundColor: '#535786',
    borderRadius: 8
  },
  button: {
    height: 50,
    marginHorizontal: 20,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#535786',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 17
  },
  buttonTextAlt: {
    color: 'white',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 17
  }
})

export default Trustline
