import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Image,
  SafeAreaView,
  TouchableOpacity,
  Text
} from 'react-native'
import PropTypes from 'prop-types'

// Components & Modules
import AppStyle from '../../../commons/AppStyle'

export default class AssetItem extends Component {
  static propTypes = {
    name: PropTypes.string,
    icons: PropTypes.object,
    symbol: PropTypes.string,
    active: PropTypes.bool,
    onPressItem: PropTypes.func
  }

  static defaultProps = {
    name: '',
    icons: {},
    symbol: '',
    active: false,
    onPressItem: () => { }
  }

  _onPress = () => {
    this.props.onPressItem(this.props.symbol, this.props.name)
  };

  render() {
    const {
      name, symbol, icons, active
    } = this.props
    return (
      <SafeAreaView style={styles.container}>
        <TouchableOpacity
          disabled={active}
          activeOpacity={0.7}
          onPress={this._onPress}
          style={styles.assetList}
        >
          <Image
            style={styles.assetIcon}
            source={active ? icons.active : icons.deactive}
            resizeMode="contain"
          />
          <Text style={styles.assetListText}>{symbol}</Text>
          <Text style={styles.assetListDesc}>({name})</Text>
        </TouchableOpacity>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  assetIcon: {
    height: 40,
    width: 40
  },
  assetList: {
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#F4F4F7',
    paddingVertical: 13,
    paddingHorizontal: 20,
    alignItems: 'center'
  },
  assetListText: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18,
    marginLeft: 14,
    width: 60
  },
  assetListDesc: {
    color: '#AFB0BD',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18
  }
})
