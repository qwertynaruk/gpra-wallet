import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Text
} from 'react-native'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'

// Components & Modules
import AppStyle from '../../../commons/AppStyle'

@observer
export default class DeveloperItem extends Component {
  static propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.bool,
    onPressItem: PropTypes.func
  }

  static defaultProps = {
    id: '',
    name: '',
    value: false,
    onPressItem: () => { }
  }

  _onPress = () => {
    this.props.onPressItem(this.props.id)
  };

  render() {
    const {
      name, value
    } = this.props
    return (
      <SafeAreaView style={styles.container}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={this._onPress}
        >
          <View style={styles.assetList}>
            <View style={styles.row}>
              <View style={styles.assetTextView}>
                <Text style={styles.assetText}>{name}</Text>
              </View>
            </View>
            <View>
              {
                value
                  ? (
                    <Text style={{ fontFamily: AppStyle.mainFontBold, color: '#007bff' }}>Active</Text>
                  )
                  : (
                    <Text style={{ fontFamily: AppStyle.mainFontBold, color: '#dc3545' }}>Deactive</Text>
                  )
              }
            </View>
          </View>
        </TouchableOpacity>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  row: {
    flexDirection: 'row'
  },
  assetText: {
    color: '#000',
    fontSize: 16,
    fontFamily: AppStyle.mainFontBoldTH
  },
  assetAKA: {
    color: '#BBB',
    fontSize: 16,
    fontFamily: AppStyle.mainFontTH,
    paddingRight: 10
  },
  assetList: {
    borderBottomWidth: 1,
    borderBottomColor: '#DDD',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 20,
    justifyContent: 'space-between'
  },
  assetIcon: {
    height: 30,
    width: 30
  },
  assetTextView: {
    flexDirection: 'row',
    alignItems: 'center'
  }
})
