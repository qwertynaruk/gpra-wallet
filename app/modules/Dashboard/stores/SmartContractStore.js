import { observable, action } from 'mobx'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import SecureDS from '../../../AppStores/DataSource/SecureDS'
import icons from '../../../commons/icons'
import Stellar from '../../../Utils/Stellar'
import api from '../../../api'
import url from '../../../api/url'

export default class SmartContractStore {
  @observable tokenList = []
  @observable offers = []
  @observable loaded = false
  @observable empty = false

  @action getTokeniz() {
    this.loaded = false
    api.Aom.tokenizeList()
      .then((res) => {
        this.loaded = true
        const { status } = res
        const { success, data } = res.data
        if (status !== 200 || !success) {
          throw new Error('Status not 200')
        }

        if (data.length) {
          this.tokenList = data.map(item => ({
            key: `${item.asset_code}-${item.issuer_id}`,
            icon: item.asset_logo ? { uri: item.asset_logo } : icons.iconTokeniz,
            code: item.asset_code,
            name: item.asset_name,
            issuer: item.issuer_id,
            offers: item.offers
          }))
        } else {
          this.empty = true
        }
      })
      .catch(() => {
        this.loaded = true
        this.empty = true
      })
  }

  @action getOffers(address) {
    Stellar.Offer.MyOffers(address)
      .then((resp) => {
        const { status, data } = resp
        if (status === 200) {
          const { records } = data._embedded
          this.offers = records
        }
      })
  }

  deleteOffer(offer) {
    if (MainStore.appState.internetConnection === 'offline') {
      NavStore.popupCustom.show('No internet connection')
      return
    }
    NavStore.lockScreen({
      onUnlock: (pincode) => {
        NavStore.showLoading()
        const ds = new SecureDS(pincode)
        const { OMWallet } = MainStore.appState
        OMWallet.setSecureDS(ds)

        // Get passphase
        const passphrase = url.OmChain.getPassphrase()

        OMWallet.derivePrivateKey()
          .then(privateKey => Stellar.Offer.MakeOffer(Stellar.Server(), privateKey, offer, OMWallet.BASE_FEE, passphrase))
          .then(pdata => api.Aom.confirmTransaction(pdata))
          .then((resp) => {
            const { status } = resp
            if (status === 200) {
              const { code, success, data } = resp.data
              this.getOffers(OMWallet.address)
              NavStore.showToastTop(`การดำเนินการเสร็จสิ้น`, {}, {})
              NavStore.hideLoading()
              NavStore.goBack()
            } else {
              const { title, extras } = resp.data
              api.Aom.reportBug({
                address: MainStore.appState.selectedWallet.address,
                error_component: 'SmartContract',
                error_function: 'deleteOffer',
                error_detail: JSON.stringify(extras),
                error_message: `${title} ${extras.result_codes.transaction}`
              })
            }
          })
          .catch(error => console.log(error))
      }
    }, true)
  }

  makeOffer(offer) {
    if (MainStore.appState.internetConnection === 'offline') {
      NavStore.popupCustom.show('No internet connection')
      return
    }
    NavStore.lockScreen({
      onUnlock: (pincode) => {
        NavStore.showLoading()
        const ds = new SecureDS(pincode)
        const { OMWallet } = MainStore.appState
        OMWallet.setSecureDS(ds)

        // Get passphase
        const passphrase = url.OmChain.getPassphrase()

        OMWallet.derivePrivateKey()
          .then(privateKey => Stellar.Offer.MakeOffer(Stellar.Server(), privateKey, offer, OMWallet.BASE_FEE, passphrase))
          .then(pdata => api.Aom.confirmTransaction(pdata))
          .then((resp) => {
            const { status } = resp
            if (status === 200) {
              const { code, success, data } = resp.data
              this.getOffers(OMWallet.address)
              NavStore.showToastTop(`การดำเนินการเสร็จสิ้น`, {}, {})
              NavStore.hideLoading()
              NavStore.goBack()
            } else {
              const { title, extras } = resp.data
              api.Aom.reportBug({
                address: MainStore.appState.selectedWallet.address,
                error_component: 'SmartContract',
                error_function: 'makeOffer',
                error_detail: JSON.stringify(extras),
                error_message: `${title} ${extras.result_codes.transaction}`
              })
            }
          })
          .catch(error => console.log(error))
      }
    }, true)
  }
}
