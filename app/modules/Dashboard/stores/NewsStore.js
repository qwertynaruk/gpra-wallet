import { action } from 'mobx'
import moment from 'moment'
import Api from '../../../api'

moment.locale('th')

export default class NewsStore {
  news = [];
  @action getNews(id) {
    const news = this.news.find(obj => obj.id === id)
    return Promise.resolve(news)
  }

  @action getAllNews() {
    return Api.Aom.getAllNews().then((res) => {
      if (res.status === 200) {
        const news = res.data.map((data) => {
          const {
            id, title, thumbnail, url, provider, color
          } = data
          const date = moment(data.date)
          const day = date.format('D')
          const month = date.format('MMMM')
          const year = Number(date.format('YYYY')) + 543
          const result = {
            id,
            title,
            thumbnail,
            url,
            provider,
            date: `${day} ${month} ${year}`,
            color
          }
          this.news.push(result)
          return result
        })
        return news
      }
      return []
    }).catch(e => console.log(e))
  }
}
