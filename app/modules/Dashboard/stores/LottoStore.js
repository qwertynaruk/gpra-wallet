// Third-party
import { observable, action, computed } from 'mobx'
import { BigNumber } from 'bignumber.js'

// Components & Modules
import api from '../../../api'

export default class LottoStore {
  @observable.ref provider = []
  @observable.ref prize = []
  @observable loading = true
  @observable minimum = 0

  @action async getLottery(address) {
    const provider = await api.Aom.getLotteryDetail()
    if (provider && provider.data && provider.status === 200) {
      this.provider = provider.data.data
    }

    const prize = await api.Aom.getLottery2(address, 0)
    if (prize && prize.data && prize.status === 200) {
      this.prize = prize.data.data
    }

    this.loading = false
  }

  @action getLottoBAAC() {
    const name = 'ธ.ก.ส'
    const { provider } = this
    if (provider.length) {
      const filtered = provider.filter(obj => obj.pd_active === 'Y' && obj.pd_title === name)

      if (filtered.length) {
        // Get sum of filtered
        const summary = filtered.reduce((prev, current) => prev + Number(current.pd_amount), 0)

        return {
          summary,
          filtered
        }
      }
    }

    return null
  }

  @action getLottoGSB() {
    const name = 'ออมสิน'
    const { provider } = this
    if (provider.length) {
      const filtered = provider.filter(obj => obj.pd_active === 'Y' && obj.pd_title === name)

      if (filtered.length) {
        // Get sum of filtered
        const summary = filtered.reduce((prev, current) => prev + Number(current.pd_amount), 0)

        return {
          summary,
          filtered
        }
      }
    }

    return null
  }

  @action summaryPeriodReward(period) {
    if (this.prize.length) {
      return this.prize.filter(item => item.ri_id === period).reduce((sum, item) => {
        if (item.user_reward.length) {
          const rewards = item.user_reward.reduce((total, obj) => new BigNumber(total).plus(obj.user_reward).toNumber(), 0)
          return new BigNumber(sum).plus(rewards).toNumber()
        }
        return 0
      }, 0)
    }

    return 0
  }

  @action setMinimum(minimum) {
    this.minimum = minimum
  }

  @action setLoading(loading) {
    this.loading = loading
  }

  @computed get summaryTotalPrize() {
    return this.prize.reduce((sum, item) => {
      if (item.user_reward.length) {
        const rewards = item.user_reward.filter(obj => obj.reward_accept === 'N').reduce((total, obj) => new BigNumber(total).plus(obj.user_reward).toNumber(), 0)
        return new BigNumber(sum).plus(rewards).toNumber()
      }
      return 0
    }, 0)
  }

  @computed get summaryTotalAsset() {
    return this.provider.reduce((sum, item) => {
      if (item.pd_active === 'Y') {
        return new BigNumber(sum).plus(item.pd_asset_balance).toNumber()
      }
      return 0
    }, 0)
  }

  @computed get canReceive() {
    const total = new BigNumber(this.summaryTotalPrize)
    return total.gte(this.minimum)
  }

  @computed get moreThenZero() {
    const total = new BigNumber(this.summaryTotalPrize)
    return total.gt(0)
  }

  get latestPrize() {
    if (this.prize.length) {
      return this.prize[0].lottery[0].total_amount
    }

    return 0
  }
}
