import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  Platform,
  ActivityIndicator,
  Image
} from 'react-native'
import WebView from 'react-native-webview'
import PropTypes from 'prop-types'

// Third-party
import Share from 'react-native-share'

// Components & Modules
import MainStore from '../../../AppStores/MainStore'
import icons from '../../../commons/icons'
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const marginTop = LayoutUtils.getExtraTop()

export default class NewsDetailScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object
  }

  static defaultProps = {
    navigation: {}
  }

  constructor(props) {
    super(props)
    this.state = {
      url: '',
      news: {}
    }

    this.newsStore = MainStore.newsStore
    this.answers = MainStore.appState.fabric.answers
    this.goBack = this.goBack.bind(this)
    this.onLoadEnd = this.onLoadEnd.bind(this)
  }

  componentWillMount() {
    const { id } = this.props.navigation.state.params

    this.newsStore.getNews(id).then((news) => {
      this.setState({
        url: news.url,
        news
      })
    })
  }

  onShare = () => {
    const { url } = this.state

    Share.open({
      url
    }).then(() => {
      this.fabric.answers.logShare('Asset', 'Custom Asset', 'asset', 'share-promptpay')
    }).catch((error) => {
      console.log(`share asset error ${error.message}`)
    })
  }

  onLoadEnd() {
    const { news } = this.state
    this.answers.logCustom('View News', {
      title: news.title,
      url: news.url,
      provider: news.provider
    })
  }

  goBack() {
    this.props.navigation.goBack()
  }

  _renderLoading() {
    return (
      <View
        style={{
          height: '100%',
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          opacity: 0.5
        }}
      >
        <ActivityIndicator size="large" color="#535786" />
        <Text style={{ marginTop: 10, fontSize: 14, fontFamily: AppStyle.mainFontTH }}>กำลังโหลดข้อมูล...</Text>
      </View >
    )
  }

  _renderError() {
    return (
      <View
        style={{
          position: 'absolute',
          height: '100%',
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          opacity: 0.5
        }}
      >
        <Image source={icons.iconWarning} />
        <Text style={{ marginTop: 10, fontSize: 14, fontFamily: AppStyle.mainFontTH }}>หน้าเว็บมีปัญหา...</Text>
      </View >
    )
  }

  render() {
    const { url } = this.state

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: marginTop + 15,
            paddingBottom: 10,
            borderBottomColor: '#DDD',
            borderBottomWidth: 1
          }}
          headerItem={{
            title: 'รายละเอียดข่าว',
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            flex: 1,
            marginRight: 20,
            textAlign: 'center',
            fontFamily: AppStyle.mainFontBoldTH,
            color: '#000000'
          }}
          action={this.goBack}
          rightView={{
            rightViewIcon: icons.iconShareAsset,
            rightViewAction: () => {
              this.onShare()
            }
          }}
        />
        <View style={{ flex: 1 }}>
          <WebView
            style={styles.container}
            renderLoading={this._renderLoading}
            renderError={this._renderError}
            source={{ uri: url }}
            onLoadEnd={this.onLoadEnd}
            startInLoadingState={true}
            useWebKit={Boolean(Platform.OS === 'ios')}
          />
        </View>
        {/*  */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
