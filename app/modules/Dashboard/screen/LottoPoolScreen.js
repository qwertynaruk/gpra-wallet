import React, { Component } from 'react'
import {
  Image,
  ScrollView,
  StatusBar,
  FlatList,
  StyleSheet,
  Text,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Components & Modules
import api from '../../../api'
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import Helper from '../../../commons/Helper'
import MainStore from '../../../AppStores/MainStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'
import NavStore from '../../../AppStores/NavStore'

const marginTop = LayoutUtils.getExtraTop()
const title = 'สลากออมทรัพย์ที่ผูกกับเหรียญ'

export default class LottoPoolScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object
  }

  static defaultProps = {
    navigation: {}
  }

  constructor(props) {
    super(props)

    this.state = {
      lotteryData: []
    }

    this.goBack = this.goBack.bind(this)
  }

  componentDidMount() {
    const wallet = MainStore.appState.selectedWallet
    if (wallet) {
      NavStore.showLoading()

      api.Aom.getLotteryDetail().then((res) => {
        const { status, data } = res
        if (status === 200 && data.data) {
          this.setState({ lotteryData: data.data })
          NavStore.hideLoading()
        } else {
          throw new Error('Not found lottery data')
        }
      }).catch((error) => {
        console.log(error.message)
        NavStore.hideLoading()
      })
    }
  }

  get lottoBAAC() {
    const name = 'ธ.ก.ส'
    const { lotteryData } = this.state
    if (lotteryData.length) {
      const filtered = lotteryData.filter(obj => obj.pd_active === 'Y' && obj.pd_title === name)

      if (filtered.length) {
        // Get sum of filtered
        const summary = filtered.reduce((prev, current) => prev + Number(current.pd_amount), 0)

        return {
          summary,
          filtered
        }
      }
    }

    return null
  }

  get lottoGSB() {
    const name = 'ออมสิน'
    const { lotteryData } = this.state
    if (lotteryData.length) {
      const filtered = lotteryData.filter(obj => obj.pd_active === 'Y' && obj.pd_title === name)

      if (filtered.length) {
        // Get sum of filtered
        const summary = filtered.reduce((prev, current) => prev + Number(current.pd_amount), 0)

        return {
          summary,
          filtered
        }
      }
    }

    return null
  }

  currencyFormat(num, position) {
    if (num) {
      if (position === 0) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
      }

      return num.toFixed(position).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    return null
  }

  goBack() {
    this.props.navigation.goBack()
  }

  _keyExtractor = item => `${item.pd_title}-${item.pd_id}`

  _renderItem = ({ item }) => (
    <View>
      <View style={[styles.cardRow, styles.cardTitle]}>
        <Text style={[styles.cardTitleText, { color: '#000' }]}>
          {`${item.pd_code}`}
        </Text>
        <Text style={[styles.cardTitleTextRight, { color: '#000', textAlign: 'right' }]}>
          {Helper.formatCUR(item.pd_stock)} หน่วย
        </Text>
      </View>

      <View style={styles.cardRow}>
        <View style={styles.cardContent}>
          <Text style={styles.cardContentTitle}>
            จำนวนเหรียญที่ผูก
          </Text>
        </View>
        <Text style={styles.cardContentTitleRight}>
          {Helper.formatCUR(item.pd_amount)} OM
        </Text>
      </View>
    </View>
  )

  _renderEmpty() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1
        }}
      >
        <Text style={{ fontFamily: AppStyle.mainFontTH }}>ไม่พบข้อมูล :(</Text>
      </View>
    )
  }

  _renderBAAC() {
    const { lottoBAAC } = this

    if (lottoBAAC) {
      return (
        <View style={styles.providers}>
          {/* start Baac Part */}
          <View style={styles.cardMain}>
            <Image source={images.iconBaac} style={{ marginBottom: 10 }} />
            <Text style={styles.cardMainItem}> สลาก ธ.ก.ส. ทั้งหมดที่ผูก </Text>
            <Text style={[styles.cardMainItem, styles.fontBiggest]}> {Helper.formatCUR(lottoBAAC.summary, false, 2)} บาท </Text>
          </View>

          {/* item one */}
          <View style={[styles.card, { borderColor: '#535786' }]}>
            <FlatList
              data={lottoBAAC.filtered}
              keyExtractor={this._keyExtractor}
              renderItem={this._renderItem}
              ListEmptyComponent={this._renderEmpty}
              ItemSeparatorComponent={() => (<View style={styles.seperator} />)}
            />
          </View>
          {/* end item one */}
          {/* end Baac Part */}
        </View>
      )
    }

    return null
  }

  _renderGSB() {
    const { lottoGSB } = this

    if (lottoGSB) {
      return (
        <View style={styles.providers}>
          {/* start Aomsin Part */}
          <View style={styles.cardMainPink}>
            <Image source={images.iconAomsin} style={{ marginBottom: 10 }} />
            <Text style={styles.cardMainItem}> สลาก ออมสิน ทั้งหมดที่ผูก </Text>
            <Text style={[styles.cardMainItem, styles.fontBiggest]}> {Helper.formatCUR(lottoGSB.summary, false, 2)} บาท </Text>
          </View>

          {/* item one */}
          <View style={[styles.card, { borderColor: '#ED078E' }]}>
            <FlatList
              data={lottoGSB.filtered}
              keyExtractor={this._keyExtractor}
              renderItem={this._renderItem}
              ListEmptyComponent={this._renderEmpty}
              ItemSeparatorComponent={() => (<View style={styles.seperator} />)}
            />
          </View>
          {/* end Aomsin part */}
        </View>
      )
    }

    return null
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: marginTop + 20,
            marginBottom: 20
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            flex: 1,
            marginRight: 20,
            textAlign: 'center',
            fontFamily: AppStyle.mainFontBoldTH,
            fontSize: 16,
            color: '#121212'
          }}
          action={this.goBack}
        />

        {/* Begin Lotto Pool */}
        <View style={{
          flex: 1
        }}
        >
          <ScrollView style={{ flex: 1, paddingHorizontal: 20 }}>
            {this._renderBAAC()}
            {this._renderGSB()}
          </ScrollView>
        </View>
        {/* End Lotto Pool */}
      </View >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  providers: {
    marginBottom: 20,
    marginTop: 10
  },
  fontBiggest: {
    fontSize: 20
  },
  seperator: {
    height: 20
  },
  cardMain: {
    backgroundColor: '#535786',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
    borderTopRightRadius: 4,
    borderTopLeftRadius: 4,
    overflow: 'hidden'
  },
  cardMainPink: {
    backgroundColor: '#ED078E',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
    borderTopRightRadius: 4,
    borderTopLeftRadius: 4,
    overflow: 'hidden'
  },
  cardMainItem: {
    color: '#fff',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 16
  },
  card: {
    flexDirection: 'column',
    padding: 15,
    borderWidth: 2,
    borderBottomRightRadius: 4,
    borderBottomLeftRadius: 4
  },
  cardRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  cardTitle: {
    marginBottom: 5
  },
  cardTitleText: {
    fontFamily: AppStyle.mainFontTH,
    flex: 1,
    fontSize: 16
  },
  cardTitleTextRight: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    paddingLeft: 10
  },
  cardContent: {
    flex: 1,
    flexDirection: 'column'
  },
  cardContentTitle: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    color: '#000'
  },
  cardContentTitleRight: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 16,
    color: '#000'
  }
})
