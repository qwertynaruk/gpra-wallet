import React, { Component } from 'react'
import {
  Text,
  View,
  Alert,
  Image,
  StyleSheet,
  StatusBar,
  Platform,
  ScrollView,
  BackHandler,
  Keyboard,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback
} from 'react-native'
import { observer } from 'mobx-react/native'
import PropsType from 'prop-types'

// Components & Modules
import NavStore from '../../../AppStores/NavStore'
import MainStore from '../../../AppStores/MainStore'
import images from '../../../commons/images'
import icons from '../../../commons/icons'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const marginTop = LayoutUtils.getExtraTop()

@observer
class ManageOfferScreen extends Component {
  static propTypes = {
    navigation: PropsType.object
  }

  static defaultProps = {
    navigation: null
  }

  constructor(props) {
    super(props)

    this.state = {
      isUpdate: false,
      offerId: 0,
      buyToken: '',
      sellToken: '',
      sellPrice: '',
      sellPriceStr: '',
      sellAmount: '',
      sellAmountStr: ''
    }
    this.error = ''

    this.dataStore = MainStore.smartContractStore

    this.handleBackPress = this.handleBackPress.bind(this)
    this.onTouchDismissKeyboard = this.onTouchDismissKeyboard.bind(this)
    this.setAmount = this.setAmount.bind(this)
    this.setBuyToken = this.setBuyToken.bind(this)
    this.setSellToken = this.setSellToken.bind(this)
    this._handleManageOffer = this._handleManageOffer.bind(this)
    this._handleDeleteOffer = this._handleDeleteOffer.bind(this)
  }

  componentWillMount() {
    const { params } = this.props.navigation.state
    if (params) {
      const {
        id, selling, buying, amount, price
      } = params

      // Set sell token
      this.setSellToken({
        asset_code: selling.asset_code,
        asset_issuer: selling.asset_issuer
      })

      // Set buy token
      this.setBuyToken({
        asset_code: buying.asset_code,
        asset_issuer: buying.asset_issuer
      })

      // Set amount
      if (amount && price) {
        this.setState({
          isUpdate: Boolean(id),
          offerId: id || 0,
          sellPrice: Number(price),
          sellPriceStr: Number(price).toString(),
          sellAmount: Number(amount),
          sellAmountStr: Number(amount).toString()
        })
      }
    }
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }

    Keyboard.dismiss()
  }

  onTouchDismissKeyboard() {
    Keyboard.dismiss()
  }

  get buyToken() {
    const { asset_code, asset_issuer } = this.state.buyToken
    return asset_code ? `${asset_code}-${asset_issuer.replace(/^([a-zA-Z0-9]{2})(.+)([a-zA-Z0-9]{3})$/, '$1..$3')}` : ''
  }

  get sellToken() {
    const { asset_code, asset_issuer } = this.state.sellToken
    return asset_code ? `${asset_code}-${asset_issuer.replace(/^([a-zA-Z0-9]{2})(.+)([a-zA-Z0-9]{3})$/, '$1..$3')}` : ''
  }

  get disabled() {
    const {
      buyToken, sellToken, sellAmount, sellPrice
    } = this.state

    if (!buyToken || !sellToken) {
      return true
    }

    if (
      (buyToken.asset_code === sellToken.asset_code) &&
      (buyToken.asset_issuer === sellToken.asset_issuer)
    ) {
      this.error = 'โทเคนที่ต้องการแลกและรับไม่ควรเหมือนกัน'
      return true
    }

    if (sellAmount !== '' && sellAmount < 1) {
      this.error = 'จำนวนที่ต้องการแลกขั้นต่ำคือ 1'
      return true
    }

    if (sellAmount !== '' && sellAmount > sellToken.balance) {
      this.error = `โทเคนของท่านมีไม่พอแลกเปลี่ยน\nโทเคนที่มี ${sellToken.balance} ${sellToken.asset_code}`
      return true
    }

    if (sellPrice !== '' && sellPrice <= 0) {
      this.error = 'ราคาแลกต่อหนึ่งหน่วยควรมากกว่า 0'
      return true
    }

    if (!sellAmount || !sellPrice) {
      this.error = ''
      return true
    }

    this.error = ''
    return false
  }

  get errorMsg() {
    if (this.error) {
      return (
        <View style={styles.errorBlock}>
          <Text style={styles.errorText}>{this.error}</Text>
        </View>
      )
    }

    return null
  }

  setAmount = type => (input) => {
    let inputAmount = input.trim()
      // eslint-disable-next-line no-useless-escape
      .replace(/([^\d\.])/g, '')
      .replace(/^0+/, '0')
      .replace(/^0([1-9])+/, '$1')
      .replace(/^\./, '0.')

    inputAmount = inputAmount
      .replace(/^(\.\d\d?).*/, '$1')
      .replace(/(\d{1,9})(\d+)?(\.?)(\d\d?)?.*/, '$1$3$4')

    const amount = this.parseAmount(inputAmount.replace(/\.$/, '.0'))

    switch (type) {
      case 'sellPrice':
        this.setState({
          sellPrice: amount,
          sellPriceStr: inputAmount.toString()
        })
        break
      case 'sellAmount':
      default:
        this.setState({
          sellAmount: amount,
          sellAmountStr: inputAmount.toString()
        })
        break
    }
  }

  setSellToken(data) {
    // Set balance
    const { asset_code, asset_issuer } = data
    const { balances } = MainStore.appState.OMWallet
    const asset = balances.find(b => b.asset_code === asset_code && b.asset_issuer === asset_issuer)
    if (asset) {
      Object.assign(data, {
        balance: asset.balance
      })
    }
    this.setState({
      sellToken: data
    })
  }

  setBuyToken(data) {
    this.setState({
      buyToken: data
    })
  }

  parseAmount(input) {
    const amount = Number(`0${input}`)
    return isFinite(amount) ? amount : 0
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  goBack() {
    NavStore.goBack()
  }

  _handleManageOffer() {
    const {
      offerId, isUpdate, buyToken, sellToken, sellAmount, sellAmountStr, sellPrice, sellPriceStr
    } = this.state

    Alert.alert(
      'ยืนยันข้อมูล',
      `${isUpdate ? 'แก้ไข' : 'สร้าง'}รายการแลกเปลี่ยน\n${sellToken.asset_code} ระหว่าง ${buyToken.asset_code}\nจำนวนที่ขาย ${sellAmountStr} ${sellToken.asset_code}\nราคาต่อหน่วย ${sellPriceStr} ${buyToken.asset_code}`, [
        {
          text: 'ยกเลิก',
          style: 'cancel'
        },
        {
          text: 'ยืนยัน',
          onPress: () => {
            this.dataStore.makeOffer({
              offerId,
              selling: sellToken,
              buying: buyToken,
              amount: sellAmount,
              price: sellPrice
            })
          }
        }
      ], {
        cancelable: true
      }
    )
  }

  _handleDeleteOffer() {
    const {
      offerId, buyToken, sellToken, sellPrice
    } = this.state

    Alert.alert(
      'กำลังลบรายการ',
      'ยืนยันลบรายการนี้ใช่หรือไม่',
      [
        {
          text: 'ยกเลิก',
          style: 'cancel'
        },
        {
          text: 'ยืนยัน',
          onPress: () => {
            this.dataStore.deleteOffer({
              offerId,
              selling: sellToken,
              buying: buyToken,
              amount: 0,
              price: sellPrice
            })
          }
        }
      ]
    )
  }

  render() {
    const {
      buyToken, sellToken, disabled, errorMsg
    } = this
    const { isUpdate, sellAmountStr, sellPriceStr } = this.state
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: marginTop + 15,
            paddingBottom: 10,
            borderBottomColor: '#DDD',
            borderBottomWidth: 1
          }}
          headerItem={{
            title: 'Manage Offer',
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            flex: 1,
            fontSize: 18,
            marginRight: 20,
            textAlign: 'center',
            fontFamily: AppStyle.mainFontBoldTH,
            color: '#000000'
          }}
          action={this.goBack}
        />
        <View style={styles.container}>
          <TouchableWithoutFeedback
            onPress={this.onTouchDismissKeyboard}
          >
            <ScrollView
              style={styles.container}
              contentContainerStyle={styles.scrollContent}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
            >
              <View>
                <View>
                  <Text style={styles.title}>Token ที่ต้องการแลก</Text>
                  <View style={[styles.textInputField, { height: 40, paddingHorizontal: 0 }]}>
                    <View style={{ flexDirection: 'row' }}>
                      <TouchableOpacity
                        style={{ flex: 1, justifyContent: 'center', paddingHorizontal: 10 }}
                        onPress={() => {
                          NavStore.pushToScreen('TokenListScreen', {
                            current: this.state.sellToken,
                            selected: this.state.buyToken,
                            returnData: this.setSellToken
                          })
                        }}
                      >
                        <View>
                          <Text
                            style={{
                              fontFamily: AppStyle.mainFontTH,
                              fontSize: 14
                            }}
                          >
                            {sellToken || 'เลือกโทเคน'}
                          </Text>
                        </View>
                      </TouchableOpacity>
                      {
                        (sellToken !== '') && (
                          <TouchableOpacity
                            style={{ marginRight: 10 }}
                            onPress={() => {
                              this.setState({
                                sellToken: ''
                              })
                            }}
                          >
                            <Image
                              source={icons.iconResetBtn}
                            />
                          </TouchableOpacity>
                        )
                      }
                    </View>
                  </View>
                </View>
                <View>
                  <Text style={styles.title}>Token ที่ต้องการรับ</Text>
                  <View style={[styles.textInputField, { height: 40, paddingHorizontal: 0 }]}>
                    <View style={{ flexDirection: 'row' }}>
                      <TouchableOpacity
                        style={{ flex: 1, justifyContent: 'center', paddingHorizontal: 10 }}
                        onPress={() => {
                          NavStore.pushToScreen('TokenListScreen', {
                            current: this.state.buyToken,
                            selected: this.state.sellToken,
                            returnData: this.setBuyToken
                          })
                        }}
                      >
                        <View>
                          <Text
                            style={{
                              fontFamily: AppStyle.mainFontTH,
                              fontSize: 14
                            }}
                          >
                            {buyToken || 'เลือกโทเคน'}
                          </Text>
                        </View>
                      </TouchableOpacity>
                      {
                        (buyToken !== '') && (
                          <TouchableOpacity
                            style={{ marginRight: 10 }}
                            onPress={() => {
                              this.setState({
                                buyToken: ''
                              })
                            }}
                          >
                            <Image
                              source={icons.iconResetBtn}
                            />
                          </TouchableOpacity>
                        )
                      }
                    </View>
                  </View>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 1, marginRight: 7 }}>
                    <Text style={styles.title}>จำนวนที่ต้องการแลก</Text>
                    <View style={styles.textInputField}>
                      <TextInput
                        ref={(ref) => { this.sellAmountTokenRef = ref }}
                        style={styles.textInput}
                        autoCapitalize="none"
                        returnKeyType={Platform.OS === 'android' ? 'done' : 'default'}
                        underlineColorAndroid="transparent"
                        maxLength={10}
                        keyboardAppearance="dark"
                        keyboardType="numeric"
                        selectionColor="#FFF"
                        value={sellAmountStr}
                        onChangeText={this.setAmount('sellAmount')}
                        placeholder="0"
                      />
                    </View>
                  </View>
                  <View style={{ flex: 1, marginLeft: 8 }}>
                    <Text style={styles.title}>ราคาต่อหนึ่งหน่วย</Text>
                    <View style={styles.textInputField}>
                      <TextInput
                        ref={(ref) => { this.sellPriceTokenRef = ref }}
                        style={styles.textInput}
                        autoCapitalize="none"
                        returnKeyType={Platform.OS === 'android' ? 'done' : 'default'}
                        underlineColorAndroid="transparent"
                        maxLength={10}
                        keyboardAppearance="dark"
                        keyboardType="numeric"
                        selectionColor="#FFF"
                        value={sellPriceStr}
                        onChangeText={this.setAmount('sellPrice')}
                        placeholder="0"
                      />
                    </View>
                  </View>
                </View>
                {errorMsg}
                {
                  isUpdate && (
                    <View>
                      <View
                        style={{
                          alignItems: 'center',
                          flexDirection: 'row'
                        }}
                      >
                        <TouchableOpacity
                          disabled={disabled}
                          style={[styles.createOfferButton, disabled ? styles.btnDisabled : {}, { backgroundColor: '#ffc107' }]}
                          onPress={this._handleManageOffer}
                        >
                          <Text style={[styles.createOfferText, disabled ? styles.btnDisabledText : {}, { color: '#000' }]}>UPDATE OFFER</Text>
                        </TouchableOpacity>
                      </View>
                      <View
                        style={{
                          alignItems: 'center',
                          flexDirection: 'row',
                          marginTop: 15
                        }}
                      >
                        <TouchableOpacity
                          disabled={disabled}
                          style={[styles.createOfferButton, disabled ? styles.btnDisabled : {}, { backgroundColor: '#f5365c' }]}
                          onPress={this._handleDeleteOffer}
                        >
                          <Text style={[styles.createOfferText, disabled ? styles.btnDisabledText : {}]}>DELETE OFFER</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  )
                }
                {
                  !isUpdate && (
                    <View
                      style={{
                        alignItems: 'center',
                        flexDirection: 'row'
                      }}
                    >
                      <TouchableOpacity
                        disabled={disabled}
                        style={[styles.createOfferButton, disabled ? styles.btnDisabled : {}]}
                        onPress={this._handleManageOffer}
                      >
                        <Text style={[styles.createOfferText, disabled ? styles.btnDisabledText : {}]}>CREATE OFFER</Text>
                      </TouchableOpacity>
                    </View>
                  )
                }
              </View>
            </ScrollView>
          </TouchableWithoutFeedback>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  scrollContent: {
    flex: 1,
    padding: 20
  },
  title: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14,
    marginBottom: 5
  },
  createOfferButton: {
    alignItems: 'center',
    backgroundColor: '#42B884',
    borderRadius: 10,
    flex: 1,
    height: 50,
    justifyContent: 'center'
  },
  createOfferText: {
    color: '#FFF',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18
  },
  btnDisabled: {
    backgroundColor: '#EEE'
  },
  btnDisabledText: {
    color: '#FFF'
  },
  textInputField: {
    borderColor: '#BBB',
    borderWidth: 1,
    borderRadius: 7,
    justifyContent: 'center',
    marginBottom: 15,
    paddingHorizontal: 10
  },
  textInput: {
    color: '#000',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 13,
    height: 40
  },
  errorBlock: {
    marginBottom: 15
  },
  errorText: {
    color: '#EA4A4A',
    fontFamily: AppStyle.mainFontSemiBoldTH,
    fontSize: 14,
    textAlign: 'center'
  }
})

export default ManageOfferScreen
