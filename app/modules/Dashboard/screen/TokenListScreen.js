import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  Platform,
  ScrollView,
  BackHandler,
  FlatList,
  Image,
  SafeAreaView,
  TouchableOpacity
} from 'react-native'
import { observer } from 'mobx-react/native'
import PropsType from 'prop-types'

// Components & Modules
import NavStore from '../../../AppStores/NavStore'
import MainStore from '../../../AppStores/MainStore'
import icons from '../../../commons/icons'
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import NavigationHeader from '../../../components/elements/NavigationHeader'
import TokenIcon from '../element/TokenIcon'

const marginTop = LayoutUtils.getExtraTop()
const title = 'เลือกโทเคนที่ต้องการ'

@observer
class TokenListScreen extends Component {
  static propTypes = {
    navigation: PropsType.object
  }

  static defaultProps = {
    navigation: null
  }

  constructor(props) {
    super(props)

    this.state = {
      current: null,
      selected: null
    }

    this.handleBackPress = this.handleBackPress.bind(this)
    this._renderToken = this._renderToken.bind(this)
    this._renderHeader = this._renderHeader.bind(this)
  }

  componentWillMount() {
    const { params } = this.props.navigation.state
    if (params) {
      const {
        current, selected
      } = params

      this.setState({
        current: current || null,
        selected: selected || null
      })
    }
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  handleBackPress() {
    this.goBack()
    return true
  }

  goBack() {
    NavStore.goBack()
  }

  _renderHeader() {
    const { OMWallet } = MainStore.appState

    if (!OMWallet.funded) {
      return null
    }

    return (
      <SafeAreaView style={styles.switchAsset}>
        <TouchableOpacity
          style={styles.createAsset}
          onPress={() => {
            NavStore.pushToScreen('CreateTrustLineScreen')
          }}
        >
          <Image source={icons.iconCreateAsset} resizeMode="contain" />
          <Text style={styles.createAssetText}>เพิ่มเหรียญในระบบ</Text>
        </TouchableOpacity>
      </SafeAreaView>
    )
  }

  _renderToken = ({ item }) => {
    if (item.asset_code) {
      const { current, selected } = this.state
      const { asset_code, asset_issuer } = item
      const issuer = asset_issuer.replace(/^([A-Z0-9]{2})(.+)([A-Z0-9]{5})$/, '$1...$3')
      const disabled = selected ? Boolean(selected.asset_issuer !== asset_issuer) : false
      const select = current ? Boolean(current.asset_code === asset_code && current.asset_issuer === asset_issuer) : false
      const duplicated = selected ? Boolean(selected.asset_code === asset_code && selected.asset_issuer === asset_issuer) : false
      return (
        <SafeAreaView style={styles.container}>
          <TouchableOpacity
            disabled={disabled || select || duplicated}
            activeOpacity={disabled || duplicated ? 0.2 : 0.7}
            style={[styles.switchAsset, disabled || duplicated ? { opacity: 0.2 } : {}, select ? { backgroundColor: '#F1FCEF' } : {}]}
            onPress={() => {
              this.props.navigation.state.params.returnData({ asset_code, asset_issuer })
              this.props.navigation.goBack()
            }}
          >
            <TokenIcon
              code={asset_code}
              issuer={asset_issuer}
              style={styles.tokenIcon}
            />
            <Text style={styles.tokenListText}>{asset_code}</Text>
            <Text style={[styles.tokenListDesc, { flex: 1 }]}>({issuer})</Text>
            {
              select && (
                <Image
                  source={icons.iconChecked}
                  resizeMode="contain"
                />
              )
            }
          </TouchableOpacity>
        </SafeAreaView>
      )
    }

    return null
  }

  render() {
    const { balances } = MainStore.appState.OMWallet

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            borderBottomColor: '#F4F4F7',
            borderBottomWidth: 1,
            marginTop: marginTop + 20,
            paddingHorizontal: 20,
            paddingBottom: 15
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            color: '#000',
            fontSize: 16,
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.goBack}
        />
        <ScrollView
          style={styles.container}
          showsVerticalScrollIndicator={false}
        >
          <FlatList
            data={balances}
            keyExtractor={item => `${item.asset_code}-${item.asset_issuer}`}
            renderItem={this._renderToken}
            ListHeaderComponent={this._renderHeader}
          />
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  switchAsset: {
    borderBottomWidth: 1,
    borderBottomColor: '#F4F4F7',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 13,
    paddingHorizontal: 20,
    justifyContent: 'space-between'
  },
  createAsset: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  createAssetText: {
    color: '#D5D6DF',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    marginLeft: 14
  },
  tokenIcon: {
    height: 40,
    width: 40
  },
  tokenListText: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18,
    marginLeft: 14
  },
  tokenListDesc: {
    color: '#AFB0BD',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    marginLeft: 14
  }
})

export default TokenListScreen
