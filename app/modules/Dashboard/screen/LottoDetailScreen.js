import React, { Component } from 'react'
import {
  BackHandler,
  FlatList,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Components & Modules
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const marginTop = LayoutUtils.getExtraTop()

export default class LottoDetailScreen extends Component {
  static propTypes = {
    navigation: PropTypes.object
  }

  static defaultProps = {
    navigation: {
      state: {
        params: {
          title: '-',
          period_date_thai: '-',
          total_reward: '-',
          detail_lottery: '-'
        }
      }
    }
  }

  constructor(props) {
    super(props)

    this.goBack = this.goBack.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  handleBackPress() {
    this.goBack()
  }

  goBack() {
    this.props.navigation.goBack()
  }

  _keyExtractor = (item, index) => index.toString()

  _renderItem = ({ item }) => {
    let detail = []
    let color = null
    let totalReward = 0

    if (item.provider_title === 'ออมสิน') {
      color = colors.pink
    } else {
      color = colors.blue
    }

    if (item.detail) {
      detail = item.detail.map((info) => {
        totalReward += parseFloat(info.amount)

        return (
          <View key={info.prize_number} style={styles.lottoRewards}>
            <View style={styles.lottoRewardItem}>
              <View>
                <Text style={styles.lottoReward}>
                  {info.prize_number}
                </Text>
                <Text style={styles.lottoRewardTextSub}>
                  {info.prize_txt} (x {info.qty})
                </Text>
              </View>
              <Text style={styles.lottoRewardText}>
                {info.amount}
              </Text>
            </View>
          </View>
        )
      })
    }

    return (
      <ScrollView style={styles.scroll}>
        <View style={styles.lottoCardRow}>
          <View style={styles.lottoCardContent}>
            <Text style={[styles.lottoCardContentSubTitle, { color }]}>
              {item.provider_title}
            </Text>
          </View>
        </View>

        {detail}

        <View style={styles.lottoRewards}>
          <View style={[styles.lottoRewardItem, styles.borderTop]}>
            <Text style={[styles.lottoRewardText, { fontFamily: AppStyle.mainFontBoldTH }]}>
              รวม
            </Text>
            <Text style={[styles.lottoRewardText, { fontFamily: AppStyle.mainFontBold }]}>
              {totalReward.toFixed(2)}
            </Text>
          </View>
        </View>

      </ScrollView>
    )
  }

  render() {
    const { navigation } = this.props
    const {
      title,
      period_date_thai,
      total_reward,
      detail_lottery
    } = navigation.state.params

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: marginTop + 20,
            marginBottom: 20
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            flex: 1,
            marginRight: 20,
            textAlign: 'center',
            fontFamily: AppStyle.mainFontBoldTH,
            fontSize: 16,
            color: '#121212'
          }}
          action={this.goBack}
        />

        <ScrollView style={styles.scrollView}>
          <View style={styles.lottoCard}>
            <View style={[styles.lottoCardRow, styles.lottoCardTitle]}>
              <Text style={[styles.lottoCardTitleText, { color: colors.absoluteblack }]}>
                {period_date_thai}
              </Text>
              <Text style={[styles.lottoCardTitleText, { color: colors.absoluteblack, marginLeft: 'auto' }]}>
                {total_reward.toFixed(2)}
              </Text>
            </View>

            <FlatList
              data={detail_lottery}
              keyExtractor={this._keyExtractor}
              renderItem={this._renderItem}
            />
          </View>
        </ScrollView>
      </View>
    )
  }
}

const colors = {
  pink: '#ED078E',
  blue: '#352882',
  gray: '#E0E0E0',
  black: '#777777',
  absoluteblack: '#000000'
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  scrollView: {
    flex: 1,
    paddingHorizontal: 20,
    marginBottom: 20
  },
  lottoCard: {
    flexDirection: 'column',
    padding: 15,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.gray
  },
  lottoCardRow: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  lottoCardTitle: {
    borderBottomWidth: 1,
    borderBottomColor: colors.gray,
    paddingBottom: 10
  },
  lottoCardTitleText: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18
  },
  lottoCardContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'baseline',
    marginTop: 16
  },
  lottoCardContentSubTitle: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18
  },
  lottoRewards: {
    flex: 1,
    flexDirection: 'column'
  },
  lottoRewardItem: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 6
  },
  lottoReward: {
    color: '#000',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18
  },
  lottoRewardText: {
    color: '#000',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18
  },
  lottoRewardTextSub: {
    color: colors.black,
    fontFamily: AppStyle.mainFontTH,
    fontSize: 13
  },
  borderTop: {
    borderTopColor: '#EEE',
    borderTopWidth: 1,
    marginTop: 10,
    paddingTop: 10
  }
})
