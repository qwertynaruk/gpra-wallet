import React, { Component } from 'react'
import {
  Alert,
  View,
  StyleSheet,
  Clipboard,
  Image,
  Platform,
  StatusBar,
  Linking,
  Text,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  BackHandler,
  ScrollView
} from 'react-native'

// Third-party
import { observer } from 'mobx-react/native'
import 'moment/locale/th'
import moment from 'moment'
import LinearGradient from 'react-native-linear-gradient'
import Modal from 'react-native-modalbox'
import QRCode from 'react-native-qrcode-svg'
import RNFS from 'react-native-fs'
import Share from 'react-native-share'
import SplashScreen from 'react-native-splash-screen'
import Swiper from 'react-native-swiper'
import ViewShot from 'react-native-view-shot'
import DeviceInfo from 'react-native-device-info'

// Components & Modules
import icons from '../../../commons/icons'
import images from '../../../commons/images'
import api from '../../../api'
import AppStyle from '../../../commons/AppStyle'
import Helper from '../../../commons/Helper'
import HeaderMenu from '../element/HeaderMenu'
import LayoutUtils from '../../../commons/LayoutUtils'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import News from '../element/News'
import AppVersion from '../../../AppStores/stores/AppVersion'
import PushNotificationHelper from '../../../commons/PushNotificationHelper'
import NotificationStore from '../../../AppStores/stores/Notification'
import Router from '../../../AppStores/Router'
import SecureDS from '../../../AppStores/DataSource/SecureDS'
import NewVersionAvailable from '../element/NewVersionAvailable'
import CompleteVerifyEmail from '../element/CompleteVerifyEmail'
import RequestMultiSign from '../element/RequestMultiSign'
import RequestKYC from '../element/RequestKYC'
import WalletList from '../element/WalletList'
import SmartContract from '../element/SmartContract'
import Transaction from '../element/Transactions'

const marginTop = LayoutUtils.getExtraTop()
const { width, height } = Dimensions.get('window')
const isSmallScreen = height < 569

@observer
class DashboardScreen extends Component {
  constructor(props) {
    super(props)
    PushNotificationHelper.resetBadgeNumber()
    this.lastIndex = 0
    this.showQR = false
    this.isOpenShare = false
    this.state = {
      enableNotification: false,
      tabIndex: 1,
      tabCurrent: 'service',
      signing: false,
      headerHeight: 0
    }

    this._goToKYC = this._goToKYC.bind(this)
    this._toggleReceiveQR = this._toggleReceiveQR.bind(this)
    this._multiSignature = this._multiSignature.bind(this)
    this._confirmMultiSignature = this._confirmMultiSignature.bind(this)
    this.activeTab = this.activeTab.bind(this)
    this.activeTabByIndex = this.activeTabByIndex.bind(this)
    this.showBalanceByIndex = this.showBalanceByIndex.bind(this)
    this.onCopy = this.onCopy.bind(this)
    this.onSendPress = this.onSendPress.bind(this)
    this.onHistoryPress = this.onHistoryPress.bind(this)
    this.onBuyPress = this.onBuyPress.bind(this)
    this.onReceivePress = this.onReceivePress.bind(this)
    this.onShare = this.onShare.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
    this.toggleNotification = this.toggleNotification.bind(this)
    this.deepLinkRTC = this.deepLinkRTC.bind(this)
    this.serviceOmDeposit = this.serviceOmDeposit.bind(this)
    this.serviceOmExplorer = this.serviceOmExplorer.bind(this)
    this.serviceOmSmartContract = this.serviceOmSmartContract.bind(this)
    this.serviceOmTokenization = this.serviceOmTokenization.bind(this)

    this.answers = MainStore.appState.fabric.answers
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }

    AppVersion.getLastest()
    setTimeout(() => {
      SplashScreen.hide()
      if (!MainStore.appState.hasPassword || MainStore.appState.wallets.length === 0) {
        NavStore.pushToScreen('TutorialStack')
      } else {
        NavStore.pushToScreen('UnlockScreen', {
          isLaunchApp: true,
          onUnlock: this.onUnlock
        })
      }
    })
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  onUnlock() {
    MainStore.appState.startAllBgJobs()
    if (!NotificationStore.isInitFromNotification) {
      NotificationStore.receivedNotification()
    } else {
      NotificationStore.isInitFromNotification = false
      NotificationStore.receivedNotification()
    }
    if (!MainStore.appState.selectedWallet) {
      const wallet = MainStore.appState.wallets.find(obj => obj.type === 'aomtoken')
      if (wallet) {
        MainStore.appState.setSelectedWallet(wallet)
        // wallet.fetchingBalance()
        // wallet.fetchingTransaction()
        NotificationStore.fetchNotifyList(wallet.address)
        MainStore.appState.fabric.setCrashlytics(wallet.address)
      } else {
        NavStore.pushToScreen('CreateWalletScreen')
      }
    } else {
      // MainStore.appState.selectedWallet.transactions = []
      // MainStore.appState.selectedWallet.fetchingBalance()
      // MainStore.appState.selectedWallet.fetchingTransaction()
      MainStore.appState.fabric.setCrashlytics(MainStore.appState.selectedWallet.address)
      NotificationStore.fetchNotifyList(MainStore.appState.selectedWallet.address)
    }
  }

  onCopy() {
    Clipboard.setString(MainStore.appState.selectedWallet.address)
    NavStore.showToastTop(
      'คัดลอกสำเร็จ',
      {
        backgroundColor: AppStyle.backgroundToastColor
      },
      {
        color: AppStyle.mainColor,
        fontFamily: AppStyle.mainFontBoldTH
      }
    )
  }

  onShare() {
    if (!this.isOpenShare) {
      this.isOpenShare = true
      this.viewShot.capture().then((uri) => {
        const filePath = Platform.OS === 'ios' ? uri : uri.replace('file://', '')
        this.openShare(filePath)
      })
    }
  }

  onSendPress() {
    const { selectedWallet } = MainStore.appState
    if (!selectedWallet) {
      return
    }

    const { kycLayerOne } = MainStore.appState

    if (kycLayerOne) {
      Router.SendAom.goToSendAom()
      MainStore.appState.setselectedToken(selectedWallet.tokens[0])
      MainStore.sendTransaction.changeIsToken(false)
      this.answers.logContentView('Send Om', 'screen', 'send-om-screen')
    } else {
      this.requestKycModal.open()
    }
  }

  onHistoryPress() {
    const { selectedWallet } = MainStore.appState
    if (!selectedWallet) {
      return
    }

    const { kycLayerOne } = MainStore.appState

    if (kycLayerOne) {
      MainStore.appState.selectedWallet.fetchingTransaction()
      this.transactionModal.open()
    } else {
      this.requestKycModal.open()
    }
  }

  onBuyPress() {
    const { kycLayerOne } = MainStore.appState
    if (kycLayerOne) {
      NavStore.pushToScreen('BuyAomScreen')
      this.answers.logContentView('Buy OM', 'screen', 'buy-aom-screen')
    } else {
      this.requestKycModal.open()
    }
  }

  onReceivePress() {
    const { kycLayerOne } = MainStore.appState
    if (kycLayerOne) {
      this._toggleReceiveQR()
      this.answers.logContentView('Receive QR Code', 'modal', 'receive-qr-modal')
    } else {
      this.requestKycModal.open()
    }
  }

  get lastestBuild() {
    return AppVersion.latestVersion.build
  }

  get shouldShowUpdate() {
    if (__DEV__) return false
    const build = DeviceInfo.getBuildNumber()
    if (build < this.lastestBuild) {
      return true
    }
    return false
  }

  get modalStyle() {
    if (width > 400) {
      return {
        width: 345
      }
    }

    return {
      paddingHorizontal: 25
    }
  }

  get requestMultiSign() {
    const { kycLayerOne, multiSign, selectedWallet } = MainStore.appState
    const { signState, signPair } = multiSign
    const {
      address, balance, funded, BASE_FEE_DP
    } = selectedWallet

    if (address && signPair && address === signPair && kycLayerOne) {
      if ((signState === 0 || signState === 2) && funded && balance > BASE_FEE_DP) {
        return true
      }
    }

    return false
  }

  get allowBuyOm() {
    const allowBuyOm = MainStore.appState.remoteConfig.getValue('allow_buy_om')
    return typeof allowBuyOm === 'boolean' ? allowBuyOm : true
  }

  handleBackPress() {
    if (NavStore.currentRouteName !== 'DashboardScreen') {
      return
    }

    if (this.showQR) {
      this._toggleReceiveQR()
      return
    }

    Alert.alert(
      'แจ้งเตือน',
      'คุณต้องการออกจากแอปออมใช่หรือไม่?', [{
        text: 'ยกเลิก',
        style: 'cancel'
      }, {
        text: 'ตกลง',
        onPress: () => BackHandler.exitApp()
      }], {
        cancelable: true
      }
    )

    return true
  }

  _mainTab = {
    _prev: false,
    _handle: false,
    wallet: { index: 0 },
    service: { index: 1 },
    news: { index: 2 }
  }

  openShare(filePath) {
    NavStore.preventOpenUnlockScreen = true
    const { userInfo } = MainStore.appState
    const { kycLayerOne } = MainStore.appState
    const { address } = MainStore.appState.selectedWallet
    this.answers.logShare('QRCode', 'Share my QR Code', 'address', 'share-my-qr', { address })

    if (Platform.OS === 'ios') {
      let shareOptions = {}
      RNFS.readFile(filePath, 'base64').then((file) => {
        if (kycLayerOne) {
          shareOptions = {
            title: 'OM Token',
            message: `Name: ${userInfo.first_name} ${userInfo.last_name} \nMy address: ${address}`,
            url: `data:image/png;base64,${file}`
          }
        } else {
          shareOptions = {
            title: 'OM Token',
            message: `Name: ${userInfo.first_name} ${userInfo.last_name} \nMy address: ${address}`
          }
        }
        Share.open(shareOptions).catch(() => { })
        this.isOpenShare = false
      })
    } else {
      let shareOptions = {}
      if (kycLayerOne) {
        shareOptions = {
          title: 'OM Token',
          message: `Name: ${userInfo.first_name} ${userInfo.last_name} \nMy address: ${address}`
        }
      } else {
        shareOptions = {
          title: 'OM Token',
          message: `My address: ${address}`
        }
      }
      Share.open(shareOptions).catch(() => { })
      this.isOpenShare = false
    }
  }

  activeTab(tab, fromButtonPass = false) {
    if (!this._mainTab[tab]) {
      return
    }
    if (this._mainTab._prev == tab) {
      return
    }
    const select_tab = this._mainTab[tab]
    const current_tab = this.state.tabIndex
    this._mainTab._prev = tab

    if (fromButtonPass && this._mainTab._handle) {
      let offset = Number(Math.abs(current_tab - select_tab.index))
      if (current_tab > select_tab.index) {
        offset = -offset
      }
      this._mainTab._handle.scrollBy(offset)
    }
  }

  activeTabByIndex(index) {
    const tab = ['wallet', 'service', 'news'][index]
    if (!tab) {
      return
    }

    this._mainTab._prev = tab
    this.setState({
      tabIndex: index,
      tabCurrent: tab
    })
  }

  showBalanceByIndex(index) {
    if (MainStore.appState.selectedWallet) {
      const { balances } = MainStore.appState.selectedWallet
      MainStore.appState.selectedWallet.asset = balances[index]
    }
  }

  toggleNotification() {
    if (!this.state.enableNotification) {
      NotificationStore.fetchNotifyList(MainStore.appState.selectedWallet.address)
      this.answers.logContentView('Notification', 'modal', 'buy-aom-screen')
    }
    this.setState(prev => ({ enableNotification: !prev.enableNotification }))
  }

  deepLinkRTC() {
    Alert.alert(
      'RTCCityBus.com',
      'ระบบจะเชื่อมต่อการบริการเร็ว ๆ นี้',
      [
        { text: 'ปิด' },
        {
          text: 'ไปยังเว็บไซต์',
          onPress: () => {
            Linking.openURL('http://rtccitybus.com/')
          }
        }
      ]
    )
  }

  serviceOmDeposit() {
    this.onBuyPress()
  }

  serviceOmExplorer() {
    Linking.openURL('https://explorer.omplatform.com/')
  }

  serviceOmSmartContract() {
    this.smartContractModal.open()
  }

  serviceOmTokenization() {
    // Alert.alert(
    //   'Tokenization Platform',
    //   'ระบบจะเปิดให้ใช้บริการเร็ว ๆ นี้',
    //   [
    //     { text: 'ปิด' }
    //   ]
    // )
    Linking.openURL('https://token.omplatform.com/')
  }

  _goToKYC() {
    const {
      kycLayerOne, kycLayerTwo, isSendVerify, kycApproved
    } = MainStore.appState

    if (!kycLayerOne && !isSendVerify) {
      NavStore.pushToScreen('KYCScreen')
      this.answers.logContentView('KYC Layer One', 'screen', 'kyc-layer-one-screen')
    } else if (!kycLayerOne && isSendVerify) {
      NavStore.pushToScreen('VerificationEmailScreen')
    } else if (kycLayerOne && !kycLayerTwo) {
      NavStore.pushToScreen('KYCLayerTwoPageOneScreen', { sourceScreen: 'DashboardScreen' })
      this.answers.logContentView('KYC Layer Two', 'screen', 'kyc-layer-two-screen')
    } else if (kycLayerOne && kycLayerTwo && !kycApproved) {
      NavStore.pushToScreen('KYCPending')
    }

    this.requestKycModal.close()
  }

  _toggleReceiveQR() {
    this.receiveQRModal.open()
    this.showQR = !this.showQR
  }

  _multiSignature() {
    NavStore.lockScreen({
      onUnlock: async (pincode) => {
        // Show loading state
        NavStore.popupCustom.hide()
        this.setState({ signing: true })

        // Get OM Wallet
        const wallet = MainStore.appState.wallets.find(obj => obj.type === 'aomtoken')
        const ds = new SecureDS(pincode)
        wallet.setSecureDS(ds)

        // Change trust
        if (wallet.address) {
          wallet.multiSignatures()
            .then((res) => {
              console.log(`_multiSignature`, res.data)
              // Update sign state
              MainStore.appState.multiSign.signState = res.sign_state
              this.requestMultiSignModal.close()
              Alert.alert(
                'Multisignature สำเร็จ',
                `กระเป๋าของท่านได้รับการทำ Multisignature เรียบร้อยแล้ว`, [
                  {
                    text: 'รับทราบ',
                    style: 'cancel'
                  }
                ], {
                  cancelable: true
                }
              )

              this.answers.logCustom('Multisignature Success')
            })
            .catch((err) => {
              const data = {
                address: wallet.address,
                error_component: 'DashboardScreen',
                error_function: 'multiSignatures',
                error_detail: err
              }

              console.log(`Error _multiSignature`, err.message)
              this.setState({ signing: false })
              // MainStore.appState.fabric.recordNonFatal(err.message)
              Alert.alert(
                'เกิดข้อผิดผลาด',
                `การทำ Multisignature ล้มเหลว!!\nCODE: ${err.message}`, [
                  {
                    text: 'ยกเลิก',
                    style: 'cancel'
                  },
                  {
                    text: 'แจ้งปัญหา',
                    onPress: () => {
                      api.Aom.reportBug(data)
                      Alert.alert(
                        'สำเร็จ !',
                        'คุณทำการแจ้งปัญหาเสร็จเรียบร้อย', [{
                          text: 'ตกลง'
                        }]
                      )
                    }
                  }
                ], {
                  cancelable: true
                }
              )
              this.answers.logCustom('Multisignature Error', {
                address: wallet.address,
                error: err.message
              })
            })
        }
      }
    }, true)
  }

  _confirmMultiSignature() {
    NavStore.popupCustom.show(
      `ยอมรับการทำ Multisignature`,
      [
        {
          text: 'ไม่ยอมรับ',
          onClick: () => {
            NavStore.popupCustom.hide()
            this.requestMultiSignModal.close()
          }
        },
        {
          text: 'ยอมรับ',
          onClick: this._multiSignature
        }
      ],
      'ยอมรับการทำ Multisignature ในกระเป๋าของท่านเพื่อให้แน่ใจว่าเราจะสามารถช่วยกู้คืนกระเป๋าของคุณได้ ในกรณีที่กระเป๋าเกิดการสูญหาย หรือเข้าถึงไม่ได้'
    )
  }

  _renderBalances() {
    const {
      totalBalance, balanceText, ext, type, asset
    } = MainStore.appState.selectedWallet

    let total = totalBalance
    let balanceStr = balanceText
    let name = ext

    if (type === 'aomtoken' && asset) {
      total = asset.balance
      balanceStr = Helper.formatETH(asset.balance, true, 2)
      name = asset.asset_code || 'OM'
    }

    return (
      <View style={styles.walletBalance}>
        <View
          style={{
            alignItems: 'center',
            height: total.toNumber() >= 100000 ? 50 : 63
          }}
        >
          <Text style={[
            styles.textBalance,
            {
              fontSize: total.toNumber() >= 100000 ? 38 : 48
            }
          ]}
          >
            {balanceStr}
          </Text>
        </View>
        <Text style={styles.textBalanceDesc}>{name}</Text>
      </View>
    )
  }

  _renderTabWallet() {
    return (
      <WalletList
        onPressTransaction={() => {
          this.transactionModal.open()
        }}
      />
    )
  }

  _renderTabService() {
    const { allowBuyOm } = this
    return (
      <ScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={styles.serviceContainer}
      >
        <View style={styles.serviceGroup}>
          {
            allowBuyOm && (
              <View style={styles.serviceItems}>
                <TouchableOpacity
                  onPress={this.serviceOmDeposit}
                  style={styles.serviceIcon}
                >
                  <Image
                    resizeMode="contain"
                    source={icons.iconOmAsset}
                    style={styles.serviceImage}
                  />
                  <Text style={styles.serviceText}>{`Buy\nOM Coin`}</Text>
                </TouchableOpacity>
              </View>
            )
          }
          <View style={styles.serviceItems}>
            <TouchableOpacity
              onPress={this.serviceOmExplorer}
              style={styles.serviceIcon}
            >
              <Image
                resizeMode="contain"
                source={icons.iconOmExplorer}
                style={styles.serviceImage}
              />
              <Text style={styles.serviceText}>{`OM\nExplorer`}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.serviceItems}>
            <TouchableOpacity
              onPress={this.serviceOmTokenization}
              style={styles.serviceIcon}
            >
              <Image
                resizeMode="contain"
                source={icons.iconOmTokenization}
                style={styles.serviceImage}
              />
              <Text style={styles.serviceText}>{`Tokenization\nPlatform`}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.serviceItems}>
            <TouchableOpacity
              onPress={this.serviceOmSmartContract}
              style={styles.serviceIcon}
            >
              <Image
                resizeMode="contain"
                source={icons.iconOmSmartContract}
                style={styles.serviceImage}
              />
              <Text style={styles.serviceText}>{`Smart\nContract`}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.serviceTitle}>
          <Text style={styles.serviceTitleText}>Partner Services</Text>
        </View>
        <View style={styles.serviceGroup}>
          <View style={styles.serviceItems}>
            <TouchableOpacity
              onPress={this.deepLinkRTC}
              style={[styles.serviceIcon]}
            >
              <Image
                resizeMode="contain"
                source={icons.iconCmTransitCircle}
                style={styles.serviceImage}
              />
              <Text style={styles.serviceText}>RTC</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    )
  }

  _renderTabNews() {
    return (
      <News />
    )
  }

  _renderDashboardHeader() {
    return (
      <View
        style={styles.dashboardHeader}
        onLayout={(event) => {
          const { layout } = event.nativeEvent
          this.setState({ headerHeight: layout.height })
        }}
      >
        <HeaderMenu toggle={this.toggleNotification} disabled={this.state.enableNotification} />
        <View style={styles.walletScreen}>
          {
            this._renderBalances()
          }

          <View style={styles.walletMenu}>

            <TouchableOpacity
              style={styles.walletMenuItem}
              onPress={this.onSendPress}
            >
              <Image style={styles.iconWallet} source={images.iconSend} />
              <Text style={styles.walletMenuText}>ส่ง</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.walletMenuItem}
              onPress={this.onHistoryPress}
            >
              <Image style={styles.iconWallet} source={images.iconTransaction} />
              <Text style={styles.walletMenuText}>ประวัติ</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.walletMenuItem}
              onPress={this.onReceivePress}
            >
              <Image style={styles.iconWallet} source={images.iconReceive} />
              <Text style={styles.walletMenuText}>รับ</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }

  _renderDashboardBody() {
    return (
      <View style={styles.dashboardBody}>
        <View style={styles.contentTab}>
          <TouchableOpacity
            activeOpacity={1}
            style={[
              styles.contentTabMenu,
              this.state.tabCurrent == 'wallet' ? styles.tabActive : {}
            ]}
            onPress={() => {
              this.activeTab('wallet', true)
            }}
          >
            <Text style={styles.contentTabText}>กระเป๋า</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={1}
            style={[
              styles.contentTabMenu, this.state.tabCurrent == 'service' ? styles.tabActive : {}
            ]}
            onPress={() => {
              this.activeTab('service', true)
            }}
          >
            <Text style={styles.contentTabText}>บริการ</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={1}
            style={[
              styles.contentTabMenu,
              this.state.tabCurrent == 'news' ? styles.tabActive : {}
            ]}
            onPress={() => {
              this.activeTab('news', true)
            }}
          >
            <Text style={styles.contentTabText}>ข่าว</Text>
          </TouchableOpacity>
        </View>

        <Swiper
          showsButtons={false}
          index={this.state.tabIndex}
          loop={false}
          showsPagination={false}
          removeClippedSubviews={false}
          onIndexChanged={(index) => {
            this.activeTabByIndex(index)
          }}
          ref={(self) => {
            this._mainTab._handle = self
          }}
        >
          <View key="tab-wallet" style={styles.tabContainer}>
            {this._renderTabWallet()}
          </View>
          <View key="tab-service" style={styles.tabContainer}>
            {this._renderTabService()}
          </View>
          <View key="tab-news" style={styles.tabContainer}>
            {this._renderTabNews()}
          </View>
        </Swiper>

      </View>
    )
  }

  _renderNotificationItem() {
    const list = NotificationStore.listItem
    return list.map((item, index) => {
      let subTitle = ''

      if (item.time) {
        subTitle = moment(item.time).startOf('second').fromNow()
      }

      return (
        <View
          key={index}
          style={[
            notiStyles.notiList,
            item.unread ? notiStyles.UnreadContainer : notiStyles.ReadContainer,
            index === 0 ? {
              borderTopColor: item.unread ? '#DBDDF6' : '#F4F4F7',
              borderTopWidth: 1
            } : {}
          ]}
        >
          <Image source={icons[item.icon]} />

          <View style={notiStyles.bodyContainer}>
            <Text style={[notiStyles.bodyText]}>{item.body}</Text>
          </View>

          <View style={notiStyles.timeContainer}>
            <Text style={[notiStyles.timeText]}>{subTitle}</Text>
          </View>
        </View>
      )
    })
  }
  _renderNotification() {
    return (
      <View style={notiStyles.container}>
        <View style={notiStyles.dashboardHeader}>
          <View style={notiStyles.menuHeader}>
            <Text style={notiStyles.title} />
            <TouchableOpacity
              onPress={() => {
                NotificationStore.clearNotifyBadgeCount()
                this.setState({ enableNotification: false })
              }}
            >
              <Image source={images.iconNoti} />
            </TouchableOpacity>
          </View>
          <View style={{
            height: 'auto',
            width: '100%',
            top: -15
          }}
          >
            <Image
              style={{
                marginLeft: 'auto',
                marginRight: 27
              }}
              source={icons.iconNotiRectangle}
            />
          </View>

          <View style={notiStyles.notiContainer}>
            {/* Header */}
            <View style={notiStyles.notiHeader}>
              <Text style={[notiStyles.title]}> การแจ้งเตือนที่ผ่านมา </Text>

              {/* <TouchableOpacity style={notiStyles.buttonMarkAsRead}>
                <Text style={notiStyles.ReadTitle}> อ่านแล้วทั้งหมด </Text>
              </TouchableOpacity> */}
            </View>
            {/* Header */}

            {/* Body */}
            <ScrollView style={{ flex: 1 }}>
              {this._renderNotificationItem()}
            </ScrollView>
            {/* Body */}
          </View>
        </View>
      </View>
    )
  }

  _renderModalKyc() {
    const { modalStyle } = this
    return (
      <Modal
        ref={(ref) => { this.requestKycModal = ref }}
        position="center"
        swipeToClose={false}
        backButtonClose={true}
        backdropPressToClose={false}
        backdropColor="#000"
        backdropOpacity={0.5}
        style={[modalStyle, {
          backgroundColor: 'transparent',
          justifyContent: 'center'
        }]}
      >
        <View>
          <View
            style={{
              alignItems: 'flex-end',
              marginBottom: 10
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.requestKycModal.close()
              }}
            >
              <Image source={icons.iconClose} />
            </TouchableOpacity>
          </View>
          <View>
            <RequestKYC
              onPress={this._goToKYC}
            />
          </View>
        </View>
      </Modal>
    )
  }

  _renderModalMultisign() {
    const { modalStyle, requestMultiSign } = this
    return (
      <Modal
        ref={(ref) => { this.requestMultiSignModal = ref }}
        isOpen={requestMultiSign}
        position="center"
        swipeToClose={false}
        backButtonClose={false}
        backdropPressToClose={false}
        backdropColor="#000"
        backdropOpacity={0.5}
        animationDuration={200}
        style={[modalStyle, {
          backgroundColor: 'transparent',
          justifyContent: 'center'
        }]}
      >
        <View>
          <View>
            <RequestMultiSign
              onPress={this._confirmMultiSignature}
              loading={this.state.signing}
            />
          </View>
        </View>
      </Modal>
    )
  }

  _renderModalVerifyEmail() {
    const shouldShowComplete = NotificationStore.receiveVerify
    const { modalStyle } = this
    return (
      <Modal
        ref={(ref) => { this.completeVerifyModal = ref }}
        isOpen={shouldShowComplete}
        position="center"
        swipeToClose={false}
        backButtonClose={true}
        backdropPressToClose={false}
        backdropColor="#000"
        backdropOpacity={0.5}
        style={[modalStyle, {
          backgroundColor: 'transparent',
          justifyContent: 'center'
        }]}
      >
        <View>
          <View>
            <CompleteVerifyEmail
              onPress={() => {
                this.completeVerifyModal.close()
              }}
            />
          </View>
        </View>
      </Modal>
    )
  }

  _renderModalQrCode() {
    const { address, ext } = MainStore.appState.selectedWallet

    return (
      <Modal
        ref={(ref) => { this.receiveQRModal = ref }}
        position="center"
        swipeToClose={true}
        swipeThreshold={100}
        swipeArea={marginTop + 57 + 30}
        backButtonClose={true}
        backdropPressToClose={false}
        style={{
          flex: 1,
          backgroundColor: 'transparent',
          paddingTop: marginTop + 57
        }}
      >
        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{
            flex: 1,
            backgroundColor: '#FFF',
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20
          }}
        >
          <View
            style={{
              alignItems: 'center',
              flex: 1,
              flexDirection: 'column',
              paddingHorizontal: 20,
              paddingVertical: 10
            }}
          >
            <TouchableOpacity
              style={{
                alignItems: 'center',
                flexDirection: 'column'
              }}
              onPress={() => {
                this.receiveQRModal.close()
              }}
            >
              <Text style={{
                color: '#C5C5C5',
                fontFamily: AppStyle.mainFontBoldTH,
                fontSize: 14,
                paddingVertical: 5
              }}
              >
                ซ่อน
              </Text>
              <Image source={images.iconArrowDown} />
            </TouchableOpacity>

            <View
              style={{
                alignItems: 'center',
                borderRadius: 10,
                borderColor: '#C5C5C5',
                borderWidth: 1,
                padding: 5,
                marginTop: 20
              }}
            >
              <ViewShot
                ref={(ref) => { this.viewShot = ref }}
                style={{
                  backgroundColor: '#FFF',
                  padding: 10
                }}
              >
                <QRCode
                  value={address}
                  size={isSmallScreen ? 200 : 260}
                />
              </ViewShot>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              <TouchableOpacity
                style={{
                  height: 35,
                  width: 144,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#657EFF',
                  borderBottomRightRadius: 5,
                  borderBottomLeftRadius: 5
                }}
                onPress={this.onShare}
              >
                <Image source={icons.iconSave} />
                <Text style={{
                  color: AppStyle.mainColor,
                  fontFamily: AppStyle.mainFontTH,
                  fontSize: 16,
                  marginLeft: 5
                }}
                >
                  แชร์ QR
                </Text>
              </TouchableOpacity>
            </View>

            <View style={{ marginTop: 15, marginBottom: 15 }}>
              <View style={{ marginHorizontal: 35 }}>
                <Text
                  style={{
                    color: 'black',
                    fontFamily: AppStyle.mainFontBold,
                    fontSize: 20,
                    textAlign: 'center'
                  }}
                >
                  {ext}
                </Text>
                <Text
                  style={{
                    color: 'black',
                    fontFamily: AppStyle.mainFont,
                    fontSize: 16,
                    textAlign: 'center'
                  }}
                >
                  {address}
                </Text>

              </View>
            </View>

            <TouchableOpacity
              style={{
                borderWidth: 2,
                borderRadius: 5,
                borderColor: '#535786',
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
                width: 200
              }}
              onPress={this.onCopy}
            >
              <Text style={{
                color: '#535786',
                fontFamily: AppStyle.mainFontBoldTH,
                fontSize: 16
              }}
              >
                คัดลอกที่อยู่กระเป๋า
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </Modal>
    )
  }

  _renderModalReceiveRewardLeyerTwo() {
    const { modalStyle } = this
    return (
      <Modal
        ref={(ref) => { this.successReceiveLottoKYCLevelTwo = ref }}
        position="center"
        swipeToClose={true}
        backButtonClose={true}
        backdropPressToClose={false}
        backdropColor="#000"
        backdropOpacity={0.5}
        style={[modalStyle, {
          backgroundColor: 'transparent',
          justifyContent: 'center',
          alignItems: 'center'
        }]}
      >
        <View style={{
          display: 'flex',
          width: '100%'
        }}
        >
          <View style={styles.popupContent}>
            <Text style={styles.title}>
              {`ได้รับเงินส่วนแบ่งรางวัลแล้ว`}
            </Text>
            <Image source={icons.iconTutorialThird} resizeMode="center" style={{ height: 230 }} />
            <Text style={[styles.description, { marginHorizontal: 20 }]}>{`ระบบกำลังกำลังทำการโอนเงิน\nส่วนแบ่งที่ได้จากการถูกสลากให้คุณ`}</Text>
            <View style={{ width: '100%', paddingHorizontal: 20 }}>
              <TouchableOpacity
                activeOpacity={0.6}
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: '100%',
                  height: 60,
                  marginBottom: 16,
                  backgroundColor: '#535786',
                  borderRadius: 8
                }}
                onPress={() => { this.successReceiveLottoKYCLevelTwo.close() }}
              >
                <Text style={[styles.buttonText, { color: colors.white }]}>กลับ</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  _renderModalReceiveRewardLeyerOne() {
    const { modalStyle } = this
    return (
      <Modal
        ref={(ref) => { this.successReceiveLottoKYCLevelOne = ref }}
        position="center"
        swipeToClose={true}
        backButtonClose={true}
        backdropPressToClose={false}
        backdropColor="#000"
        backdropOpacity={0.5}
        style={[modalStyle, {
          backgroundColor: 'transparent',
          justifyContent: 'center',
          alignItems: 'center'
        }]}
      >
        <View style={{
          display: 'flex',
          width: '100%'
        }}
        >
          <View style={styles.popupContent}>
            <Text style={styles.title}>
              {`คุณต้องยืนยันเอกสารก่อน`}
            </Text>
            <Image source={icons.iconConfirmKYC2} resizeMode="center" />
            <Text style={styles.description}>{`คุณต้องทำการยืนยันเอกสารและ\nข้อมูลส่วนตัวเพิ่มเติมก่อน\nรับส่วนแบ่งสลากที่ได้รางวัล`}</Text>
            <View style={{ width: '100%', paddingHorizontal: 20 }}>
              <TouchableOpacity
                activeOpacity={0.6}
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: '100%',
                  height: 60,
                  marginBottom: 16,
                  backgroundColor: '#535786',
                  borderRadius: 8
                }}
                onPress={() => {
                  this.successReceiveLottoKYCLevelOne.close()
                  setTimeout(() => {
                    this._goToKYC()
                  }, 2000)
                }}
              >
                <Text style={[styles.buttonText, { color: colors.white }]}>ยืนยันตัวตน</Text>
              </TouchableOpacity>
            </View>

            <View style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              width: '100%',
              marginBottom: 22
            }}
            >
              <TouchableOpacity
                activeOpacity={0.6}
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: '100%'
                }}
                onPress={() => {
                  this.successReceiveLottoKYCLevelOne.close()
                }}
              >
                <Text style={[styles.buttonText, { color: colors.blue }]}>กลับ</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  _renderModalTransaction() {
    const { headerHeight } = this.state

    return (
      <Modal
        ref={(ref) => { this.transactionModal = ref }}
        position="center"
        swipeToClose={false}
        backButtonClose={false}
        backdropPressToClose={false}
        backdropColor="#000"
        backdropOpacity={0.5}
        style={{
          flex: 1,
          backgroundColor: 'transparent',
          paddingTop: headerHeight - 40
        }}
      >
        <View style={{ flex: 1 }}>
          <View
            style={{
              alignItems: 'flex-end',
              marginBottom: 10,
              paddingRight: 15
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.transactionModal.close()
              }}
            >
              <Image source={icons.iconClose} />
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              backgroundColor: '#FFF',
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20
            }}
          >
            <Transaction />
          </View>
        </View>
      </Modal>
    )
  }

  _renderModaSmartContract() {
    const { headerHeight } = this.state

    return (
      <Modal
        ref={(ref) => { this.smartContractModal = ref }}
        position="center"
        swipeToClose={false}
        backButtonClose={false}
        backdropPressToClose={false}
        backdropColor="#000"
        backdropOpacity={0.5}
        style={{
          flex: 1,
          backgroundColor: 'transparent',
          paddingTop: headerHeight - 40
        }}
      >
        <View style={{ flex: 1 }}>
          <View
            style={{
              alignItems: 'flex-end',
              marginBottom: 10,
              paddingRight: 15
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.smartContractModal.close()
              }}
            >
              <Image source={icons.iconClose} />
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              backgroundColor: '#FFF',
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20
            }}
          >
            <SmartContract />
          </View>
        </View>
      </Modal>
    )
  }

  _renderShouldShowUpdate() {
    const { shouldShowUpdate } = this
    const { modalStyle } = this

    return (
      <Modal
        ref={(ref) => { this.shouldShowUpdateModal = ref }}
        isOpen={shouldShowUpdate}
        position="center"
        swipeToClose={false}
        backButtonClose={true}
        backdropPressToClose={false}
        backdropColor="#000"
        backdropOpacity={0.5}
        style={[modalStyle, {
          backgroundColor: 'transparent',
          justifyContent: 'center'
        }]}
      >
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <View
            style={{
              alignItems: 'flex-end',
              marginBottom: 10
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.shouldShowUpdateModal.close()
              }}
            >
              <Image source={icons.iconClose} />
            </TouchableOpacity>
          </View>
          <View>
            <View>
              <NewVersionAvailable />
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  render() {
    const {
      hasPassword, selectedWallet
    } = MainStore.appState

    if (hasPassword && MainStore.appState.wallets.length !== 0 && selectedWallet) {
      return (
        <View style={styles.container}>
          <StatusBar
            backgroundColor="transparent"
            barStyle="light-content"
            translucent
          />
          <Image
            source={images.backgroundDashboard}
            style={styles.backgroundImage}
            resizeMode="stretch"
          />
          <LinearGradient colors={['rgba(0,0,0,0.5)', 'rgba(0,0,0,0)']} style={styles.linearGradient} />
          {
            this._renderDashboardHeader()
          }
          {
            this._renderDashboardBody()
          }

          {/* popup request kyc */}
          {
            this._renderModalKyc()
          }
          {/* end request kyc */}

          {/* popup request multisign */}
          {
            this._renderModalMultisign()
          }
          {/* end request multisign */}

          {/* popup complete verify email */}
          {
            this._renderModalVerifyEmail()
          }
          {/* end complete verify email */}

          {/* my qrcode */}
          {
            this._renderModalQrCode()
          }
          {/* end my qrcode */}

          {/* Begin modal */}
          {
            this._renderModalReceiveRewardLeyerTwo()
          }
          {/* End modal */}

          {/* Begin KYCLevelOne */}
          {
            this._renderModalReceiveRewardLeyerOne()
          }
          {/* End KYCLevelOne */}

          {/* Begin Transaction */}
          {
            this._renderModalTransaction()
          }
          {/* End Transaction */}

          {/* Begin Smart Contract */}
          {
            this._renderModaSmartContract()
          }
          {/* End Smart Contract */}

          {/* Begin Should show update */}
          {
            this._renderShouldShowUpdate()
          }
          {/* End Should show update */}

          {/* Begin darken cointainer */}
          {this.state.enableNotification &&
            <TouchableWithoutFeedback
              onPressIn={() => this.setState({ enableNotification: false })}
            >
              <View style={notiStyles.darkenContainer} />
            </TouchableWithoutFeedback>}
          {/* End darken cointainer */}

          {/* Begin notification container */}
          {this.state.enableNotification && this._renderNotification()}
          {/* End notification container */}
        </View>
      )
    }

    return null
  }
}

const colors = {
  white: '#FFFFFF',
  blue: '#535786'
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    flexWrap: 'wrap',
    alignItems: 'flex-start'
  },
  backgroundImage: {
    backgroundColor: '#131C4F',
    flex: 1,
    resizeMode: 'center',
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center'
  },
  dashboardHeader: {
    borderRadius: 10,
    borderTopRightRadius: 20,
    height: 270,
    paddingBottom: 15,
    paddingLeft: 17,
    paddingRight: 17,
    paddingTop: marginTop,
    width: '100%',
    alignItems: 'flex-start'
  },
  dashboardBody: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flex: 1,
    width: '100%'
  },
  linearGradient: {
    height: 150,
    position: 'absolute',
    width: '100%',
    top: 0
  },
  walletScreen: {
    backgroundColor: 'rgba(240,240,240,0.97)',
    borderRadius: 10,
    flex: 1,
    flexDirection: 'column',
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    alignSelf: 'stretch',
    overflow: 'hidden'
  },
  walletBalance: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: '100%'
  },
  walletMenu: {
    alignItems: 'center',
    justifyContent: 'center',
    borderTopWidth: 1,
    borderTopColor: 'rgba(0,0,0,0.1)',
    height: 65,
    flexDirection: 'row',
    width: '100%'
  },
  walletMenuItem: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  walletMenuText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 14
  },
  iconWallet: {
    height: 30,
    width: 30
  },
  textBalance: {
    color: '#000',
    fontFamily: AppStyle.mainFontBold
  },
  textBalanceDesc: {
    color: '#878997',
    fontFamily: AppStyle.mainFontBold,
    fontSize: 14
  },
  contentTab: {
    backgroundColor: '#f2f2f2',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    height: 56,
    flexDirection: 'row',
    alignItems: 'stretch'
  },
  contentTabMenu: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  contentTabText: {
    color: '#000',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18
  },
  tabActive: {
    backgroundColor: '#fff',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  tabContainer: {
    flex: 1
  },
  popupContent: {
    backgroundColor: colors.white,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    color: '#535786',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20,
    marginHorizontal: 25,
    marginBottom: 26,
    marginTop: 35
  },
  description: {
    color: '#121212',
    fontFamily: AppStyle.mainFontTH,
    lineHeight: 24,
    fontSize: 18,
    textAlign: 'center',
    marginHorizontal: 25,
    marginBottom: 25,
    marginTop: 27
  },
  buttonText: {
    color: '#535786',
    fontFamily: AppStyle.mainFontTH,
    lineHeight: 36,
    fontSize: 20
  },
  serviceContainer: {
    flex: 1
  },
  serviceGroup: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    marginTop: 20,
    paddingHorizontal: 10
  },
  serviceItems: {
    flexBasis: '25%',
    alignItems: 'center',
    marginBottom: 15,
    paddingHorizontal: 10
  },
  serviceIcon: {
    flex: 1
  },
  serviceImage: {
    width: 60,
    height: 60
  },
  serviceText: {
    color: '#000',
    fontFamily: AppStyle.mainFontBold,
    fontSize: width > 400 ? 13 : 10,
    textAlign: 'center',
    paddingTop: 7,
    flexWrap: 'nowrap'
  },
  serviceTitle: {
    borderTopColor: '#EEE',
    borderTopWidth: 1,
    color: '#000',
    marginTop: 10,
    marginHorizontal: 20,
    paddingTop: 20
  },
  serviceTitleText: {
    color: '#000',
    fontFamily: AppStyle.mainFontBold,
    fontSize: 20
  }
})

const notiStyles = StyleSheet.create({
  container: {
    width: '100%',
    height: '80%',
    position: 'absolute'
  },
  darkenContainer: {
    flexDirection: 'column',
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: '#000000',
    opacity: 0.7
  },
  dashboardHeader: {
    width: '100%',
    height: 'auto',
    alignItems: 'flex-start',
    paddingTop: marginTop + 8
  },
  menuHeader: {
    paddingRight: 17,
    paddingBottom: 15,
    paddingLeft: 17,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%'
  },
  notiContainer: {
    height: '90%',
    width: '100%',
    top: -26,
    backgroundColor: '#FFF',
    borderColor: '#F4F4F7',
    borderTopColor: '#FFF',
    borderWidth: 1
  },
  notiHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 13
  },
  title: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18,
    color: '#000',
    marginLeft: 10
  },
  bodyText: {
    marginLeft: 10,
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    color: '#000'
  },
  bodyContainer: {
    flex: 1
  },
  timeText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 12,
    color: '#000000',
    opacity: 0.75
  },
  notiList: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 12
  },
  UnreadContainer: {
    backgroundColor: '#EEEFF8',
    borderBottomWidth: 1,
    borderBottomColor: '#DBDDF6'
  },
  ReadContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#F4F4F7'
  },
  timeContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    justifyContent: 'flex-end',
    width: 80
  }
})

export default DashboardScreen
