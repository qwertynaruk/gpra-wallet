import React, { Component } from 'react'
import {
  ActivityIndicator,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import MainStore from '../../../AppStores/MainStore'
import icons from '../../../commons/icons'
import AppStyle from '../../../commons/AppStyle'

@observer
class RequestMultiSign extends Component {
  static propTypes = {
    onPress: PropTypes.func,
    loading: PropTypes.bool
  }

  static defaultProps = {
    onPress: () => { },
    loading: false
  }

  render() {
    const { BASE_FEE_DP } = MainStore.appState.selectedWallet
    const { loading } = this.props
    return (
      <View style={styles.popupContent}>
        <Text style={styles.title}>
          {`Multisignature`}
        </Text>
        <Image source={icons.iconFreedomDescription} style={styles.icon} resizeMode="contain" />
        <Text style={styles.description}>{`กระเป๋าของท่านจะถูกเข้ารหัสผ่านด้วย กุญแจส่วนตัว และกุญแจของระบบ\n(Multisignature) เพื่อให้แน่ใจว่าเราจะสามารถช่วยกู้คืนกระเป๋าของคุณได้ ในกรณีที่กระเป๋าเกิดการสูญหาย หรือเข้าถึงไม่ได้`}</Text>
        <Text style={styles.small}>(ค่าธรรมเนียมการทำรายการ {BASE_FEE_DP} Om)</Text>
        <View style={styles.buttonsContainer}>
          {
            loading && (
              <ActivityIndicator size="large" color="#535786" />
            )
          }
          {
            !loading && (
              <View style={styles.buttonContainer}>
                <TouchableOpacity
                  activeOpacity={0.6}
                  style={styles.button}
                  onPress={this.props.onPress}
                >
                  <Text style={styles.buttonText}>ยอมรับในเงื่อนไข</Text>
                </TouchableOpacity>
              </View>
            )
          }
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  popupContent: {
    backgroundColor: '#FFF',
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20
  },
  title: {
    color: '#535786',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20
  },
  icon: {
    marginTop: 20,
    marginBottom: 20,
    height: 120
  },
  description: {
    color: '#121212',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 17,
    textAlign: 'center',
    paddingHorizontal: 15
  },
  small: {
    color: '#666',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 13,
    textAlign: 'center',
    paddingHorizontal: 15
  },
  buttonsContainer: {
    flexDirection: 'column',
    marginBottom: 15,
    marginTop: 15,
    width: '100%'
  },
  buttonContainer: {
    flexDirection: 'row'
  },
  button: {
    backgroundColor: '#535786',
    borderRadius: 8,
    height: 60,
    marginHorizontal: 15,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#FFF',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20
  }
})

export default RequestMultiSign
