import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Image
} from 'react-native'
import { observer } from 'mobx-react/native'
import MainStore from '../../../../AppStores/MainStore'
import NavStore from '../../../../AppStores/NavStore'
import AppStyle from '../../../../commons/AppStyle'
import NewsStore from '../../stores/NewsStore'

const { width } = Dimensions.get('window')

@observer
class News extends Component {
  constructor(props) {
    super(props)

    this.state = {
      news: [],
      loaded: false
    }
    MainStore.newsStore = new NewsStore()
    this.newsStore = MainStore.newsStore
  }

  componentDidMount() {
    this.newsStore.getAllNews().then(news => this.setState({
      news,
      loaded: true
    }))
  }

  _goToNewsDetail = id => () => {
    NavStore.pushToScreen('NewsDetailScreen', {
      id
    })
  }

  _keyExtractor = item => `${item.provider}-${item.id}`

  _renderItem = ({ item }) => (
    <TouchableOpacity
      activeOpacity={0.9}
      onPress={this._goToNewsDetail(item.id)}
    >
      <View style={{ flexDirection: 'row', paddingBottom: 15 }}>
        <View style={{ borderWidth: 1, borderColor: '#EEE' }}>
          <Image
            style={{
              alignSelf: 'center',
              width: 120,
              height: width > 380 ? 75 : 85
            }}
            source={{ uri: item.thumbnail }}
            resizeMode="cover"
          />
        </View>

        <View style={{ flex: 1, marginLeft: 10 }}>
          <Text style={styles.title} numberOfLines={2} ellipsizeMode="tail">{item.title}</Text>
          <View style={width > 380 ? { flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'space-between' } : {}}>
            <Text style={styles.timeText}>{item.date}</Text>
            <View style={{
              alignSelf: 'flex-start',
              backgroundColor: item.color,
              borderRadius: 6
            }}
            >
              <Text style={styles.provider}>{item.provider}</Text>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  )

  _renderEmpty() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1
        }}
      >
        <Text style={{ fontFamily: AppStyle.mainFontTH }}>ยังไม่มีข่าวใหม่ :(</Text>
      </View>
    )
  }

  _renderLoading() {
    return (
      <View
        style={{
          alignItems: 'center',
          flex: 1
        }}
      >
        <View style={styles.loading}>
          <ActivityIndicator size="large" color={AppStyle.redColor} />
          <Text style={{ fontFamily: AppStyle.mainFontTH }}>กำลังโหลดข้อมูล...</Text>
        </View>
      </View>
    )
  }

  render() {
    const { loaded } = this.state

    if (!loaded) {
      return this._renderLoading()
    }

    return (
      <ScrollView style={styles.scroll}>
        <View style={styles.container}>
          <FlatList
            data={this.state.news}
            extraData={this.state}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            ListEmptyComponent={this._renderEmpty}
          />
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  scroll: {
    flex: 1,
    flexDirection: 'column'
  },
  container: {
    flex: 1,
    padding: 15,
    paddingBottom: 0
  },
  title: {
    color: '#000',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 14,
    textAlign: 'left'
  },
  timeText: {
    color: AppStyle.grayColor,
    fontFamily: AppStyle.mainFontTH,
    fontSize: 12,
    textAlign: 'left',
    marginTop: 3
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    opacity: 0.5,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center'
  },
  provider: {
    color: '#FFF',
    marginVertical: 2,
    marginHorizontal: 6,
    fontFamily: AppStyle.mainFontTH,
    fontSize: 12
  }
})

export default News
