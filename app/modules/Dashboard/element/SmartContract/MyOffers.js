import React, { Component } from 'react'
import {
  View,
  Text,
  Image,
  FlatList,
  StyleSheet,
  ScrollView,
  TouchableOpacity
} from 'react-native'
import { observer } from 'mobx-react/native'

// Components & Modules
import AppStyle from '../../../../commons/AppStyle'
import icons from '../../../../commons/icons'
import Helper from '../../../../commons/Helper'
import NavStore from '../../../../AppStores/NavStore'
import MainStore from '../../../../AppStores/MainStore'
import TokenIcon from '../TokenIcon'

@observer
class MyOffers extends Component {
  constructor(props) {
    super(props)

    this.dataStore = MainStore.smartContractStore
    this._renderHead = this._renderHead.bind(this)
    this._renderItem = this._renderItem.bind(this)
    this._onPressManageOffer = this._onPressManageOffer.bind(this)

    const { OMWallet } = MainStore.appState

    // Request my offers
    this.dataStore.getOffers(OMWallet.address)
  }

  _onPressManageOffer = offer => () => {
    NavStore.pushToScreen('ManageOfferScreen', offer)
  }

  _renderEmptyOffers = () => (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center'
      }}
    >
      <Text
        style={{
          color: '#BBB',
          fontFamily: AppStyle.mainFontTH,
          fontSize: 13,
          paddingLeft: 10
        }}
      >
        ไม่พบข้อมูล
      </Text>
    </View>
  )

  _renderItem({ item }) {
    const price = Helper.formatETH(item.price, false, 2)
    const amount = Helper.formatETH(item.amount, false, 2)
    return (
      <View style={styles.offerCard}>
        <View style={styles.row}>
          <View style={styles.offerBlock}>
            <TokenIcon
              code={item.buying.asset_code || ''}
              issuer={item.buying.asset_issuer || ''}
              style={styles.offersIcon}
            />
            <Text style={styles.offerText}>{item.buying.asset_code || 'OM'}</Text>
          </View>
          <View style={styles.offerExchangeIcon}>
            <Image source={icons.iconExchange} />
          </View>
          <View style={styles.offerBlock}>
            <TokenIcon
              code={item.selling.asset_code || ''}
              issuer={item.selling.asset_issuer || ''}
              style={styles.offersIcon}
            />
            <Text style={styles.offerText}>{item.selling.asset_code || 'OM'}</Text>
          </View>
        </View>
        <View style={styles.exchangeBlock}>
          <Text style={styles.exchangeTitle}>Exchange Rate</Text>
          <Text style={styles.exchangeRate}>{price} {item.buying.asset_code || 'OM'} : 1 {item.selling.asset_code || 'OM'}</Text>
          <Text style={styles.exchangeAmount}>Remaining</Text>
          <Text style={styles.exchangeRate}>{amount}</Text>
        </View>
        <View style={styles.buttonsBlock}>
          <TouchableOpacity
            style={styles.editButton}
            onPress={this._onPressManageOffer(item)}
          >
            <Text style={styles.editButtonText}>แก้ไข</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  _renderHead() {
    return (
      <View>
        <View
          style={{
            alignItems: 'center',
            flexDirection: 'row'
          }}
        >
          <TouchableOpacity
            style={styles.createOfferButton}
            onPress={() => {
              NavStore.pushToScreen('ManageOfferScreen')
            }}
          >
            <Text style={styles.createOfferText}>CREATE OFFER</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.titleBlock}>
          <Text style={styles.titleBlockText}>MY OFFERS LIST</Text>
        </View>
      </View>
    )
  }

  render() {
    const { offers } = this.dataStore
    return (
      <View style={styles.flatListContainer}>
        <ScrollView
          contentContainerStyle={styles.contentContainer}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{ flex: 1 }}
        >
          <FlatList
            data={offers}
            showsVerticalScrollIndicator={false}
            keyExtractor={obj => obj.id.toString()}
            renderItem={this._renderItem}
            ItemSeparatorComponent={() => <View style={{ height: 10 }} />}
            ListEmptyComponent={this._renderEmptyOffers}
            ListHeaderComponent={this._renderHead}
          />
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    padding: 20
  },
  row: {
    flexDirection: 'row'
  },
  flatListContainer: {
    flex: 1
  },
  createOfferButton: {
    alignItems: 'center',
    backgroundColor: '#42B884',
    borderRadius: 10,
    flex: 1,
    height: 50,
    justifyContent: 'center'
  },
  createOfferText: {
    color: '#FFF',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18
  },
  titleBlock: {
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
    marginBottom: 10,
    marginTop: 10,
    paddingBottom: 3
  },
  titleBlockText: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 16
  },
  offerCard: {
    borderColor: '#EEE',
    borderRadius: 5,
    borderWidth: 1,
    flex: 1,
    padding: 5
  },
  offerText: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 16
  },
  offerBlock: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    paddingVertical: 5
  },
  offersIcon: {
    height: 40,
    marginBottom: 5,
    width: 40
  },
  offerExchangeIcon: {
    marginTop: 15
  },
  exchangeBlock: {
    alignItems: 'center',
    flex: 1
  },
  exchangeTitle: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14
  },
  exchangeAmount: {
    color: '#000',
    fontFamily: AppStyle.mainFontSemiBoldTH,
    fontSize: 14,
    marginTop: 5
  },
  exchangeRate: {
    color: '#000',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 12
  },
  buttonsBlock: {
    flexDirection: 'row',
    marginTop: 10
  },
  editButton: {
    alignItems: 'center',
    backgroundColor: '#f4f5f7',
    borderRadius: 5,
    flex: 1,
    height: 30,
    justifyContent: 'center',
    marginRight: 2
  },
  editButtonText: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14
  }
})

export default MyOffers
