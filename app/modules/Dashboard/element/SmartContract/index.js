import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import { observer } from 'mobx-react/native'

// Third-party
import Swiper from 'react-native-swiper'

// Components & Modules
import MainStore from '../../../../AppStores/MainStore'
import AppStyle from '../../../../commons/AppStyle'
import TokenList from './TokenList'
import MyOffers from './MyOffers'
import SmartContractStore from '../../stores/SmartContractStore'

@observer
class SmartContract extends Component {
  constructor(props) {
    super(props)

    this.state = {
      tabIndex: 0
    }
    this.prevTab = 0
    this.onPressTab = this.onPressTab.bind(this)

    MainStore.smartContractStore = new SmartContractStore()
  }

  onPressTab = index => () => {
    if (this.prevTab !== index) {
      const offset = index - this.prevTab
      this.prevTab = index
      this.tabRef.scrollBy(offset)
    }
  }

  render() {
    const { tabIndex } = this.state
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.tabContainer}>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.buttonTouch}
              onPress={this.onPressTab(0)}
            >
              <Text style={[styles.buttonText, tabIndex === 1 ? { color: '#BBB' } : {}]}>TOKEN LIST</Text>
            </TouchableOpacity>
          </View>
          <View style={[styles.buttonContainer, styles.buttonBorderLeft]}>
            <TouchableOpacity
              style={styles.buttonTouch}
              onPress={this.onPressTab(1)}
            >
              <Text style={[styles.buttonText, tabIndex === 0 ? { color: '#BBB' } : {}]}>MY OFFERS</Text>
            </TouchableOpacity>
          </View>
        </View>
        <Swiper
          ref={(ref) => { this.tabRef = ref }}
          showsButtons={false}
          scrollEnabled={false}
          index={this.state.tabIndex}
          loop={false}
          showsPagination={false}
          removeClippedSubviews={false}
          onIndexChanged={(index) => {
            this.setState({ tabIndex: index })
          }}
        >
          <TokenList />
          <MyOffers />
        </Swiper>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  tabContainer: {
    backgroundColor: '#F2F2F2',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  buttonContainer: {
    alignItems: 'center',
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
    flex: 1,
    flexDirection: 'row'
  },
  buttonBorderLeft: {
    borderLeftColor: '#E5E5E5',
    borderLeftWidth: 1
  },
  buttonTouch: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: 10
  },
  buttonText: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14
  }
})

export default SmartContract
