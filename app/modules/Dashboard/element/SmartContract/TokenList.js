import React, { Component } from 'react'
import {
  View,
  Text,
  Alert,
  Image,
  FlatList,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native'
import { observer } from 'mobx-react/native'

// Third-party
import { Collapse, CollapseHeader, CollapseBody } from 'accordion-collapse-react-native'

// Components & Modules
import MainStore from '../../../../AppStores/MainStore'
import NavStore from '../../../../AppStores/NavStore'
import AppStyle from '../../../../commons/AppStyle'
import icons from '../../../../commons/icons'
import Helper from '../../../../commons/Helper'
import TokenIcon from '../TokenIcon'

@observer
class TokenList extends Component {
  constructor(props) {
    super(props)

    this.dataStore = MainStore.smartContractStore
    this._keyExtractor = this._keyExtractor.bind(this)
    this._renderItem = this._renderItem.bind(this)
    this._renderHeaderOffers = this._renderHeaderOffers.bind(this)
    this._renderEmptyOffers = this._renderEmptyOffers.bind(this)
    this._renderOffers = this._renderOffers.bind(this)
    this._onPressExchange = this._onPressExchange.bind(this)

    // Request token list
    this.dataStore.getTokeniz()
  }

  _onPressExchange = item => () => {
    // Check asset
    const assetList = MainStore.appState.OMWallet.balances.filter(as => as.asset_code)
    const sell = assetList.find(a => a.asset_code === item.selling && a.asset_issuer === item.selling_issuer)
    const buy = assetList.find(a => a.asset_code === item.buying && a.asset_issuer === item.buying_issuer)

    if (sell && buy) {
      NavStore.pushToScreen('ManageOfferScreen', {
        selling: {
          asset_code: item.selling,
          asset_issuer: item.selling_issuer
        },
        buying: {
          asset_code: item.buying,
          asset_issuer: item.buying_issuer
        }
      })
    } else {
      const missing = []
      if (!sell) {
        missing.push(`  - ${item.selling}`)
      }
      if (!buy) {
        missing.push(`  - ${item.buying}`)
      }
      Alert.alert(
        'ไม่สามารถดำเนินการได้',
        `เนื่องจากท่านยังไม่ได้ทำการเชื่อมเหรียญดังต่อไปนี้\n${missing.join(`\n`)}`,
        [
          { text: 'ปิด' }
        ]
      )
    }
  }

  _renderHeaderOffers = asset => () => {
    const { asset_code, asset_issuer } = asset
    const assetList = MainStore.appState.OMWallet.balances.filter(as => as.asset_code)
    const found = assetList.find(obj => obj.asset_code === asset_code && obj.asset_issuer === asset_issuer)
    if (found) {
      return null
    }

    return (
      <View
        style={{
          flex: 1,
          marginLeft: 50
        }}
      >
        <TouchableOpacity
          style={styles.connectBtn}
          onPress={() => {
            NavStore.pushToScreen('CreateTrustLineScreen', { asset_code, asset_issuer })
          }}
        >
          <Text style={styles.connectText}>{`เชื่อมเหรียญ ${asset_code}`}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  _renderEmptyOffers = () => (
    <View
      style={{
        borderColor: '#EEE',
        borderRadius: 5,
        borderWidth: 1,
        flex: 1,
        marginLeft: 50,
        padding: 5
      }}
    >
      <Text
        style={{
          color: '#BBB',
          fontFamily: AppStyle.mainFontTH,
          fontSize: 12,
          textAlign: 'center'
        }}
      >
        ไม่พบข้อมูล
      </Text>
    </View>
  )

  _renderOffers = ({ item }) => {
    const price = Helper.formatETH(item.price, false, 2)
    const amount = Helper.formatETH(item.amount, false, 2)
    return (
      <View style={styles.offerCard}>
        <View style={styles.row}>
          <View style={styles.offerBlock}>
            {/* <TokenIcon
              code={item.selling}
              issuer={item.selling_issuer}
              style={styles.offersIcon}
            /> */}
            <Text style={styles.offerText}>{item.selling}</Text>
          </View>
          <View style={styles.offerExchangeIcon}>
            <Image source={icons.iconExchange} />
          </View>
          <View style={styles.offerBlock}>
            {/* <TokenIcon
              code={item.buying}
              issuer={item.buying_issuer}
              style={styles.offersIcon}
            /> */}
            <Text style={styles.offerText}>{item.buying}</Text>
          </View>
        </View>
        <View style={styles.exchangeBlock}>
          <Text style={styles.exchangeTitle}>Exchange Rate</Text>
          <Text style={styles.exchangeRate}>{price} {item.selling} : 1 {item.buying}</Text>
          <Text style={styles.exchangeAmount}>Remaining</Text>
          <Text style={styles.exchangeRate}>{amount}</Text>
        </View>
        <View>
          <TouchableOpacity
            style={styles.exchangeBtn}
            onPress={this._onPressExchange(item)}
          >
            <Text style={styles.exchangeText}>แลกเปลี่ยนโทเคน</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  _keyExtractor = item => item.key

  _renderItem = ({ item }) => {
    // Filter offer
    const offers = item.offers.filter(o => o.selling === item.code)
    return (
      <Collapse>
        <CollapseHeader>
          <View
            style={{
              alignItems: 'center',
              flexDirection: 'row',
              marginVertical: 6
            }}
          >
            <Image
              style={styles.iconStyle}
              source={item.icon}
              resizeMode="contain"
            />
            <View
              style={{
                backgroundColor: '#EEE',
                borderRadius: 10,
                flex: 1,
                marginLeft: 10,
                paddingHorizontal: 10,
                paddingVertical: 5
              }}
            >
              <Text style={styles.assetName}>{item.code}</Text>
              <Text style={styles.assetIssuer}>{item.name}</Text>
            </View>
          </View>
        </CollapseHeader>
        <CollapseBody>
          <FlatList
            data={offers}
            showsVerticalScrollIndicator={false}
            keyExtractor={obj => obj.id.toString()}
            ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
            ListEmptyComponent={this._renderEmptyOffers}
            ListHeaderComponent={this._renderHeaderOffers({ asset_code: item.code, asset_issuer: item.issuer })}
            renderItem={this._renderOffers}
          />
        </CollapseBody>
      </Collapse>
    )
  }

  render() {
    const { tokenList, loaded, empty } = this.dataStore

    if (!loaded) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'center'
          }}
        >
          <ActivityIndicator size="small" color="#BBB" />
          <Text
            style={{
              color: '#BBB',
              fontFamily: AppStyle.mainFontTH,
              fontSize: 13,
              paddingLeft: 10
            }}
          >
            กำลังโหลดข้อมูล
          </Text>
        </View>
      )
    }

    if (loaded && empty) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'center'
          }}
        >
          <Text
            style={{
              color: '#BBB',
              fontFamily: AppStyle.mainFontTH,
              fontSize: 13,
              paddingLeft: 10
            }}
          >
            ไม่พบข้อมูล
          </Text>
        </View>
      )
    }

    return (
      <View style={styles.flatListContainer}>
        <ScrollView
          contentContainerStyle={styles.contentContainer}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{ flex: 1 }}
        >
          <FlatList
            data={tokenList}
            showsVerticalScrollIndicator={false}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            contentContainerStyle={{ paddingVertical: 15 }}
            ListFooterComponent={this._renderFooter}
          />
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    paddingHorizontal: 20
  },
  flatListContainer: {
    flex: 1
  },
  row: {
    flexDirection: 'row'
  },
  assetName: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 16
  },
  assetIssuer: {
    color: '#000',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 12
  },
  exchangeBtn: {
    backgroundColor: '#4267B2',
    borderRadius: 5,
    marginTop: 10,
    paddingHorizontal: 10,
    paddingVertical: 8
  },
  exchangeText: {
    color: '#FFF',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14,
    textAlign: 'center'
  },
  connectBtn: {
    backgroundColor: '#42B884',
    borderRadius: 5,
    marginBottom: 5,
    paddingHorizontal: 10,
    paddingVertical: 8
  },
  connectText: {
    color: '#FFF',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14,
    textAlign: 'center'
  },
  offerText: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 14
  },
  iconStyle: {
    height: 40,
    width: 40
  },
  offerCard: {
    borderColor: '#EEE',
    borderRadius: 5,
    borderWidth: 1,
    flex: 1,
    marginLeft: 50,
    padding: 5
  },
  offerBlock: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    paddingVertical: 5
  },
  offersIcon: {
    height: 40,
    marginBottom: 5,
    width: 40
  },
  offerExchangeIcon: {
    marginTop: 15
  },
  exchangeBlock: {
    alignItems: 'center',
    flex: 1
  },
  exchangeTitle: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 12
  },
  exchangeAmount: {
    color: '#000',
    fontFamily: AppStyle.mainFontSemiBoldTH,
    fontSize: 12,
    marginTop: 5
  },
  exchangeRate: {
    color: '#000',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 11
  }
})

export default TokenList
