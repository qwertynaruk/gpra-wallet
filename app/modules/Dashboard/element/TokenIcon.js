import React, { Component } from 'react'
import { Image } from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import api from '../../../api'
import icons from '../../../commons/icons'

@observer
class TokenIcon extends Component {
  static propTypes = {
    style: PropTypes.object,
    code: PropTypes.string,
    issuer: PropTypes.string
  }

  static defaultProps = {
    style: {},
    code: '',
    issuer: ''
  }

  constructor(props) {
    super(props)

    this.state = {
      url: ''
    }
  }

  componentDidMount() {
    this._isMounted = true
    api.Aom.tokenizeIcon(this.props.code, this.props.issuer)
      .then((res) => {
        if (this._isMounted) {
          const { status, data } = res
          if (status === 200) {
            const { url } = data.data
            this.setState({ url: url ? { uri: url } : icons.iconTokeniz })
          } else {
            this.setState({ url: icons.iconTokeniz })
          }
        }
      }).catch(() => {
        this.setState({ url: icons.iconTokeniz })
      })
  }

  componentWillUnmount() {
    this._isMounted = false
  }

  _isMounted = false

  render() {
    const { url } = this.state
    return (
      <Image
        style={this.props.style}
        source={url || icons.iconTokeniz}
        resizeMode="contain"
      />
    )
  }
}

export default TokenIcon
