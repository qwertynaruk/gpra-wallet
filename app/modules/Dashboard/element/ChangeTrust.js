import React, { Component } from 'react'
import {
  Alert,
  Image,
  StyleSheet,
  ActivityIndicator,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import SecureDS from '../../../AppStores/DataSource/SecureDS'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import AppStyle from '../../../commons/AppStyle'
import images from '../../../commons/images'

@observer
export default class ChangeTrust extends Component {
  static propTypes = {
    code: PropTypes.string,
    issuer: PropTypes.string,
    exit: PropTypes.func
  }

  static defaultProps = {
    code: '',
    issuer: '',
    exit: () => { }
  }

  constructor(props) {
    super(props)

    this.state = {
      finish: false,
      loading: false
    }

    this.onPressAccept = this.onPressAccept.bind(this)
    this.fabric = MainStore.appState.fabric
  }

  onPressAccept() {
    NavStore.preventOpenUnlockScreen = false
    NavStore.lockScreen({
      onUnlock: (pincode) => {
        if (this.state.loading) return

        this.setState({
          loading: true
        })

        // Get OM Wallet
        const wallet = MainStore.appState.wallets.find(obj => obj.type === 'aomtoken')
        const ds = new SecureDS(pincode)
        wallet.setSecureDS(ds)

        // Token data
        const { issuer, code } = this.props

        // Create Trust line
        if (wallet.address) {
          wallet.createTrustLine(issuer, code)
            .then(() => {
              // Set state
              this.setState({
                finish: true
              })

              // Alert
              Alert.alert(
                `ยอมรับโทเคน ${code} สำเร็จ`,
                `กระเป๋าของท่านสามารถใช้โทเคน ${code} ได้แล้ว`, [
                  {
                    text: 'รับทราบ',
                    style: 'cancel',
                    onPress: this.props.exit
                  }
                ], {
                  cancelable: true
                }
              )

              // Log
              this.fabric.answers.logCustom(`ChangeTrust Success`, {
                address: wallet.address,
                asset_issuer: issuer,
                asset_code: code
              })
              
            })
            .catch((err) => {
              // Set state
              this.setState({
                finish: true
              })

              // Alert
              Alert.alert(
                'เกิดข้อผิดผลาด',
                `ยอมรับโทเคน ${code} ล้มเหลวเนื่องจาก\n${err.message}`, [
                  {
                    text: 'ปิด',
                    style: 'cancel',
                    onPress: this.props.exit
                  }
                ], {
                  cancelable: true
                }
              )

              // Log
              // this.fabric.recordNonFatal(err.message)
              this.fabric.answers.logCustom('ChangeTrust Error', {
                address: wallet.address,
                asset_issuer: issuer,
                asset_code: code,
                error: err.message
              })
            })
        }
      }
    }, true)
  }

  render() {
    const { loading, finish } = this.state
    return (
      <View style={styles.popupContent}>
        <Text style={styles.title}>
          {`ยอมรับโทเคนในกระเป๋าของคุณ`}
        </Text>
        <Image source={images.imgChangeTrust} style={{ width: 260 }} resizeMode="center" />
        <Text style={styles.assetCode}>{this.props.code}</Text>
        <Text style={styles.description}>
          {`${this.props.code} เป็นโทเคนที่อยู่บนเครือข่าย Om Chain ดังนั้นคุณจะต้องทำการยอมรับโทเคนนี้ก่อน จึงจะสามารถใช้งานโทเคนนี้ในกระเป๋าของคุณได้`}
        </Text>
        {
          loading && !finish && (
            <View
              style={{
                alignItems: 'center',
                flexDirection: 'row',
                justifyContent: 'center',
                height: 40,
                marginVertical: 10
              }}
            >
              <ActivityIndicator size="small" color="#BBB" />
              <Text
                style={{
                  color: '#BBB',
                  fontFamily: AppStyle.mainFontTH,
                  fontSize: 16,
                  paddingLeft: 10
                }}
              >
                กำลังโหลดประมวลผล
              </Text>
            </View>
          )
        }
        {
          !loading && !finish && (
            <View style={styles.buttonGroup}>
              <TouchableOpacity
                activeOpacity={0.7}
                style={[styles.buttonContainer, styles.backgroundButton]}
                onPress={this.onPressAccept}
              >
                <Text style={styles.buttonText}>ยอมรับโทเคนนี้</Text>
              </TouchableOpacity>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.buttonContainer}
                onPress={this.props.exit}
              >
                <Text style={[styles.buttonText, styles.cancelText]}>ปฏิเสธ</Text>
              </TouchableOpacity>
            </View>
          )
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  popupContent: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFF',
    borderRadius: 3
  },
  title: {
    textAlign: 'center',
    color: '#535786',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20,
    marginBottom: 10,
    marginTop: 25
  },
  assetCode: {
    color: '#121212',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20,
    paddingVertical: 10
  },
  description: {
    color: '#121212',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    marginBottom: 10,
    paddingHorizontal: 15,
    textAlign: 'center'
  },
  buttonGroup: {
    paddingHorizontal: 15,
    paddingBottom: 5,
    paddingTop: 15,
    width: '100%'
  },
  buttonsContainer: {
    flexDirection: 'column',
    width: '100%'
  },
  buttonContainer: {
    width: '100%',
    height: 60,
    alignItems: 'center',
    justifyContent: 'center'
  },
  backgroundButton: {
    backgroundColor: '#535786',
    borderRadius: 8
  },
  buttonText: {
    color: '#FFF',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18
  },
  cancelText: {
    color: '#C30000',
    fontFamily: AppStyle.mainFontTH
  }
})
