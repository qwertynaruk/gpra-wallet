import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NotificationStore from '../../../AppStores/stores/Notification'

@observer
class HeaderMenu extends Component {
  static propTypes = {
    toggle: PropTypes.func,
    disabled: PropTypes.bool
  }

  static defaultProps = {
    toggle: () => { },
    disabled: false
  }

  constructor(props) {
    super(props)

    this.answers = MainStore.appState.fabric.answers
  }

  onToggle = () => {
    const { toggle } = this.props
    toggle()
  }

  _goToSettings = () => {
    NavStore.pushToScreen('SettingStack')
    this.answers.logContentView('Setting', 'screen', 'setting-screen')
  }

  render() {
    const { userInfo } = MainStore.appState

    return (
      <View style={styles.menuHeader}>
        <TouchableOpacity
          style={styles.iconHeader}
          onPress={this._goToSettings}
        >
          <Image
            key={userInfo && userInfo.profile_image ? userInfo.profile_image : 'avatar'}
            style={[
              styles.iconHeader,
              {
                marginTop: 5,
                width: 36,
                height: 36,
                borderWidth: 2,
                borderColor: '#FFF',
                borderRadius: 18
              }
            ]}
            source={userInfo && userInfo.profile_image ? { uri: userInfo.profile_image } : images.iconProfile}
            resizeMode="cover"
          />
        </TouchableOpacity>
        <Text style={styles.title} />
        {!this.props.disabled &&
          <TouchableOpacity style={styles.iconHeader} onPress={this.onToggle}>
            {
              NotificationStore.notifyBadgeCount === 0 &&
              <Image source={images.iconNoti} />
            }
            {
              NotificationStore.notifyBadgeCount > 0 &&
              <Image source={images.iconNotiActive} />
            }
          </TouchableOpacity>
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  menuHeader: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 8
  },
  iconHeader: {
    width: 44,
    height: 44
  },
  title: {
    flex: 1,
    textAlign: 'center',
    fontFamily: AppStyle.mainFontBold,
    fontSize: 16,
    color: '#FFFFFF'
  }
})

export default HeaderMenu
