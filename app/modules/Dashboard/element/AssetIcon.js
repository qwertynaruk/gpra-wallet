import React, { Component } from 'react'
import { Image } from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import api from '../../../api'
import icons from '../../../commons/icons'

@observer
class AssetIcon extends Component {
  static propTypes = {
    style: PropTypes.object,
    id: PropTypes.string,
    type: PropTypes.string
  }

  static defaultProps = {
    style: {},
    id: '',
    type: ''
  }

  constructor(props) {
    super(props)

    this.state = {
      icon: ''
    }
  }

  componentWillMount() {
    this._isMounted = true
    const { type, id } = this.props
    if (type === 'aomtoken') {
      if (id === 'OM') {
        this.setState({ icon: icons.logoOm })
      } else {
        const [code, issuer] = id.split('-')
        api.Aom.tokenizeIcon(code, issuer)
          .then((res) => {
            if (this._isMounted) {
              const { status, data } = res
              if (status === 200) {
                const { url } = data.data
                this.setState({ icon: url ? { uri: url } : icons.iconTokeniz })
              } else {
                this.setState({ icon: icons.iconTokeniz })
              }
            }
          }).catch(() => {
            if (this._isMounted) {
              this.setState({ icon: icons.iconTokeniz })
            }
          })
      }
    } else if (type === 'ethereum') {
      this.setState({ icon: icons.logoEth })
    } else if (type === 'bitcoin') {
      this.setState({ icon: icons.logoBtc })
    } else if (type === 'ripple') {
      this.setState({ icon: icons.logoXrp })
    } else if (type === 'stellar') {
      this.setState({ icon: icons.logoXlm })
    }
  }

  componentWillUnmount() {
    this._isMounted = false
  }

  _isMounted = false

  render() {
    const { icon } = this.state
    if (icon) {
      return (
        <Image
          style={this.props.style}
          source={icon || icons.iconTokeniz}
          resizeMode="contain"
        />
      )
    }

    return null
  }
}

export default AssetIcon
