import React, { Component } from 'react'
import {
  View,
  ScrollView,
  Dimensions,
  StyleSheet,
  Image,
  Text
} from 'react-native'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react/native'
import AppStyle from '../../../commons/AppStyle'
import images from '../../../commons/images'
import AomViewButton from '../../../components/elements/AomViewButton'

const { width } = Dimensions.get('window')
@observer
export default class Trustline extends Component {
  static propTypes = {
    modalMode: PropTypes.bool,
    modalCloseAction: PropTypes.func,
    onPress: PropTypes.func
  }

  static defaultProps = {
    modalMode: false,
    modalCloseAction: () => {},
    onPress: () => { }
  }

  state = {
    extraViewStyle: {
      paddingTop: 20
    }
  }

  onLayout = (e) => {
    if (this.props.modalMode) {
      return
    }
    const _container_height = e.nativeEvent.layout.height
    const _view_height = 430
    if (_container_height > _view_height) {
      const _paddingTop = (_container_height - _view_height) / 1.8
      this.setState({
        extraViewStyle: { paddingTop: _paddingTop }
      })
    }
  }

  render() {
    return (
      <View
        style={styles.container}
        onLayout={this.onLayout}
      >
        <ScrollView
          style={[styles.scrollContainer, { width }]}
        >
          <View
            style={[
              styles.mainContent,
              this.state.extraViewStyle
            ]}
          >
            <Text style={styles.title}>ยอมรับ Om Token ในกระเป๋าของคุณ</Text>
            <Image
              source={images.iconTrustLine}
              style={styles.icon}
            />
            <Text style={styles.paragraph}>
              {`Om Token เป็นโทเคนที่อยู่บน
เครือข่าย Stellar ดังนั้นคุณจะต้องทำการ
ยอมรับ Om Token Trustline
บนเครือข่ายก่อนจึงจะสามารถใช้งาน Om
บนกระเป๋าใหม่ของคุณได้`}
            </Text>
            <Text style={styles.buttonLink}>
              เรียนรู้เพิ่มเติมเกี่ยวกับ Stellar Trustline
            </Text>
            <View style={{ width: 320 }}>
              <AomViewButton onPress={this.props.onPress} title="ยอมรับ Om Token" />
              {
                this.props.modalMode &&
                <AomViewButton onPress={this.props.modalCloseAction} btnStyle="normal" title="ปิด" />
              }
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    alignContent: 'center'
  },
  mainContent: {
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: 'center',
    alignContent: 'center'
  },
  icon: {
    marginVertical: 10
  },
  title: {
    color: '#121212',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18
  },
  paragraph: {
    color: '#121212',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    textAlign: 'center'
  },
  buttonLink: {
    color: '#4E52B0',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 14,
    marginTop: 5
  }
})
