import React, { Component } from 'react'
import {
  Text,
  View,
  Image,
  Linking,
  FlatList,
  ScrollView,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  RefreshControl
} from 'react-native'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react/native'
import { Collapse, CollapseHeader, CollapseBody } from 'accordion-collapse-react-native'

import moment from 'moment'
import 'moment/locale/th'

import MainStore from '../../../AppStores/MainStore'
import AppStyle from '../../../commons/AppStyle'
import images from '../../../commons/images'
import Helper from '../../../commons/Helper'
import NavStore from '../../../AppStores/NavStore'

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 20
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom
}

@observer
class Transactions extends Component {
  static propTypes = {
    onPress: PropTypes.func
  }

  static defaultProps = {
    onPress: () => {
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      loadMore: false,
      refreshing: false,
      collapsed: []
    }

    this.loadMoreData = this.loadMoreData.bind(this)
    this._renderFooter = this._renderFooter.bind(this)
  }

  onPress = () => {
    const { onPress } = this.props
    onPress()
  }

  get addressBooks() {
    return MainStore.appState.addressBooks
  }

  decimalAfterDot = (type) => {
    switch (type) {
      case 'BTC':
        return 8
      case 'ETH':
        return 18
      case 'XRP':
        return 15
      case 'XLM':
        return 7
      default:
        return 2
    }
  }

  gotoTransactionHash = hash => () => {
    Linking.openURL(hash)
  }

  loadMoreData = () => {
    const { loadMore } = this.state
    const { selectedWallet } = MainStore.appState
    const full = selectedWallet.multiple && selectedWallet.multiple >= 5
    if (loadMore || full) {
      return
    }
    this.setState({ loadMore: true })
    selectedWallet
      .fetchingTransaction(true)
      .then(() => {
        this.setState({ loadMore: false })
      })
  }

  _keyExtractor = (item, index) => index.toString()

  _renderItem = ({ item }) => {
    const resource = {
      create: { msg: `เริ่มออม`, sender: 'บัญชีผู้สร้าง:', icon: images.iconProfile },
      receive: { msg: `ได้รับ`, sender: 'บัญชีผู้ส่ง:', icon: images.iconReceiveAom },
      payment: { msg: `ส่ง`, sender: 'บัญชีผู้รับ:', icon: images.iconPaymentAom },
      waitingReceive: { msg: `รอยืนยันรายการรับ`, sender: 'บัญชีผู้ส่ง:', icon: images.iconPendingTransaction },
      waitingPayment: { msg: `รอยืนยันรายการส่ง`, sender: 'บัญชีผู้รับ:', icon: images.iconPendingTransaction }
    }
    const assets = resource[item.type]

    if (!assets) {
      return null
    }

    const { collapsed } = this.state
    const isCollapsed = collapsed.includes(item.hash)

    const receive = (item.type === 'waitingReceive' || item.type === 'receive' || item.type === 'create') ? 1 : 0
    const date = moment(item.created_at)
    const day = date.format('DD')
    const month = date.format('MMM')
    const year = date.format('YYYY')
    const time = date.format('HH:mm')
    const address = item.address.replace(/^([a-zA-Z0-9]{2})(.+)([a-zA-Z0-9]{3})$/, '$1...$3')
    const favorite = this.addressBooks.find(obj => obj.address === item.address)
    const BDYear = `${(Number(year) + 543)}`.substr(2)
    return (
      <Collapse
        isCollapsed={isCollapsed}
        onToggle={(toggled) => {
          const index = collapsed.indexOf(item.hash)
          if (toggled && index === -1) {
            collapsed.push(item.hash)
          } else if (!toggled && index !== -1) {
            collapsed.splice(index, 1)
          }

          this.setState({ collapsed })
        }}
      >
        <CollapseHeader>
          <View style={styles.itemContainer}>
            <View style={styles.leftColumn}>
              <Image
                source={assets.icon}
                style={styles.icon}
                resizeMode="contain"
              />
              {item.type === 'waitingReceive' || item.type === 'waitingPayment'
                ? <Text style={styles.amount}>{`${assets.msg} ${item.asset_code}`}</Text>
                : <Text style={styles.amount}>{`${assets.msg} ${Helper.formatETH(item.amount, true, this.decimalAfterDot(item.asset_code))} ${item.asset_code}`}</Text>
              }
            </View>
            <View style={styles.rightColumn}>
              {item.type === 'waitingReceive' || item.type === 'waitingPayment'
                ? <Text style={styles.datetime}>{`${item.numberConfirm}/${item.asset_code === 'ETH' ? 16 : 6}`}</Text>
                : <Text style={styles.datetime}>{`${day} ${month} ${BDYear} ${time} น.`}</Text>
              }
            </View>
          </View>
        </CollapseHeader>
        <CollapseBody>
          <View
            style={receive ? styles.itemReceiveTransactionContainer : styles.itemSendTransactionContainer}
          >
            {
              favorite && (
                <View style={styles.item}>
                  <View style={styles.leftFlexColumn}>
                    <Text style={styles.sender}>
                      {receive ? 'ผู้ส่ง:' : 'ผู้รับ:'}
                    </Text>
                  </View>
                  <View style={styles.rightFlexColumn}>
                    <Text style={styles.nameSender}>{favorite.title}</Text>
                  </View>
                </View>
              )
            }

            {
              !receive && (
                <View style={styles.item}>
                  <View style={styles.leftFlexColumn}>
                    <Text style={styles.sender}>
                      ค่าธรรมเนียม:
                    </Text>
                  </View>
                  <View style={styles.rightFlexColumn}>
                    <Text style={styles.nameSender}>
                      {item.fee}
                    </Text>
                  </View>
                </View>
              )
            }

            <View style={styles.item}>
              <View style={styles.leftFlexColumn}>
                <Text style={styles.sender}>
                  {assets.sender}
                </Text>
              </View>
              <View style={styles.rightFlexColumn}>
                <Text style={styles.nameSender}>
                  {address}
                </Text>
              </View>
            </View>

            <View style={styles.item}>
              <View style={styles.leftFlexColumn}>
                <Text style={styles.sender}>
                  {receive ? 'เวลาที่ได้รับ:' : 'เวลาที่ส่ง:'}
                </Text>
              </View>
              <View style={styles.rightFlexColumn}>
                <Text style={styles.nameSender}>
                  {`${day} ${month} ${BDYear} ${time} น.`}
                </Text>
              </View>
            </View>
            <View style={styles.item}>
              <View style={styles.leftFlexColumn} />
              <View style={styles.rightFlexColumn}>
                <TouchableOpacity onPress={this.gotoTransactionHash(item.hash)}>
                  <Text style={styles.detailBlockchain}>
                    ดูข้อมูลบน Blockchain
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

        </CollapseBody>
      </Collapse>
    )
  }

  _renderFooter() {
    const { loadMore } = this.state
    if (loadMore) {
      return (
        <View
          style={{
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'center',
            height: 40
          }}
        >
          <ActivityIndicator size="small" color="#BBB" />
          <Text
            style={{
              color: '#BBB',
              fontFamily: AppStyle.mainFontTH,
              fontSize: 13,
              paddingLeft: 10
            }}
          >
            กำลังโหลดข้อมูล
          </Text>
        </View>
      )
    }

    return null
  }

  // Popup transaction details
  _transactionDetail() {
    NavStore.popupCustom.show(
      `ได้รับ 3.96 Aom`, [

        {
          // need to add close Button
          onClick: () => {
            NavStore.popupCustom.hide()
          }
        }
      ], 'จาก \n นาย สรรพ์ศิริ คำผลศิริ \n 0xea32ee64723a3bbd397f6243e4c19244ca4a9abe',
      'สถานะ \nSuccess',
      'Transaction ID: \n0xea328e...a4a9a2be',
      'Block #: \n51723123 ',
      'Transaction Time: \n11/21/2018 09:49:26 +0700',
      'ดูข้อมูลเพิ่มเติมบน Stellar Block Explorer' // add link
    )
  }

  // End Popup transaction

  render() {
    const { transactions, transactionLoaded } = MainStore.appState.selectedWallet

    if (!transactions || transactions.length == 0) {
      return (
        <View style={{ flex: 1 }}>
          <View style={[styles.container, styles.horizontal]}>
            <View style={styles.loading}>
              {!transactionLoaded && <ActivityIndicator size="large" color="#535786" />}
              <Text style={{ fontFamily: AppStyle.mainFontTH }}>{transactionLoaded ? 'ยังไม่มีประวัติทางธุรกรรม' : 'กำลังโหลดข้อมูล...'}</Text>
            </View>
          </View>
        </View>
      )
    }

    return (
      <View style={{ flex: 1 }}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>ประวัติการทำรายการ</Text>
        </View>
        <ScrollView
          style={{ flex: 1 }}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                // app/AppStores/stores/Wallet/Wallet.aom.js:136
                // MainStore.appState.selectedWallet.transactions = []
                MainStore.appState.selectedWallet.fetchingBalance()
                MainStore.appState.selectedWallet.fetchingTransaction()
              }}
            />
          }
          onScroll={({ nativeEvent }) => {
            if (isCloseToBottom(nativeEvent)) {
              this.loadMoreData()
            }
          }}
        >
          <FlatList
            data={transactions}
            extraData={this.state}
            showsVerticalScrollIndicator={false}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            ListFooterComponent={this._renderFooter}
          />
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
    paddingLeft: 40
  },
  titleContainer: {
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1
  },
  title: {
    color: '#000',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18,
    paddingVertical: 10,
    textAlign: 'center'
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  },
  itemContainer: {
    flex: 1,
    flexDirection: 'row',
    alignContent: 'space-between',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    paddingVertical: 10,
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1
  },
  itemSendTransactionContainer: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 10,
    marginVertical: 10,
    marginHorizontal: 30,
    paddingVertical: 10,
    borderLeftColor: '#5347E0',
    borderLeftWidth: 1,
    backgroundColor: '#FBFBFE'
  },
  itemReceiveTransactionContainer: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 10,
    marginVertical: 10,
    marginHorizontal: 30,
    paddingVertical: 10,
    borderLeftColor: '#1FAB92',
    borderLeftWidth: 1,
    backgroundColor: '#F9FEFD'
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  leftColumn: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  rightColumn: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 15
  },
  leftFlexColumn: {
    paddingRight: 10
  },
  rightFlexColumn: {
    flexWrap: 'wrap'
  },
  icon: {
    marginRight: 10,
    width: 24
  },
  amount: {
    color: '#000',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16
  },
  sender: {
    color: '#949AAB',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 14
  },
  nameSender: {
    color: '#283250',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 14,
    textAlign: 'right',
    flexGrow: 1,
    flex: 1
  },
  detailBlockchain: {
    fontFamily: AppStyle.mainFontTH,
    color: '#4E52B0',
    fontSize: 14,
    textAlign: 'right'
  },
  datetime: {
    color: '#B0B0B0',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 14,
    textAlign: 'right'
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    opacity: 0.5,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default Transactions
