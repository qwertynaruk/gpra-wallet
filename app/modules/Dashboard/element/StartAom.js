import React, { Component } from 'react'
import {
  Dimensions,
  View,
  ScrollView,
  StyleSheet,
  Image,
  Text
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import AomViewButton from '../../../components/elements/AomViewButton'

const { width, height } = Dimensions.get('window')

@observer
export default class StartAom extends Component {
  static propTypes = {
    onPress: PropTypes.func
  }

  static defaultProps = {
    onPress: () => { }
  }

  render() {
    return (
      <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flex: 1 }}>
        <View style={styles.container}>
          <Image
            resizeMode="center"
            source={images.iconAomStart}
            style={styles.icon}
          />
          <Text style={styles.title}>สามารถเริ่มต้นออมกับเราง่ายๆได้เลย</Text>
          <AomViewButton onPress={this.props.onPress} title="เริ่มต้นออม" />
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    paddingHorizontal: 20
  },
  icon: {
    marginVertical: 20
  },
  title: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 0.056 * width,
    color: '#121212'
  }
})
