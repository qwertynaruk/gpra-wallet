import React, { Component } from 'react'
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'

const { width } = Dimensions.get('window')

@observer
class RequestKYC extends Component {
  static propTypes = {
    onPress: PropTypes.func
  }

  static defaultProps = {
    onPress: () => { }
  }

  render() {
    return (
      <View style={styles.popupContent}>
        <Text style={styles.title}>
          {`คุณต้องทำการยืนยันตัวตนก่อน`}
        </Text>
        <Image source={images.iconRequestKyc} style={styles.icon} />
        <Text style={styles.description}>
          {`คุณสามารถใช้กระเป๋า Om Token\nได้อย่างสมบูรณ์หลังจากได้รับการยืนยัน\nข้อมูลจากระบบ`}
        </Text>
        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              activeOpacity={0.6}
              style={styles.button}
              onPress={this.props.onPress}
            >
              <Text style={styles.buttonText}>ยืนยันตัวตน</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  popupContent: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFF',
    borderRadius: 3
  },
  title: {
    textAlign: 'center',
    color: '#535786',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 0.056 * width,
    marginTop: 35,
    marginBottom: 20
  },
  icon: {
    marginVertical: 20
  },
  description: {
    marginBottom: 25,
    textAlign: 'center',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 0.05 * width,
    color: '#121212'
  },
  buttonsContainer: {
    flexDirection: 'column',
    width: '100%'
  },
  buttonContainer: {
    flexDirection: 'row',
    borderTopColor: '#E7EAF0',
    borderTopWidth: 1,
    marginHorizontal: 5
  },
  button: {
    height: 70,
    marginHorizontal: 20,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#535786',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20
  }
})

export default RequestKYC
