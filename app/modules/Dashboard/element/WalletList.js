import React, { Component } from 'react'
import {
  View,
  FlatList,
  Image,
  TouchableOpacity,
  StyleSheet,
  Text
} from 'react-native'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import MainStore from '../../../AppStores/MainStore'
import AppStyle from '../../../commons/AppStyle'
import icons from '../../../commons/icons'
import Helper from '../../../commons/Helper'
import AssetIcon from './AssetIcon'

@observer
class WalletList extends Component {
  constructor(props) {
    super(props)

    this.state = {
      select: ''
    }
  }

  componentWillMount() {
    const { ext } = MainStore.appState.OMWallet

    // Set selected
    this.setState({
      select: `${ext}`
    })
  }

  getSymbol(type) {
    switch (type) {
      case 'ethereum': return 'Ethereum'
      case 'bitcoin': return 'Bitcoin'
      case 'litecoin': return 'Litecoin'
      case 'dogecoin': return 'Dogecoin'
      case 'stellar': return 'Stellar'
      case 'ripple': return 'Ripple'
      case 'aomtoken': return 'OmCoin'
      default: return 'BTC'
    }
  }

  getRate(type) {
    switch (type) {
      case 'ethereum': return MainStore.appState.rateETH
      case 'bitcoin': return MainStore.appState.rateBTC
      case 'ripple': return MainStore.appState.rateXRP
      case 'stellar': return MainStore.appState.rateXLM
      case 'aomtoken': return MainStore.appState.rateAOM
      default: return MainStore.appState.rateAOM
    }
  }

  getLogo(type) {
    switch (type) {
      case 'ethereum': return icons.logoEth
      case 'bitcoin': return icons.logoBtc
      case 'ripple': return icons.logoXrp
      case 'stellar': return icons.logoXlm
      case 'aomtoken': return icons.logoOm
      default: return icons.logoOm
    }
  }

  _keyExtractor = item => item.key

  _renderItem = ({ item }) => {
    const { select } = this.state
    const {
      key, type, balance, ext, index, balanceText
    } = item
    const rate = this.getRate(type)
    const symbol = this.getSymbol(type)
    const selected = select === key
    return (
      <TouchableOpacity
        key={`wallet-${type}-${index}`}
        style={[styles.card, selected ? styles.cardActive : {}]}
        activeOpacity={0.9}
        onPress={() => {
          if (select !== key) {
            this.setState({
              select: key
            })

            // Set selected by type
            MainStore.appState.setSelectedByType(type)

            // Check is type aomtoken
            if (type === 'aomtoken') {
              const { balances } = MainStore.appState.selectedWallet
              const indexed = balances.findIndex(b => b.key === key)
              MainStore.appState.selectedWallet.asset = balances[indexed]
            }
          }
        }}
      >
        <View style={styles.cardRow}>
          <View style={styles.cardIcon}>
            <AssetIcon
              id={key}
              type={type}
              style={styles.assetLogo}
            />
          </View>
          <View style={styles.cardBody}>
            <View style={styles.cardTitle}>
              <Text style={styles.assetExt}>{ext}</Text>
              {
                selected && (
                  <View>
                    <Text style={styles.activatedText}>ACTIVATED</Text>
                  </View>
                )
              }
            </View>
            <Text style={styles.assetTitle}>{symbol}</Text>
          </View>
          <View style={styles.cardBalance}>
            <Text style={styles.priceText}>{balanceText}</Text>
            <Text style={styles.priceCalculated}>{Helper.formatCUR(balance.multipliedBy(rate).toNumber())} THB</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const cards = []
    MainStore.appState.cards.forEach((obj) => {
      if (Array.isArray(obj)) {
        Array.from(obj).forEach((card) => {
          cards.push(card)
        })
      } else {
        cards.push(obj)
      }
    })
    return (
      <View style={styles.container}>
        <FlatList
          data={cards}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          extraData={MainStore.appState.selectedWallet}
          contentContainerStyle={cards.length ? {} : { flex: 1 }}
          ItemSeparatorComponent={() => (<View style={{ height: 10 }} />)}
          ListEmptyComponent={() => (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: 10
              }}
            >
              <Text style={styles.priceReference}>ยังไม่มีข้อมูลการ์ด</Text>
            </View>
          )}
          ListHeaderComponent={() => (<View style={{ height: 20 }} />)}
          ListFooterComponent={() => {
            if (cards.length) {
              return (
                <View style={styles.reference}>
                  <Text style={styles.priceReference}>ข้อมูลราคาจาก bitkub.com</Text>
                </View>
              )
            }

            return null
          }}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  assetLogo: {
    height: 40,
    width: 40
  },
  assetExt: {
    color: '#0D051E',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 18
  },
  assetTitle: {
    color: '#0D051E',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 12
  },
  priceText: {
    color: '#0D051E',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18
  },
  priceCalculated: {
    color: '#0D051E',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 12,
    opacity: 0.6
  },
  card: {
    backgroundColor: '#F2F3F9',
    borderRadius: 10,
    marginHorizontal: 20,
    padding: 15,
    shadowColor: '#291E6B',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 1,
    elevation: 2
  },
  cardActive: {
    backgroundColor: '#D6D4FE'
  },
  cardRow: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  cardIcon: {
    marginRight: 15
  },
  cardBody: {
    flex: 1
  },
  cardTitle: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  cardBalance: {
    alignItems: 'flex-end'
  },
  activatedText: {
    backgroundColor: '#8581CC',
    borderRadius: 3,
    color: '#FFF',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 9,
    marginLeft: 5,
    paddingHorizontal: 3
  },
  reference: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10,
    paddingTop: 10
  },
  priceReference: {
    color: '#ADACB5',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 12,
    textAlign: 'center'
  }
})

export default WalletList
