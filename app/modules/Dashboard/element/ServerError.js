import React, { Component } from 'react'
import {
  Image,
  StyleSheet,
  Text,
  View
} from 'react-native'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import icons from '../../../commons/icons'
import AppStyle from '../../../commons/AppStyle'

@observer
export default class ServerError extends Component {
  render() {
    return (
      <View style={styles.popupContent}>
        <Text style={styles.title}>
          {`ปิดปรับปรุงระบบ`}
        </Text>
        <Image source={icons.iconWarning} style={styles.icon} />
        <Text style={styles.description}>{`ขออภัยในความไม่สะดวก\nระบบยังไม่สามารถให้บริการได้ในขณะนี้\nทางเราจะดำเนินการแจ้งให้ทราบอีกครั้งภายหลัง`}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  popupContent: {
    backgroundColor: '#FFF',
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 35
  },
  title: {
    color: '#535786',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20,
    marginBottom: 20
  },
  icon: {
    marginTop: 20,
    marginBottom: 20
  },
  description: {
    color: '#121212',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 25,
    paddingHorizontal: 20
  }
})
