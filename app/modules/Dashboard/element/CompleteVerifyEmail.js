import React, { Component } from 'react'
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'

@observer
class CompleteVerifyEmail extends Component {
  static propTypes = {
    onPress: PropTypes.func
  }

  static defaultProps = {
    onPress: () => { }
  }

  render() {
    return (
      <View style={styles.popupContent}>
        <Text style={styles.title}>
          {`เสร็จการยืนยันตัวตนเบื้องต้น`}
        </Text>
        <Image source={images.iconVerifyEmail} style={styles.icon} />
        <Text style={styles.description}>คุณสามารถเริ่มต้นใช้กระเป๋า Om Wallet ได้แล้ว</Text>
        <View style={styles.buttonsContainer}>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              activeOpacity={0.6}
              style={styles.button}
              onPress={this.props.onPress}
            >
              <Text style={styles.buttonText}>กลับสู่หน้าหลัก</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  popupContent: {
    backgroundColor: '#FFF',
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 35
  },
  title: {
    color: '#535786',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20,
    marginBottom: 20
  },
  icon: {
    marginTop: 20,
    marginBottom: 20
  },
  description: {
    color: '#121212',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 25
  },
  buttonsContainer: {
    flexDirection: 'column',
    width: '100%'
  },
  buttonContainer: {
    flexDirection: 'row',
    borderTopColor: '#E7EAF0',
    borderTopWidth: 1,
    marginHorizontal: 5
  },
  button: {
    height: 70,
    marginHorizontal: 20,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#535786',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20
  }
})

export default CompleteVerifyEmail
