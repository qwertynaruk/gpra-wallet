import React, { Component } from 'react'
import {
  Dimensions,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  View
} from 'react-native'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'
import TagList from '../elements/TagList'

const marginTop = LayoutUtils.getExtraTop()
const title = 'คำกู้ข้อมูล'
const descriptionText = `จด 12 คำนี้ไว้ในกระดาษแล้วเก็บไว้ในที่ปลอดภัย\nเพื่อใช้ในการกู้ข้อมูลกระเป๋าของคุณในอนาคต`

@observer
export default class MnemonicScreen extends Component {
  onBack = () => {
    NavStore.goBack()
  }

  renderMnemonic = () => (
    <TagList
      isShowOrder
      arrayMnemonic={MainStore.backupStore.listMnemonic.slice()}
      style={{
        itemTextColor: '#0C1B44',
        paddingVerticalOfItem: 8,
        numberOfWordInRow: 2,
        marginHorizontal: 25,
        marginTop: 20,
        backgroundDisable: 'transparent',
        itemTextColorDisable: AppStyle.mainTextColor,
        fontFamily: AppStyle.mainFontTH,
        itemFontSize: 15,
        userInteractionEnabled: false
      }}
    />
  )

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <NavigationHeader
          style={{
            marginTop: Platform.OS === 'ios' ? marginTop + 10 : marginTop + 20,
            marginBottom: 20
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            color: '#000',
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.onBack}
        />

        {/* Begin Mnemonic */}
        <View style={styles.container}>
          <View style={styles.description}>
            <Text style={styles.subText}>
              {descriptionText}
            </Text>
          </View>
          {this.renderMnemonic()}
        </View>
        {/* End Mnemonic */}
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  description: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  subText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    color: '#5A5A5A'
  }
})
