import React, { Component } from 'react'
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import HapticHandler from '../../../Handler/HapticHandler'
import AppStyle from '../../../commons/AppStyle'

const { width } = Dimensions.get('window')

export default class TagList extends Component {
  static propTypes = {
    isShowOrder: PropTypes.bool,
    style: PropTypes.object,
    arrayMnemonic: PropTypes.array,
    onItemPress: PropTypes.func,
    buttonStates: PropTypes.array,
    isCenter: PropTypes.bool
  }

  static defaultProps = {
    style: {
      paddingVerticalOfItem: 20,
      numberOfWordInRow: 3,
      marginHorizontal: 20,
      backgroundColor: '#dfdfdf',
      itemBackgroundColor: '#eeeeee',
      itemFontSize: 14,
      userInteractionEnabled: false,
      itemTextColor: '#000',
      fontFamily: 'Helvetica',
      fontWeight: 'regular',
      marginTop: 0,
      marginBottom: 0,
      backgroundDisable: '#dfdfdf',
      itemTextColorDisable: '#000'
    },
    arrayMnemonic: [],
    onItemPress: () => { },
    buttonStates: [],
    isShowOrder: false,
    isCenter: false
  }

  _checkDisableButton(index) {
    return this.props.buttonStates.length === 0
      ? !this.props.style.userInteractionEnabled
      : !this.props.buttonStates[index]
  }

  render() {
    const {
      arrayMnemonic,
      style,
      onItemPress,
      isShowOrder,
      isCenter
    } = this.props
    const {
      numberOfWordInRow,
      paddingVerticalOfItem,
      backgroundColor,
      itemBackgroundColor,
      marginHorizontal,
      itemFontSize,
      itemTextColor,
      fontFamily,
      fontWeight,
      marginTop,
      marginBottom,
      backgroundDisable,
      itemTextColorDisable
    } = style

    const itemWidth = (width - marginHorizontal * 2) / numberOfWordInRow
    const haflPaddingOfVerticalItem = paddingVerticalOfItem / 2
    const textAlign = isCenter ? { textAlign: 'center' } : {}
    return (
      <View
        style={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          backgroundColor,
          paddingVertical: haflPaddingOfVerticalItem,
          marginHorizontal,
          marginTop,
          marginBottom,
          justifyContent: 'space-between'
        }}
      >
        {arrayMnemonic.map((str, index) => (
          <View
            key={index}
            style={{
              width: itemWidth - 7,
              paddingVertical: haflPaddingOfVerticalItem
            }}
          >
            <TouchableOpacity
              onPress={() => {
                HapticHandler.ImpactLight()
                onItemPress(str, index)
              }}
              disabled={this._checkDisableButton(index)}
              style={{
                height: width >= 375 ? 42 : 32,
                borderRadius: 60,
                borderColor: '#CBCCD9',
                borderWidth: 1,
                backgroundColor: this._checkDisableButton(index) ? backgroundDisable : (str === '' ? backgroundDisable : itemBackgroundColor),
                paddingHorizontal: 10,
                alignItems: 'center',
                flexDirection: 'row'
              }}
            >
              {
                isShowOrder &&
                <View style={styles.numberBullet}>
                  <Text style={{
                    color: '#FFF',
                    fontSize: 11,
                    fontFamily: AppStyle.mainFontBold
                  }}
                  >
                    {index + 1}
                  </Text>
                </View>
              }
              <Text
                // allowFontScaling={false}
                style={[{
                  fontFamily,
                  fontWeight,
                  color: this._checkDisableButton(index) ? itemTextColorDisable : itemTextColor,
                  fontSize: itemFontSize
                }, textAlign]}
              >
                {str}
              </Text>
            </TouchableOpacity>
          </View>
        ))
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  numberBullet: {
    backgroundColor: '#CBCCD9',
    height: 20,
    width: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 9,
    borderRadius: 10
  }
})
