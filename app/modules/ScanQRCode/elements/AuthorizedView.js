import React, { PureComponent } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Image
} from 'react-native'
import PropsType from 'prop-types'
import AppStyle from '../../../commons/AppStyle'
import images from '../../../commons/images'
import constants from '../../../commons/constant'

export default class AuthorizedView extends PureComponent {
  static propTypes = {
    imageStyle: PropsType.object,
    textStyle: PropsType.object,
    onPressImage: PropsType.func
  }

  static defaultProps = {
    imageStyle: {},
    textStyle: {},
    onPressImage: () => { }
  }
  render() {
    const {
      imageStyle = {},
      textStyle = {},
      onPressImage = () => { }
    } = this.props

    return (
      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
        <Image
          style={imageStyle}
          source={images.scanFrame}
        />
        <View style={textStyle}>
          <Text
            style={[styles.description]}
          >
            {constants.SCAN_QR_FRAME}
          </Text>
          <TouchableWithoutFeedback
            onPress={onPressImage}
          >
            <View
              style={styles.button}
            >
              <Image
                source={images.iconAddPhoto}
              />
              <Text style={styles.buttonText}>
                {constants.ADD_FROM_ALBUM}
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  description: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    color: AppStyle.mainColor
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10
  },
  buttonText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    color: AppStyle.mainColor,
    marginLeft: 5
  }
})
