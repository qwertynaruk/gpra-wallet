import React, { Component } from 'react'
import {
  Animated,
  Dimensions,
  StyleSheet,
  Text,
  View
} from 'react-native'
import PropTypes from 'prop-types'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import AppStyle from '../../../commons/AppStyle'
import images from '../../../commons/images'

const { width } = Dimensions.get('window')

@observer
export default class StartAom extends Component {
  static propTypes = {
    title: PropTypes.string
  }

  static defaultProps = {
    title: ''
  }

  componentWillMount() {
    this.animatedValue = new Animated.Value(0)
  }

  componentDidMount() {
    this.runAnimation()
  }

  runAnimation() {
    this.animatedValue.setValue(0)
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 800,
      useNativeDriver: true
    }).start(() => {
      this.runAnimation()
    })
  }

  render() {
    const { title } = this.props
    const interpolateRotation = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })
    const animatedStyle = {
      transform: [
        { rotate: interpolateRotation }
      ]
    }
    const popupWidth = width < 400 ? {
      flex: 1,
      marginHorizontal: 20
    } : {
      width: 345
    }

    return (
      <View style={styles.overlay}>
        <View
          style={styles.overlayBackground}
        >
          <View style={[styles.popupContent, popupWidth]}>
            <Text style={styles.title}>{title}</Text>
            <Animated.Image source={images.iconLoading} style={animatedStyle} />
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  overlayBackground: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%'
  },
  popupContent: {
    backgroundColor: '#FFF',
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 30
  },
  title: {
    color: '#535786',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 20,
    marginBottom: 20
  }
})
