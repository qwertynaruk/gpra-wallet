import React, { Component } from 'react'
import {
  View,
  BackHandler,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  Platform,
  TouchableOpacity
} from 'react-native'

// Third-party
import Carousel, { Pagination } from 'react-native-snap-carousel'

// Components & Modules
import NavStore from '../../../AppStores/NavStore'
import AppStyle from '../../../commons/AppStyle'

// Data
import TutorialConstant from '../../../data/TutorialConstant'

const { width } = Dimensions.get('window')

export default class TutorialScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      entries: TutorialConstant.ENTRIES,
      activeSlide: 0
    }
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  pagination() {
    const { entries, activeSlide } = this.state

    return (
      <Pagination
        dotsLength={entries.length}
        activeDotIndex={activeSlide}
        containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}
        dotStyle={{
          width: 12,
          height: 12,
          borderRadius: 10,
          marginHorizontal: 8,
          backgroundColor: '#535786'
        }}
        inactiveDotStyle={{
          width: 12,
          height: 12,
          borderRadius: 10,
          backgroundColor: '#E3E4F6'
        }}
        inactiveDotOpacity={1}
        inactiveDotScale={1}
      />
    )
  }

  handleBackPress = () => true

  _goToAddCardScreen = () => {
    NavStore.pushToScreen('CreateWalletScreen')
  }

  _renderItem({ item, index }) {
    return (
      <View style={styles.container}>
        <View style={item.container.first}>
          <Image
            style={item.style}
            source={item.image}
            resizeMode={item.resizeMode}
          />
        </View>
        <View style={item.container.second}>
          <Text style={styles.descriptionText}>
            {item.description.firstLine}
          </Text>
          <Text style={styles.descriptionText}>
            {item.description.secondLine}
          </Text>
          <Text style={styles.descriptionText}>
            {item.description.thirdLine}
          </Text>
        </View>
      </View>
    )
  }

  render() {
    const { activeSlide } = this.state
    const btnText = activeSlide < 2 ? 'ต่อไป' : 'เริ่มออมเลย'
    const btnFunc = activeSlide < 2 ? () => { this._carousel.snapToNext() } : this._goToAddCardScreen

    return (
      <View style={styles.container}>
        <View style={{
          flex: 10,
          alignItems: 'flex-end',
          justifyContent: 'flex-end'
        }}
        >
          {/* <TouchableOpacity>
            <View style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center'
            }}
            >
              <Text style={styles.languageText}>
                TH
              </Text>
              <Image
                style={{
                  marginRight: 25,
                  marginLeft: 10
                }}
                source={icons.iconArrowDown}
                resizeMode="center"
              />
            </View>
          </TouchableOpacity> */}
        </View>

        {/* Begin carousel container */}
        <View style={{ flex: 75 }}>
          <Carousel
            ref={(c) => { this._carousel = c }}
            data={this.state.entries}
            renderItem={this._renderItem}
            sliderWidth={width}
            itemWidth={width}
            removeClippedSubviews={false}
            onSnapToItem={(index) => {
              this.setState({ activeSlide: index })
            }}
            vertical={false}
          />
          {this.pagination()}
        </View>
        {/* End carousel container */}

        {/* Begin button container */}
        <View style={{ flex: 15 }}>
          <TouchableOpacity
            style={styles.containerCreateWalletBtn}
            onPress={btnFunc}
          >
            <Text style={styles.createWalletText} >
              {btnText}
            </Text>
          </TouchableOpacity>
        </View>
        {/* End button container */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  containerCreateWalletBtn: {
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 20,
    backgroundColor: '#535786',
    borderRadius: 10
  },
  createWalletText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 22,
    color: '#FFFFFF'
  },
  descriptionText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20,
    color: AppStyle.colorBlack
  }
})
