import React, { Component } from 'react'
import {
  Keyboard,
  StyleSheet,
  Image,
  Text,
  Platform,
  BackHandler,
  StatusBar,
  ScrollView,
  View
} from 'react-native'

// Components & Modules
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const marginTop = LayoutUtils.getExtraTop()
const title = 'วิธีสำรองข้อมูล'

export default class BackupMnemonicTutorialScreen extends Component {
  constructor(props) {
    super(props)

    this.goBack = this.goBack.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }

    Keyboard.dismiss()
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  goBack() {
    NavStore.pushToScreen('DashboardScreen')
  }

  handleBackPress() {
    this.goBack()
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: marginTop + 10,
            marginBottom: 20
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            color: '#000',
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.goBack}
        />
        <ScrollView style={styles.container}>
          <Text style={styles.descText}>{`เข้าสู่เมนูการตั้งของของกระเป๋า\nดังภาพด้านล่าง`}</Text>

          <View
            style={{
              marginBottom: 16,
              marginTop: 10,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Image source={images.imgGotoSetting} />
          </View>

          <Text style={styles.descText}>{`กดเมนู คำกู้ข้อมูล (Seed Words)\nดังภาพด้านล่าง`}</Text>

          <View
            style={{
              marginBottom: 16,
              marginTop: 10,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Image source={images.imgSeedWordMenu} />
          </View>

          <Text style={styles.descText}>{`หากคุณได้เปิดหน้าต่างคำกู้ข้อมูล\nคุณจะเจอคำต่างๆตามตัวอย่างข้างล่าง`}</Text>

          <View style={{ marginTop: 16, justifyContent: 'center', alignItems: 'center' }}>
            <Image source={images.imgSeedWordsSample} />
          </View>

          <Text style={[styles.descText, { marginVertical: 16 }]}>{`จดบันทึกคำกู้ข้อมูล 12 คำที่ได้ตามลำดับ\nและจัดเก็บไว้ในที่ปลอดภัย`}</Text>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  descText: {
    color: '#5A5A5A',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    textAlign: 'center'
  }
})
