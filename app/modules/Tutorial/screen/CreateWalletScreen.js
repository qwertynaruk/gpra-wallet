import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity
} from 'react-native'
import NavStore from '../../../AppStores/NavStore'
import AppStyle from '../../../commons/AppStyle'
import images from '../../../commons/images'

export default class CreateWalletScreen extends Component {
  _goToTermsAndConditionsScreen = () => {
    NavStore.pushToScreen('TermsAndConditionsScreen')
  }

  _goToImportMnemonic = () => {
    NavStore.pushToScreen('ImportMnemonicScreen')
  }

  render() {
    return (
      <View style={[styles.container]}>
        <View style={{ flex: 65 }}>
          <View style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center'
          }}
          >
            <Image source={images.landingLogo} />
          </View>
        </View>
        <View style={{ flex: 30 }}>
          <TouchableOpacity
            onPress={this._goToTermsAndConditionsScreen}
            style={styles.containerCreateWalletBtn}
          >
            <Text style={styles.createWalletText}>
              สร้างกระเป๋าใหม่
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this._goToImportMnemonic}
          >
            <Text style={styles.importWalletText}>
              เคยมีกระเป๋าอยู่แล้ว
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 5 }} >
          <Text style={styles.poweredText}>
            powered by swaple.io
          </Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  containerCreateWalletBtn: {
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 20,
    marginRight: 20,
    borderRadius: 10,
    backgroundColor: '#535786'
  },
  createWalletText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 22,
    color: '#FFF'
  },
  importWalletText: {
    textAlign: 'center',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 22,
    color: '#535786',
    marginTop: 15
  },
  poweredText: {
    textAlign: 'center',
    fontSize: 14,
    color: '#000000',
    opacity: 0.33
  }
})
