import React, { Component } from 'react'
import {
  Keyboard,
  StyleSheet,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from 'react-native'

// Third-party
import { observer } from 'mobx-react/native'
import CircleCheckBox from 'react-native-circle-checkbox'

// Components & Modules
import { chainNames } from '../../../Utils/WalletAddresses'
import api from '../../../api'
import icons from '../../../commons/icons'
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import ImportMnemonicStore from '../store/ImportMnemonicStore'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'
import Loading from '../elements/Loading'

const marginTop = LayoutUtils.getExtraTop()
const title = 'นำเข้ากระเป๋าของคุณ'

@observer
export default class ImportMnemonicScreen extends Component {
  constructor(props) {
    super(props)
    MainStore.importMnemonicStore = new ImportMnemonicStore()
    this.importMnemonicStore = MainStore.importMnemonicStore

    this.state = {
      seedWords: '',
      keyboardShow: false,
      checked: false,
      loading: false
    }

    this._handleConfirm = this._handleConfirm.bind(this)
    this.goBack = this.goBack.bind(this)
    this.goImportTutorial = this.goImportTutorial.bind(this)
    this.onTouchDismissKeyboard = this.onTouchDismissKeyboard.bind(this)
    this._keyboardDidShow = this._keyboardDidShow.bind(this)
    this._keyboardDidHide = this._keyboardDidHide.bind(this)
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide)
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  onTouchDismissKeyboard() {
    Keyboard.dismiss()
  }

  _keyboardDidShow() {
    this.setState({ keyboardShow: true })
  }

  _keyboardDidHide() {
    this.setState({ keyboardShow: false })
  }

  goBack() {
    NavStore.goBack()
    Keyboard.dismiss()
  }

  goImportTutorial() {
    NavStore.pushToScreen('ImportMnemonicTutorialScreen')
  }

  isLoading(status) {
    this.setState({ loading: status })
  }

  _handleConfirm() {
    Keyboard.dismiss()
    this.isLoading(true)

    setTimeout(() => {
      this.importMnemonicStore.generateWallets(chainNames.AOM)
        .then((wallets) => {
          this.importMnemonicStore.setSelectedWallet(wallets.shift())
          this.importMnemonicStore.setTitle('OM Token')
          this.importMnemonicStore.unlockWallet(chainNames.AOM)
        })
        .catch((err) => {
          api.Aom.reportBug({
            address: 'NEW USER',
            error_component: 'ImportMnemonicScreen',
            error_function: 'generateWallets',
            error_detail: err.message
          })
        })
    })
  }

  render() {
    const { seedWords, checked, keyboardShow } = this.state
    const { errorMnemonic } = this.importMnemonicStore
    const disabled = errorMnemonic || !checked || seedWords === ''

    return (
      <TouchableWithoutFeedback
        style={styles.container}
        onPress={this.onTouchDismissKeyboard}
      >
        <View style={{ flex: 1 }}>
          <NavigationHeader
            style={{
              marginTop: marginTop + 20,
              marginBottom: 20
            }}
            headerItem={{
              title,
              icon: null,
              button: images.iconBackAlt
            }}
            titleStyle={{
              color: '#000',
              fontFamily: AppStyle.mainFontBoldTH,
              textAlign: 'center',
              flex: 1,
              marginRight: 20
            }}
            action={this.goBack}
          />
          <Text style={styles.descText}>{`กรุณากรอกคำกู้ข้อมูล (Seed Words) 12 คำ\nที่คุณได้จดบันทึกไว้ ตามลำดับ`}</Text>
          <View style={styles.textInputContainer}>
            <TextInput
              autoCapitalize="none"
              onChangeText={(value) => {
                this.setState({ seedWords: value })
                this.importMnemonicStore.onChangeMnemonic(value)
              }}
              placeholder="คำกู้ข้อมูล 12 คำ"
              placeholderTextColor="#CBCCD9"
              style={styles.mnemonicInput}
              multiline={true}
              numberOfLines={5}
              underlineColorAndroid="transparent"
              value={seedWords}
            />
            {
              errorMnemonic &&
              <Text style={styles.errorMnemonic}>รูปแบบคำกู้ข้อมูลไม่ถูกต้อง</Text>
            }
          </View>
          <View style={styles.acceptTermAndCond}>
            <CircleCheckBox
              checked={this.state.checked}
              onToggle={(value) => {
                Keyboard.dismiss()
                this.setState({
                  checked: value
                })
              }}
              outerColor="#535786"
              innerColor="#535786"
              outerSize={40}
              filterSize={35}
            />
            <View style={{ marginLeft: 14 }}>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.acceptNormalText}>ฉันยอมรับ</Text>
                <Text style={styles.acceptLinkText}>เงื่อนไขการใช้บริการ</Text>
                <Text style={styles.acceptNormalText}>และ</Text>
              </View>
              <View>
                <Text style={styles.acceptLinkText}>นโยบายความเป็นส่วนตัว</Text>
              </View>
            </View>
          </View>

          <View style={styles.buttonContainer}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={[styles.button, {
                opacity: disabled ? 0.6 : 1
              }]}
              disabled={disabled}
              onPress={this._handleConfirm}
            >
              <Text style={styles.buttonText}>ต่อไป</Text>
            </TouchableOpacity>
          </View>

          {
            !keyboardShow && (
              <View style={{
                flex: 1,
                flexDirection: 'row',
                alignItems: 'flex-end',
                paddingBottom: 30
              }}
              >
                <TouchableOpacity
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                  }}
                  onPress={this.goImportTutorial}
                >
                  <Image source={icons.iconInfo2} style={{ width: 17, height: 17 }} />
                  <Text style={styles.importTutorialText}>วิธีกู้ข้อมูลด้วย Seed Words</Text>
                </TouchableOpacity>
              </View>
            )
          }

          {
            this.state.loading &&
            <Loading title="กำลังนำเข้ากระเป๋า" />
          }
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  descText: {
    color: '#5A5A5A',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    textAlign: 'center'
  },
  acceptTermAndCond: {
    flexDirection: 'row',
    marginHorizontal: 25
  },
  acceptNormalText: {
    color: '#909090',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16
  },
  acceptLinkText: {
    color: '#909090',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16
  },
  errorMnemonic: {
    color: '#C30000',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    textAlign: 'center',
    marginTop: 5
  },
  textInputContainer: {
    marginVertical: 22,
    marginHorizontal: 25
  },
  mnemonicInput: {
    textAlignVertical: 'top',
    paddingRight: 16,
    paddingBottom: 10,
    paddingLeft: 16,
    paddingTop: 10,
    fontFamily: AppStyle.mainFontTH,
    fontSize: 18,
    color: '#5A5A5A',
    borderRadius: 13,
    borderWidth: 1,
    borderColor: '#CBCCD9'
  },
  buttonContainer: {
    flexDirection: 'row',
    marginTop: 22,
    marginHorizontal: 25
  },
  button: {
    backgroundColor: '#535786',
    borderRadius: 8,
    height: 60,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: 'white',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20
  },
  importTutorialText: {
    marginLeft: 6,
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    color: '#535786'
  }
})
