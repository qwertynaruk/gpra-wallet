import React, { Component } from 'react'
import {
  Keyboard,
  StyleSheet,
  Image,
  Text,
  Platform,
  BackHandler,
  StatusBar,
  ScrollView,
  View
} from 'react-native'

// Components & Modules
import images from '../../../commons/images'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import NavStore from '../../../AppStores/NavStore'
import NavigationHeader from '../../../components/elements/NavigationHeader'

const marginTop = LayoutUtils.getExtraTop()
const title = 'วิธีกู้ข้อมูล'

export default class ImportMnemonicTutorialScreen extends Component {
  constructor(props) {
    super(props)

    this.goBack = this.goBack.bind(this)
    this.handleBackPress = this.handleBackPress.bind(this)
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }

    Keyboard.dismiss()
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  goBack() {
    NavStore.goBack()
  }

  handleBackPress() {
    this.goBack()
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent
        />
        <NavigationHeader
          style={{
            marginTop: marginTop + 10,
            marginBottom: 20
          }}
          headerItem={{
            title,
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            color: '#000',
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={this.goBack}
        />
        <ScrollView style={styles.container}>
          <Text style={styles.descText}>{`หากคุณได้เปิดหน้าต่างคำกู้ข้อมูล\nคุณจะเจอคำต่างๆตามตัวอย่างข้างล่าง`}</Text>

          <View style={{ marginTop: 16, justifyContent: 'center', alignItems: 'center' }}>
            <Image source={images.imgSeedWordsSample} />
          </View>

          <Text style={[styles.descText, { marginTop: 16 }]}>{`ให้กรอกคำกู้ข้อมูล 12\nคำที่ได้ตามลำดับที่ได้จดไว้ลงในกล่องข้อความ\nหากคำกู้ข้อมูลถูกต้องคุณจะสามารถกดต่อไปได้`}</Text>

          <View style={{ marginTop: 18, justifyContent: 'center', alignItems: 'center' }}>
            <Image source={images.imgSeedWordsInputSample} />
          </View>

          <Text style={[styles.descText, { marginBottom: 20, marginTop: 16, color: '#C30000' }]}>{`** กรุณาเช็คคำกู้ข้อมูลให้ตรงกับลำดับ\nและเว้นวรรคให้ถูกต้อง มิฉะนั้นจะไม่สามารถกู้ข้อมูลได้`}</Text>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  descText: {
    color: '#5A5A5A',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16,
    textAlign: 'center'
  }
})
