import React, { Component } from 'react'
import {
  View,
  BackHandler,
  StyleSheet,
  Text,
  ScrollView,
  Platform,
  TouchableOpacity
} from 'react-native'

// Components & Modules
import NavStore from '../../../AppStores/NavStore'
import SecureDS from '../../../AppStores/DataSource/SecureDS'
import CreateWalletStore from '../store/CreateWalletStore'
import AppStyle from '../../../commons/AppStyle'
import LayoutUtils from '../../../commons/LayoutUtils'
import { chainNames } from '../../../Utils/WalletAddresses'
import Loading from '../elements/Loading'

const marginTop = LayoutUtils.getExtraTop()
const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 20
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom
}

export default class TermsAndConditionsScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      atBottom: false
    }
    this.createWalletStore = new CreateWalletStore()
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  handleBackPress = () => {
    NavStore.goBack()
    return true
  }

  _gotoCreateWallet = () => {
    NavStore.lockScreen({
      onUnlock: (pincode) => {
        this.setState({ loading: true })
        setTimeout(() => {
          const ds = new SecureDS(pincode)
          this.createWalletStore.setTitle('OM Token')
          this.createWalletStore.createWallet(chainNames.AOM, ds)
            .then(() => {
              this.setState({ loading: false })
            })
        }, 1000)
      },
      isLaunchApp: true
    }, true)
  }

  render() {
    const { atBottom } = this.state
    return (
      <View style={styles.container}>
        <View style={{
          alignItems: 'center',
          borderBottomWidth: 1,
          borderBottomColor: '#E7EAF0',
          justifyContent: 'flex-end',
          marginTop,
          paddingBottom: 10,
          paddingTop: 10
        }}
        >
          <Text style={styles.mainText}>TERMS AND CONDITIONS</Text>
        </View>
        <ScrollView
          style={styles.container}
          onScroll={({ nativeEvent }) => {
            if (!atBottom) {
              if (isCloseToBottom(nativeEvent)) {
                this.setState({ atBottom: true })
              }
            }
          }}
        >
          <View style={{ padding: 20 }}>
            <Text style={[styles.textHeader, styles.marginBottom]}>INTRODUCTION</Text>

            <Text style={styles.textNormal}>Welcome to Om Wallet which is owned and operated by Om Platform Co., Ltd. (collectively, “Om,” “We,” or “Us”), a company based in Thailand.</Text>
            <Text style={styles.textNormal}>This agreement shall be governed by, and construed in all respects in accordance with Thai Law.</Text>
            <Text style={styles.textNormal}>These terms and conditions apply to the Om Platform and its associated Om Wallet.</Text>
            <Text style={styles.textNormal}>By using either the Om platform or the Om Wallet, you expressly agree to be bound by all of the terms and conditions set hereforth.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>In accepting this agreement, you acknowledge that you have read this agreement, understand it, and have had an opportunity to seek independent legal counsel before agreeing to its terms.</Text>

            <Text style={styles.textNormal}>You also certify that you confirm the following clauses:</Text>
            <Text style={styles.textNormal}>1) You are at least 18 years old and have full mental capacity to contract and use the Om Platform and the Om Wallet;</Text>
            <Text style={styles.textNormal}>2) You are transacting with, and seeking storage for, legally-obtained cryptoassets that belong to you;</Text>
            <Text style={styles.textNormal}>3) You are not engaging in any unlawful activity through your relationship with Om or through your use of the Om Platform and the Om Wallet;</Text>
            <Text style={styles.textNormal}>4) You are comporting with and obeying any-and-all applicable laws of which you are currently aware;</Text>
            <Text style={styles.textNormal}>5) You are legally permitted to use the Om Platform and the Om Wallet in your jurisdiction;</Text>
            <Text style={styles.textNormal}>6) You will take sole responsibility for restrictions and risks associated with the Om Platform and the Om Wallet;</Text>
            <Text style={styles.textNormal}>7) You have a reasonable understanding of the usage and intricacies of blockchain-based crypto tokens.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>At any point, if you do not agree to any portion of the current terms, you should not proceed to use the Om Platform or the Om Wallet.</Text>

            <Text style={[styles.textHeader, styles.marginBottom, styles.marginTop]}>USER RISK</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>To participate in using the Om Platform or the Om Wallet, you must read and understand the following information regarding potential user risks.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Services Offered</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>The Om Platform and the Om Wallet are expressly for use with blockchain-based cryptocurrencies and tokens. Om users can purchase, sell, trade, and store cryptoassets using the Om Platform and the Om Wallet. Once a user uses the Om Platform or the Om Wallet to purchase, transfer, or store his/her crypto tokens, he/she confirms that he/she understands and accepts the risks noted here and any potential irreversible losses that may be associated with them.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>General Risk</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Purchasing, selling, and storing digital blockchain-based crypto tokens is always associated with some measure of risk and using the Om Platform or the Om Wallet represents the potential for such risk. As such, all users have the potential to incur risks of loss, theft, non-compliance, and/or misconception as well as other risks that may not be stated here. Further, Om Platform and Om Wallet users must understand that the Om Platform and the Om Wallet do not in any way intentionally or expressly represent a risk-free environment for the transacting or safekeeping of digital assets. It is recommended that users study and learn the basics of cryptoassets before participating in Om’s service offerings.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Risk Stipulations</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Any risks specified in these terms are by no means comprehensive. Also, these terms may not contain certain risks which cannot be predicted at the present time. If the user is not sure that he/she can estimate the level of risk by his/herself, it is advised here that the purchaser contact an appropriate expert or lawyer before attempting to interact with the Om Platform or the Om Wallet.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Personal Responsibility Risk</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Om and cryptocurrency generally, is grounded in the concept of people functioning as their own bank. As such, users who fail to do their own due-diligence before purchasing, transferring, storing or otherwise using cryptoassets are culpable for themselves as well as their own actions on the Om Platform or the Om Wallet.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Private Key Risk</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>It is incumbent to make plain to users that private keys and other private data are the responsibility of each individual and therefore they must do everything within their capacity to store such information safely and sensibly. This is because Om has no access to your personal key and other private information and therefore is unable to recover it. Thus, if personal key information is lost, then this loss may result in a permanent loss of access to the user’s own digital assets. It is strongly recommended that users carefully backup such information and safeguard it from loss, virus attack or theft.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Digital Assets Risk</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Om is not the creator of any of the digital assets or blockchains that the Om Platform or the Om Wallet allows users to access. As such, Om also retains absolutely no control whatsoever over these assets and/or blockchains either. Thus, users will take sole responsibility over their decisions related to the digital assets that they choose to use and interact with. This undoubtedly represents a risk as not every digital asset is created equal. That said, no language in the terms of this agreement or any other information generated by Om anywhere else or at any other time shall be construed as cryptoasset investment advice.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Transaction Risk</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Om has created software that allows users to send digital asset transfer instructions to their related blockchains. As such, it is imperative to understand that the only authentic record of such transactions transpiring at all are on the relevant and applicable blockchains associated with the given asset or assets involved in the transaction. Furthermore, most blockchains require some measure of a transaction fee for facilitating the transfer. Any such fee that is encountered by the user is the sole responsibility of that user. Users shall not have any claim whatsoever to recoup any digital assets sent via the Om Platform or the Om Wallet.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>No Investment Advice</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Om does not provide investment, tax, or legal advice, nor does Om broker trades on your behalf. All Om transactions are executed automatically, based on the parameters of your swap instructions, and you solely are responsible for determining whether any investment, investment strategy or related transaction is appropriate for you based on your personal investment objectives, financial circumstances and risk tolerance. You should consult your legal or tax professional regarding your specific financial situation.</Text>

            <Text style={[styles.textHeader, styles.marginBottom, styles.marginTop]}>LIABILITY AND LIMITATIONS</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>General Limitations</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Users of the Om Platform and Om Wallet acknowledge and agree that they will not hold Om (or any person or entity associated with Om) responsible for any damages, or loss whatsoever caused by or related to their use of the Om Platform or the Om Wallet.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>The user further agrees not to ever seek to hold any person/entity associated with Om liable and that the risk of buying/holding/using OM utility tokens rests solely with the user.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Internal Disruption of Operations</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Om does not guarantee the permanent, optimal and/or uninterruptible operation of the Om Platform or the Om Wallet. Further, Om does not take any responsibility for direct, indirect, accidental, special, or circumstantial damages for the mistakes and/or technical issues in Om’s operation of the Om platform and/or the Om Wallet.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>External Disruption of Operations</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>As it is impossible for Om to anticipate or control all potential circumstances that may negatively effect the quality, consistency or ongoing provision of its services, it is incumbent to state that any and all interruptions or systems failures caused by any event that is beyond the bounds and purview of Om including, but not limited to, those related to political, economical, infrastructural or natural causes, are not the responsibility of Om and thus Om cannot be held liable for them.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Suspension or Termination of Operations</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Om reserves the right to alter, suspend or discontinue the services it provides with the Om Platform and the Om Wallet as well as any access to these services at its own discretion and is exempt of any wrongdoing or loss by anyone for any reason as a result of such actions or decisions.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Operation Restrictions</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Om has no control over any restrictions which might or could be made on the services it provides with the Om Platform and the Om Wallet by an authority in any territory or jurisdiction. As such, Om cannot be held liable for any inconveniences, damages, losses or otherwise negative consequences associated with such jurisdictional restrictions.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Technical Support</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Free technical support is provided by Om for the Om Platform and the Om Wallet. However, Om is not, and cannot be held responsible for losses or otherwise negative consequences related to misunderstandings or misinformation provided by any IT support technician employed by Om.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Negligence and Misconduct</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Om is not, and cannot be held responsible for any negligence, willful misconduct, or general misunderstandings made by users of the Om Platform or the Om Wallet or by employees of Om.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>3rd Party Integration Links</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Om reserves the right to partner with, and provide links to 3rd party service providers and websites that it deems worthwhile and appropriate. However, because such organizations or websites are not under Om’s direct control, Om will not be held liable for any of the actions taken by such 3rd parties or for the content they present on the Om website or anywhere else.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Taxes</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Authorities requiring taxes payable based on a given user’s activities on the Om Platform or with the Om Wallet are, and will always remain, the lone responsibility of the that given user in question.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Modification of Terms</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>The right is reserved by Om to change the terms and conditions of this agreement at any time, for any reason and without prior notice. Upon the implementation of any such changes, all rights offered in this current agreement will be forever terminated in favor of the new terms provided. In such circumstances, the new terms and conditions will be presented accordingly online and via email to all registered users. It is the users’ responsibility to review these new terms and conditions when presented with them. Any disagreement to the new terms and conditions can be remedied solely by discontinuing their use of the Om Platform and the Om Wallet. Om will not be liable if such modifications affect your use of Om’s services. If a user uses the Om Platform or the Om Wallet after the new terms have been posted, it will be deemed that the user understood and agreed with the new adaptation.</Text>
            <Text style={[styles.textNormal, styles.marginBottom]}>Language</Text>
            <Text style={styles.textNormal}>If these terms and conditions are ever translated into another language, the English version will always take legal precedence and authority over any other translated version.</Text>
          </View>
        </ScrollView>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            disabled={!atBottom}
            onPress={this._gotoCreateWallet}
          >
            <Text style={[styles.agreeText, atBottom ? {} : styles.disabledText]}>ฉันยอมรับ</Text>
          </TouchableOpacity>
        </View>

        {
          this.state.loading &&
          <Loading title="กำลังสร้างกระเป๋า" />
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  mainText: {
    fontFamily: AppStyle.mainFontBold,
    fontSize: 24,
    color: '#121212'
  },
  disabledText: {
    color: '#DDD'
  },
  agreeText: {
    color: '#535786',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 22,
    textAlign: 'center'
  },
  buttonContainer: {
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#E7EAF0',
    justifyContent: 'center',
    height: 60
  },
  textHeader: {
    color: '#5A5A5A',
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 16
  },
  textNormal: {
    color: '#5A5A5A',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 16
  },
  marginBottom: {
    marginBottom: 15
  },
  marginTop: {
    marginTop: 15
  }
})
