import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  StatusBar,
  Text,
  Animated,
  BackHandler,
  Platform,
  TouchableOpacity
} from 'react-native'
import { observer } from 'mobx-react/native'
import MainStore from '../../../AppStores/MainStore'
import NavStore from '../../../AppStores/NavStore'
import AppStyle from '../../../commons/AppStyle'
import images from '../../../commons/images'
import constant from '../../../commons/constant'
import DisableView from '../elements/DisableView'

const { height } = Dimensions.get('window')
const isSmallScreen = height < 569
const dataNumber1 = [
  { number: '1' },
  { number: '2' },
  { number: '3' }
]
const dataNumber2 = [
  { number: '4' },
  { number: '5' },
  { number: '6' }
]
const dataNumber3 = [
  { number: '7' },
  { number: '8' },
  { number: '9' }
]
const dataNumber4 = [
  {
    actions: 'cancel'
  },
  { number: '0' },
  {
    icon: images.iconBackspace,
    actions: 'delete'
  }
]

@observer
class UnlockPincode extends Component {
  componentDidMount() {
    const changePincodeStore = MainStore.changePincode
    changePincodeStore.setup()
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress)
    }
  }

  get shouldShowDisableView() {
    const changePincodeStore = MainStore.changePincode
    const { wrongPincodeCount, timeRemaining } = changePincodeStore
    return wrongPincodeCount > 5 && timeRemaining > 0
  }

  handleBackPress = () => {
    return true
  }

  renderDots(numberOfDots) {
    const dots = []
    const { pinTyped } = MainStore.changePincode

    const styleDot = {
      width: 24,
      height: 24,
      borderRadius: 12,
      borderWidth: 3,
      borderColor: AppStyle.mainTextColor,
      marginHorizontal: 8
    }
    for (let i = 0; i < numberOfDots; i++) {
      const backgroundColor = i < pinTyped ? { backgroundColor: AppStyle.mainTextColor } : {}
      const dot = <View style={[styleDot, backgroundColor]} key={i} />
      dots.push(dot)
    }
    return dots
  }

  renderNumber(arrayNumber) {
    const changePincodeStore = MainStore.changePincode
    const nums = arrayNumber.map((num, i) => {
      if (num.number) {
        return (
          <TouchableOpacity
            onPress={() => {
              changePincodeStore.handlePress(num.number)
            }}
            key={num.number}
          >
            <View style={styles.numberField}>
              <Text style={styles.numberText}>{num.number}</Text>
            </View>
          </TouchableOpacity>
        )
      }

      return (
        <TouchableOpacity
          key={num.actions}
          onPress={() => {
            if (num.actions === 'delete') {
              changePincodeStore.handleBackSpace()
            }
            if (num.actions === 'cancel') {
              NavStore.goBack()
            }
          }}
        >
          <View style={styles.numberField}>
            {num.actions !== 'cancel' &&
              <Image
                source={num.icon}
              />
            }
            {num.actions === 'cancel' &&
              <Text style={styles.cancelText}>{constant.cancel}</Text>
            }
          </View>
        </TouchableOpacity>
      )
    })

    return (
      <View style={styles.arrayNumber}>
        {nums}
      </View>
    )
  }

  render() {
    const changePincodeStore = MainStore.changePincode
    const unlockDescription = changePincodeStore.title
    const { warningPincodeFail } = changePincodeStore
    const animationShake = changePincodeStore.animatedValue.interpolate({
      inputRange: [0, 0.3, 0.7, 1],
      outputRange: [0, -20, 20, 0],
      useNativeDriver: true
    })

    if (this.shouldShowDisableView) {
      return <DisableView />
    }

    return (
      <View style={styles.container}>
        <StatusBar
          hidden
        />
        <Image
          source={images.iconUnlock}
        />
        <Text style={styles.desText}>{unlockDescription}</Text>
        {warningPincodeFail &&
          <Text style={styles.warningField}>{warningPincodeFail}</Text>
        }
        <Animated.View
          style={[styles.pinField, {
            transform: [
              {
                translateX: animationShake
              }
            ]
          }]}
        >
          {this.renderDots(6)}
        </Animated.View>
        <View style={{ marginTop: isSmallScreen ? 10 : height * 0.03 }}>
          {this.renderNumber(dataNumber1)}
          {this.renderNumber(dataNumber2)}
          {this.renderNumber(dataNumber3)}
          {this.renderNumber(dataNumber4)}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center'
  },
  desText: {
    color: AppStyle.textColorBlack,
    fontSize: isSmallScreen ? 14 : 22,
    fontFamily: AppStyle.mainFontBoldTH,
    marginTop: isSmallScreen ? 10 : height * 0.03,
    marginBottom: isSmallScreen ? 5 : height * 0.01
  },
  pinField: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: isSmallScreen ? 13 : height * 0.05
  },
  arrayNumber: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: height * 0.02
  },
  numberField: {
    width: isSmallScreen ? 60 : 75,
    height: isSmallScreen ? 60 : 75,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 18
  },
  numberText: {
    fontFamily: AppStyle.mainFontBold,
    fontSize: 34,
    color: AppStyle.mainTextColor
  },
  cancelText: {
    fontFamily: AppStyle.mainFontBoldTH,
    fontSize: 24,
    color: AppStyle.mainTextColor
  },
  warningField: {
    color: AppStyle.errorColor,
    fontFamily: AppStyle.mainFontSemiBoldTH,
    fontSize: 16
  }
})

export default UnlockPincode
