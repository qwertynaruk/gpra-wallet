import { observable, computed, action } from 'mobx'
import { AsyncStorage, Animated } from 'react-native'
import * as Keychain from 'react-native-keychain'
import HapticHandler from '../../../Handler/HapticHandler'
import SecureDS from '../../../AppStores/DataSource/SecureDS'
import NavStore from '../../../AppStores/NavStore'
import UnlockDS from '../../Unlock/UnlockDS'
import AppDS from '../../../AppStores/DataSource/AppDS'
import { decryptString } from '../../../Utils/DataCrypto'
import MainStore from '../../../AppStores/MainStore'
import constant from '../../../commons/constant'

const IVKey = `IVKey`
const minute = 60000
export default class ChangePincodeStore {
  oldPincode = ''
  newPincode = ''
  @observable pinCode = ''
  @observable confirmPincode = ''
  @observable type = 0
  @observable canPress = true

  @observable wrongPincodeCount = 0
  @observable timeRemaining = 0

  @observable isSamePin = false
  @observable isWrongPin = false

  animatedValue = new Animated.Value(0)
  isShake = false

  @action upWrongPincodeCount = () => { this.wrongPincodeCount++ }
  @action setTimeRemaining = () => {
    if (this.wrongPincodeCount === 6) {
      this.timeRemaining = minute
    } else if (this.wrongPincodeCount === 7) {
      this.timeRemaining = minute * 5
    } else if (this.wrongPincodeCount === 8) {
      this.timeRemaining = minute * 15
    } else if (this.wrongPincodeCount === 9) {
      this.timeRemaining = minute * 60
    } else if (this.wrongPincodeCount === 10) {
      this.timeRemaining = minute * 120
    }
  }

  @action setup() {
    UnlockDS.getDisableData().then((res) => {
      if (res) {
        this.wrongPincodeCount = res.wrongPincodeCount
        this.timeRemaining = res.timeRemaining
        this.countdown()
      }
    })
  }

  @action countdown() {
    this.timer = setInterval(() => {
      if (this.timeRemaining > 0) {
        this.timeRemaining -= 1000
        this.saveDisableData()
      } else {
        this.timer && clearInterval(this.timer)
      }
    }, 1000)
  }

  @action enraseData() {
    Keychain.resetGenericPassword()
    AppDS.enraseData()
      .then(async (res) => {
        this.resetDisable()
        MainStore.appState.resetAppState()
        await AppDS.readAppData()
        this.setup()
        NavStore.pushToScreen('EnraseNotifScreen')
      })
      .catch(e => console.error(e))
  }

  @action handlePress(number) {
    if (!this.canPress) return false
    HapticHandler.ImpactLight()
    if (this.pinTyped === 5) {
      this.canPress = false
      this.pinCode = `${this.pinCode}${number}`
      this.onFullFill()
      return true
    }
    if (this.pinTyped < 5) {
      this.pinCode = `${this.pinCode}${number}`
      return true
    }
    return false
  }

  @action handleBackSpace() {
    if (!this.canPress) return
    HapticHandler.ImpactLight()
    if (this.pinCode.length > 0) {
      this.pinCode = this.pinCode.slice(0, -1)
    }
  }

  @action resetDisable() {
    if (this.wrongPincodeCount === 0) {
      return
    }
    this.wrongPincodeCount = 0
    this.setTimeRemaining()
    this.saveDisableData()
  }

  @action setData(data) {
    this.data = {
      ...this.data,
      ...data
    }
  }

  @action saveDisableData() {
    UnlockDS.saveDisableData({
      wrongPincodeCount: this.wrongPincodeCount,
      timeRemaining: this.timeRemaining
    })
  }

  @computed get pinTyped() {
    const pinTyped = this.pinCode.length
    return pinTyped
  }

  @computed get countdownMsg() {
    const { timeRemaining } = this
    const minuteNum = parseInt(timeRemaining / minute, 10)
    const secondNum = (timeRemaining - minuteNum * minute) / 1000
    const minuteStr = minuteNum < 10 ? `0${minuteNum}` : `${minuteNum}`
    const secondStr = secondNum < 10 ? `0${secondNum}` : `${secondNum}`
    return `Try again in ${minuteStr}:${secondStr}`
  }

  _handleErrorPin(e) {
    const { animatedValue, isShake } = this
    HapticHandler.NotificationError()
    this.upWrongPincodeCount()
    this.setTimeRemaining()
    this.saveDisableData()
    if (this.wrongPincodeCount === 10) {
      this.wrongPincodeCount = 5
    } else if (this.wrongPincodeCount > 5) {
      this.countdown()
    }

    this.isWrongPin = true

    Animated.spring(
      animatedValue,
      {
        toValue: isShake ? 0 : 1,
        duration: 250,
        tension: 80,
        friction: 4
      }
    ).start()
    setTimeout(() => {
      this.isShake = !isShake
      this.canPress = true
      this.clearPincode()
    }, 500)
  }

  _handleSamePin(e) {
    const { animatedValue, isShake } = this

    this.isSamePin = true

    Animated.spring(
      animatedValue,
      {
        toValue: isShake ? 0 : 1,
        duration: 250,
        tension: 80,
        friction: 4
      }
    ).start()
    setTimeout(() => {
      this.isShake = !isShake
      this.canPress = true
      this.clearPincode()
    }, 500)
  }

  @action clearPincode() {
    this.pinCode = ''
  }

  @action async _handleCheckPincode() {
    const { pinCode } = this
    const secureDS = await SecureDS.getInstance(pinCode)
    if (!secureDS) {
      this._handleErrorPin()
    } else {
      HapticHandler.NotificationSuccess()
      this.oldPincode = this.pinCode
      this.type = 1
      this.clearPincode()
      this.isWrongPin = false
    }
    this.canPress = true
  }

  @action _handleCreatePin() {
    if (this.oldPincode === this.pinCode) {
      this._handleSamePin()
    } else {
      this.confirmPincode = this.pinCode
      this.type = 2
      this.clearPincode()
      this.canPress = true
      this.isSamePin = false
    }
  }

  @action _handleConfirmPin() {
    if (this.confirmPincode === this.pinCode) {
      this.newPincode = this.pinCode
      NavStore.showLoading()
      this._encriptDataWithNewPincode()
    } else {
      this._handleErrorPin()
    }
  }

  onFullFill() {
    switch (this.type) {
      case 0:
        this._handleCheckPincode()
        break

      case 1:
        this._handleCreatePin()
        break

      case 2:
        this._handleConfirmPin()
        break

      default:
        break
    }
  }

  @computed get title() {
    switch (this.type) {
      case 0:
        return constant.ENTER_YOUR_OLD_PIN

      case 1:
        return constant.CREATE_YOUR_PIN

      case 2:
        return constant.CONFIRM_YOUR_PIN

      default:
        return ''
    }
  }

  @computed get warningPincodeFail() {
    if (this.isWrongPin && this.wrongPincodeCount > 0 && this.wrongPincodeCount <= 5) {
      return `มีโอกาสลองอีก ${6 - this.wrongPincodeCount} ครั้ง`
    } else if (this.isSamePin) {
      return `รหัสผ่านใหม่ไม่ควรซ้ำกับรหัสผ่านล่าสุด`
    }
    return null
  }

  getPasswordFromKeychain = async () => {
    const credentials = await Keychain.getGenericPassword()
    const randomKey = credentials.password
    return randomKey
  }

  decryptData(dataEncrypted, pinCode, iv) {
    const dataDecrypted = decryptString(dataEncrypted, pinCode, iv, 'aes-256-cbc')
    return dataDecrypted
  }

  async getIV() {
    return this.getItem(IVKey)
  }

  async _encriptDataWithNewPincode() {
    try {
      const iv = await this.getIV()
      const encryptPassword = await this.getPasswordFromKeychain()
      const password = this.decryptData(encryptPassword, this.oldPincode, iv)
      if (password && password.length === 16) {
        SecureDS.forceSavePassword(password, iv, this.newPincode)
        HapticHandler.NotificationSuccess()
        NavStore.goBack()
        NavStore.hideLoading()
      } else {
        alert('Wrong pincode')
        NavStore.hideLoading()
      }
    } catch (error) {
      NavStore.hideLoading()
    }
  }

  saveItem = async (key, value) => {
    await AsyncStorage.setItem(key, value)
  }

  getItem = async (key) => {
    const dataLocal = await AsyncStorage.getItem(key)
    return dataLocal
  }
}
