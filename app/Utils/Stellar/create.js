import StellarSdk from '@pigzbe/react-native-stellar-sdk'
import { BigNumber } from 'bignumber.js'
import Server from './server'

function checkNativeBalance(balances) {
  balances.map((balance) => {
    if (balance.asset_type === 'native') {
      if (BigNumber('1e-5').gt(balance.balance)) {
        throw new Error(`You AOM don't have enought native coin to execute transaction`)
      }
    }
  })
}

function getAssetBalance(balances) {
  let _balance = 0
  balances.map((balance) => {
    if (balance.asset_type === 'native') {
      _balance = balance.balance
    }
  })
  return _balance
}

function Create(server, privateKey, destination, amount, fee, passphrase) {
  const sourceKeys = StellarSdk.Keypair.fromSecret(privateKey)
  const destinationId = destination
  // Transaction will hold a built transaction we can resubmit if the result is unknown.
  let transaction = null
  const pdata = {
    xdr: '',
    address: sourceKeys.publicKey(),
    amt: amount,
    acb: 0,
    dcb: 0
  }
  // Create my asset
  const native = StellarSdk.Asset.native()

  console.log(`private key ${privateKey}`)

  // loadAccount
  return Server().accounts().accountId(destinationId).call()
    // If the account is not found, surface a nicer error message for logging.
    .catch(StellarSdk.NotFoundError, (error) => {
      console.log('SENDAOM StellarSdk.NotFoundError')
      return Promise.reject(error)
    })

    // If there was no error, load up-to-date information on your account.
    .then((res) => {
      pdata.dcb = getAssetBalance(res.balances)
      // getAssetBalance
      return server.loadAccount(sourceKeys.publicKey())
    })

    .then((sourceAccount) => {
      // Set option
      const options = { fee }
      if (passphrase) {
        Object.assign(options, { networkPassphrase: passphrase })
      }

      // Check native balance
      checkNativeBalance(sourceAccount.balances)
      pdata.acb = getAssetBalance(sourceAccount.balances)
      transaction = new StellarSdk.TransactionBuilder(sourceAccount, options)
        .addOperation(StellarSdk.Operation.createAccount({
          destination: destinationId,
          startingBalance: `${amount}`
        }))
        // A memo allows you to add your own metadata to a transaction. It's
        // optional and does not affect how Stellar treats the transaction.
        .addMemo(StellarSdk.Memo.text('Transfer Using OM Wallet'))
        .setTimeout(30000)
        .build()
      // Sign the transaction to prove you are actually the person sending it.
      console.log('SENDAOM transaction sign')
      transaction.sign(sourceKeys)

      // And finally, send it off to Stellar!
      pdata.xdr = transaction.toEnvelope().toXDR().toString('base64')
      // console.log(pdata.xdr)
      // return server.submitTransaction(transaction)
      return pdata
    })
    .catch((error) => {
      console.log(error)
      throw new Error(error)
    })
}

export default Create
