import StellarSdk from '@pigzbe/react-native-stellar-sdk'
import axios from 'axios'
import Server from './server'
import URL from '../../api/url'

function MyOffers(address, limit, cursor) {
  const endpoint = `${URL.OmChain.apiURL()}/accounts/${address}/offers`
  const params = []

  if (limit) {
    params.push(`limit=${limit}`)
  }

  if (cursor) {
    params.push(`cursor=${cursor}`)
  }

  // Add order
  params.push('order=desc')

  // Concat
  const url = `${endpoint}?${params.join('&')}`

  return axios.get(url).then(resp => resp)
}

function MakeOffer(server, privateKey, offer, fee, passphrase) {
  // Get source key
  const sourceKeys = StellarSdk.Keypair.fromSecret(privateKey)

  // Extract offer data
  const {
    offerId, selling, buying, amount, price
  } = offer

  // Selling asset
  const sellAsset = selling.asset_code ? new StellarSdk.Asset(selling.asset_code, selling.asset_issuer) : StellarSdk.Asset.native()

  // Buying asset
  const buyAsset = buying.asset_code ? new StellarSdk.Asset(buying.asset_code, buying.asset_issuer) : StellarSdk.Asset.native()

  // Set default pdata
  const pdata = {
    xdr: '',
    address: sourceKeys.publicKey()
  }

  // Load account
  return server.loadAccount(sourceKeys.publicKey())
    .then((sourceAccount) => {
      // Set option
      const options = { fee }
      if (passphrase) {
        Object.assign(options, { networkPassphrase: passphrase })
      }

      // Create transaction
      const transaction = new StellarSdk.TransactionBuilder(sourceAccount, options)
        .addOperation(StellarSdk.Operation.manageOffer({
          selling: sellAsset,
          buying: buyAsset,
          amount: amount.toString(),
          price: price.toString(),
          offerId: (offerId || '0').toString()
        }))
        .setTimeout(StellarSdk.TimeoutInfinite)
        .addMemo(StellarSdk.Memo.text('Manage offer via OM Wallet'))
        .build()

      // Sign transaction
      transaction.sign(sourceKeys)

      // Set xdr data
      pdata.xdr = transaction.toEnvelope().toXDR().toString('base64')
      return pdata
    })
    .catch(error => Promise.reject(error.response))
}

export default {
  MyOffers,
  MakeOffer
}
