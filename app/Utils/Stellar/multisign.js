import StellarSdk from '@pigzbe/react-native-stellar-sdk'

function MultiSignature(server, secret, signer, fee, passphrase) {
  const secretInvestor = secret

  // Keys for accounts to issue and receive the new asset
  const accountKeys = StellarSdk.Keypair.fromSecret(secretInvestor)

  // Set pdata
  const pdata = {
    xdr: '',
    address: accountKeys.publicKey(),
    ast: 'OM'
  }

  // First, the receiving account must trust the asset
  return server.loadAccount(accountKeys.publicKey())
    // Multisign
    .then((account) => {
      // Set option
      const options = { fee }
      if (passphrase) {
        Object.assign(options, { networkPassphrase: passphrase })
      }

      // Create transaction
      const transaction = new StellarSdk.TransactionBuilder(account, options)
        .addOperation(StellarSdk.Operation.setOptions({
          sourceAccount: accountKeys.publicKey(),
          masterWeight: 1,
          lowThreshold: 2,
          medThreshold: 2,
          highThreshold: 2,
          signer: {
            ed25519PublicKey: signer,
            weight: 1
          }
        }))
        .setTimeout(30000)
        .build()
      transaction.sign(accountKeys)
      pdata.xdr = transaction.toEnvelope().toXDR().toString('base64')
      return pdata
    })

    .catch((error) => {
      throw new Error(error.message)
    })
}

export default MultiSignature
