import StellarSdk from '@pigzbe/react-native-stellar-sdk'
import URL from '../../api/url'

function Server() {
  const url = URL.OmChain.apiURL()
  const server = new StellarSdk.Server(url)
  return server
}

export default Server
