import StellarSdk from '@pigzbe/react-native-stellar-sdk'
import { BigNumber } from 'bignumber.js'

function checkNativeBalance(balances) {
  balances.map((balance) => {
    if (balance.asset_type === 'native') {
      if (BigNumber('1e-5').gt(balance.balance)) {
        throw new Error(`You AOM don't have enought native coin to execute transaction`)
      }
    }
  })
}

function getAssetBalance(balances) {
  let _balance = 0
  balances.map((balance) => {
    if (balance.asset_type === 'native') {
      _balance = balance.balance
    }
  })
  return _balance
}

function Payment(server, privateKey, destination, amount, fee, asset, issuer, submit = false, passphrase) {
  const isNative = !(asset && issuer)
  const sourceKeys = StellarSdk.Keypair.fromSecret(privateKey)
  const destinationId = destination
  // Transaction will hold a built transaction we can resubmit if the result is unknown.
  let transaction = null
  const pdata = {
    xdr: '',
    address: sourceKeys.publicKey(),
    amt: amount,
    acb: 0,
    dcb: 0,
    ast: isNative ? 'OM' : asset
  }
  // Create my asset
  const assetType = isNative ? StellarSdk.Asset.native() : new StellarSdk.Asset(asset, issuer)

  // loadAccount
  return server.accounts().accountId(destinationId).call()
    // If there was no error, load up-to-date information on your account.
    .then((res) => {
      pdata.dcb = getAssetBalance(res.balances)
      // getAssetBalance
      return server.loadAccount(sourceKeys.publicKey())
    })

    .then((sourceAccount) => {
      // Set option
      const options = { fee }
      if (passphrase) {
        Object.assign(options, { networkPassphrase: passphrase })
      }

      // Check native balance
      checkNativeBalance(sourceAccount.balances)
      pdata.acb = getAssetBalance(sourceAccount.balances)
      transaction = new StellarSdk.TransactionBuilder(sourceAccount, options)
        .addOperation(StellarSdk.Operation.payment({
          destination: destinationId,
          // Because Stellar allows transaction in many currencies, you must
          // specify the asset type. The special "native" asset represents Lumens.
          asset: assetType,
          amount: `${amount}`
        }))
        .setTimeout(30000)
        // A memo allows you to add your own metadata to a transaction. It's
        // optional and does not affect how Stellar treats the transaction.
        // error max byte
        .addMemo(StellarSdk.Memo.text('Transfer Using OM Wallet'))
        .build()
      // Sign the transaction to prove you are actually the person sending it.
      console.log('SENDAOM transaction sign')
      transaction.sign(sourceKeys)

      // Self submit
      if (submit) {
        return server.submitTransaction(transaction)
      }

      // And finally, send it off to Stellar!
      pdata.xdr = transaction.toEnvelope().toXDR().toString('base64')
      // console.log(pdata.xdr)
      return pdata
    })
    .catch(error => Promise.reject(error.response))
}

export default Payment
