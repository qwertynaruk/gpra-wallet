import Server from './server'

function Balance(address) {
  return Server().loadAccount(address)
}

export default Balance
