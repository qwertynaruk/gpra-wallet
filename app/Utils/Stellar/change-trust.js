import StellarSdk from '@pigzbe/react-native-stellar-sdk'

function ChangeTrust(server, secret, asset, issuer, fee, passphrase) {
  // Keys for accounts to issue and receive the new asset
  const accountKeys = StellarSdk.Keypair.fromSecret(secret)

  // Create an object to represent the new asset
  const newToken = new StellarSdk.Asset(asset, issuer)

  // Set output data
  const pdata = {
    xdr: '',
    address: accountKeys.publicKey(),
    ast: 'OM'
  }

  // First, the receiving account must trust the asset
  return server.loadAccount(accountKeys.publicKey())
    // The `changeTrust` operation creates (or alters) a trustline
    .then((account) => {
      // Set option
      const options = { fee }
      if (passphrase) {
        Object.assign(options, { networkPassphrase: passphrase })
      }

      // Create transaction
      const transaction = new StellarSdk.TransactionBuilder(account, options)
        .addOperation(StellarSdk.Operation.changeTrust({
          asset: newToken
        }))
        .setTimeout(30000)
        .build()
      transaction.sign(accountKeys)

      // And finally, send it off to Stellar!
      pdata.xdr = transaction.toEnvelope().toXDR().toString('base64')
      return pdata
    })
    .catch((error) => {
      throw new Error(error.message)
    })
}

export default ChangeTrust
