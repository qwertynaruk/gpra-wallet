import StellarSdk from '@pigzbe/react-native-stellar-sdk'

function Networks() {
  return StellarSdk.Networks
}

export default Networks
