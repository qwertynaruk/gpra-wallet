import Stellar from '@pigzbe/react-native-stellar-sdk'
import bip39 from 'react-native-bip39'
import ED25519 from '@hawkingnetwork/ed25519-hd-key-rn'

function MnemonicToPrivKey(mnemonic, path, from, to) {
  const derivePath = path.split('/').slice(0, 3).join('/')
  if (from >= 0 && to > 0) {
    const keys = []
    for (let i = from; i < to + 1; i++) {
      const seedHex = bip39.mnemonicToSeedHex(mnemonic)
      const keyPath = ED25519.derivePath(`${derivePath}/${i}'`, seedHex)
      const keyPair = Stellar.Keypair.fromRawEd25519Seed(keyPath.key)
      const private_key = keyPair.secret()

      keys.push(private_key)
    }
    return Promise.resolve(keys)
  }

  const seedHex = bip39.mnemonicToSeedHex(mnemonic)
  const keyPath = ED25519.derivePath(`${derivePath}/${from < 0 ? 0 : from}'`, seedHex)
  const keyPair = Stellar.Keypair.fromRawEd25519Seed(keyPath.key)
  const private_key = keyPair.secret()

  return Promise.resolve(private_key)
}

export default MnemonicToPrivKey
