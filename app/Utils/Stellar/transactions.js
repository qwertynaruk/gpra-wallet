import axios from 'axios'
import URL from '../../api/url'

function Transactions(address, limit, cursor) {
  const endpoint = `${URL.OmChain.apiURL()}/accounts/${address}/payments`
  const params = []

  if (limit) {
    params.push(`limit=${limit}`)
  }

  if (cursor) {
    params.push(`cursor=${cursor}`)
  }

  // Add order
  params.push('order=desc')

  // Concat
  const url = `${endpoint}?${params.join('&')}`

  return axios.get(url).then(resp => resp.data)
}

export default Transactions
