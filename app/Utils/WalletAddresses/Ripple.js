import * as keypairs from 'ripple-keypairs'

export default class RippleAddress {
  private_key = null;

  constructor(private_key) {
    if (!private_key) throw new Error('Private key is required')
    this.private_key = private_key
  }

  get address() {
    console.log('XRP', 'RippleAddress', this.private_key)
    return keypairs.deriveAddress(this.private_key)
  }
}
