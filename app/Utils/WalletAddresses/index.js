import EthereumAddress from './Ethereum'
import BitcoinAddress from './Bitcoin'
import LitecoinAddress from './Litecoin'
import DogecoinAddress from './Dogecoin'
import AomtokenAddress from './Aomtoken'
import StellarAddress from './Stellar'
import RippleAddress from './Ripple'

export const chainNames = {
  ETH: 'Ethereum',
  BTC: 'Bitcoin',
  LTC: 'Litecoin',
  DOGE: 'Dogecoin',
  XRP: 'Ripple',
  XLM: 'Stellar',
  AOM: 'AOM Token'
}

export default (privateKey, chainName = 'Ethereum', network = 'mainnet') => {
  switch (chainName) {
    case chainNames.ETH:
      return new EthereumAddress(privateKey)
    case chainNames.BTC:
      return new BitcoinAddress(privateKey, network)
    case chainNames.LTC:
      return new LitecoinAddress(privateKey)
    case chainNames.DOGE:
      return new DogecoinAddress(privateKey)
    case chainNames.XRP:
      return new RippleAddress(privateKey)
    case chainNames.XLM:
      return new StellarAddress(privateKey)
    case chainNames.AOM:
      return new AomtokenAddress(privateKey)
    default: return new EthereumAddress(privateKey)
  }
}
