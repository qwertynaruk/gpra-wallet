import Stellar from '@pigzbe/react-native-stellar-sdk'

export default class AomtokenAddress {
  private_key = null;

  constructor(private_key) {
    if (!private_key) throw new Error('Private key is required')
    this.private_key = private_key
  }

  get address() {
    const keyPair = Stellar.Keypair.fromSecret(this.private_key)
    const address = keyPair.publicKey()

    return address
  }
}
