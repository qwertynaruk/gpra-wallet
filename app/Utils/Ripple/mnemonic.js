import bip39 from 'react-native-bip39'
import keypairs from 'ripple-keypairs'
import { fromSeed } from 'bip32'

function MnemonicToPubKey(mnemonic, path, from, to) {
  const derivePath = path.split('/').slice(0, 5).join('/')
  if (from >= 0 && to > 0) {
    const keys = []
    for (let i = from; i < to + 1; i++) {
      const seed = bip39.mnemonicToSeed(mnemonic)
      const master = fromSeed(seed)
      const derive = master.derivePath(`${derivePath}/${i}`)
      const hexPublicKey = derive.publicKey.toString('hex').toUpperCase()
      const address = keypairs.deriveAddress(hexPublicKey)

      keys.push(address)
    }
    return Promise.resolve(keys)
  }

  const seed = bip39.mnemonicToSeed(mnemonic)
  const master = fromSeed(seed)
  const derive = master.derivePath(`${derivePath}/${from < 0 ? 0 : from}`)
  const hexPublicKey = derive.publicKey.toString('hex').toUpperCase()
  const address = keypairs.deriveAddress(hexPublicKey)

  return Promise.resolve(address)
}

export default MnemonicToPubKey
