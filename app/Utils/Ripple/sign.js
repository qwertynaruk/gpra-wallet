import bip39 from 'react-native-bip39'
import signKeypairs from 'ripple-sign-keypairs'
import { fromSeed } from 'bip32'

function Sign(txJSON, mnemonic) {
  const path = `m/44'/144'/0'/0/0`
  const seed = bip39.mnemonicToSeed(mnemonic)
  const master = fromSeed(seed)
  const derive = master.derivePath(path)
  const keypairs = KeyPairs(derive)
  const txSign = signKeypairs(txJSON, keypairs)

  return Promise.resolve(txSign)
}

function KeyPairs(derive) {
  const hexPublicKey = derive.publicKey.toString('hex').toUpperCase()
  const hexPrivateKey = derive.privateKey.toString('hex').toUpperCase()
  const prefixPrivateKey = '00'

  return {
    publicKey: hexPublicKey,
    privateKey: prefixPrivateKey.concat(hexPrivateKey)
  }
}

export default Sign
