import { createStackNavigator } from 'react-navigation'
import MnemonicScreen from '../modules/WalletBackup/screen/MnemonicScreen'
import AppStyle from '../commons/AppStyle'

const BackupStack = createStackNavigator(
  {
    MnemonicScreen: {
      screen: MnemonicScreen,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: 'MnemonicScreen',
    cardStyle: { backgroundColor: AppStyle.backgroundColor }
  }
)

export default BackupStack
