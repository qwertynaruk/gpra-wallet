import { createStackNavigator } from 'react-navigation'
import TutorialScreen from '../modules/Tutorial/screen/TutorialScreen'
import CreateWalletScreen from '../modules/Tutorial/screen/CreateWalletScreen'
import TermsAndConditionsScreen from '../modules/Tutorial/screen/TermsAndConditionsScreen'
import ImportMnemonicScreen from '../modules/Tutorial/screen/ImportMnemonicScreen'
import ImportMnemonicTutorialScreen from '../modules/Tutorial/screen/ImportMnemonicTutorialScreen'
import BackupMnemonicTutorialScreen from '../modules/Tutorial/screen/BackupMnemonicTutorialScreen'
import AppStyle from '../commons/AppStyle'

const TutorialStack = createStackNavigator(
  {
    TutorialScreen: {
      screen: TutorialScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    CreateWalletScreen: {
      screen: CreateWalletScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    ImportMnemonicScreen: {
      screen: ImportMnemonicScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    ImportMnemonicTutorialScreen: {
      screen: ImportMnemonicTutorialScreen,
      navigationOptions: {
        header: null
      }
    },
    BackupMnemonicTutorialScreen: {
      screen: BackupMnemonicTutorialScreen,
      navigationOptions: {
        header: null
      }
    },
    TermsAndConditionsScreen: {
      screen: TermsAndConditionsScreen,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: 'TutorialScreen',
    cardStyle: { backgroundColor: AppStyle.backgroundColor }
  }
)

export default TutorialStack
