import { createStackNavigator } from 'react-navigation'
import AddressInputScreen from '../modules/SendAom/screen/AddressInputScreen'
import ScanQRCodeScreen from '../modules/ScanQRCode'
import ConfirmScreen from '../modules/SendAom/screen/ConfirmScreen'
import Favorite from '../modules/SendAom/screen/Favorite'
import TransactionPassScreen from '../modules/SendAom/screen/TransactionPassScreen'
import AppStyle from '../commons/AppStyle'

const SendAomStack = createStackNavigator(
  {
    AddressInputScreen: {
      screen: AddressInputScreen,
      navigationOptions: {
        header: null
      }
    },
    Favorite: {
      screen: Favorite,
      navigationOptions: {
        header: null
      }
    },
    ScanQRCodeScreen: {
      screen: ScanQRCodeScreen,
      navigationOptions: {
        header: null
      }
    },
    ConfirmScreen: {
      screen: ConfirmScreen,
      navigationOptions: {
        header: null
      }
    },
    TransactionPassScreen: {
      screen: TransactionPassScreen,
      navigationOptions: {
        gesturesEnabled: false,
        header: null
      }
    }
  },
  {
    initialRouteName: 'AddressInputScreen',
    cardStyle: { backgroundColor: AppStyle.backgroundColor }
  }
)

export default SendAomStack
