import { createStackNavigator } from 'react-navigation'
import KYCScreen from '../modules/KYC/screen/KYCScreen'
import KYCLayerTwoPageOneScreen from '../modules/KYC/screen/KYCLayerTwoPageOneScreen'
import KYCLayerTwoPageTwoScreen from '../modules/KYC/screen/KYCLayerTwoPageTwoScreen'
import KYCLayerTwoPageThreeFetchaAScreen from '../modules/KYC/screen/KYCLayerTwoPageThreeFetchaAScreen'
import KYCLayerTwoPageThreeFetchaBScreen from '../modules/KYC/screen/KYCLayerTwoPageThreeFetchaBScreen'
import KYCCameraAutoFillScreen from '../modules/KYC/screen/KYCCameraAutoFillScreen'
import VerificationEmailScreen from '../modules/KYC/screen/VerificationEmailScreen'
import KYCPending from '../modules/KYC/screen/KYCPending'
import AppStyle from '../commons/AppStyle'

const KYCStack = createStackNavigator(
  {
    KYCCameraAutoFillScreen: {
      screen: KYCCameraAutoFillScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    KYCScreen: {
      screen: KYCScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    KYCLayerTwoPageOneScreen: {
      screen: KYCLayerTwoPageOneScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    KYCLayerTwoPageTwoScreen: {
      screen: KYCLayerTwoPageTwoScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    KYCLayerTwoPageThreeFetchaAScreen: {
      screen: KYCLayerTwoPageThreeFetchaAScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    KYCLayerTwoPageThreeFetchaBScreen: {
      screen: KYCLayerTwoPageThreeFetchaBScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    VerificationEmailScreen: {
      screen: VerificationEmailScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    KYCPending: {
      screen: KYCPending,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    }
  },
  {
    initialRouteName: 'KYCScreen',
    cardStyle: { backgroundColor: AppStyle.backgroundColor }
  }
)

export default KYCStack
