import { createStackNavigator } from 'react-navigation'
import AppStyle from '../commons/AppStyle'
import BuyAomScreen from '../modules/KYC/screen/BuyAomScreen'
import BuyAomSecondScreen from '../modules/KYC/screen/BuyAomSecondScreen'
import DashboardScreen from '../modules/Dashboard/screen/DashboardScreen'
import LottoDetailScreen from '../modules/Dashboard/screen/LottoDetailScreen'
import LottoPoolScreen from '../modules/Dashboard/screen/LottoPoolScreen'
import NewsDetailScreen from '../modules/Dashboard/screen/NewsDetailScreen'
import ManageOfferScreen from '../modules/Dashboard/screen/ManageOfferScreen'
import TokenListScreen from '../modules/Dashboard/screen/TokenListScreen'
import CreateTrustLineScreen from '../modules/Setting/screen/CreateTrustLineScreen'
import ScanQRCodeScreen from '../modules/ScanQRCode'

const DashboardStack = createStackNavigator(
  {
    DashboardScreen: {
      screen: DashboardScreen,
      navigationOptions: {
        header: null
      }
    },
    LottoDetailScreen: {
      screen: LottoDetailScreen,
      navigationOptions: {
        header: null
      }
    },
    LottoPoolScreen: {
      screen: LottoPoolScreen,
      navigationOptions: {
        header: null
      }
    },
    NewsDetailScreen: {
      screen: NewsDetailScreen,
      navigationOptions: {
        header: null
      }
    },
    BuyAomScreen: {
      screen: BuyAomScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    BuyAomSecondScreen: {
      screen: BuyAomSecondScreen,
      navigationOptions: {
        header: null
      }
    },
    ManageOfferScreen: {
      screen: ManageOfferScreen,
      navigationOptions: {
        header: null
      }
    },
    TokenListScreen: {
      screen: TokenListScreen,
      navigationOptions: {
        header: null
      }
    },
    CreateTrustLineScreen: {
      screen: CreateTrustLineScreen,
      navigationOptions: {
        header: null
      }
    },
    ScanQRCodeScreen: {
      screen: ScanQRCodeScreen,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: 'DashboardScreen',
    cardStyle: { backgroundColor: AppStyle.backgroundColor }
  }
)

export default DashboardStack
