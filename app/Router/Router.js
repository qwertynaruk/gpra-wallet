import { createStackNavigator } from 'react-navigation'
import AppStyle from '../commons/AppStyle'
import DashboardStack from './DashboardStack'
import UnlockScreen from '../modules/Unlock/screen/UnlockScreen'
import TutorialStack from './TutorialStack'
import KYCStack from './KYCStack'
import SendAomStack from './SendAomStack'
import SettingStack from './SettingStack'
import BackupStack from './BackupStack'

const Router = createStackNavigator(
  {
    DashboardStack: {
      screen: DashboardStack,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    UnlockScreen: {
      screen: UnlockScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    TutorialStack: {
      screen: TutorialStack,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    KYCStack: {
      screen: KYCStack,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    SendAomStack: {
      screen: SendAomStack,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    SettingStack: {
      screen: SettingStack,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    BackupStack: {
      screen: BackupStack,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    }
  },
  {
    initialRouteName: 'DashboardStack',
    cardStyle: { backgroundColor: AppStyle.backgroundColor },
    mode: 'modal'
  }
)

export default Router
