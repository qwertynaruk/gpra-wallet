import { createStackNavigator } from 'react-navigation'
import AppStyle from '../commons/AppStyle'
import SettingScreen from '../modules/Setting/screen/SettingScreen'
import UnlockPincode from '../modules/ChangePincode/screen/UnlockPincode'
import ExportPrivateKeyScreen from '../modules/Setting/screen/ExportPrivateKeyScreen'
import FingerPrintSettingScreen from '../modules/Setting/screen/FingerPrintSettingScreen'
import WalletInfoScreen from '../modules/Setting/screen/WalletInfoScreen'
import CreateTrustLineScreen from '../modules/Setting/screen/CreateTrustLineScreen'
import ConnectAssetScreen from '../modules/Setting/screen/ConnectAssetScreen'
import AnotherAssetScreen from '../modules/Setting/screen/AnotherAssetScreen'
import ScanQRCodeScreen from '../modules/ScanQRCode'
import SecurityScreen from '../modules/Setting/screen/SecurityScreen'

const SettingStack = createStackNavigator(
  {
    SettingScreen: {
      screen: SettingScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    ChangePincodeScreen: {
      screen: UnlockPincode,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    FingerPrintSettingScreen: {
      screen: FingerPrintSettingScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    ExportPrivateKeyScreen: {
      screen: ExportPrivateKeyScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    WalletInfoScreen: {
      screen: WalletInfoScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    CreateTrustLineScreen: {
      screen: CreateTrustLineScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    ScanQRCodeScreen: {
      screen: ScanQRCodeScreen,
      navigationOptions: {
        header: null
      }
    },
    ConnectAssetScreen: {
      screen: ConnectAssetScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    AnotherAssetScreen: {
      screen: AnotherAssetScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    SecurityScreen: {
      screen: SecurityScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    }
  },
  {
    initialRouteName: 'SettingScreen',
    cardStyle: { backgroundColor: AppStyle.backgroundColor }
  }
)

export default SettingStack
