import React, { Component } from 'react'
import {
  Dimensions,
  StyleSheet,
  View
} from 'react-native'
import { BlurView } from '@react-native-community/blur'

const { width, height } = Dimensions.get('window')

export default class BlindScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isShow: false
    }
  }

  showBlind() {
    this.setState({
      isShow: true
    })
  }

  hideBlind() {
    this.setState({
      isShow: false
    })
  }

  render() {
    const { isShow } = this.state

    if (!isShow) {
      return <View />
    }

    return (
      <BlurView
        style={styles.container}
        blurType="light"
        blurAmount={10}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width,
    height,
    position: 'absolute',
    backgroundColor: 'rgba(255,255,255,0.75)',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
