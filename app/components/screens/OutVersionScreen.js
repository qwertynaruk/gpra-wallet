import React, { Component } from 'react'
import {
  Text,
  TouchableOpacity,
  Image,
  Platform,
  StyleSheet,
  Linking,
  View
} from 'react-native'

// Components & Modules
import MainStore from '../../AppStores/MainStore'
import NavStore from '../../AppStores/NavStore'
import AppStyle from '../../commons/AppStyle'
import images from '../../commons/images'
import icons from '../../commons/icons'

export default class OutVersionScreen extends Component {
  constructor(props) {
    super(props)

    this.update = this.update.bind(this)
    this.goBackupTutorial = this.goBackupTutorial.bind(this)
  }

  get updateLink() {
    return MainStore.appState.remoteConfig.getValue('update_link')
  }

  update = () => {
    const { updateLink } = this
    const link = Platform.OS === 'ios' ? 'https://testflight.apple.com/join/Q01kAm3O' : 'https://download.omplatform.com/app-release-latest.apk'
    const url = updateLink || link
    Linking.openURL(url)
  }

  goBackupTutorial() {
    NavStore.pushToScreen('ImportMnemonicTutorialScreen')
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={images.imgUpdateVersion} />
        <Text style={styles.noInternetTitle}>ถึงเวลาอัพเดตกระเป๋า!</Text>
        <Text style={styles.noInternetDescription}> {`เราได้ทำการอัพเดตกระเป๋าหลายๆจุด \n ในเวอร์ชั่นใหม่ กรุณาอัพเดตแอปพลิเคชัน \n ของคุณ เพื่อให้ใช้งานได้อย่างสมบูรณ์`} </Text>
        <Text style={styles.warningDescription}> {`ท่านควรสำรอง "คำกู้ข้อมูล (Seed Words)"\nก่อนทำการอัพเดทกระเป๋าของท่าน`} </Text>
        <View style={{ width: '100%', paddingHorizontal: 25 }}>
          <TouchableOpacity
            style={styles.buttonContainer}
            onPress={this.update}
          >
            <Text style={styles.updateText}>อัพเดตกระเป๋า</Text>
          </TouchableOpacity>
        </View>
        <View style={{
          flexDirection: 'row',
          marginVertical: 20
        }}
        >
          <TouchableOpacity
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center'
            }}
            onPress={this.goBackupTutorial}
          >
            <Image source={icons.iconInfo2} style={{ width: 17, height: 17 }} />
            <Text style={styles.howtoText}>วิธีสำรองคำกู้ข้อมูล (Seed Words)</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: '#FFF'
  },
  noInternetTitle: {
    marginTop: 21,
    fontSize: 20,
    lineHeight: 36,
    fontFamily: AppStyle.mainFontBoldTH,
    color: '#173B64'
  },
  noInternetDescription: {
    marginTop: 8,
    textAlign: 'center',
    fontSize: 18,
    lineHeight: 24,
    fontFamily: AppStyle.mainFontTH,
    color: '#173B64',
    paddingHorizontal: 20
  },
  warningDescription: {
    marginTop: 8,
    textAlign: 'center',
    fontSize: 14,
    fontFamily: AppStyle.mainFontTH,
    color: '#FF1B43',
    paddingHorizontal: 20
  },
  buttonContainer: {
    width: '100%',
    height: 60,
    marginTop: 26,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#535786',
    borderRadius: 8
  },
  updateText: {
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20,
    color: '#FFF'
  },
  howtoText: {
    marginLeft: 6,
    fontFamily: AppStyle.mainFontTH,
    fontSize: 14,
    color: '#535786'
  }
})
