import React, { Component } from 'react'
import {
  Text,
  Image,
  StyleSheet,
  View
} from 'react-native'

// Third-party
import { observer } from 'mobx-react/native'

// Components & Modules
import images from '../../commons/images'
import AppStyle from '../../commons/AppStyle'

// Stores
import MainStore from '../../AppStores/MainStore'

@observer
export default class NoInternetScreen extends Component {
  render() {
    const { internetConnection } = MainStore.appState

    if (internetConnection === 'offline') {
      return (
        <View style={styles.container}>
          <Image source={images.imgNoInternet} />
          <Text style={styles.noInternetTitle}>ไม่สามารถติดต่อเครือข่ายได้</Text>
          <Text style={styles.noInternetDescription}> {`กรุณาเช็คการเชื่อมต่ออินเทอร์เน็ต \n ของคุณและลองใหม่อีกครั้ง`} </Text>
        </View>
      )
    }

    return null
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: '#FFF'
  },
  noInternetTitle: {
    marginTop: 38.76,
    fontSize: 20,
    lineHeight: 36,
    fontFamily: AppStyle.mainFontBoldTH,
    color: '#173B64'
  },
  noInternetDescription: {
    marginTop: 8,
    textAlign: 'center',
    fontSize: 18,
    lineHeight: 24,
    fontFamily: AppStyle.mainFontTH,
    color: '#173B64'
  }
})
