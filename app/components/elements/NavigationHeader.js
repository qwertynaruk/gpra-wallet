import React, { Component } from 'react'
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import images from './../../commons/images'
import AppStyle from '../../commons/AppStyle'

export default class NavigationHeader extends Component {
  static propTypes = {
    style: PropTypes.object,
    containerStyle: PropTypes.object,
    headerItem: PropTypes.object,
    titleStyle: PropTypes.object,
    rightView: PropTypes.object,
    action: PropTypes.func
  }

  static defaultProps = {
    style: {},
    containerStyle: {},
    headerItem: {
      title: '',
      icon: null,
      button: images.backButton
    },
    rightView: {
      rightViewIcon: null,
      rightViewAction: () => { },
      rightViewTitle: null,
      styleTitle: {}
    },
    titleStyle: {},
    action: () => { }
  }

  render() {
    const {
      style,
      containerStyle,
      titleStyle,
      headerItem,
      rightView,
      action
    } = this.props
    const {
      title,
      icon,
      button
    } = headerItem
    const {
      rightViewIcon,
      rightViewAction = () => { },
      rightViewTitle,
      styleTitle,
      styleContainer
    } = rightView
    return (
      <View style={[styles.container, style]}>
        {/* Begin Button */}
        <View style={{
          height: 20,
          width: 30
        }}
        >
          <TouchableOpacity
            onPress={() => { action() }}
            style={{ flex: 1 }}
          >
            {button &&
              <Image
                source={button}
              />
            }
            {icon &&
              <Image
                style={{
                  width: 40,
                  height: 40,
                  marginLeft: 18
                }}
                source={icon}
              />
            }
          </TouchableOpacity>
        </View>
        {/* End Button */}

        {/* Begin Title */}
        {title &&
          <Text
            style={[styles.titleStyle, titleStyle]}
          >
            {title}
          </Text>
        }
        {/* End Title */}

        {/* Begin Right View */}
        {rightViewIcon &&
          <View style={{ marginLeft: 'auto' }}>
            <TouchableOpacity
              onPress={rightViewAction}
            >
              {rightViewIcon &&
                <Image
                  source={rightViewIcon}
                  resizeMode="center"
                />
              }

            </TouchableOpacity>
          </View>
        }

        {
          rightViewTitle &&
          <View style={{ marginLeft: 'auto' }}>
            <Text style={[styles.rightTitle, styleTitle]}>{rightViewTitle}</Text>
          </View>
        }

        {/* End Right View */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  titleStyle: {
    fontSize: 20,
    color: '#000',
    fontFamily: 'OpenSans-Bold'
  },
  rightTitle: {
    fontFamily: 'OpenSans-Semibold',
    fontSize: 18,
    color: AppStyle.mainColor,
    marginLeft: 4
  }
})
