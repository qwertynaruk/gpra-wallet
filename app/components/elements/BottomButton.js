import React, { Component } from 'react'
import {
  Animated,
  Text,
  StyleSheet,
  TouchableOpacity,
  Keyboard,
  Platform
} from 'react-native'
import PropTypes from 'prop-types'
import LayoutUtils from '../../commons/LayoutUtils'
import AppStyle from '../../commons/AppStyle'
import constant from '../../commons/constant'

const isIPX = LayoutUtils.getIsIPX()
const extraBottom = LayoutUtils.getExtraBottom()

export default class BottomButton extends Component {
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    text: PropTypes.string,
    disable: PropTypes.bool,
    backgroundColor: PropTypes.string
  }

  static defaultProps = {
    text: constant.DONE,
    disable: false,
    backgroundColor: AppStyle.backgroundDarkenColor
  }

  state = {
    bottom: new Animated.Value(20 + extraBottom),
    marginVertical: new Animated.Value(20),
    borderRadius: 8
  }

  componentWillMount() {
    const show = Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow'
    const hide = Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide'
    this.keyboardDidShowListener = Keyboard.addListener(show, e => this._keyboardDidShow(e))
    this.keyboardDidHideListener = Keyboard.addListener(hide, e => this._keyboardDidHide(e))
  }

  componentDidMount() {
    this.isPress = false
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  onPress = () => {
    if (this.isPress) {
      return
    }
    this.isPress = true
    const { onPress } = this.props
    Keyboard.dismiss()
    setTimeout(() => { this.isPress = false }, 500)
    onPress()
  }

  _runKeyboardAnim(toValue) {
    const duration = Platform.OS === 'ios' ? 250 : 0
    Animated.parallel([
      Animated.timing(
        this.state.bottom,
        {
          toValue,
          duration
        }
      ),
      Animated.timing(
        this.state.marginVertical,
        {
          toValue: toValue === 20 + extraBottom ? 20 : 0,
          duration
        }
      )
    ]).start()
  }

  _keyboardDidShow(e) {
    let value = Platform.OS === 'ios' ? e.endCoordinates.height + extraBottom : 0

    if (isIPX) {
      value -= 34
    }
    this._runKeyboardAnim(value)
    this.setState({ borderRadius: 0 })
  }

  _keyboardDidHide(e) {
    this._runKeyboardAnim(20 + extraBottom)
    this.setState({ borderRadius: 8 })
  }

  render() {
    const {
      text, disable, backgroundColor
    } = this.props
    return (
      <Animated.View style={{
        position: 'absolute',
        bottom: this.state.bottom,
        marginTop: 10,
        backgroundColor,
        borderRadius: this.state.borderRadius,
        left: this.state.marginVertical,
        right: this.state.marginVertical
      }}
      >
        <TouchableOpacity
          disabled={disable}
          onPress={this.onPress}
          style={styles.bbutton}
        >
          <Text style={{
            fontSize: 18,
            color: AppStyle.mainColor,
            fontFamily: AppStyle.mainFontBoldTH,
            opacity: disable ? 0.3 : 1
          }}
          >
            {text}
          </Text>
        </TouchableOpacity>
      </Animated.View>
    )
  }
}

const styles = StyleSheet.create({
  bbutton: {
    paddingVertical: 16,
    alignItems: 'center',
    justifyContent: 'center'
  }
})
