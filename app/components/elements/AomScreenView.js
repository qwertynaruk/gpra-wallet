import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Text,
  View,
  StatusBar,
  Dimensions,

} from 'react-native'

import images from '../../commons/images';
import AppStyle from '../../commons/AppStyle';
import NavigationHeader from './NavigationHeader';
import LayoutUtils from '../../commons/LayoutUtils';

const marginTop = LayoutUtils.getExtraTop()

export default class AomScreenView extends Component {
  static propTypes = {
    title: PropTypes.string,
    navigation: PropTypes.object
  }

  static defaultProps = {
    title: '',
    navigation: {
      goBack: ()=>{}
    }
  }

  render() {
    const { navigation } = this.props
    const dimensions = Dimensions.get('window')
    const width = dimensions.width - 40

    return (
      <View style={ {flex: 1} }>
        <StatusBar backgroundColor="transparent" barStyle="dark-content" translucent />
        <NavigationHeader
          style={{
            marginTop: marginTop + 20, width
          }}
          headerItem={{
            title: this.props.title,
            icon: null,
            button: images.iconBackAlt
          }}
          titleStyle={{
            color: '#000',
            fontFamily: AppStyle.mainFontBoldTH,
            textAlign: 'center',
            flex: 1,
            marginRight: 20
          }}
          action={ navigation.goBack }
        />
        {this.props.children}
      </View>
    )
  }
}