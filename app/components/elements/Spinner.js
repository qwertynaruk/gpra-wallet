import React, { Component } from 'react'
import {
  Animated,
  View,
  StyleSheet,
  StatusBar
} from 'react-native'
import PropTypes from 'prop-types'
import images from '../../commons/images'
/* eslint-disable-next-line */
import GoldenLoading from './GoldenLoading'

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default class Spinner extends Component {
  static propTypes = {
    style: PropTypes.array,
    visible: PropTypes.bool
  }

  static defaultProps = {
    style: [],
    visible: true
  }

  constructor(props) {
    super(props)
    this.state = {
      visible: props.visible
    }
  }

  componentWillMount() {
    this.animatedValue = new Animated.Value(0)
  }

  componentDidMount() {
    this.runAnimation()
  }

  _show = () => {
    this.setState({
      visible: true
    })
  }

  _hide = () => {
    this.setState({
      visible: false
    })
  }

  runAnimation() {
    this.animatedValue.setValue(0)
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 500,
      useNativeDriver: true
    }).start(() => {
      this.runAnimation()
    })
  }

  render() {
    const {
      style
    } = this.props
    const { visible } = this.state
    if (!visible) {
      return <View key="invisible" />
    }

    const interpolateRotation = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })
    const animatedStyle = {
      transform: [
        { rotate: interpolateRotation }
      ]
    }

    return (
      <View key="visible" style={[styles.container, { backgroundColor: 'rgba(0,0,0,0.75)' }, style]}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="light-content"
          translucent
        />
        <Animated.Image
          source={images.loadingLogo}
          style={animatedStyle}
        />
      </View>
    )
  }
}
