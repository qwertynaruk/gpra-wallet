import React, { Component } from 'react'
import {
  Animated,
  StyleSheet,
  Dimensions,
  View,
  Text
} from 'react-native'
// import PropTypes from 'prop-types'
import HapticHandler from '../../Handler/HapticHandler'
import LayoutUtils from '../../commons/LayoutUtils'

const { width } = Dimensions.get('window')
const { heightNotif } = LayoutUtils
const _heightNotif = heightNotif + 22

export default class CustomToastTop extends Component {
  constructor(props) {
    super(props)
    this.offsetToast = new Animated.Value(-_heightNotif)
    this.state = {
      content: '',
      styleText: {},
      style: {}
    }
  }

  showToast(content, style = {}, styleText = {}) {
    this.setState({
      content,
      style,
      styleText
    })
    setTimeout(() => HapticHandler.ImpactLight(), 100)
    Animated.timing(this.offsetToast, {
      toValue: 0,
      duration: 250
    }).start()
    setTimeout(() => this.hideToast(), 2500)
  }

  hideToast() {
    Animated.timing(this.offsetToast, {
      toValue: -_heightNotif,
      duration: 250
    }).start()
  }

  render() {
    const { content, style, styleText } = this.state
    return (
      <Animated.View
        style={[styles.container, {
          transform: [
            {
              translateY: this.offsetToast
            }
          ]
        }]}
      >
        <View style={[styles.contentBody, style]}>
          <Text style={[styles.copyText, styleText]}>{content}</Text>
        </View>
      </Animated.View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    height: _heightNotif,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
  },
  contentBody: {
    borderWidth: 1,
    borderRadius: 10,
    borderColor: 'rgba(0, 0, 0, 0.5)',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
  },
  copyText: {
    fontSize: 16,
    fontFamily: 'OpenSans-Bold',
    // color: '#4A90E2',
    color: '#FFF',
    margin: 8
    // marginBottom: 10
  }
})
