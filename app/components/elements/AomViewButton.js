import React, { PureComponent } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import AppStyle from '../../commons/AppStyle'

export default class AomViewButton extends PureComponent {
  static propTypes = {
    onPress: PropTypes.func,
    enable: PropTypes.bool,
    btnStyle: PropTypes.string,
    title: PropTypes.string
  }

  static defaultProps = {
    onPress: () => { },
    btnStyle: 'primary',
    enable: true,
    colorDot: '#4A90E2'
  }

  render() {
    const { onPress, title } = this.props

    let bgPanel = {}
    let fgPanel = {}

    switch( this.props.btnStyle ) {
      case 'primary':
        break;
      case 'normal':
        bgPanel.backgroundColor = '#EFEFEF'
        fgPanel.color = '#555'
        break;
    }

    return (
      <TouchableOpacity
        activeOpacity={ this.props.enable ? 0.8 : 0.5 }
        onPress={ this.props.enable ? onPress : ()=>{} }
        style={[ styles.container, { opacity: this.props.enable ? 1.0 : 0.5 } ]}
      >
        <View style={[ styles.button, bgPanel]}>
          <Text style={ [ styles.buttonText, fgPanel ]}> { title } </Text>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 20,
    height: 58,
    alignSelf: 'stretch'
  },
  button: {
    backgroundColor: '#535786',
    borderRadius: 8,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: '#fff',
    fontFamily: AppStyle.mainFontTH,
    fontSize: 20
  }
})
