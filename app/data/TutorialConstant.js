import { StyleSheet } from 'react-native'

// Components & Modules
import icons from '../commons/icons'

class TutorialConstant {
  ENTRIES = [
    {
      container: {
        first: {
          flex: 70,
          alignItems: 'center',
          justifyContent: 'center'
        },
        second: {
          flex: 30,
          alignItems: 'center',
          justifyContent: 'center'
        }
      },
      image: icons.iconTutorialFirst,
      style: styles.tutorialFirstImage,
      resizeMode: 'center',
      description: {
        firstLine: 'OM Wallet',
        secondLine: 'Lifestyle Crypto Wallet',
        thirdLine: 'ของคนรุ่นใหม่ที่ทั้งรวดเร็ว ใช้ง่าย'
      }
    },
    {
      container: {
        first: {
          flex: 70,
          alignItems: 'center',
          justifyContent: 'center'
        },
        second: {
          flex: 30,
          alignItems: 'center',
          justifyContent: 'center'
        }
      },
      image: icons.iconTutorialSecond,
      style: styles.tutorialSecondImage,
      resizeMode: 'center',
      description: {
        firstLine: 'ด้วยเทคโนโลยี Blockchain บน',
        secondLine: 'OmChain จึงทำให้การถือครองสินทรัพย์',
        thirdLine: 'มีความปลอดภัย โปร่งใส'
      }
    },
    {
      container: {
        first: {
          flex: 80,
          alignItems: 'center',
          justifyContent: 'center'
        },
        second: {
          flex: 20,
          alignItems: 'center',
          justifyContent: 'center'
        }
      },
      image: icons.iconTutorialThird,
      style: styles.tutorialThirdImage,
      resizeMode: 'center',
      description: {
        firstLine: 'เริ่มใช้งานกระเป๋าออม',
        secondLine: 'กับบริการหลากหลายอื่นๆที่คุณเลือกได้',
        thirdLine: ''
      }
    }
  ]
}

const styles = StyleSheet.create({
  tutorialFirstImage: {
    flex: 0.7
  },
  tutorialSecondImage: {
    flex: 0.85
  },
  tutorialThirdImage: {
    flex: 1
  }
})

export default new TutorialConstant()
