import { observable, action, computed } from 'mobx'
import firebase from 'react-native-firebase'
import BigNumber from 'bignumber.js'
import Config from './stores/Config'
import RemoteConfig from './stores/RemoteConfig'
import Constants from '../commons/constant'
import AppWalletsStore from './AppWalletsStore'
import AppDS from './DataSource/AppDS'
import Reactions from './Reactions'
import AddressBookDS from './DataSource/AddressBookDS'
import UnspendTransactionDS from './DataSource/UnspendTransactionDS'
import BgJobs from './BackgroundJobs'
import api from '../api'
import Fabric from '../Handler/Fabric'
import NotificationStore from './stores/Notification'

class AppState {
  dataVersion = '1'
  @observable.ref biometryType = ''
  @observable enableTouchFaceID = null
  @observable config = new Config('mainnet', Constants.INFURA_API_KEY)
  @observable remoteConfig = new RemoteConfig()
  @observable defaultWallet = null // for web3 dapp
  @observable selectedWallet = null // for sending transaction
  @observable selectedToken = null // for sending transaction
  @observable selectedTransaction = null
  @observable addressBooks = []
  @observable rateETHDollar = new BigNumber(0)
  @observable rateBTCDollar = new BigNumber(0)
  @observable rateLTCDollar = new BigNumber(0)
  @observable rateDOGEDollar = new BigNumber(0)
  @observable rateXRPDollar = new BigNumber(0)
  @observable rateXLMDollar = new BigNumber(0)
  @observable rateAOMDollar = new BigNumber(0)
  @observable rateETH = new BigNumber(0)
  @observable rateBTC = new BigNumber(0)
  @observable rateXRP = new BigNumber(0)
  @observable rateXLM = new BigNumber(0)
  @observable rateAOM = new BigNumber(1)
  @observable hasPassword = false
  @observable kycLayerOne = false
  @observable kycLayerTwo = null
  @observable kycApproved = false
  @observable kycRejected = null
  @observable isSendVerify = false
  @observable didBackup = false
  @observable multiSign = {}
  @observable kycData = {}
  @observable userInfo = {}
  currentWalletIndex = 0
  currentBTCWalletIndex = 0
  currentLTCWalletIndex = 0
  currentDOGEWalletIndex = 0
  currentXRPWalletIndex = 0
  currentXLMWalletIndex = 0
  currentAOMWalletIndex = 0
  @observable internetConnection = 'online' // online || offline
  @observable unpendTransactions = []
  @observable gasPriceEstimate = {
    slow: 2,
    standard: 20,
    fast: 60
  }
  @observable allowDailyUsage = null

  @observable currentCardIndex = 0
  lastestVersionRead = ''
  shouldShowUpdatePopup = true
  homeCarousel = null
  fabric = null

  static TIME_INTERVAL = 20000

  constructor() {
    this.appWalletsStore = new AppWalletsStore()
    this.BgJobs = {
      Aom: new BgJobs.AomJob(this),
      CheckBalance: new BgJobs.CheckBalance(this, this.TIME_INTERVAL),
      CheckPendingTransaction: new BgJobs.CheckPendingTransaction(this, this.TIME_INTERVAL)
    }
  }

  startAllServices() {
    Reactions.auto.listenConfig(this)
    Reactions.auto.listenConnection(this)
    this.getRateTHB()
  }

  startAllBgJobs() {
    this.BgJobs.CheckBalance.doOnce()
    // this.BgJobs.CheckPendingTransaction.doOnce()
    this.BgJobs.CheckBalance.start()
    // this.BgJobs.CheckPendingTransaction.start()
  }

  @action setEnableTouchFaceID = (enable) => {
    this.enableTouchFaceID = enable
    this.save()
  }

  @action setBiometryType = (biometryType) => {
    this.biometryType = biometryType
    this.save()
  }

  initFabric() {
    this.fabric = new Fabric()
  }

  async syncRemoteConfig() {
    if (__DEV__) {
      firebase.config().enableDeveloperMode()
    }

    await firebase.config().fetch()
      .then(() => firebase.config().activateFetched())
      .then(() => firebase.config().getKeysByPrefix())
      .then(arr => firebase.config().getValues(arr))
      .then((objects) => {
        Object.keys(objects).forEach((key) => {
          this.remoteConfig.setValue(key, objects[key].val())
        })
      })
      .catch(error => console.log(`Error processing config: ${error}`))
  }

  syncWalletAddresses() {
    this.aomInfo()
    NotificationStore.updateWallets()
  }

  @action setConfig = (cf) => { this.config = cf }
  @action setBackup = (isBackup) => { this.didBackup = isBackup }
  @action setSelectedWallet = (w) => { this.selectedWallet = w }
  @action setInternetConnection = (ic) => { this.internetConnection = ic }
  @action setselectedToken = (t) => { this.selectedToken = t }
  @action setSelectedTransaction = (tx) => { this.selectedTransaction = tx }
  @action setUnpendTransactions = (ut) => { this.unpendTransactions = ut }
  @action setLastestVersionRead = (lvr) => { this.lastestVersionRead = lvr }
  @action setShouldShowUpdatePopup = (isShow) => { this.shouldShowUpdatePopup = isShow }

  @action async syncAddressBooks() {
    await AddressBookDS.getAddressBooks().then((_addressBooks) => {
      const addressBooks = _addressBooks
      const addressBookMap = addressBooks.reduce((_rs, ab, i) => {
        const rs = _rs
        rs[ab.address] = i
        return rs
      }, {})

      this.addressBooks.forEach((ab) => {
        const index = addressBookMap[ab.address]
        addressBooks[index] = ab
      })

      this.addressBooks = addressBooks
    })
  }

  @action autoSetSelectedWallet() {
    const lastIndex = this.wallets.length - 1
    if (lastIndex < 0) this.setSelectedWallet(null)

    this.setSelectedWallet(this.wallets[lastIndex])
  }

  @action setSelectedByType(type) {
    const index = this.wallets.findIndex(w => w.type === type)
    this.setSelectedWallet(this.wallets[index])
  }

  @action setHasPassword(hasPassword) {
    this.hasPassword = hasPassword
  }

  @action setKycLayerOne(status) {
    this.kycLayerOne = status
    this.save()
  }

  @action setKycLayerTwo(status) {
    this.kycLayerTwo = status
    this.save()
  }

  @action setKycApproved(status) {
    this.kycApproved = status
    this.save()
  }

  @action setKycRejected(status) {
    this.kycRejected = status
  }

  @action setIsSendVerify(status) {
    this.isSendVerify = status
    this.save()
  }

  @action setCurrentWalletIndex(index) {
    this.currentWalletIndex = index
  }

  @action setUserInfo(info) {
    this.userInfo = info
    this.save()
  }

  @action setAllowDailyUsage(isEnable) {
    this.allowDailyUsage = isEnable
    this.save()
  }

  @action setCurrentBTCWalletIndex(index) {
    this.currentBTCWalletIndex = index
    this.save()
  }

  @action setCurrentLTCWalletIndex(index) {
    this.currentLTCWalletIndex = index
    this.save()
  }

  @action setCurrentDOGEWalletIndex(index) {
    this.currentDOGEWalletIndex = index
    this.save()
  }

  @action setCurrentXRPWalletIndex(index) {
    this.currentXRPWalletIndex = index
    this.save()
  }

  @action setCurrentXLMWalletIndex(index) {
    this.currentXLMWalletIndex = index
    this.save()
  }

  @action setCurrentAOMWalletIndex(index) {
    this.currentAOMWalletIndex = index
    this.save()
  }

  @action async getRateTHB() {
    setTimeout(async () => {
      if (this.internetConnection === 'online') {
        const rs = await api.fetchRateTHB()
        const rates = rs.data

        if (rates) {
          if (rates.THB_ETH && rates.THB_ETH.last !== this.rateETH) {
            this.rateETH = new BigNumber(rates.THB_ETH.last)
          }
          if (rates.THB_BTC && rates.THB_BTC.last !== this.rateBTC) {
            this.rateBTC = new BigNumber(rates.THB_BTC.last)
          }
          if (rates.THB_XRP && rates.THB_XRP.last !== this.rateXRP) {
            this.rateXRP = new BigNumber(rates.THB_XRP.last)
          }
          if (rates.THB_XLM && rates.THB_XLM.last !== this.rateXLM) {
            this.rateXLM = new BigNumber(rates.THB_XLM.last)
          }
          this.rateAOM = new BigNumber(1)
        }
      }
    }, 100)
  }

  @action async getRateETHDollar() {
    setTimeout(async () => {
      if (this.internetConnection === 'online') {
        const rs = await api.fetchRateETHDollar()
        const rate = rs.data && rs.data.RAW && rs.data.RAW.ETH && rs.data.RAW.ETH.USD

        if (rate.PRICE != this.rateETHDollar) {
          this.rateETHDollar = new BigNumber(rate.PRICE)
        }
      }
    }, 100)
  }

  @action async getRateBTCDollar() {
    setTimeout(async () => {
      const rs = await api.fetchRateBTCDollar()
      const rate = rs.data && rs.data.RAW && rs.data.RAW.BTC && rs.data.RAW.BTC.USD

      if (rate.PRICE != this.rateBTCDollar) {
        this.rateBTCDollar = new BigNumber(rate.PRICE)
      }
    }, 100)
  }

  @action async getRateLTCDollar() {
    setTimeout(async () => {
      const rs = await api.fetchRateLTCDollar()
      const rate = rs.data && rs.data.RAW && rs.data.RAW.LTC && rs.data.RAW.LTC.USD

      if (rate.PRICE != this.rateBTCDollar) {
        this.rateLTCDollar = new BigNumber(rate.PRICE)
      }
    }, 100)
  }

  @action async getRateDOGEDollar() {
    setTimeout(async () => {
      const rs = await api.fetchRateDOGEDollar()
      const rate = rs.data && rs.data.RAW && rs.data.RAW.DOGE && rs.data.RAW.DOGE.USD

      if (rate.PRICE != this.rateBTCDollar) {
        this.rateDOGEDollar = new BigNumber(rate.PRICE)
      }
    }, 100)
  }

  @action async getGasPriceEstimate() {
    setTimeout(async () => {
      if (this.config.network === Config.networks.mainnet && this.internetConnection === 'online') {
        const res = await api.fetchGasPrice()
        const data = typeof res.data === 'object'
          ? {
            slow: !isNaN(res.data.safeLow / 10) ? Math.floor(res.data.safeLow / 10) : 2,
            standard: !isNaN(res.data.average / 10) ? Math.floor(res.data.average / 10) : 10,
            fast: !isNaN(res.data.fastest / 10) ? Math.floor(res.data.fastest / 10) : 60
          }
          : this.gasPriceEstimate

        this.gasPriceEstimate = data
      } else {
        this.gasPriceEstimate = {
          slow: 2,
          standard: 10,
          fast: 60
        }
      }
    }, 0)
  }

  @action setCurrentCardIndex(index) {
    this.currentCardIndex = index
  }

  @action async deletUnpendTransactions(hash) {
    const unspendTransactions = await UnspendTransactionDS.deleteTransaction(hash)
    this.unpendTransactions = unspendTransactions
  }

  @action async loadPendingTxs() {
    const unspendTransactions = await UnspendTransactionDS.getTransactions()
    this.unpendTransactions = unspendTransactions
  }

  @action async import(orgData) {
    const data = orgData
    this.config = new Config(data.config.network, data.config.infuraKey)
    this.hasPassword = data.hasPassword
    this.didBackup = data.didBackup
    this.enableTouchFaceID = data.enableTouchFaceID
    this.biometryType = data.biometryType || ''
    this.currentWalletIndex = data.currentWalletIndex || 0
    this.currentBTCWalletIndex = data.currentBTCWalletIndex || 0
    this.currentLTCWalletIndex = data.currentLTCWalletIndex || 0
    this.currentDOGEWalletIndex = data.currentDOGEWalletIndex || 0
    this.currentXRPWalletIndex = data.currentXRPWalletIndex || 0
    this.currentXLMWalletIndex = data.currentXLMWalletIndex || 0
    this.currentAOMWalletIndex = data.currentAOMWalletIndex || 0
    const addressBooks = await AddressBookDS.getAddressBooks()
    this.addressBooks = addressBooks
    this.shouldShowUpdatePopup = data.shouldShowUpdatePopup !== undefined ? data.shouldShowUpdatePopup : true
    this.lastestVersionRead = data.lastestVersionRead
    this.allowDailyUsage = data.allowDailyUsage

    await this.loadPendingTxs()
    await this.appWalletsStore.getWalletFromDS()

    if (this.wallets.length > 0) {
      this.setSelectedWallet(this.wallets[0])
    }

    this.rateETHDollar = new BigNumber(data.rateETHDollar || 0)
    this.rateBTCDollar = new BigNumber(data.rateBTCDollar || 0)
    this.rateLTCDollar = new BigNumber(data.rateLTCDollar || 0)
    this.rateDOGEDollar = new BigNumber(data.rateDOGEDollar || 0)
    this.rateAOMDollar = new BigNumber(data.rateAOMDollar || 0)

    this.rateETH = new BigNumber(data.rateETH || 0)
    this.rateBTC = new BigNumber(data.rateBTC || 0)
    this.rateXRP = new BigNumber(data.rateXRP || 0)
    this.rateXLM = new BigNumber(data.rateXLM || 0)
    this.rateAOM = new BigNumber(data.rateAOM || 0)

    this.gasPriceEstimate = data.gasPriceEstimate

    if (data.kycLayerOne) {
      this.kycLayerOne = data.kycLayerOne
    }
    if (data.kycLayerTwo) {
      this.kycLayerTwo = data.kycLayerTwo
    }
    if (data.kycApproved) {
      this.kycApproved = data.kycApproved
    }
    if (data.kycRejected) {
      this.kycRejected = data.kycRejected
    }
    if (data.isSendVerify) {
      this.isSendVerify = data.isSendVerify
    }
    if (data.multiSign) {
      this.multiSign = data.multiSign
    }
    if (data.userInfo) {
      this.userInfo = data.userInfo
    }
  }

  @action async aomInfo() {
    const wallet = this.OMWallet
    if (wallet) {
      const status = {
        layerOne: false,
        layerTwo: false,
        approved: false,
        rejected: false
      }

      const userInfo = await api.getKYCProfile(wallet.address)
      this.userInfo = userInfo.data.data

      const walletInfo = await api.getAOMWalletInfo(wallet.address).then((res) => {
        if (res.status === 200) {
          return res.data.data
        }

        api.Aom.reportBug({
          address: wallet.address,
          error_component: 'App',
          error_function: 'getAOMWalletInfo',
          error_detail: res.data
        })
        return false
      }).catch((error) => {
        api.Aom.reportBug({
          address: wallet.address,
          error_component: 'App',
          error_function: 'getAOMWalletInfo',
          error_detail: error.message
        })
      })
      this.multiSign = {
        signState: Number(walletInfo.sign_state),
        signKey: walletInfo.sign_id,
        signPair: walletInfo.account_id
      }

      const kycStatus = await api.getKYCAOM(wallet.address)
        .then((res) => {
          if (res.status === 200) {
            status.layerOne = res.data.data.kyc_layer_1 === 'true' || res.data.data.kyc_layer_1 === true || res.data.data.kyc_layer_1 === 1
            status.layerTwo = res.data.data.kyc_layer_2 === 'true' || res.data.data.kyc_layer_2 === true || res.data.data.kyc_layer_2 === 1
            status.approved = res.data.data.kycd_approved === 'true' || res.data.data.kycd_approved === true || res.data.data.kycd_approved === 1
            status.rejected = res.data.data.reject_comment
            return status
          }

          throw new Error('Status not 200')
        })
        .catch((err) => {
          api.Aom.reportBug({
            address: wallet.address,
            error_component: 'App',
            error_function: 'getKYCAOM',
            error_detail: err.message
          })
          return status
        })
      this.kycLayerOne = kycStatus.layerOne
      this.kycLayerTwo = kycStatus.layerTwo
      this.kycApproved = kycStatus.approved
      this.kycRejected = kycStatus.rejected
      this.save()
    }
  }

  @computed get isShowDappButton() {
    const wallet = this.selectedWallet
    const idx = this.wallets.length
    if (this.currentCardIndex !== idx && wallet) {
      return wallet.type === 'ethereum' && wallet.canSendTransaction
    }
    return false
  }

  @computed get isShowLottoButton() {
    const wallet = this.selectedWallet
    const idx = this.wallets.length
    if (this.currentCardIndex !== idx && wallet) {
      return wallet.type === 'aomtoken'
    }
    return false
  }

  @computed get isShowSendButton() {
    const idx = this.wallets.length
    const wallet = this.selectedWallet
    if (this.currentCardIndex === idx || !wallet) {
      return false
    }
    if (wallet.importType === 'Address') {
      return false
    }

    if (wallet.type === 'aomtoken' && !wallet.funded) {
      return false
    }
    return true
    // return wallet.canSendTransaction
  }

  @computed get getUnpendTransactions() {
    return this.unpendTransactions
  }

  @computed get networkName() {
    return this.config.network
  }

  @computed get wallets() {
    if (this.networkName !== Config.networks.mainnet) {
      const ethWallets = this.appWalletsStore.wallets.filter(w => w.type === 'ethereum')
      return ethWallets
    }
    return this.appWalletsStore.wallets
  }

  @computed get cards() {
    return this.wallets.map((obj) => {
      return obj.card
    })
  }

  @computed get enableNotification() {
    if (this.wallets.length == 0) return true
    for (let i = 0; i < this.wallets.length; i++) {
      if (this.wallets[i].enableNotification) return true
    }
    return false
  }

  get OMWallet() {
    const omWallet = this.wallets.filter(w => w.type === 'aomtoken')
    return omWallet.pop()
  }

  get kycLevel() {
    const { kycLayerOne, kycLayerTwo, kycApproved } = this
    if (kycLayerOne && (!kycLayerTwo || !kycApproved)) {
      return 1
    }
    if (kycLayerOne && kycLayerTwo && kycApproved) {
      return 2
    }

    return 0
  }

  resetAppState() {
    this.config = new Config('mainnet', Constants.INFURA_API_KEY)
    this.setHasPassword(false)
    this.setBackup(false)
    this.currentWalletIndex = 0
    this.setUnpendTransactions([])
    this.addressBooks = []
    this.appWalletsStore.removeAll()
  }

  save() {
    return AppDS.saveAppData(this.toJSON())
  }

  // for local storage: be careful with MobX observable
  toJSON() {
    return {
      dataVersion: this.dataVersion,
      config: this.config.toJSON(),
      defaultWallet: this.defaultWallet ? this.defaultWallet.address : null,
      selectedWallet: this.selectedWallet ? this.selectedWallet.address : null,
      selectedToken: this.selectedToken ? this.selectedToken.address : null,
      hasPassword: this.hasPassword,
      rateETHDollar: this.rateETHDollar.toString(10),
      rateBTCDollar: this.rateBTCDollar.toString(10),
      rateLTCDollar: this.rateLTCDollar.toString(10),
      rateDOGEDollar: this.rateDOGEDollar.toString(10),
      rateAOMDollar: this.rateAOMDollar.toString(10),
      rateETH: this.rateETH.toString(10),
      rateBTC: this.rateBTC.toString(10),
      rateXRP: this.rateXRP.toString(10),
      rateXLM: this.rateXLM.toString(10),
      rateAOM: this.rateAOM.toString(10),
      currentWalletIndex: this.currentWalletIndex,
      currentBTCWalletIndex: this.currentBTCWalletIndex,
      currentXRPWalletIndex: this.currentXRPWalletIndex,
      currentXLMWalletIndex: this.currentXLMWalletIndex,
      currentAOMWalletIndex: this.currentAOMWalletIndex,
      didBackup: this.didBackup,
      gasPriceEstimate: this.gasPriceEstimate,
      enableNotification: this.enableNotification,
      lastestVersionRead: this.lastestVersionRead,
      shouldShowUpdatePopup: this.shouldShowUpdatePopup,
      enableTouchFaceID: this.enableTouchFaceID,
      biometryType: this.biometryType,
      allowDailyUsage: this.allowDailyUsage,
      kycLayerOne: this.kycLayerOne,
      kycLayerTwo: this.kycLayerTwo,
      kycApproved: this.kycApproved,
      kycRejected: this.kycRejected,
      isSendVerify: this.isSendVerify,
      multiSign: this.multiSign,
      userInfo: this.userInfo
    }
  }
}

export default new AppState()
