import { AsyncStorage } from 'react-native'

const dataKey = 'ASSET_STORAGE'

class AssetDS {
  Assets = [] // for caching

  async getAssets() {
    const AssetsStr = await AsyncStorage.getItem(dataKey)
    if (!AssetsStr) return []

    this.Assets = JSON.parse(AssetsStr)
    return this.Assets
  }

  saveAssets(AssetsArray) {
    return AsyncStorage.setItem(dataKey, JSON.stringify(AssetsArray))
  }

  async addNewAsset(asset) {
    const Assets = await this.getAssets()
    const find = Assets.includes(asset)
    if (!find) Assets.push(asset)
    this.saveAssets(Assets)
    return Assets
  }

  async deleteAsset(type) {
    const Assets = await this.getAssets()
    const result = Assets.filter(asset => asset !== type)
    this.saveAssets(result)
    return result
  }
}

export default new AssetDS()
