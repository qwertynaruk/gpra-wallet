import { AsyncStorage } from 'react-native'
import crypto from 'crypto'
import { ETHWallet, BTCWallet, LTCWallet, DOGEWallet, AOMWallet, XLMWallet, XRPWallet } from '../stores/Wallet'
import NavStore from '../NavStore'
import SecureDS from './SecureDS'

const dataKey = 'WALLETS_STORAGE'

function sha1(data) {
  const generator = crypto.createHash('sha1')
  generator.update(data)
  return generator.digest('hex')
}

class WalletDataSource {
  wallets = [] // for caching

  async getWallets() {
    const walletsStr = await AsyncStorage.getItem(dataKey)
    if (!walletsStr) return []

    this.wallets = JSON.parse(walletsStr).map((js) => {
      if (js.type === 'ethereum') return new ETHWallet(js)
      if (js.type === 'bitcoin') return new BTCWallet(js)
      if (js.type === 'litecoin') return new LTCWallet(js)
      if (js.type === 'dogecoin') return new DOGEWallet(js)
      if (js.type === 'stellar') return new XLMWallet(js)
      if (js.type === 'ripple') return new XRPWallet(js)
      if (js.type === 'aomtoken') return new AOMWallet(js)
      if (js.type === 'ripple') return new XRPWallet(js)
      return new BTCWallet(js)
    })
    return this.wallets
  }

  async getWalletAtAddress(address) {
    const wallets = this.wallets || await this.getWallets()
    return wallets.find(w => w.address === address)
  }

  async getIndexAtAddress(address) {
    const wallets = this.wallets || await this.getWallets()
    const wallet = await this.getWalletAtAddress(address)
    return wallets.indexOf(wallet)
  }

  _prevSaveWalletsHash = false
  saveWallets(walletsArray) {
    const walelts = walletsArray.map(w => w.toJSON())
    const json = JSON.stringify(walelts)
    const hash = sha1(json)

    if (this._prevSaveWalletsHash === hash) {
      return true
    }

    this._prevSaveWalletsHash = hash
    return AsyncStorage.setItem(dataKey, json)
  }

  async updateWallet(wallet) {
    const wallets = await this.getWallets()

    for (let i = 0; i < wallets.length; i++) {
      if (wallets[i].address === wallet.address) {
        wallets[i] = wallet
        this.saveWallets(wallets)
        break
      }
    }
  }

  async addNewWallet(wallet) {
    const wallets = await this.getWallets()
    const find = wallets.find(w => w.address === wallet.address)
    if (!find) wallets.push(wallet)
    return this.saveWallets(wallets)
  }

  async deleteWallet(address) {
    NavStore.lockScreen({
      onUnlock: async (pincode) => {
        const wallets = await this.getWallets()
        const result = wallets.filter(w => w.address != address)
        new SecureDS(pincode).removePrivateKey(address)
        return this.saveWallets(result)
      }
    }, true)
  }
}

export default new WalletDataSource()
