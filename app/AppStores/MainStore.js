import { observable, action } from 'mobx'
import AppDS from './DataSource/AppDS'
import appState from './AppState'
import PushNotificationHelper from '../commons/PushNotificationHelper'

class MainStore {
  @observable appState = appState
  secureStorage = null
  sendTransaction = null
  unlock = null
  importStore = null
  backupStore = null
  changePincode = null
  dapp = null
  addressBookStore = null
  importMnemonicStore = null
  importPrivateKeyStore = null
  importAddressStore = null
  kycStore = null
  newsStore = null
  smartContractStore = null

  // Start
  @action async startApp() {
    await AppDS.readAppData()
    await PushNotificationHelper.init()
    await appState.syncRemoteConfig()
    appState.syncWalletAddresses()
    appState.initFabric()
    appState.startAllServices()
  }
}

export default new MainStore()
