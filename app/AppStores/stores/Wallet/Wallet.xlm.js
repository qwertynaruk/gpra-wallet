import { observable, action } from 'mobx'
import BigNumber from 'bignumber.js'
import Wallet from './Wallet'
import Keystore from '../../../../Libs/react-native-golden-keystore'
import api from '../../../api'
import url from '../../../api/url'
import MainStore from '../../MainStore'
import WalletTokenXLM from '../WalletToken.xlm'
import GetAddress, { chainNames } from '../../../Utils/WalletAddresses'
import Stellar from '../../../Utils/Stellar'

const defaultObjWallet = {
  ext: 'XLM',
  title: '',
  address: '',
  balance: '0',
  type: 'stellar',
  path: Keystore.CoinType.XLM.path,
  external: false,
  didBackup: true,
  index: 1,
  isCold: false,
  canSendTransaction: false,
  nonce: 1,
  funded: false,
  trusted: false,
  trustRequest: false,
  BASE_FEE: 0,
  BASE_FEE_DP: 0,
  PASSPHRASE: ''
}
export default class WalletXLM extends Wallet {
  path = Keystore.CoinType.XLM.path
  type = 'stellar'
  @observable isFetchingBalance = false
  @observable totalBalance = new BigNumber('0')
  @observable isHideValue = false
  @observable enableNotification = true
  @observable isRefresh = false
  @observable transactions = []
  @observable transactionLoaded = false
  @observable selectedAsset = null
  @observable multiple = 1
  balances = []

  constructor(obj, secureDS) {
    super(obj, secureDS)
    this.secureDS = secureDS
    const initObj = Object.assign({}, defaultObjWallet, obj) // copy
    this._validateData(initObj)

    Object.keys(initObj).forEach((k) => {
      if (k === 'balance') initObj[k] = new BigNumber(initObj.balance)
      if (k === 'totalBalance') initObj[k] = new BigNumber(initObj.totalBalance)
      if (k === 'address') initObj[k] = initObj.address

      this[k] = initObj[k]
    })
    this.tokens = [this.getTokenXLM()]
  }

  @action offLoading() {
    this.isFetchingBalance = false
    this.isRefresh = false
    this.loading = false
  }

  @action fetchingBalance(isRefresh = false, isBackground = false) {
    if (this.loading) return null

    console.log(`@fetchingBalance XLM`)

    this.loading = true
    this.isRefresh = isRefresh
    this.isFetchingBalance = !isRefresh && !isBackground

    return api.fetchWalletXLMInfo(this.address)
      .then((res) => {
        const { balances } = res
        const native = balances.find(obj => obj.asset_type === 'native')
        this.balance = new BigNumber(native.balance)
        this.totalBalance = this.balance

        this.update()
        this.offLoading()
      })
      .catch(() => {
        this.offLoading()
      })
  }

  @action async implementPrivateKey(secureDS, privateKey, coin = chainNames.AOM) {
    this.canSendTransaction = true
    this.importType = 'Private Key'
    const { address } = GetAddress(privateKey, coin)
    if (coin === chainNames.AOM && address !== this.address) {
      throw new Error('Invalid Private Key')
    }
    secureDS.savePrivateKey(this.address, privateKey)
  }

  _hashFetchTransaction = false

  @action fetchingTransaction(plus = false) {
    if (this._hashFetchTransaction) {
      return true
    }
    this._hashFetchTransaction = true
    this.transactionLoaded = false

    if (plus) {
      this.multiple += 1
    }

    // Calculate limit
    let limit = this.multiple * 10
    limit = limit > 50 ? 50 : limit

    console.log('@fetchingTransaction XLM')
    return api.fetchXLMTransactions(this.address, limit)
      .then((res) => {
        const result = res.data
        const records = []
        if (!result._embedded) {
          console.log('@fetchingTransaction Empty')
          this._hashFetchTransaction = false
          this.transactionLoaded = true
          return []
        }
        console.log(`@fetchingTransaction Done ${result._embedded.records.length}`)
        const explorerURL = url.Stellar.explorerURL()
        result._embedded.records.forEach((obj) => {
          const asset_code = obj.asset_type === 'native' ? 'XLM' : obj.asset_code
          if (obj.to === this.address && obj.type === 'payment') {
            records.push({
              type: 'receive',
              address: obj.from,
              hash: `${explorerURL}/tx/${obj.transaction_hash}`,
              amount: obj.amount,
              asset_code,
              created_at: obj.created_at
            })
          } else if (obj.account === this.address && obj.type === 'create_account') {
            records.push({
              type: 'create',
              address: obj.funder,
              hash: `${explorerURL}/tx/${obj.transaction_hash}`,
              amount: obj.starting_balance,
              asset_code: 'XLM',
              created_at: obj.created_at
            })
          } else if (obj.from === this.address && obj.type === 'payment') {
            records.push({
              type: 'payment',
              address: obj.to,
              hash: `${explorerURL}/tx/${obj.transaction_hash}`,
              amount: obj.amount,
              asset_code,
              created_at: obj.created_at
            })
          }
        })
        this.transactions = records
        this._hashFetchTransaction = false
        this.transactionLoaded = true
        return records
      })
      .catch((err) => {
        this._hashFetchTransaction = false
        this.transactionLoaded = true
        console.log(`fetchingTransaction ${err.message}`)
        throw err
      })
  }

  async derivePrivateKey() {
    if (!this.secureDS) throw new Error('Secure data source is required')
    // if (this.external) return await this.secureDS.derivePrivateKey(this.address)

    const mnemonic = await this.secureDS.deriveMnemonic()
    const index = this.index === 0 ? 1 : this.index
    const private_key = Stellar.MnemonicToPrivKey(mnemonic, this.path, index)
    return private_key
  }

  getTokenXLM() {
    const tokenXLM = {
      tokenInfo: {
        address: this.address,
        name: 'Stellar',
        symbol: 'XLM',
        decimals: 8,
        price: {
          rate: 1
        }
      },
      balance: this.balance.toString(10)
    }

    return new WalletTokenXLM(tokenXLM, this.address)
  }
}
