import { observable, action, computed } from 'mobx'
import BigNumber from 'bignumber.js'
import Wallet from './Wallet'
import Keystore from '../../../../Libs/react-native-golden-keystore'
import api from '../../../api'
import url from '../../../api/url'
import Helper from '../../../commons/Helper'
import MainStore from '../../MainStore'
import WalletTokenAOM from '../WalletToken.aom'
import GetAddress, { chainNames } from '../../../Utils/WalletAddresses'
import Stellar from '../../../Utils/Stellar'
import Checker from '../../../Handler/Checker'

const defaultObjWallet = {
  ext: 'OM',
  title: '',
  address: '',
  balance: '0',
  type: 'aomtoken',
  path: Keystore.CoinType.AOM.path,
  external: false,
  didBackup: true,
  index: 0,
  isCold: false,
  canSendTransaction: false,
  nonce: 1,
  funded: false,
  trusted: false,
  trustRequest: false,
  BASE_FEE: 0,
  BASE_FEE_DP: 0,
  PASSPHRASE: ''
}
export default class WalletAOM extends Wallet {
  path = Keystore.CoinType.AOM.path
  type = 'aomtoken'
  @observable isFetchingBalance = false
  @observable totalBalance = new BigNumber('0')
  @observable isHideValue = false
  @observable enableNotification = true
  @observable isRefresh = false
  @observable transactions = []
  @observable transactionLoaded = false
  @observable multiple = 1
  @observable selected = {}
  @observable balances = []
  @observable asset = null
  _hashFetchTransaction = false

  constructor(obj, secureDS) {
    super(obj, secureDS)
    this.secureDS = secureDS
    const initObj = Object.assign({}, defaultObjWallet, obj) // copy
    this._validateData(initObj)

    Object.keys(initObj).forEach((k) => {
      if (k === 'balance') initObj[k] = new BigNumber(initObj.balance)
      if (k === 'totalBalance') initObj[k] = new BigNumber(initObj.totalBalance)
      if (k === 'address') initObj[k] = initObj.address

      this[k] = initObj[k]
    })
    this.tokens = [this.getTokenAOM()]
    this.selected = `${this.ext}`
  }

  @action offLoading() {
    this.isFetchingBalance = false
    this.isRefresh = false
    this.loading = false
  }

  @action async fetchingBalance(isRefresh = false, isBackground = false) {
    if (this.loading) return null

    console.log(`@fetchingBalance OM`)

    this.loading = true
    this.isRefresh = isRefresh
    this.isFetchingBalance = !isRefresh && !isBackground

    return Stellar.Balance(this.address).then((res) => {
      if (res !== false) {
        const balances = this.getBalances(res.balances)
        this.balance = new BigNumber(balances.native)
      } else {
        this.funded = false
        this.balance = new BigNumber(`0`)
      }

      this.totalBalance = this.balance
      this.update()
      this.offLoading()
    }).catch(() => {
      this.offLoading()
    })
  }

  @action async implementPrivateKey(secureDS, privateKey, coin = chainNames.AOM) {
    this.canSendTransaction = true
    this.importType = 'Private Key'
    const { address } = GetAddress(privateKey, coin)
    if (coin === chainNames.AOM && address !== this.address) {
      throw new Error('Invalid Private Key')
    }
    secureDS.savePrivateKey(this.address, privateKey)
  }

  @action async multiSignatures() {
    this.loading = true

    const privateKey = await this.derivePrivateKey()

    const { signState, signPair, signKey } = MainStore.appState.multiSign
    const { BASE_FEE } = MainStore.appState.selectedWallet

    if (this.address === signPair && (signState === 0 || signState === 2) && Checker.checkAddressAOM(signKey)) {
      let envelope = ''
      const passphrase = url.OmChain.getPassphrase()
      return Stellar.MultiSignature(Stellar.Server(), privateKey, signKey, BASE_FEE, passphrase)
        .then((res) => {
          console.log(`xdr`, res)
          return res
        })
        .then((pdata) => {
          console.log(`@multiSignatures pdata`, pdata)
          envelope = pdata
          return api.Aom.confirmTransaction(pdata)
        })
        .then((result) => {
          const { data } = result.data
          console.log(`confirmTransaction result`)
          console.log(data)
          if (!result || !result.status) {
            throw new Error(result)
          }
          if (result.status !== 200 || !(data && data.href)) {
            throw new Error(result.data.message ? result.data.message : String(data))
          }

          this.loading = false
          this.fetchingBalance()

          return api.multisignatureUpdate(this.address)
        })
        .catch((error) => {
          console.log(error)
          this.loading = false
          this.fetchingBalance()
          api.Aom.reportBug({
            address: this.address,
            error_component: 'WalletAom',
            error_function: 'multiSignatures',
            error_detail: envelope
          })
          return Promise.reject(error)
        })
    }
    throw new Error('This account has already MultiSignature')
  }

  @action async createTrustLine(issuer, code) {
    this.loading = true

    const privateKey = await this.derivePrivateKey()
    const { BASE_FEE } = this

    const exist = this.balances.find(obj => obj.asset_code === code && obj.asset_issuer === issuer)

    if (exist) {
      console.log(`ท่านมีโทเคน ${code} ในกระเป่าของท่านแล้ว`)
      throw new Error(`ท่านมีโทเคน ${code} ในกระเป่าของท่านแล้ว`)
    } else if (Checker.checkAddressAOM(issuer) && code) {
      let envelope = ''
      const passphrase = url.OmChain.getPassphrase()
      return Stellar.ChangeTrust(Stellar.Server(), privateKey, code.toUpperCase(), issuer, BASE_FEE, passphrase)
        .then((res) => {
          console.log(`xdr`, res)
          return res
        })
        .then((pdata) => {
          console.log(`@createTrustLine pdata`, pdata)
          envelope = pdata
          return api.Aom.confirmTransaction(pdata)
        })
        .then((result) => {
          const { data } = result.data
          console.log(`@createTrustLine result`)
          console.log(data)
          if (!result || !result.status) {
            throw new Error(result)
          }
          if (result.status !== 200 || !(data && data.href)) {
            throw new Error(result.data.message ? result.data.message : String(data))
          }

          this.loading = false
          this.fetchingBalance()

          return {
            issuer,
            code
          }
        })
        .catch((error) => {
          this.loading = false
          this.fetchingBalance()
          api.Aom.reportBug({
            address: this.address,
            error_component: 'WalletAom',
            error_function: 'createTrustLine',
            error_detail: envelope
          })
          throw new Error(error.message)
        })
    }
    throw new Error(`ข้อมูลการโทเคนไม่ถูกต้อง`)
  }

  @action async fetchingTransaction(plus = false) {
    if (this._hashFetchTransaction) {
      return true
    }
    this._hashFetchTransaction = true
    this.transactionLoaded = false

    if (plus) {
      this.multiple += 1
    }

    // Calculate limit
    let limit = this.multiple * 10
    limit = limit > 50 ? 50 : limit

    console.log('@fetchingTransaction OM')
    return Stellar.Transactions(this.address, limit).then((result) => {
      const records = []
      if (!result._embedded) {
        console.log('@fetchingTransaction Empty')
        this._hashFetchTransaction = false
        this.transactionLoaded = true
        return []
      }
      console.log(`@fetchingTransaction Done ${result._embedded.records.length}`)
      result._embedded.records.forEach((obj) => {
        const asset_code = obj.asset_type === 'native' ? 'OM' : obj.asset_code
        if (obj.to === this.address && obj.type === 'payment') {
          records.push({
            type: 'receive',
            address: obj.from,
            hash: url.Aomtoken.explorer(`tx/${obj.transaction_hash}`),
            amount: obj.amount,
            asset_code,
            created_at: obj.created_at
          })
        } else if (obj.from === this.address && obj.type === 'payment') {
          records.push({
            fee: `${this.BASE_FEE_DP} OM`,
            type: 'payment',
            address: obj.to,
            hash: url.Aomtoken.explorer(`tx/${obj.transaction_hash}`),
            amount: obj.amount,
            asset_code,
            created_at: obj.created_at
          })
        } else if (obj.account === this.address && obj.type === 'create_account') {
          records.push({
            type: 'create',
            address: obj.funder,
            hash: url.Aomtoken.explorer(`tx/${obj.transaction_hash}`),
            amount: obj.starting_balance,
            asset_code: 'OM',
            created_at: obj.created_at
          })
        }
      })
      this.transactions = records
      this._hashFetchTransaction = false
      this.transactionLoaded = true
      return records
    }).catch((err) => {
      this._hashFetchTransaction = false
      this.transactionLoaded = true
      console.log(`fetchingTransaction ${err.message}`)
      throw err
    })
  }

  @computed get totalBalanceDollar() {
    const rate = 1
    return this.totalBalanceETH.multipliedBy(rate)
  }

  @computed get balanceText() {
    return Helper.formatETH(this.totalBalance, true, 2)
  }

  @computed get card() {
    const cards = []

    // Check balances
    if (this.balances.length) {
      this.balances.forEach((asset) => {
        if (asset.asset_code) {
          cards.push({
            key: `${asset.asset_code}-${asset.asset_issuer}`,
            ext: asset.asset_code,
            address: this.address,
            type: this.type,
            balance: asset.balance,
            balanceText: Helper.formatETH(asset.balance, true, 2)
          })
        } else {
          cards.push({
            key: `OM`,
            ext: `OM`,
            address: this.address,
            type: this.type,
            balance: asset.balance,
            balanceText: Helper.formatETH(asset.balance, true, 2)
          })
        }
      })
    } else {
      cards.push({
        key: `OM`,
        ext: `OM`,
        address: this.address,
        type: this.type,
        balance: this.balance,
        balanceText: Helper.formatETH(this.balance, true, 2)
      })
    }

    return cards
  }

  get selectedAsset() {
    return this.asset ? this.asset : {
      balance: new BigNumber(0)
    }
  }

  async derivePrivateKey() {
    if (!this.secureDS) throw new Error('Secure data source is required')
    // if (this.external) return await this.secureDS.derivePrivateKey(this.address)

    const mnemonic = await this.secureDS.deriveMnemonic()
    const private_key = Stellar.MnemonicToPrivKey(mnemonic, this.path, this.index)
    return private_key
  }

  getBalances(balances) {
    let amount = 0
    let native = false

    if (this.balances.length) {
      const update = []
      balances.forEach((obj) => {
        const {
          balance, asset_code, asset_issuer, asset_type
        } = obj
        if (asset_type === 'native') {
          native = obj
          update.unshift({
            key: `OM`,
            balance: new BigNumber(balance)
          })
        } else if (obj.asset_code) {
          update.push({
            key: `${asset_code}-${asset_issuer}`,
            balance: new BigNumber(balance),
            asset_code,
            asset_issuer
          })
        }
      })
      this.balances = update
    } else {
      balances.forEach((obj) => {
        const {
          balance, asset_code, asset_issuer, asset_type
        } = obj
        if (asset_type === 'native') {
          native = obj
          this.balances.unshift({
            key: `OM`,
            balance: new BigNumber(balance)
          })
        } else if (obj.asset_code) {
          this.balances.push({
            key: `${asset_code}-${asset_issuer}`,
            balance: new BigNumber(balance),
            asset_code,
            asset_issuer
          })
        }
      })
    }

    if (!this.asset) {
      this.asset = this.balances.find(item => !item.asset_code)
    } else {
      this.asset = this.balances.find(item => this.asset.asset_code === item.asset_code)
    }

    if (native) {
      amount = native.balance
      this.funded = true
      this.canSendTransaction = true
    }

    return {
      native: amount
    }
  }

  getTokenAOM() {
    const tokenAOM = {
      tokenInfo: {
        address: this.address,
        name: 'OM',
        symbol: 'XML',
        decimals: 8,
        price: {
          rate: 1
        }
      },
      balance: this.balance.toString(10)
    }

    return new WalletTokenAOM(tokenAOM, this.address)
  }
}
