import { observable, action, computed } from 'mobx'
import BigNumber from 'bignumber.js'
import Wallet from './Wallet'
import Keystore from '../../../../Libs/react-native-golden-keystore'
import api from '../../../api'
import url from '../../../api/url'
import Helper from '../../../commons/Helper'
import MainStore from '../../MainStore'
import WalletTokenBTC from '../WalletToken.btc'
import GetAddress, { chainNames } from '../../../Utils/WalletAddresses'

const defaultObjWallet = {
  ext: 'BTC',
  title: '',
  address: '',
  balance: '0',
  type: 'bitcoin',
  path: Keystore.CoinType.BTC.path,
  external: false,
  didBackup: true,
  index: 0,
  isCold: false,
  canSendTransaction: true,
  nonce: 1
}
export default class WalletBTC extends Wallet {
  path = Keystore.CoinType.BTC.path
  type = 'bitcoin'
  @observable isFetchingBalance = false
  @observable totalBalance = new BigNumber('0')
  @observable isHideValue = false
  @observable enableNotification = true
  @observable isRefresh = false

  @observable transactions = []
  @observable transactionLoaded = false
  @observable multiple = 1
  _hashFetchTransaction = false
  balances = []

  constructor(obj, secureDS) {
    super(obj, secureDS)
    this.secureDS = secureDS
    const initObj = Object.assign({}, defaultObjWallet, obj) // copy
    this._validateData(initObj)

    Object.keys(initObj).forEach((k) => {
      if (k === 'balance') initObj[k] = new BigNumber(initObj.balance)
      if (k === 'totalBalance') initObj[k] = new BigNumber(initObj.totalBalance)
      if (k === 'address') initObj[k] = initObj.address

      this[k] = initObj[k]
    })
    this.tokens = [this.getTokenBTC()]
  }

  @action offLoading() {
    this.isFetchingBalance = false
    this.isRefresh = false
    this.loading = false
  }

  @action async fetchingBalance(isRefresh = false, isBackground = false) {
    if (this.loading) return
    console.log('@fetchingBalance BTC')
    this.loading = true
    this.isRefresh = isRefresh
    this.isFetchingBalance = !isRefresh && !isBackground
    try {
      const res = await api.fetchWalletBTCInfo(this.address)
      if (res.status !== 200) {
        this.offLoading()
        if (this.balance.toString(10) > 0) return
        this.balance = new BigNumber(`0`)
        this.totalBalance = this.balance
      } else if (res.data) {
        const { final_balance } = res.data
        this.balance = new BigNumber(final_balance).dividedBy(1e+8)
        this.totalBalance = this.balance
      } else {
        this.balance = new BigNumber(`0`)
        this.totalBalance = this.balance
      }

      this.update()
      this.offLoading()
    } catch (e) {
      this.offLoading()
    }
  }

  @action async implementPrivateKey(secureDS, privateKey, coin = chainNames.BTC) {
    this.canSendTransaction = true
    this.importType = 'Private Key'
    const { address } = GetAddress(privateKey, coin)
    if (coin === chainNames.BTC && address !== this.address) {
      throw new Error('Invalid Private Key')
    }
    secureDS.savePrivateKey(this.address, privateKey)
  }

  @action async fetchingTransaction(plus = false) {
    console.log('@fetchingTransaction BTC')
    if (this._hashFetchTransaction) {
      return true
    }
    this._hashFetchTransaction = true
    this.transactionLoaded = false

    if (plus) {
      this.multiple += 1
    }

    // Calculate limit
    let limit = this.multiple * 10
    limit = limit > 50 ? 50 : limit

    return api.fetchBTCTransactions(this.address).then((data) => {
      // eslint-disable-next-line no-param-reassign
      // result.data = result.data.data
      const currentBlock = data.currentBlock.data

      const result = {}
      result.data = data.transaction.data
      const records = []
      if (result.data.n_tx === 0) {
        console.log('@fetchingTransaction Empty')
        this._hashFetchTransaction = false
        this.transactionLoaded = true
        return []
      }

      console.log(`@fetchingTransaction Done ${result.data.txs.length}`)
      const explorerURL = url.BlockChainInfo.webURL()
      result.data.txs.forEach((obj, i) => {
        if (i + 1 <= limit) {
          // Convert timestamp Unix to ISO format
          // eslint-disable-next-line no-param-reassign
          obj.time = new Date(obj.time * 1000).toISOString()
          const asset_code = 'BTC'

          let numberConfirm = 0
          if (obj.block_height) {
            numberConfirm = currentBlock - obj.block_height + 1
          }
          // Check if user are sender
          let isSender = false
          for (let x = 0; x < obj.inputs.length; x++) {
            // console.log(obj.inputs[x].prev_out.addr === this.address)
            if (obj.inputs[x].prev_out.addr === this.address) {
              isSender = true
            }
          }

          const value = BigNumber(obj.out[0].value)
          if (!isSender) {
            records.push({
              type: numberConfirm > 5 ? 'receive' : 'waitingReceive',
              address: obj.inputs[0].prev_out.addr,
              hash: `${explorerURL}/btc/tx/${obj.hash}`,
              amount: value.dividedBy(1e+8).toNumber(),
              asset_code,
              created_at: obj.time,
              numberConfirm
            })
          } else {
            let totalInput = 0
            for (let x = 0; x < obj.inputs.length; x++) {
              totalInput += obj.inputs[x].prev_out.value
            }

            let totalOutput = 0
            for (let x = 0; x < obj.out.length; x++) {
              totalOutput += obj.out[x].value
            }

            const fee = new BigNumber(totalInput - totalOutput).dividedBy(1e+8)
            records.push({
              fee: `${fee.toNumber()} BTC`,
              type: numberConfirm > 5 ? 'payment' : 'waitingPayment',
              address: obj.inputs[0].prev_out.addr,
              hash: `${explorerURL}/btc/tx/${obj.hash}`,
              amount: value.dividedBy(1e+8).toNumber(),
              asset_code,
              created_at: obj.time,
              numberConfirm
            })
          }
        }
      })

      this.transactions = records
      this._hashFetchTransaction = false
      this.transactionLoaded = true
      return records
    }).catch((err) => {
      this._hashFetchTransaction = false
      this.transactionLoaded = true
      console.log(`fetchingTransaction ${err.message}`)
      throw err
    })
  }

  @computed get totalBalanceDollar() {
    const rate = MainStore.appState.rateBTCDollar
    return this.totalBalanceETH.multipliedBy(rate)
  }

  getTokenBTC() {
    const tokenBTC = {
      tokenInfo: {
        address: this.address,
        name: 'Bitcoin',
        symbol: 'BTC',
        decimals: 8,
        price: {
          rate: MainStore.appState.rateBTCDollar.toString(10)
        }
      },
      balance: this.balance.toString(10)
    }

    return new WalletTokenBTC(tokenBTC, this.address)
  }
}
