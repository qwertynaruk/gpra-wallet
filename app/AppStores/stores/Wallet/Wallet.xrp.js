import { observable, action, computed } from 'mobx'
import BigNumber from 'bignumber.js'
import Wallet from './Wallet'
import Keystore from '../../../../Libs/react-native-golden-keystore'
import api from '../../../api'
import url from '../../../api/url'
import MainStore from '../../MainStore'
import WalletTokenXRP from '../WalletToken.xrp'
import GetAddress, { chainNames } from '../../../Utils/WalletAddresses'

const defaultObjWallet = {
  ext: 'XRP',
  title: '',
  address: '',
  balance: '0',
  type: 'ripple',
  path: Keystore.CoinType.XRP.path,
  external: false,
  didBackup: true,
  index: 0,
  isCold: false,
  canSendTransaction: true,
  nonce: 1
}
export default class WalletXRP extends Wallet {
  path = Keystore.CoinType.XRP.path
  type = 'ripple'
  @observable isFetchingBalance = false
  @observable totalBalance = new BigNumber('0')
  @observable isHideValue = false
  @observable enableNotification = true
  @observable isRefresh = false

  @observable transactions = []
  @observable transactionLoaded = false
  @observable multiple = 1
  _hashFetchTransaction = false
  balances = []

  constructor(obj, secureDS) {
    super(obj, secureDS)
    this.secureDS = secureDS
    const initObj = Object.assign({}, defaultObjWallet, obj) // copy
    this._validateData(initObj)

    Object.keys(initObj).forEach((k) => {
      if (k === 'balance') initObj[k] = new BigNumber(initObj.balance)
      if (k === 'totalBalance') initObj[k] = new BigNumber(initObj.totalBalance)
      if (k === 'address') initObj[k] = initObj.address

      this[k] = initObj[k]
    })
    this.tokens = [this.getTokenXRP()]
  }

  @action offLoading() {
    this.isFetchingBalance = false
    this.isRefresh = false
    this.loading = false
  }

  @action async fetchingBalance(isRefresh = false, isBackground = false) {
    console.log('@fetchingBalance XRP')

    if (this.loading) return null

    this.loading = true
    this.isRefresh = isRefresh
    this.isFetchingBalance = !isRefresh && !isBackground
    return api.fetchWalletXRPInfo(this.address)
      .then((res) => {
        if (res.data.result === 'success') {
          this.balance = new BigNumber(`${res.data.balances[0].value}`)
          this.totalBalance = this.balance
          this.update()
        } else if (res.data.result === 'error') {
          this.balance = new BigNumber(0)
          this.totalBalance = this.balance
          this.update()
        }

        this.offLoading()
      })
      .catch((err) => {
        console.log(err)
        this.offLoading()
      })
  }

  @action async implementPrivateKey(secureDS, privateKey, coin = chainNames.XRP) {
    this.canSendTransaction = true
    this.importType = 'Private Key'
    const { address } = GetAddress(privateKey, coin)
    if (coin === chainNames.XRP && address !== this.address) {
      throw new Error('Invalid Private Key')
    }
    secureDS.savePrivateKey(this.address, privateKey)
  }

  @action async fetchingTransaction(plus = false) {
    console.log('@fetchingTransaction XRP')

    if (this._hashFetchTransaction) {
      return true
    }
    this._hashFetchTransaction = true
    this.transactionLoaded = false

    if (plus) {
      this.multiple += 1
    }

    // Calculate limit
    let limit = this.multiple * 10
    limit = limit > 50 ? 50 : limit

    const data = {
      descending: true,
      limit
    }

    return api.fetchXRPTransactions(this.address, data).then((result) => {
      const records = []
      if (result.data.count === 0) {
        console.log('@fetchingTransaction Empty')
        this._hashFetchTransaction = false
        this.transactionLoaded = true
        return []
      }

      console.log(`@fetchingTransaction Done ${result.data.transactions.length}`)
      const explorerURL = url.Ripple.explorerURL()
      result.data.transactions.forEach((obj) => {
        console.log(obj)
        const asset_code = 'XRP'
        if (obj.meta.TransactionResult === 'tesSUCCESS' && obj.tx.Destination === this.address) {
          records.push({
            type: 'receive',
            address: obj.tx.Account,
            hash: `${explorerURL}/${obj.hash}`,
            amount: new BigNumber(obj.tx.Amount).dividedBy(1e+6),
            asset_code,
            created_at: obj.date
          })
        } else if (obj.meta.TransactionResult === 'tesSUCCESS' && obj.tx.Account === this.address) {
          const fee = new BigNumber(obj.tx.Fee).dividedBy(1e+6).toNumber()
          records.push({
            fee: `${fee} XRP`,
            type: 'payment',
            address: obj.tx.Destination,
            hash: `${explorerURL}/${obj.hash}`,
            amount: new BigNumber(obj.tx.Amount).dividedBy(1e+6),
            asset_code,
            created_at: obj.date
          })
        }
      })

      this.transactions = records
      this._hashFetchTransaction = false
      this.transactionLoaded = true
      return records
    }).catch((err) => {
      this._hashFetchTransaction = false
      this.transactionLoaded = true
      console.log(`fetchingTransaction ${err.message}`)
      throw err
    })
  }

  @computed get totalBalanceDollar() {
    const rate = MainStore.appState.rateXRPDollar
    return this.totalBalanceETH.multipliedBy(rate)
  }

  // May get from local and decrypt or from mnemonic
  async derivePrivateKey() {
    if (!this.secureDS) throw new Error('Secure data source is required')

    const mnemonic = await this.secureDS.deriveMnemonic()
    return mnemonic
  }

  getTokenXRP() {
    const tokenXRP = {
      tokenInfo: {
        address: this.address,
        name: 'Ripple',
        symbol: 'XRP',
        decimals: 6,
        price: {
          rate: MainStore.appState.rateXRP.toString(10)
        }
      },
      balance: this.balance.toString(10)
    }

    return new WalletTokenXRP(tokenXRP, this.address)
  }
}
