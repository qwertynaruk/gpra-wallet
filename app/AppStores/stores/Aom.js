import { observable, action } from 'mobx'
import DeviceInfo from 'react-native-device-info'
import MainStore from '../MainStore'
import api from '../../api'

class Aom {
  @observable.ref latestVersion = {
    version_number: DeviceInfo.getVersion(),
    change_logs: '',
    created_at: '2018-08-23T11:41:25.828Z'
  }
  @observable listVersion = []

  @action setChangelogsList(vs) {
    const data = this.listVersion.map((item) => {
      const temp = item.expanse
      return {
        ...item,
        expanse: item.version_number === vs ? !temp : temp
      }
    })

    this.listVersion = data
  }

  @action async getChangelogsLatest() {
    if (MainStore.appState.internetConnection === 'online') {
      const res = await api.changelogsLatest()
      if (res && res.data) {
        this.latestVersion = res.data.data
      }
    }
  }

  @action async getChangelogsList() {
    if (MainStore.appState.internetConnection === 'online') {
      const res = await api.changelogsList()
      if (res && res.data) {
        const data = res.data.data.map((item) => {
          return {
            ...item,
            expanse: false,
            type: 'expanse'
          }
        })
        this.listVersion = data
      }
    }
  }
}

export default new Aom()
