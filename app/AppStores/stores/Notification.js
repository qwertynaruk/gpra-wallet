import { observable, action, computed } from 'mobx'
import { Platform } from 'react-native'
import WalletToken from './WalletToken'
import MainStore from '../MainStore'
import NavStore from '../../AppStores/NavStore'
import NotificationDS from '../DataSource/NotificationDS'
import API from '../../api'
import Transaction from '../../AppStores/stores/Transaction'
import TransactionBTC from '../../AppStores/stores/Transaction.btc'
import TransactionLTC from '../../AppStores/stores/Transaction.ltc'
import TransactionDOGE from '../../AppStores/stores/Transaction.doge'
import AppState from '../AppState'

class Notification {
  @observable currentNotifyList = null
  @observable currentNotif = null
  @observable tokenFromNotif = null
  @observable receiveVerify = false

  @observable notifyBadgeCount = 0
  @observable listItem = []

  isInitFromNotification = false
  isOpenFromTray = false
  lastedWalletAddress = null
  lastedTokenAddress = null
  deviceToken = null
  appState = 'active'

  get shouldShowNotifInApp() {
    const { notif, isInitFromNotification, appState } = this
    const { currentRouteName } = NavStore
    if (currentRouteName === '' || currentRouteName === 'UnlockScreen') {
      return false
    }
    if (!notif) {
      return false
    }
    if (isInitFromNotification) {
      return false
    }
    if (appState === 'active') {
      // return notif.from || notif.inputs
      return true
    }
    return false
  }

  getSymbol(type) {
    switch (type) {
      case 'ethereum': return 'ETH'
      case 'bitcoin': return 'BTC'
      case 'litecoin': return 'LTC'
      case 'dogecoin': return 'DOGE'
      case 'stellar': return 'XLM'
      case 'aomtoken': return 'OM'
      default: return 'BTC'
    }
  }

  saveNotifID(address, id) {
    NotificationDS.saveNotifID(address, id)
  }

  async getNotifID(address) {
    return await NotificationDS.getNotifID(address)
  }

  addWallet(name, address, type) {
    if (!this.deviceToken) {
      return null
    }

    return API.addWallet(name, address, this.deviceToken, type).then((res) => {
      const { data, success } = res.data
      if (success) {
        this.saveNotifID(address, data.id)
      }
      return res.data
    })
  }

  updateWallets() {
    if (!this.deviceToken) {
      return null
    }
    const wallets = MainStore.appState.wallets.filter(w => w.enableNotification).map((w) => {
      return {
        name: w.title,
        address: w.address,
        type: this.getSymbol(w.type)
      }
    })
    if (!wallets.length) {
      return null
    }
    return API.updateWallets(wallets, this.deviceToken).then((res) => {
      const { data, success } = res.data
      if (success) {
        data.wallets.forEach((d) => {
          this.saveNotifID(d.address, d.id)
        })
      }
      return res.data.success
    })
  }

  onNotif() {
    return API.onNotification(this.deviceToken)
  }

  offNotif() {
    return API.offNotification(this.deviceToken)
  }

  async removeWallet(address) {
    const id = await this.getNotifID(address)
    if (!id) {
      return null
    }
    return API.removeWallet(id)
  }

  async removeWallets() {
    const removeWalletsPromise = MainStore.appState.wallets.map(w => this.removeWallet(w.address))
    return Promise.all(removeWalletsPromise)
  }

  async editWalletName(name, address) {
    await this.removeWallet(address)
    this.addWallet(name, address).then((res) => { })
  }

  checkExistedWallet(address) {
    const wallet = MainStore.appState.wallets.find(w => w.address === address)
    if (wallet) {
      return true
    }
    return false
  }

  getWallet(address) {
    return MainStore.appState.wallets.find(w => w.address === address)
  }

  @action setCurrentNotif = (notif) => { this.currentNotif = notif }
  @action setDeviceToken = (dt) => { this.deviceToken = dt }
  @action setTokenFromNotif = () => {
    if (!this.notif) {
      return
    }

    MainStore.appState.setSelectedTransaction(null)
    if (this.walletType === 'ETH') {
      const { address, contract } = this.notif
      WalletToken.fetchTokenDetail(address, contract).then(async (token) => {
        const wallet = MainStore.appState.wallets
          .find(w => w.address.toLowerCase() === address.toLowerCase())
        if (!wallet) {
          return
        }
        const transaction = new Transaction(this.notif, token, this.notif.status)
        MainStore.appState.setSelectedTransaction(transaction)
      })
    } else if (this.walletType === 'BTC') {
      const transaction = new TransactionBTC(this.notif)
      MainStore.appState.setSelectedTransaction(transaction)
    } else if (this.walletType === 'LTC') {
      const transaction = new TransactionLTC(this.notif)
      MainStore.appState.setSelectedTransaction(transaction)
    } else if (this.walletType === 'DOGE') {
      const transaction = new TransactionDOGE(this.notif)
      MainStore.appState.setSelectedTransaction(transaction)
    } else if (this.walletType === 'AOM') {
      const transaction = new TransactionDOGE(this.notif)
      MainStore.appState.setSelectedTransaction(transaction)
    }
  }

  @action receivedNotification() {
    if (!this.notif) return
    if (this.notif && !this.checkExistedWallet(this.notif.address)) {
      NavStore.popupCustom.show('Wallet not Found')
    }
  }

  @action gotoTransaction() {
    if (!this.notif) return
    this.setTokenFromNotif()
    if (this.notif && !this.checkExistedWallet(this.notif.address)) {
      NavStore.popupCustom.show('Wallet not Found')
      return
    }
    if (this.walletType === 'ETH') {
      NavStore.pushToScreen('TransactionDetailScreen', { fromNotif: true })
    } else {
      NavStore.pushToScreen('TransactionBTCDetailScreen', { fromNotif: true })
    }
  }

  _fetchAll() {
    const wallet = MainStore.appState.selectedWallet
    wallet.transactions = []
    wallet.fetchingBalance()
    wallet.fetchingTransaction()
  }

  @action addNotifyItem(data) {
    const item = data
    if (!item.body) {
      return
    }
    const map = {
      'lot-in': 'iconAomWin',
      'aom-buy': 'iconAomWin',
      'aom-in': 'iconReceived',
      'aom-init': 'iconAomWin',
      'aom-info': 'iconInfo',
      'trust-requset': 'iconInfo',
      'msign-request': 'iconInfo',
      'aom-out': 'iconAomOut',
      'om-in': 'iconReceived',
      'om-out': 'iconAomOut',
      'om-buy': 'iconAomWin',
      error: 'iconHighPriority'
    }
    if (!(item.icon && map[item.icon])) {
      item.icon = map[item.tag] ? map[item.tag] : 'iconInfo'
    }
    if (!(this.listItem.find(obj => obj.id === item.id))) {
      this.listItem.unshift(item)
    }
  }

  @action fetchNotifyList(address) {
    console.debug('@fetchNotifyList')
    API.Aom.getNotifyList(address)
      .then((res) => {
        res.reverse().map((r) => {
          if (r.t != 'aom-out' || r.t != 'om-out') {
            this.addNotifyItem({
              id: Number(r.i),
              tag: r.t,
              time: r.d,
              body: r.b
            })
          }
          return r
        })
      }).catch((r) => {
        console.error(r)
      })
  }

  @action clearNotifyBadgeCount() {
    if (this.notifyBadgeCount > 0 && MainStore.appState.selectedWallet) {
      this.notifyBadgeCount = 0
      API.Aom.clearNotifyRead(MainStore.appState.selectedWallet.address)
    }
    return !this.notifyBadgeCount
  }

  @action executeNotification() {
    const { notif, shouldShowNotifInApp } = this
    const wallet = MainStore.appState.selectedWallet

    const data = notif.data && typeof notif.data === 'string' ? JSON.parse(notif.data) : {}
    const event = notif.event || 'false'
    let customEvent = null
    let customContent = null

    if (!(notif && wallet)) {
      return
    }

    if ((event === 'aom-in' || event === 'aom-buy' || event === 'om-in' || event === 'om-buy') && !wallet.funded) {
      wallet.funded = true
      wallet.signkey = notif.sign_id
      this._fetchAll()
      if (shouldShowNotifInApp) {
        NavStore.pushToScreen('DashboardStack')
      }
      customEvent = 'aom-info'
      customContent = 'Trust OM ในกระเป๋าของคุณ เพื่อรับเหรียญ'
    } else if ((event === 'aom-in' || event === 'om-in') || (event === 'aom-update' || event === 'om-update')) {
      this._fetchAll()
    } else if (event === 'trust-requset') {
      this._fetchAll()
    } else if (event === 'msign-request') {
      wallet.multisign = false
      wallet.signkey = notif.sign_id
      this._fetchAll()

      // prevent complatable
    } else if (event === 'om-verify') {
      customEvent = 'aom-info'
      this.receiveVerify = true
      this._fetchAll()
      if (shouldShowNotifInApp) {
        NavStore.reset()
      }
      AppState.aomInfo()
    } else if (event === 'om-approved') {
      customEvent = 'aom-info'
      MainStore.appState.setKycLayerTwo(true)
      MainStore.appState.setKycApproved(true)
      if (shouldShowNotifInApp) {
        NavStore.reset()
      }
      AppState.aomInfo()
    } else if (event === 'om-reject') {
      customEvent = 'aom-info'
      MainStore.appState.setKycLayerTwo(false)
      MainStore.appState.setKycApproved(false)
      if (shouldShowNotifInApp) {
        NavStore.reset()
      }
      AppState.aomInfo()
    } else if (notif.transaction) {
      this._fetchAll()
    }

    if (event != 'false') {
    //   this.addNotifyItem({
    //     id: new Date().getTime(),
    //     tag: customEvent || event,
    //     time: moment().format(),
    //     body: customContent || notif.content
    //   })
      this.notifyBadgeCount++
    }

    this.fetchNotifyList(wallet.address)
    if (shouldShowNotifInApp && notif.content) {
      NavStore.showToastTop(notif.content)
    }
  }

  @computed get notif() {
    const { currentNotif } = this
    if (!currentNotif) {
      return null
    }

    // const tx = JSON.parse(currentNotif.tx)
    const content = Platform.OS === 'ios'
      ? currentNotif._body
      : currentNotif._body
    const data = currentNotif._data

    return Object.assign(
      {},
      currentNotif,
      {
        content,
        address: data.address,
        contract: data.contract,
        data: data.data,
        event: data.event
      }
    )
  }

  @computed get walletType() {
    const { currentNotif } = this
    if (!currentNotif) return null
    return currentNotif.walletType
  }
}

export default new Notification()
