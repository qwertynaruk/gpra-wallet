// Third-party
import { observable, action } from 'mobx'

// Components & Modules
import icons from '../../commons/icons'
import AssetDS from '../DataSource/AssetDS'

export default class AssetStore {
  @observable assetList = [
    {
      key: 'btc',
      symbol: 'BTC',
      type: 'bitcoin',
      name: 'Bitcoin',
      icons: {
        active: icons.iconBuyBTC,
        deactive: icons.iconAltBuyBTC
      },
      networks: [
        {
          key: 'mainnet',
          name: 'Mainnet'
        },
        {
          key: 'testnet',
          name: 'Testnet'
        }
      ]
    },
    {
      key: 'eth',
      symbol: 'ETH',
      type: 'ethereum',
      name: 'Ethereum',
      icons: {
        active: icons.iconBuyETH,
        deactive: icons.iconAltBuyETH
      },
      networks: [
        {
          key: 'mainnet',
          name: 'Mainnet'
        },
        {
          key: 'kovan',
          name: 'Kovan'
        },
        {
          key: 'rinkeby',
          name: 'Rinkeby'
        },
        {
          key: 'ropsten',
          name: 'Ropsten'
        }
      ]
    },
    {
      key: 'xrp',
      symbol: 'XRP',
      type: 'ripple',
      name: 'Ripple',
      icons: {
        active: icons.iconBuyXRP,
        deactive: icons.iconAltBuyXRP
      },
      networks: [
        {
          key: 'mainnet',
          name: 'Mainnet'
        },
        {
          key: 'testnet',
          name: 'Testnet'
        }
      ]
    },
    {
      key: 'xlm',
      symbol: 'XLM',
      type: 'stellar',
      name: 'Stellar',
      icons: {
        active: icons.iconBuyXLM,
        deactive: icons.iconAltBuyXLM
      },
      networks: [
        {
          key: 'mainnet',
          name: 'Mainnet'
        },
        {
          key: 'testnet',
          name: 'Testnet'
        }
      ]
    }
  ]

  @action async getAsset() {
    return await AssetDS.getAssets().then((asset) => {
      return asset
    })
  }

  async save(type) {
    return AssetDS.addNewAsset(type)
  }

  async delete(type) {
    return AssetDS.deleteAsset(type)
  }
}
