import { observable } from 'mobx'

export default class RemoteConfig {
  @observable values = {}

  setValue(key, value) {
    this.values[key] = value
  }

  getValue(key) {
    return this.values[key]
  }
}
