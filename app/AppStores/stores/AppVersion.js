
import { Platform } from 'react-native'
import { observable, action } from 'mobx'
import DeviceInfo from 'react-native-device-info'
import MainStore from '../MainStore'
import api from '../../api'

class AppVersion {
  @observable.ref latestVersion = {
    platform: '',
    version: DeviceInfo.getVersion(),
    build: DeviceInfo.getBuildNumber()
  }

  @action async getLastest() {
    if (MainStore.appState.internetConnection === 'online') {
      const res = await api.Aom.fetchAppVersion(Platform.OS)
      if (res && res.data && res.status === 200) {
        this.latestVersion = res.data
      }
    }
  }
}

export default new AppVersion()
