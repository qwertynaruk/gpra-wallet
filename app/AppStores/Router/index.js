import SendAom from './SendAom'
import ChangePinCode from './ChangePinCode'
import Backup from './Backup'

export default {
  SendAom,
  ChangePinCode,
  Backup
}
