import MainStore from '../MainStore'
import SendStore from '../../modules/SendAom/stores/SendStore'
import NavStore from '../NavStore'

class SendAom {
  goToSendAom() {
    MainStore.sendTransaction = new SendStore()
    NavStore.pushToScreen('SendAomStack')
  }
}

export default new SendAom()
