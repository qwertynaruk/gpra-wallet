export default class AomJob {
  appState = null
  jobId = null

  constructor(appState) {
    this.appState = appState
  }

  init() {
    this.appState.aomInfo()
  }
}
