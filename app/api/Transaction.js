import caller from './api-caller'
import appState from '../AppStores/AppState'
import URL from './url'
/**
 *
 * @param {String} addressStr
 * @param {Object} data
 */
export const fetchTransactions = (addressStr, data, page = 1) => {
  const url = URL.EtherScan.apiURL(appState.networkName)
  const params = data
  params.page = page

  if (!addressStr) return Promise.reject()
  return caller.get(url, params, true)
}

export const fetchBTCTransactions = async (addressStr) => {
  // const url = URL.BlockChainInfo.apiURL()
  // const params = data

  // const url = `${URL.ChainSo.apiURL()}/address/BTC/${addressStr}`
  const url1 = `${URL.BlockChainInfo.apiURL()}/rawaddr/${addressStr}`
  const url2 = `${URL.BlockChainInfo.apiURL()}/q/getblockcount`

  if (!addressStr) return Promise.reject()

  const transaction = await caller.get(url1, {}, true)
  const currentBlock = await caller.get(url2, {}, false)
  const result = {
    transaction,
    currentBlock
  }

  return result
}

export const fetchXRPTransactions = (addressStr, data) => {
  const url = `${URL.Ripple.apiURL()}/accounts/${addressStr}/transactions`
  const params = data

  if (!addressStr) return Promise.reject()
  return caller.get(url, params, true)
}

export const fetchStellarTransactions = (addressStr, data) => {
  const url = URL.OmChain.apiURL()
  const params = data

  if (!addressStr) return Promise.reject()
  return caller.get(url, params, true)
}

/**
 *
 * @param {String} txHash
 */
export const checkStatusTransaction = (txHash) => {
  const url = URL.EtherScan.apiURL(appState.networkName)
  const apikey = 'SVUJNQSR2APDFX89JJ1VKQU4TKMB6W756M'
  const params = {
    module: 'transaction',
    action: 'gettxreceiptstatus',
    txHash,
    apikey
  }
  return caller.get(url, params, true)
}

export const fetchGasPrice = () => caller.get(`${URL.EthGasStation.apiURL()}/json/ethgasAPI.json`)

export const checkTxHasBeenDroppedOrFailed = (txHash) => {
  const url = `${URL.EtherScan.webURL(appState.networkName)}/tx/${txHash}`

  return caller.get(url)
    .then((res) => {
      if (res.data && typeof res.data === 'string') {
        const htmlString = res.data

        const removed = htmlString.includes('Dropped&Replaced') ||
          htmlString.includes('Dropped') ||
          htmlString.includes('Replaced') ||
          htmlString.includes('<font color="red">Fail</font>')

        const notBroadCast = htmlString.includes('we are unable to locate this Transaction Hash')
        if (notBroadCast) {
          return 'notBroadCast'
        }

        return removed
      }
      return true
    })
}

export const getTxID = address => caller.get(`${URL.BlockChainInfo.apiURL()}/unspent?active=${address}`)

export const getTxIDLTC = address => caller.get(`${URL.ChainSo.apiURL()}/get_tx_unspent/LTC/${address}`)

export const getTxIDDOGE = address => caller.get(`${URL.ChainSo.apiURL()}/get_tx_unspent/DOGE/${address}`)

export const pushTxBTC = (rawTx) => {
  const data = {
    tx: rawTx
  }
  return caller.post(`${URL.BlockChainInfo.apiURL()}/pushtx`, data, false)
}

export const pushTxLTC = (rawTx) => {
  const data = { tx_hex: rawTx }
  return caller.post(`${URL.ChainSo.apiURL()}/send_tx/LTC`, data, false)
}

export const getTxDetailLtc = txId => caller.get(`${URL.ChainSo.apiURL()}/tx/LTC/${txId}`)

export const pushTxDOGE = (rawTx) => {
  const data = { tx_hex: rawTx }
  return caller.post(`${URL.ChainSo.apiURL()}/send_tx/DOGE`, data, false)
}

export const getTxDetailDOGE = txId => caller.get(`${URL.ChainSo.apiURL()}/tx/DOGE/${txId}`)

let _assetAom = false
export const getAssetAOM = () => {
  if (_assetAom) {
    return Promise.resolve(_assetAom)
  }
  return caller.get(`${URL.Aomtoken.apiURL()}/info`).then(($res) => {
    if ($res.status == 200) {
      _assetAom = $res
    }
    return $res
  })
}

export const getKYCAOM = (address) => {
  const data = { address }
  return caller.post(`${URL.Aomtoken.apiURL()}/kyc/user/layer/check`, data, true)
}

export const getKYCProfile = (address) => {
  const data = { address }
  return caller.post(`${URL.Aomtoken.apiURL()}/kyc/profile`, data, true)
}

export const getAOMWalletInfo = (address) => {
  const data = { address }
  return caller.post(`${URL.Aomtoken.apiURL()}/wallet/info`, data, true)
}
