import StellarSdk from '@pigzbe/react-native-stellar-sdk'
import { RippleAPI } from 'ripple-lib'

const _url = {
  stellar: 'https://prenet.omchain.network',
  stellarNetwork: 'omtoken',
  stellarPassphrase: 'Omplatform Stellar Network ; April 2019'
}

export default {
  EtherScan: {
    apiURL: network => ((network === 'mainnet')
      ? `https://api.etherscan.io/api`
      : `https://api-${network}.etherscan.io/api`),
    webURL: network => ((network === 'mainnet')
      ? `https://etherscan.io`
      : `https://${network}.etherscan.io`)
  },
  CryptoCompare: {
    apiURL: () => `https://min-api.cryptocompare.com`
  },
  EthGasStation: {
    apiURL: () => `https://ethgasstation.info`
  },
  Skylab: {
    apiURL: () => `http://wallet.skylab.vn`,
    privacyURL: () => `https://goldenwallet.io/privacy`,
    termsURL: () => `https://goldenwallet.io/terms`
  },
  OpenSea: {
    apiURL: () => `https://opensea-api.herokuapp.com`
  },
  BlockExplorer: {
    apiURL: () => `https://blockexplorer.com`
  },
  BlockChainInfo: {
    apiURL: () => `https://blockchain.info`,
    webURL: () => `https://www.blockchain.com`
  },
  Ripple: {
    apiURL: testnet => (testnet
      ? `https://testnet.data.api.ripple.com/v2`
      : `https://data.ripple.com/v2`),
    explorerURL: testnet => (testnet
      ? `https://test.bithomp.com/explorer`
      : `https://bithomp.com/explorer`),
    rippleAPI: (testnet) => {
      if (testnet) {
        // TESTNET
        return new RippleAPI({ server: 'wss://s.altnet.rippletest.net:51233' })
      }
      // MAINNET
      return new RippleAPI({ server: 'wss://s1.ripple.com' })
    }
  },
  ChainSo: {
    apiURL: () => `https://chain.so/api/v2`
  },
  BlockCypher: {
    webURL: () => `https://live.blockcypher.com`
  },
  Lottery: {
    apiURL: () => `https://api.omplatform.com/api/lottery/v1`
  },
  Stellar: {
    horizonURL: testnet => (testnet
      ? `https://horizon-testnet.stellar.org`
      : `https://horizon.stellar.org`),
    apiURL: (testnet) => {
      if (testnet) {
        StellarSdk.Network.useTestNetwork()
        return new StellarSdk.Server('https://horizon-testnet.stellar.org')
      }

      StellarSdk.Network.usePublicNetwork()
      return new StellarSdk.Server('https://horizon.stellar.org')
    },
    explorerURL: testnet => (testnet
      ? `https://stellar.expert/explorer/testnet`
      : `https://stellar.expert/explorer/public`)
  },
  OmChain: {
    setNetwork: (endpoint, network, passp) => {
      console.log(`set endpoint ${endpoint}, ${network}, ${passp}`)
      if (endpoint) {
        _url.stellar = endpoint
      }

      if (network) {
        _url.stellarNetwork = network
      }

      if (passp) {
        _url.stellarPassphrase = passp
      }
    },
    signType: () => _url.stellarNetwork,
    fetchUrl() {
      StellarSdk.Network.usePublicNetwork()
      return _url.stellar
    },
    apiURL: () => {
      StellarSdk.Network.use(new StellarSdk.Network(_url.stellarPassphrase))
      return _url.stellar
    },
    getPassphrase: () => _url.stellarPassphrase
  },
  Aomtoken: {
    api: endPoint => `https://api.omplatform.com/api/stellar/v1${endPoint || ''}`,
    apiV2: endPoint => `https://api.omplatform.com/api/stellar/v2${endPoint || ''}`,
    apiV3: endPoint => `https://api.omplatform.com/api/stellar/v3${endPoint || ''}`,
    apiURL: () => 'https://api.omplatform.com/api/stellar/v1',
    omURL: endPoint => `https://www.omplatform.com/api/v1${endPoint || ''}`,
    stangURL: () => 'https://api.omplatform.com/api/satang/v1',
    topupURL: () => 'https://api.aomtoken.network/api/dcp/v1/topup/request',
    bitkubURL: () => 'https://api.aomtoken.network/api/dcp/v1/ticker/get',
    tokenizeURL: () => 'https://token.omplatform.com/wp-json',
    privacyURL: () => `https://www.omplatform.com/privacy`,
    termsURL: () => `https://www.omplatform.com/terms`,
    explorer: endPoint => `https://explorer.omplatform.com/${endPoint || ''}`
  },
  BitCoinFee: {
    apiURL: () => 'https://bitcoinfees.earn.com/api/v1/fees/recommended'
  },
  VisionGoogle: {
    apiURL: () => 'https://api.aomtoken.network/api/stellar/v1/google-vision/'
  }

}
