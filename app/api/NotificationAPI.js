// Third-party
import DeviceInfo from 'react-native-device-info'

// Components & Modules
import ApiCaller from './api-caller'
import URL from './url'
import NotificationStore from '../AppStores/stores/Notification'
import MainStore from '../AppStores/MainStore'

export const getDeviceID = deviceToken => (DeviceInfo.getUniqueID() !== '' ? DeviceInfo.getUniqueID() : deviceToken.substring(0, 10))

export const addWallet = (name, address, deviceToken, type) => {
  const device_udid = getDeviceID(deviceToken)
  const data = {
    name, address, device_token: deviceToken, device_udid, type
  }

  return ApiCaller.post(URL.Aomtoken.api(`/wallets`), data, true)
}

export const removeWallet = id => ApiCaller.delete(URL.Aomtoken.api(`/wallets/${id}`), {}, false)

export const updateWallets = (wallets, deviceToken) => {
  const device_udid = getDeviceID(deviceToken)
  const bundle_id = DeviceInfo.getBundleId()
  const data = {
    wallets, device_token: deviceToken, device_udid, bundle_id
  }

  return ApiCaller.post(URL.Aomtoken.api(`/wallets/list`), data, true).then((res) => {
    if (res.data && res.data.data) {
      const response = res.data.data
      URL.OmChain.setNetwork(response.network, response.sign, response.passp)
      NotificationStore.notifyBadgeCount = response.badge || 0
      MainStore.appState.selectedWallet.BASE_FEE = parseInt(response.fee || 100, 10)
      MainStore.appState.selectedWallet.BASE_FEE_DP = parseFloat(response.fee || 100) / 10000000
      if (response.passp) {
        console.log(`Set Network "${response.passp}"`)
        MainStore.appState.selectedWallet.PASSPHRASE = response.passp
      }
    }

    return res
  })
}

export const initNotification = async (wallets = [], deviceToken) => {
  const device_udid = getDeviceID(deviceToken)
  const bundle_id = DeviceInfo.getBundleId()
  const data = {
    wallets, device_token: deviceToken, device_udid, status: 1, bundle_id
  }

  return ApiCaller.post(URL.Aomtoken.api(`/wallets/list`), data, true).then((res) => {
    if (res.data && res.data.data) {
      const response = res.data.data
      URL.OmChain.setNetwork(response.network, response.sign, response.passp)
      NotificationStore.notifyBadgeCount = response.badge || 0
      MainStore.appState.selectedWallet.BASE_FEE = parseInt(response.fee || 100, 10)
      MainStore.appState.selectedWallet.BASE_FEE_DP = parseFloat(response.fee || 100) / 10000000
      if (response.passp) {
        console.log(`Set Network "${response.passp}"`)
        MainStore.appState.selectedWallet.PASSPHRASE = response.passp
      }
    }

    return res
  })
}

export const offNotification = (deviceToken) => {
  const device_udid = getDeviceID(deviceToken)
  return ApiCaller.post(URL.Aomtoken.api(`/noti/device/${device_udid}/disable`), { id: deviceToken })
}

export const onNotification = (deviceToken) => {
  const device_udid = getDeviceID(deviceToken)
  return ApiCaller.post(URL.Aomtoken.api(`/noti/device/${device_udid}/enable`), { id: deviceToken })
}

export const paymentNotification = (address, destination, amount, href) => {
  const data = {
    address, destination, amount, href
  }

  return ApiCaller.post(URL.Aomtoken.api(`/wallets/payment`), data, true)
}

export const multisignatureUpdate = (address) => {
  const data = {
    address
  }

  return ApiCaller.post(URL.Aomtoken.api(`/wallets/msign/update`), data, true)
}

export const kycLayerOne = (
  fnameTH,
  lnameTH,
  fnameEN,
  lnameEN,
  cardNo,
  dob,
  laserCode,
  frontCardImage,
  backCardImage,
  selfieImage,
  status,
  gender,
  telephone,
  nationality,
  email,
  wallet
) => {

  console.log('kycLayerOne')
  const data = {
    public_key: wallet,
    profile: {
      fnameTH,
      lnameTH,
      fnameEN,
      lnameEN,
      cardNo,
      dob,
      laserCode,
      frontCardImage,
      backCardImage,
      selfieImage,
      status,
      gender,
      telephone,
      nationality,
      email
    }
  }

  return ApiCaller.post(URL.Aomtoken.apiV3(`/kyc/level/one`), data, true)
}

export const kycLayerTwo = (address, img_id_card, img_household) => {
  const data = {
    address, img_id_card, img_household
  }

  return ApiCaller.post(URL.Aomtoken.api(`/kyc/user/layer/2`), data, true)
}

export const kycCameraAutoFill = (type, img_base64) => {
  let typeDetection = 'TEXT_DETECTION'
  if (type === 'selfie') {
    typeDetection = 'FACE_DETECTION'
  }

  const data = {
    image_base64: img_base64,
    type: typeDetection
  }

  console.log('kycCameraAutoFill')
  console.log(data)

  return ApiCaller.post(URL.VisionGoogle.apiURL(), data, true, { 'OM-Api-Secret': 'Yj1w3irCVBNfefucpcBj92P6qOxpBhtT' })
}

export const uploadUserProfile = (address, image) => {
  const data = {
    address, image
  }

  return ApiCaller.post(URL.Aomtoken.api(`/kyc/user/update/profile/image`), data, true)
}

export const userInfo = (address) => {
  const data = {
    address
  }

  return ApiCaller.post(URL.Aomtoken.api(`/kyc/user/info`), data, true)
}

export const verificationState = address => ApiCaller.get(URL.Aomtoken.api(`/kyc/email/pending/address=${address}`), {}, true)

export const resendVerification = address => ApiCaller.get(URL.Aomtoken.api(`/kyc/email/resend/address=${address}`), {}, true)
