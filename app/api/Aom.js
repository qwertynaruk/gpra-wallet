import ApiCaller from './api-caller'
import URL from './url'

// Transaction API
export const confirmTransaction = (pdata) => {
  const data = {
    address: pdata.address,
    xdr: pdata.xdr,
    asset: pdata.ast,
    network: URL.OmChain.apiURL()
  }

  return ApiCaller.post(URL.Aomtoken.api(`/wallets/transaction/confirm`), data, true)
}

// Notification API
export function getNotifyList(address) {
  return ApiCaller.get(URL.Aomtoken.api(`/adr/${address}/notify`), {}, true).then((res) => {
    if (res.status === 200) {
      if (res.data && res.data.data) {
        return res.data.data
      }
    }

    return []
  })
}

export function clearNotifyRead(address) {
  return ApiCaller.post(URL.Aomtoken.api(`/adr/${address}/notify`), { action: 'readed' }, true)
}

// Lotto API
export const redeem_lottery = data => ApiCaller.post(`${URL.Lottery.apiURL()}/winning_lottery`, data, true)

export const redeem_lottery_all = data => ApiCaller.post(`${URL.Lottery.apiURL()}/winning_lottery/all`, data, true)

export const getLottery2 = (address, offset) => ApiCaller.get(`${URL.Lottery.apiURL()}/adr/${address}/${offset}`, {}, true)

export const getLotteryDetail = () => ApiCaller.get(`${URL.Lottery.apiURL()}/provider`, {}, true)

// News API
export const getAllNews = () => ApiCaller.get(URL.Aomtoken.omURL(`/news`), {}, true)

// Report Bug API
export const reportBug = data => ApiCaller.post(URL.Aomtoken.api('/report/bug'), data, true)

// Utility API
export const getAvatar = address => ApiCaller.get(URL.Aomtoken.api(`/kyc/profile/image/address=${address}`), {}, true)

export const fetchAppVersion = platform => ApiCaller.get(URL.Aomtoken.api(`/application/version/platform=${platform}`), {}, true)

// KYC Verication API
export const kycVerication = data => ApiCaller.post(URL.Aomtoken.apiV3('/kyc/level/two'), data, true)

// Tokenize
export const tokenizeList = () => {
  const url = URL.Aomtoken.tokenizeURL()
  const endpoint = `${url}/tokeni/v2/assets/lists`
  return ApiCaller.get(endpoint, {}, true, {
    'OM-Tokeni-Secret': 'YmAHQ!571eMRQmf-?t+_Kxr+KOyHy:o%8z6/xaV&~^VD<c8>O)qZe>Bl@y8r&$'
  })
}

// Tokenize
export const tokenizeIcon = (code, issuer) => {
  const url = URL.Aomtoken.tokenizeURL()
  const endpoint = `${url}/tokeni/v1/assets/icon/${code}/${issuer}`
  return ApiCaller.get(endpoint, {}, true, {
    'OM-Tokeni-Secret': 'YmAHQ!571eMRQmf-?t+_Kxr+KOyHy:o%8z6/xaV&~^VD<c8>O)qZe>Bl@y8r&$'
  })
}
