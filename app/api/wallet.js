import BigNumber from 'bignumber.js'
import caller from './api-caller'
import appState from '../AppStores/AppState'
import NetworkConfig from '../AppStores/stores/Config'
import URL from './url'

/**
 *
 * @param {String} address
 */
export const fetchWalletInfo = (address) => {
  if (!address) return Promise.reject()
  if (appState.networkName === NetworkConfig.networks.mainnet) {
    const urlTest = URL.EtherScan.apiURL(appState.networkName)
    const apikey = 'VW7UN4BXFPXI6GFMU64B5U1D6S9Q5JIRHX'
    const data = {
      module: 'account',
      action: 'balance',
      address,
      tag: 'latest',
      apikey
    }
    return new Promise((resolve, reject) => {
      caller.get(urlTest, data, true).then((res) => {
        const balance = res && res.data && res.data.result ? new BigNumber(`${res.data.result}e-18`) : new BigNumber('0')
        const result = {
          data: {
            data: {
              ETH: { balance: balance.toString(10) },
              address,
              tokens: []
            }
          }
        }
        resolve(result)
      }).catch(e => reject(e))
    })
  }
  const url = `${URL.Skylab.apiURL()}/balance/${address}`
  return caller.get(url, {}, true)
}

export const fetchWalletBTCInfo = (address) => {
  const url = `${URL.BlockChainInfo.apiURL()}/rawaddr/${address}`
  return caller.get(url, {}, true)

  // const url = `${URL.ChainSo.apiURL()}/address/BTC/${address}`
  // return caller.get(url, {}, true)
}

export const fetchWalletLTCInfo = (address) => {
  const url = `${URL.ChainSo.apiURL()}/address/LTC/${address}`
  return caller.get(url, {}, true)
}

export const fetchWalletDOGEInfo = (address) => {
  const url = `${URL.ChainSo.apiURL()}/address/DOGE/${address}`
  return caller.get(url, {}, true)
}

export const fetchWalletXRPInfo = (address) => {
  const url = `${URL.Ripple.apiURL()}/accounts/${address}/balances`
  return caller.get(url, {}, true)
}

export const fetchWalletXLMInfo = (address) => {
  const server = URL.Stellar.apiURL()
  return server.loadAccount(address)
}

export const fetchXLMTransactions = (address, limit, cursor) => {
  const endpoint = `${URL.Stellar.horizonURL()}/accounts/${address}/payments`
  const params = []

  if (limit) {
    params.push(`limit=${limit}`)
  }

  if (cursor) {
    params.push(`cursor=${cursor}`)
  }

  // Add order
  params.push('order=desc')

  // Concat
  const url = `${endpoint}?${params.join('&')}`
  return caller.get(url, {}, true)
}

export const fetchWalletAOMInfo = (address) => {
  const url = `${URL.OmChain.apiURL()}/accounts/${address}`
  return caller.get(url, {}, true)
}

export const changeTrustComplete = (address) => {
  const url = `${URL.Aomtoken.apiURL()}/wallets/trusted`
  const data = {
    address
  }
  return caller.post(url, data, true)
}

export const fetchRateTHB = () => {
  return caller.get(URL.Aomtoken.bitkubURL(), {}, true)
}

export const fetchRateETHDollar = () => {
  const data = {
    fsyms: 'ETH',
    tsyms: 'BTC,USD,EUR,GBP,AUD,CAD,CNY,JPY,RUB'
  }
  return caller.get(`${URL.CryptoCompare.apiURL()}/data/pricemultifull`, data, true)
}

export const fetchRateBTCDollar = () => {
  const data = {
    fsyms: 'BTC',
    tsyms: 'BTC,USD,EUR,GBP,AUD,CAD,CNY,JPY,RUB'
  }
  return caller.get(`${URL.CryptoCompare.apiURL()}/data/pricemultifull`, data, true)
}

export const fetchRateLTCDollar = () => {
  const data = {
    fsyms: 'LTC',
    tsyms: 'BTC,USD,EUR,GBP,AUD,CAD,CNY,JPY,RUB'
  }
  return caller.get(`${URL.CryptoCompare.apiURL()}/data/pricemultifull`, data, true)
}

export const fetchRateDOGEDollar = () => {
  const data = {
    fsyms: 'DOGE',
    tsyms: 'BTC,USD,EUR,GBP,AUD,CAD,CNY,JPY,RUB'
  }
  return caller.get(`${URL.CryptoCompare.apiURL()}/data/pricemultifull`, data, true)
}

export const fetchRateAOMDollar = () => {
  const data = {
    fsyms: 'AOM',
    tsyms: 'BTC,USD,EUR,GBP,AUD,CAD,CNY,JPY,RUB'
  }
  return caller.get(`${URL.CryptoCompare.apiURL()}/data/pricemultifull`, data, true)
}

export const buyAom = (address, amount) => {
  const data = {
    address,
    amount
  }
  return caller.post(`${URL.Aomtoken.stangURL()}/payment/create`, data, true)
}

export const topup = (address, amount) => {
  const data = {
    address,
    amount
  }
  return caller.post(`${URL.Aomtoken.apiURL()}/topup/request`, data, true)
}

export const fetchEstimateFeeBTC = () => {
  return caller.get(`${URL.BitCoinFee.apiURL()}`, {}, true)
}

