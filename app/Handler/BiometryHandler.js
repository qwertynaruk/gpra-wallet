// Components & Modules
import TouchID from '../../Libs/react-native-touch-id'
import constant from './../commons/constant'

const optionalConfigObject = {
  title: 'รับรองความถูกต้อง', // Android
  color: '#e00606', // Android
  sensorDescription: 'ตรวจสอบลายนิ้วมือ', // Android
  cancelText: 'Cancel', // Android
  fallbackLabel: 'แสดงรหัสผ่าน', // iOS (if empty, then label is hidden)
  unifiedErrors: false // use unified error messages (default false)
}

export default class BiometryHandler {
  static show(onSuccess) {
    TouchID.authenticate(constant.UNLOCK_YOUR_WALLET, optionalConfigObject)
      .then(() => {
        onSuccess()
      })
  }
}
