// Components & Modules
import { chainNames } from '../Utils/WalletAddresses'

class Checker {
  static checkAddress(address, coin) {
    if (coin === chainNames.ETH) {
      return this.checkAddressETH(address)
    } else if (coin === chainNames.AOM || coin === chainNames.XLM) {
      return this.checkAddressAOM(address)
    } else if (coin === chainNames.XRP) {
      return this.checkAddressXRP(address)
    }

    return this.checkAddressBTC(address)
    // if (address.length !== validateLength) {
    //   return false
    // }
    // return address.match(regx)
  }

  static checkAddressBTC(address) {
    if (address.length < 25 || address.length > 34) {
      return false
    }

    const regx = /^[0-9A-Za-z]{25,34}$/
    return address.match(regx)
  }

  static checkAddressAOM(address) {
    if (address.length !== 56) {
      return false
    }

    const regx = /^G([0-9A-Z]{55})$/
    return address.match(regx)
  }

  static checkAddressXRP(address) {
    const regx = /r[0-9a-zA-Z]{24,34}/
    return address.match(regx)
  }

  static checkAddressETH(address) {
    const regx = /^0x[0-9A-Fa-f]{40}$/

    if (address.length != 42) {
      return false
    }

    return address.match(regx)
  }

  static checkWIFBTC(wif) {
    if (wif.length !== 52) {
      return false
    }

    const regx = /^[0-9A-Za-z]{52}$/
    return wif.match(regx)
  }

  static checkAddressQR(address, coin = chainNames.ETH) {
    let regx = ''

    if (coin === chainNames.ETH) {
      regx = /^0x[0-9A-Fa-f]{40}$/
    } else if (coin === chainNames.AOM) {
      regx = /^G[0-9A-Z]{55}$/
    } else {
      regx = /^[0-9A-Za-z]{25,34}$/
    }

    return address.match(regx)
  }

  static checkAddressQRBTC(address) {
    const regx = /[0-9A-Za-z]{25,34}/
    return address.match(regx)
  }

  static checkPrivateKey(key) {
    // Stellar Private key
    if (key.length === 56) {
      return key.match(/^S([0-9A-Z]{55})$/)
    }
    if (key.length === 52) {
      return key.match(/^[0-9A-Za-z]{52}$/)
    }

    const regx = /^[0-9A-Fa-f]{64}$/
    return key.match(regx)
  }
  static checkWalletIsExist(wallets, address) {
    const isExist = wallets.find((w) => {
      if (w.type === 'ethereum') return w.address.toLowerCase() === address.toLowerCase()
      return w.address === address
    })

    return isExist
  }

  static checkNameIsExist(wallets, name) {
    const isExist = wallets.find(w => w.title.toLowerCase() === name.toLowerCase())
    return isExist
  }
}

export default Checker
