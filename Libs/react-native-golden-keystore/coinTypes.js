const networks = {
  BTC: {
    name: 'bitcoin',
    path: "m/49'/0'/0'/0/index"
  },
  ETH: {
    name: 'ethereum',
    path: "m/44'/60'/0'/0/index"
  },
  NEO: {
    name: 'neo',
    path: "m/44'/888'/0'/0/index"
  },
  LTC: {
    name: 'litecoin',
    path: "m/44'/2'/0'/0/index"
  },
  XRP: {
    name: 'ripple',
    path: "m/44'/144'/0'/0/index"
  },
  XLM: {
    name: 'stellar',
    path: "m/44'/148'/0'/0/index"
  },
  AOM: {
    name: 'aomtoken',
    path: "m/44'/148'/0'/0/index"
  }
}

export default networks
